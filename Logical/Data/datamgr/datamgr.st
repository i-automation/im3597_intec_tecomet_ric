(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: datamgr
 * File: datamgr.st
 * Author: ?
 * Created: ?
 * Version: 1.00.0
 * Modified Date:
 * Modified BY:
 ********************************************************************
 * Implementation OF PROGRAM datamgr

 * Description:

	This task ensures that our recipes have properly loaded. If so,
	DataMgrInit is set to true.

 * Options:

 * Version History:

 ********************************************************************)
PROGRAM _CYCLIC
	
	//handled in hmimgr state machine before export/import states
	CASE State OF

		//XXX - might need to add an error check for the error corresponding to a file device already exists, so that we can go through a quick devunlink or other process for clearing that aside from power cycling
		STEP_INIT:
			IF DataMgrInit THEN
				DevLinked:= FALSE;
				DevLinking:= FALSE;
				DevLinkTimeout:= FALSE;
				DevLinkFailed:= FALSE;
				TON_DevLinkTimout.IN:= TRUE;
				State:= STEP_CREATE_FILE_DEVICE;
			END_IF
	
		STEP_CREATE_FILE_DEVICE:
			DevLinking:= TRUE;
			DevLink_gCode.enable:= TRUE;
			DevLink_gCode.pDevice:= ADR('gCode'); //this file device is specified anytime we are wanting to link external to the PLC, so it is uesd at the top of the Parse task to specify where we are reaching to for the file
			DevLink_gCode.pParam:= ADR(FileDeviceParam); //this param, which includes the IP address, Share name, User, password, etc. is found in the HW task
			IF (DevLink_gCode.status = ERR_OK) THEN
				DevLinkHandle:= DevLink_gCode.handle;
				DevLink_gCode.enable:= FALSE;
				State:= STEP_DONE;
			ELSIF ( (DevLink_gCode.status <> ERR_FUB_BUSY) AND (DevLink_gCode.status <> ERR_FUB_ENABLE_FALSE) ) THEN
				DevLink_gCode.enable:= FALSE;
				DevLinkHandle:= FALSE;
				DevLinkFailed:= TRUE;
				State:= STEP_ERROR;
			ELSIF TON_DevLinkTimout.Q THEN
				DevLink_gCode.enable:= FALSE;
				DevLinkHandle:= FALSE;
				DevLinkTimeout:= TRUE;
				State:= STEP_ERROR;
			END_IF
		
		STEP_DONE:
			TON_DevLinkTimout.IN:= FALSE;
			DevLinking:= FALSE;
			DevLinked:= TRUE;
		
		STEP_ERROR:
			TON_DevLinkTimout.IN:= FALSE;
			DevLinked:= FALSE;
			DevLinking:= FALSE;
			IF ResetDevLink THEN
				DevLinkTimeout:= FALSE;
				DevLinkFailed:= FALSE;
				State:= STEP_INIT;
			END_IF;
				
	END_CASE;
	
	
	DevLink_gCode();
	TON_DevLinkTimout();
				

	// RECIPE CYCLIC
	
	//NOTE: For this system only systemsettings are used, all part-specific information is stored in the G-Code file
	//		Some recipe variables/references remain where iArecipe requires it.
	
	RecipesCyclic.Internal:= ADR(RecipeInternal);
	RecipesCyclic.ActiveFileMismatch:= ADR(RecipeMismatch);   // this variable must be in permanent memory
	RecipesCyclic.NActiveFile:= ADR(SystemSettings.RecipeNum);
	RecipesCyclic.MaxFiles:= ADR(SystemSettings.Display.MaxRecipes);
	RecipesCyclic.NewSys:= ADR(HMI.OEM.OEMRestoreDefaultsPB);
	
	RecipesCyclic();		
	
	DataMgrInit:= RecipesCyclic.Init;
	
END_PROGRAM


PROGRAM _INIT

	(* ------- configuration options ----------
	 !!!! MAKE SURE THERE ARE ENOUGH B&R OBJECTS ALLOCATED IN THE SW CONFIGURATION 
	      AND THAT THE RECIPE NAMES ARRAY IS SIZED APPROPRIATELY !!!!
	*)

	// RECIPES
	RecipeConfig.FileAutoSave:= TRUE;
	RecipeConfig.SaveInterval:= T#1s;
	RecipeConfig.pRemoteFileDevice:= ADR('gCode');
	RecipeConfig.pNameSys:= ADR('SystemSettings');   // this should be the PVI name of the SystemSettigns structure
	RecipeConfig.pNameFile:= ADR('datamgr:RecipeTemp');  // this should be the PVI name of the Recipe_temp structure
	strcpy(ADR(RecipeConfig.SysObjName), ADR('SYS'));
	strcpy(ADR(RecipeConfig.FileObjBaseName), ADR('REC'));
	RecipeConfig.pFile:= ADR(Recipe);
	RecipeConfig.pFileView:= ADR(RecipeView);
	RecipeConfig.pFileBckGnd:= ADR(RecipeBckGnd);
	RecipeConfig.pFileName:= ADR(Recipe.Name);
	RecipeConfig.pFileTemp:= ADR(RecipeTemp);
	RecipeConfig.szFile:= SIZEOF(Recipe);
	RecipeConfig.szFileName:= SIZEOF(Recipe.Name);
	RecipeConfig.pSys:= ADR(SystemSettings);
	RecipeConfig.pSys_init:= ADR(SystemSettings_init);
	RecipeConfig.szSys:= SIZEOF(SystemSettings);

	RecipesInit.Internal:= ADR(RecipeInternal);
	RecipesInit.Config:= ADR(RecipeConfig);

	RecipesInit();
	
	TON_DevLinkTimout.PT:= T#10s;

END_PROGRAM