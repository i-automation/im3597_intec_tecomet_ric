
// Filling in blank/missing configuration fields automatically for each axis
ACTION AutoCompleteAxisSettings: 
	
	// Setting secondary velocity/accel parameters based on primary if they're not configured
	IF (SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.MovementLimits.VelocityNegative = 0) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.MovementLimits.VelocityNegative:= SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.MovementLimits.VelocityPositive; // units/sec
	END_IF
	IF (SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.MovementLimits.Deceleration = 0) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.MovementLimits.Deceleration:= SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.MovementLimits.Acceleration; // units/sec^2
	END_IF
	
	// Default t_predict and t_total (since we don't want to have to set these every time). 
	IF (SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Position.PredictionTime = 0) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Position.PredictionTime:= 0.0004; // sec
	END_IF
	IF (SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Position.TotalDelayTime = 0) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Position.TotalDelayTime:= 0.0004; // sec
	END_IF	
	
	// Defaults related to scaling
	IF (SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Transformation.ReferenceDistance = 0) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Transformation.ReferenceDistance:= 1; // units/load rev
	END_IF
	
	// Configuring UserChannel to read back current cyclically (only if physical motor present)
	IF (SystemSettings.AxisConfig[AxisIndex].Motor.Type <> MOTOR_NOTREQUIRED) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.CyclicReadChannels.UserChannelParameterID:= ACP10PAR_ICTRL_ISQ_ACT;
	// Otherwise just passing in a dummy value to keep it happy. 
	ELSE		
		SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.CyclicReadChannels.UserChannelParameterID:= ACP10PAR_S_SET_VAX1;
	END_IF
	
	// Set position controller mode depending upon motor configuration	
	// We're only overwriting the input value if it's required by the motor type, otherwise we let it be
	CASE SystemSettings.AxisConfig[AxisIndex].Motor.Type OF	
		MOTOR_UNDEFINED:
			// do nothing		
		MOTOR_AC:
			SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Mode:= mpAXIS_CTRL_MODE_UF;								
		MOTOR_NOTREQUIRED, MOTOR_STEPPER: // SDC, ACPmulti power supply, stepper
			SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Mode:= mpAXIS_CTRL_MODE_POSITION; 									
		ELSE // regular motors
			// If ff is currently disabled, we check for ff gains and re-enable it if necessary
			IF ((SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.FeedForward.Inertia <> 0) OR 
				(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.FeedForward.SpeedTorqueFactor <> 0) OR 
				(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.FeedForward.TorqueLoad <> 0) OR
				(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.FeedForward.TorqueNegative <> 0) OR
				(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.FeedForward.TorquePositive <> 0)) THEN
				SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Mode:= mpAXIS_CTRL_MODE_POSITION_FF;
			END_IF									
	END_CASE
	// Catching case where mode hasn't been assigned at all yet. 
	IF ((SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Mode <> mpAXIS_CTRL_MODE_UF) AND 
		(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Mode <> mpAXIS_CTRL_MODE_POSITION) AND 
		(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Mode <> mpAXIS_CTRL_MODE_POSITION_FF)) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.Mode:= mpAXIS_CTRL_MODE_POSITION;
	END_IF
	
	// UF controller defaults
	IF ((SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.VoltageFrequency.Type <> mpAXIS_CTRL_UF_LINEAR) AND
		(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.VoltageFrequency.Type <> mpAXIS_CTRL_UF_LINEAR2) AND
		(SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.VoltageFrequency.Type <> mpAXIS_CTRL_UF_QUADRATIC)) THEN
		SystemSettings.AxisConfig[AxisIndex].Mapp.Drive.Controller.VoltageFrequency.Type:= mpAXIS_CTRL_UF_LINEAR;
	END_IF

END_ACTION
