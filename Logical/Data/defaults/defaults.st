(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: defaults
 * File: defaults.st
 * Author: amusser
 * Created: January 06, 2012
 * Version: 1.20.0
 * Modified Date: 8/14/2017
 * Modified BY: amusser
 ********************************************************************
 * Implementation of program Defaults

 * Description:

	This PROGRAM lists default settings, and it is possible TO DO so
	FOR several different configurations.

 * Options:

 * Version History:
 	1.0 -- First release
	1.1 -- Added safety settings
	1.2 -- Updated axis configuration

 ********************************************************************)

PROGRAM _CYCLIC
	
	IF (DataMgrInit) THEN
		
		// Populating the list of available motors.
		// The strings should correspond to the name of the parameter table object in the Logical View
		// that contains the parameters for the given motor. 
		SystemSettings.MotorList[MOTOR_UNDEFINED]:= '';
		SystemSettings.MotorList[MOTOR_AC]:= 'ACMotor'; // this string not used, by here as a placeholder.
		SystemSettings.MotorList[MOTOR_MSA2SV1]:= 'm_msa2sv1'; 
		SystemSettings.MotorList[MOTOR_JSA42V1]:= 'm_jsa42v1';
		SystemSettings.MotorList[MOTOR_JSA52V1]:= 'm_jsa52v1';
		SystemSettings.MotorList[MOTOR_JSA73V1]:= 'm_jsa73v1';
		SystemSettings.MotorList[MOTOR_LSA35V1]:= 'm_lsa35v1';
		
		IF (HMI.OEM.OEMRestoreDefaultsPB) THEN
			HMI.OEM.OEMRestoreDefaultsPB:= FALSE;
			
			memset(ADR(SystemSettings), 0, SIZEOF(SystemSettings));
			
			SystemSettings.Display.MaxRecipes:= 25;
			SystemSettings.Display.Language:= 0; //XXX - USE CONSTANTS!
			SystemSettings.Display.UnitType:= 0; //XXX - USE CONSTANTS!
			SystemSettings.Display.Brightness:= 100;
			
			SystemSettings.LineVoltage:= 115;
			SystemSettings.PhaseMonIgnore:= FALSE;
			
			
			//Rivet Type Configuration
			strcpy(ADR(SystemSettings.Rivet[0].Name),ADR('0143-4'));
			SystemSettings.Rivet[0].PressProgram:= 1;
			strcpy(ADR(SystemSettings.Rivet[1].Name),ADR('0143-7'));
			SystemSettings.Rivet[1].PressProgram:= 2;
			strcpy(ADR(SystemSettings.Rivet[2].Name),ADR('0143-8'));
			SystemSettings.Rivet[2].PressProgram:= 3;
			strcpy(ADR(SystemSettings.Rivet[3].Name),ADR('0143-10'));
			SystemSettings.Rivet[3].PressProgram:= 4;
			strcpy(ADR(SystemSettings.Rivet[4].Name),ADR('0143-11'));
			SystemSettings.Rivet[4].PressProgram:= 5;
			
			FOR i:= 0 TO (SIZEOF(SystemSettings.Rivet)/SIZEOF(SystemSettings.Rivet[0])-1) DO
				SystemSettings.Rivet[i].SingulatorExtTime:= 250;//ms
				SystemSettings.Rivet[i].SingulatorTimeoutTime:= 1000;//ms
				SystemSettings.Rivet[i].NestTimeoutTime:= 1000;//ms
				SystemSettings.Rivet[i].SingulatorToNestGapTime:= 50;//ms
			END_FOR
			
			//Press Program for Purging (same for all rivet types)
			SystemSettings.PurgePressProg:= 9;
			
			
			//Barcode Timeout
			SystemSettings.BarcodeTimeout:= 100;//ms
			
			//Network Share - XXX setup defaults based on site network settings
			SystemSettings.NetworkShare.Domain:='';
			SystemSettings.NetworkShare.Name:='';
			SystemSettings.NetworkShare.Password:='';
			SystemSettings.NetworkShare.Server:='';
			SystemSettings.NetworkShare.User:='';
			
			SystemSettings.HardwarePositions.BarcodeReader.X:= 24.5; //in
			SystemSettings.HardwarePositions.BarcodeReader.Y:= 0; //in
			SystemSettings.HardwarePositions.Camera.X:= 15; //in
			SystemSettings.HardwarePositions.Camera.Y:= 6.5; //in
			SystemSettings.HardwarePositions.Press.X:= 14.603; //in -determined by lining up with a datum point on the plate (dead center of plate)
			SystemSettings.HardwarePositions.Press.Y:= 19.064; //in -determined by lining up with a datum point on the plate (dead center of plate)
			SystemSettings.HardwarePositions.Purge[0].X:= 18; //in
			SystemSettings.HardwarePositions.Purge[0].Y:= 19; //in
			SystemSettings.HardwarePositions.Purge[1].X:= 11; //in
			SystemSettings.HardwarePositions.Purge[1].Y:= 19; //in
			
			SystemSettings.YAxisRestriction_NegLim:= 10.63; //in
			SystemSettings.YAxisRestriction_PosLim:= 26.5; //in
			
			//Press
			SystemSettings.PressConfig.Disable:= FALSE;
			SystemSettings.PressConfig.JogSpeed:= 10; // 10mm/sec
			SystemSettings.PressConfig.JogSpeedHigh:= 50; // 50mm/sec
			SystemSettings.PressConfig.JogIncrement:= 0.5; // 0.5mm
			
			// X axis
			// Scaling is for motor -> 1:1 gearbox -> 16 mm pitch ball screw
			SystemSettings.AxisConfig[AX_X].Motor.Type:= MOTOR_NOTREQUIRED;
			SystemSettings.AxisConfig[AX_X].Mapp.AxisName:= 'AxX';
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.BaseType:= mpAXIS_LIMITED_LINEAR;
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MeasurementResolution:= 0.0001;
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MeasurementUnit:= mpAXIS_UNIT_GENERIC;
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.SoftwareLimitPositions.LowerLimit:= 0; // 0in
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.SoftwareLimitPositions.UpperLimit:= 29; // 60in
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.PeriodSettings.Period:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Gearbox.Input:= 1;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Gearbox.Output:= 1;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Gearbox.Direction:= mpAXIS_DIR_COUNTERCLOCKWISE;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Transformation.ReferenceDistance:= 0.6299; // 16mm per revolution of the ball screw shaft / 0.6299in
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.Position.ProportionalGain:= 450;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.Speed.ProportionalGain:= 0.5;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.Speed.IntegralTime:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.Speed.FilterTime:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.FeedForward.TorqueLoad:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.FeedForward.TorquePositive:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.FeedForward.TorqueNegative:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.FeedForward.SpeedTorqueFactor:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.Controller.FeedForward.Inertia:= 0;	
			SystemSettings.AxisConfig[AX_X].Move.JogSpeed:= 0.5; // 0.5in/sec
			SystemSettings.AxisConfig[AX_X].Move.JogSpeedHigh:= 2; // 1in/sec
			SystemSettings.AxisConfig[AX_X].Move.JogIncrement:= 0.004; // 0.25in
			SystemSettings.AxisConfig[AX_X].Move.Accel:= 10; // 40in/sec^2
			SystemSettings.AxisConfig[AX_X].Move.Decel:= 10; // 40in/sec^2
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.VelocityPositive:= 10; // 20in/s
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.VelocityNegative:= 10; // 20in/s
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.JerkTime:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.PositionErrorStopLimit:= 0.05; // 0.05in
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.VelocityErrorStopLimit:= 0;
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.VelocityErrorStopLimitMode:= mpAXIS_VEL_MODE_OFF;
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.Acceleration:= 50; // 200in/s^2
			SystemSettings.AxisConfig[AX_X].Mapp.Axis.MovementLimits.Deceleration:= 50; // 200in/s^2
			
			// Y axis
			// Scaling is for motor -> 1:1 gearbox -> 16 mm pitch ball screw
			SystemSettings.AxisConfig[AX_Y].Motor.Type:= MOTOR_NOTREQUIRED;
			SystemSettings.AxisConfig[AX_Y].Mapp.AxisName:= 'AxY';
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.BaseType:= mpAXIS_LIMITED_LINEAR;
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MeasurementResolution:= 0.0001;
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MeasurementUnit:= mpAXIS_UNIT_GENERIC;
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.SoftwareLimitPositions.LowerLimit:= 0; // 0in
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.SoftwareLimitPositions.UpperLimit:= 26.5; // 60in
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Gearbox.Input:= 1;
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Gearbox.Output:= 1;
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Gearbox.Direction:= mpAXIS_DIR_CLOCKWISE;
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Transformation.ReferenceDistance:= 0.6299; // 16mm per revolution of the ball screw shaft / 0.6299in
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Controller.Position.ProportionalGain:= 400;
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Controller.Speed.ProportionalGain:= 0.7;
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Controller.Speed.IntegralTime:= 0;
			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.Controller.Speed.FilterTime:= 0;			
			SystemSettings.AxisConfig[AX_Y].Move.JogSpeed:= 0.5; // 0.5in/sec
			SystemSettings.AxisConfig[AX_Y].Move.JogSpeedHigh:= 2; // 1in/sec
			SystemSettings.AxisConfig[AX_Y].Move.JogIncrement:= 0.004; // 0.25in
			SystemSettings.AxisConfig[AX_Y].Move.Accel:= 10; // 40in/sec^2
			SystemSettings.AxisConfig[AX_Y].Move.Decel:= 10; // 40in/sec^2
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MovementLimits.VelocityPositive:= 10; // 20in/s
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MovementLimits.VelocityNegative:= 10; // 20in/s
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MovementLimits.JerkTime:= 0;
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MovementLimits.PositionErrorStopLimit:= 0.05; // 0.05in
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MovementLimits.VelocityErrorStopLimitMode:= mpAXIS_VEL_MODE_OFF;	
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MovementLimits.Acceleration:= 50; // 200in/s^2
			SystemSettings.AxisConfig[AX_Y].Mapp.Axis.MovementLimits.Deceleration:= 50; // 200in/s^2
			
			//Drive
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.DigitalInputs.Level.HomingSwitch:= mpAXIS_IO_ACTIVE_HI;
//			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.DigitalInputs.Level.HomingSwitch:= mpAXIS_IO_ACTIVE_HI;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.DigitalInputs.Level.Trigger1:= mpAXIS_IO_ACTIVE_HI;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.DigitalInputs.Level.Trigger2:= mpAXIS_IO_ACTIVE_HI;
//			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.DigitalInputs.Level.Trigger1:= mpAXIS_IO_ACTIVE_HI;
//			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.DigitalInputs.Level.Trigger2:= mpAXIS_IO_ACTIVE_HI;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.DigitalInputs.Level.NegativeLimitSwitch:= mpAXIS_IO_ACTIVE_HI;
			SystemSettings.AxisConfig[AX_X].Mapp.Drive.DigitalInputs.Level.PositiveLimitSwitch:= mpAXIS_IO_ACTIVE_HI;
//			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.DigitalInputs.Level.NegativeLimitSwitch:= mpAXIS_IO_ACTIVE_HI;
//			SystemSettings.AxisConfig[AX_Y].Mapp.Drive.DigitalInputs.Level.PositiveLimitSwitch:= mpAXIS_IO_ACTIVE_HI;

			FOR AxisIndex:= 0 TO MAX_AXIS_INDEX DO
				// Filling in blank/missing configuration fields automatically for each axis
				AutoCompleteAxisSettings;
			END_FOR
			
			//	AlarmMgr.RestartRequired:= 1;
			HMI.OEM.OEMSysSettingsStatus:= 7;   // factory defaults restored  XXX Use Constants!
						
		END_IF
	END_IF
END_PROGRAM

PROGRAM _INIT
	
	// Because the PV_xgetadr function will not work unless the axis PV is explicitly referenced in code, 
	// we have to type out all of the axis names here. 
	// XXX1 I need to make this better, this really sucks...
	// Axis names...
	AxX;
	AxY;
	// MpLink names...
	MpLinkAxX;
	MpLinkAxY;
	
END_PROGRAM


