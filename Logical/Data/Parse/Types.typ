
TYPE
	States_enm : 
		(
		IDLE,
		DIR_READ,
		DIR_SEARCH,
		LOAD,
		READ,
		PARSE,
		CLOSE,
		VALIDATE,
		ERROR
		);
	ReadType_enm : 
		(
		NONE,
		X_LOC,
		Y_LOC,
		RIV_TYPE,
		CAM_PROG
		);
END_TYPE
