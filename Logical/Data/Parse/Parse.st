PROGRAM _INIT
	strcpy(ADR(FileDevice),ADR('gCode'));
END_PROGRAM

PROGRAM _CYCLIC

	
//Overview:	
//	#1 - The file name and parse command come from barcode scanner and main program (via ParseCtrl struct)
//	#2 - Load the file from Windows Share or thumbdrive
//	#3 - Scan file for next "X", "Y", or "R" key characters
//  #4 - When key characters are found, read the following characters into the corresponding value until another key character or non-numeric char is found.
//  #5 - If a full set of parameters (X, Y, R) are found on a line, then store the values in the Rivet list.
//	#6 - Read next line of file and return to #3 until end-of-file is encountered

	
	
	
	
//Programs which haven�t yet been run on a machine are stored in an �unproven� directory on the network. After they�ve been run they�re saved TO a TO a �proven� directory. 
//Search the proven directory first, the unproven folder second providing they�re in the same root folder? IF it�s in unproven, have a notification on the HMI that it is NOT proven as a warning?
//Filename convention: 85-3295_E.tfi (E is the part revision) The example I sent 8/17 was missing the revision in the filename.

	
	
	// State Trace;
	IF (State <> StateTrace[0]) THEN
		FOR i:= ((SIZEOF(StateTrace)/SIZEOF(StateTrace[0]))-1) TO 1 BY (-1) DO
			StateTrace[i]:= StateTrace[i-1];
		END_FOR
		StateTrace[0]:= State;
	END_IF
	
	CASE State OF
		IDLE:
			//XXX - Need to ensure parsing is locked out while machine is running in auto mode so that Rivetlist can't change while running
			IF ParseCtrl.Cmd.ParseFile THEN
				memset(ADR(RivetList),0,SIZEOF(RivetList));
				RivetList.CameraProg:= 65535;
				DirRead_0.entry:= 0;
				DirRead_0.pPath:= ADR('Proven');
				State:= DIR_READ;
			END_IF
			ParseCtrl.Cmd.ErrAck:= FALSE;
		
		DIR_READ:
			DirRead_0.enable:= TRUE;
			DirRead_0.data_len:= SIZEOF(DirData);
			DirRead_0.option:= fiFILE;
			DirRead_0.pData:= ADR(DirData);
			DirRead_0.pDevice:= ADR(FileDevice);
			State:= DIR_SEARCH;
						
		DIR_SEARCH:
			IF (DirRead_0.status = ERR_OK) THEN
				DirRead_0.enable:= FALSE;
				IF ((memcmp(ADR(DirData.Filename),ADR(ParseCtrl.Par.PartName),strlen(ADR(ParseCtrl.Par.PartName))))=0) THEN //PartName is at the beginning of filename
					IF RivetList.Unproven THEN
						strcpy(ADR(Filename),ADR('Unproven'));
					ELSE
						strcpy(ADR(Filename),ADR('Proven'));
					END_IF
					strcat(ADR(Filename),ADR('/'));
					strcat(ADR(Filename),ADR(DirData.Filename));
//					memcpy(ADR(RivetList.PartVersion), ADR(DirData.Filename) + strlen(ADR(ParseCtrl.Par.PartName)) + 1, MAX((strlen(ADR(DirData.Filename)) - strlen(ADR(ParseCtrl.Par.PartName)) - 5),0)); //+2 ignores underscore, -4 removes extension; needed to comment out for now since was crashing when running on PLC
					State:= LOAD;
				ELSE //keep looking
					DirRead_0.entry:= DirRead_0.entry + 1;
					State:= DIR_READ;
				END_IF
			ELSIF (DirRead_0.status = fiERR_NO_MORE_ENTRIES) THEN
				IF RivetList.Unproven THEN
					//Not found in the unproven folder either, error.					
					RivetList.Unproven:= FALSE;
					ParseCtrl.Stat.Error:= PARSE_FILE_NOT_FOUND;
					State:= ERROR;
				ELSE
					//Try again in the unproven folder					
					DirRead_0.enable:= FALSE;
					RivetList.Unproven:= TRUE;
					DirRead_0.entry:= 0;
					DirRead_0.pPath:= ADR('Unproven');
					State:= DIR_READ;
				END_IF
			ELSIF (DirRead_0.status < ERR_FUB_BUSY) THEN
				ParseCtrl.Stat.Error:= PARSE_DIRECTORY_ERROR;
				State:= ERROR;
			END_IF

		LOAD:
			//XXX Add ability to pull files from other locations - using local file device for now for testing
			FileOpen_0.enable:= TRUE;
			FileOpen_0.mode:= fiREAD_ONLY;
			FileOpen_0.pDevice:= ADR(FileDevice); 
			FileOpen_0.pFile:= ADR(Filename);
			IF (FileOpen_0.status = ERR_OK) THEN
				FileOpen_0.enable:= FALSE;
				FileLen:= FileOpen_0.filelen;
				FileIdent:= FileOpen_0.ident;
				ReadOffset:= 0;
				State:= READ;
			ELSIF (FileOpen_0.status = fiERR_FILE_NOT_FOUND)  THEN
				ParseCtrl.Stat.Error:= PARSE_FILE_NOT_FOUND;
				State:= ERROR;
			ELSIF (FileOpen_0.status < ERR_FUB_ENABLE_FALSE) THEN
				ParseCtrl.Stat.Error:= PARSE_LOAD_ERROR;
				State:= ERROR;
			END_IF
		
		READ:
			//Note: File is now open, if errors happen beyond this point always go through the close state on the way to the error state
			FileRead_0.enable:= TRUE;
			FileRead_0.ident:= FileIdent;
			FileRead_0.len:= MIN(LINE_LEN,FileLen);
			FileRead_0.offset:= ReadOffset;
			FileRead_0.pDest:= ADR(tmpFileData);
			IF FileRead_0.status = ERR_OK THEN
				FileRead_0.enable:= FALSE;
				State:= PARSE;				
			ELSIF FileRead_0.status < ERR_FUB_ENABLE_FALSE THEN
				ParseCtrl.Stat.Error:= PARSE_READ_ERROR;
				ErrorClose:= TRUE;
				State:= CLOSE;			
			END_IF

		PARSE:
			//Determine Length of 1st line in current read chunk
			FOR i:= 0 TO UDINT_TO_USINT(FileRead_0.len)  DO
				IF memcmp(ADR(tmpFileData)+i,ADR('$r$n'),2) = 0 THEN
					EXIT;
				END_IF
			END_FOR
			LineLen:= i;
              
			//XXX - Add Header check (set filetype error if incorrect)
                                  
			//Scan through the line, when key characters are encountered then read the next chars in until either another key char or space is encountered.
			memset(ADR(Xstr),0,SIZEOF(Xstr));
			Xptr:= 0;
			memset(ADR(Ystr),0,SIZEOF(Ystr));
			Yptr:= 0;
			memset(ADR(Rstr),0,SIZEOF(Rstr));
			Rptr:= 0;
			memset(ADR(Cstr),0,SIZEOF(Cstr));
			Cptr:= 0;
			FOR i:= 0 TO LineLen DO
				memcpy(ADR(CharVal),ADR(tmpFileData)+i,1);
				IF CharVal = ASCII_X THEN
					ReadType:= X_LOC;
				ELSIF CharVal = ASCII_Y THEN
					ReadType:= Y_LOC;
				ELSIF CharVal = ASCII_R THEN
					ReadType:= RIV_TYPE;
				ELSIF CharVal = ASCII_C THEN
					ReadType:= CAM_PROG;
				ELSIF ((CharVal >= ASCII_0) AND (CharVal <= ASCII_9)) OR (CharVal = ASCII_Decimal) OR (CharVal = ASCII_Minus) THEN
					//It's a number or '-' or '.', keep reading current readtype
				ELSE
					//It's a space or the start of some unrecognized command, stop reading
					ReadType:= NONE;
				END_IF
				CASE ReadType OF
					X_LOC:   memcpy(ADR(Xstr)+Xptr,ADR(CharVal),1);
						Xptr:= Xptr+1;
					Y_LOC:   memcpy(ADR(Ystr)+Yptr,ADR(CharVal),1);
						Yptr:= Yptr+1;
					RIV_TYPE: memcpy(ADR(Rstr)+Rptr,ADR(CharVal),1);
						Rptr:= Rptr+1;
					CAM_PROG: memcpy(ADR(Cstr)+Cptr,ADR(CharVal),1);
						Cptr:= Cptr+1;
				END_CASE
			END_FOR
              
			IF (Xptr>0) AND (Yptr>0) AND (Rptr>0) THEN //Full set was found, valid rivet.
				RivetList.Rivet[RivetList.NumRivets].X:= atof(ADR(Xstr)+1);
				RivetList.Rivet[RivetList.NumRivets].Y:= atof(ADR(Ystr)+1);
				strcpy(ADR(RivetList.Rivet[RivetList.NumRivets].Type),ADR(Rstr)+1);
				RivetList.NumRivets:= RivetList.NumRivets + 1;
			ELSIF (Cptr>0) THEN //It was a camera program number
				RivetList.CameraProg:= (atoi(ADR(Cstr)+1));
			END_IF

			ReadOffset:= ReadOffset + LineLen + 2; //(The additional 2 char accounts for the CRLF at end of line)
			IF ReadOffset>FileLen THEN
				State:= CLOSE;
			ELSE
				State:= READ;
			END_IF
			
		CLOSE:
			FileClose_0.enable:= TRUE;
			FileClose_0.ident:= FileIdent;
			IF FileClose_0.status = ERR_OK THEN
				FileClose_0.enable:= FALSE;
				ParseCtrl.Cmd.ParseFile:= FALSE;
				IF ErrorClose THEN
					State:= ERROR;
				ELSE
					State:= VALIDATE;
				END_IF
			ELSIF FileClose_0.status < ERR_FUB_ENABLE_FALSE THEN
				ParseCtrl.Stat.Error:= PARSE_CLOSE_ERROR;
				State:= ERROR;
			END_IF			
		
		VALIDATE:
			IF (RivetList.NumRivets = 0) THEN
				ParseCtrl.Stat.Error:= PARSE_NO_RIVETS_FOUND;
				State:= ERROR;
			ELSIF (RivetList.CameraProg = 65535) THEN
				ParseCtrl.Stat.Error:= PARSE_CAMERA_PROG_NOT_FOUND;
				State:= ERROR;
			ELSIF ((RivetList.CameraProg < 0) OR (RivetList.CameraProg > 999)) THEN
				ParseCtrl.Stat.Error:= PARSE_CAMERA_PROG_OUT_OF_RANGE;
				State:= ERROR;
			ELSE
				//XXX - May make sense to move this check to the rivet start state instead.
				FOR i:= 0 TO UINT_TO_USINT(RivetList.NumRivets-1) DO 
					FeederMatch:= FALSE;
					FOR j:= 0 TO MAX_FEEDER DO
						IF (strcmp(ADR(RivetList.Rivet[i].Type),ADR(Feeder[j].RivetType))=0) THEN
							FeederMatch:= TRUE;
						END_IF
					END_FOR
					IF NOT(FeederMatch) THEN //Failed to find a feeder with a rivet type corresponding to this rivet in the list
						ParseCtrl.Stat.Error:= PARSE_RIVET_TYPE_NOT_IN_FEEDER; 
						strcpy(ADR(ParseCtrl.Stat.MissingRivetType),ADR(RivetList.Rivet[i].Type));
						EXIT;
					END_IF
				END_FOR
				IF (ParseCtrl.Stat.Error = PARSE_RIVET_TYPE_NOT_IN_FEEDER) THEN
					//This also covers the case where more rivets types are used than # feeders, since the extra rivets won't have a feeder to match with	
					State:= ERROR;
				ELSE
					RivetList.Valid:= TRUE;
					State:= IDLE;
				END_IF
			END_IF
		
		ERROR:
			IF ParseCtrl.Cmd.ErrAck THEN
				strcpy(ADR(ParseCtrl.Stat.MissingRivetType),ADR(''));
				ErrorClose:= FALSE;
				ParseCtrl.Cmd.ErrAck:= FALSE;
				ParseCtrl.Cmd.ParseFile:= FALSE;
				FileOpen_0.enable:= FALSE;
				FileRead_0.enable:= FALSE;
				FileClose_0.enable:= FALSE;
				DirRead_0.enable:= FALSE;
				ParseCtrl.Stat.Error:= PARSE_OK;
				State:= IDLE;
			END_IF
		
	END_CASE
	DirRead_0();
	FileOpen_0();
	FileRead_0();
	FileClose_0();
	
END_PROGRAM

