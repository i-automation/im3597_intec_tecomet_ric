@ECHO OFF
ECHO !!! GENERATING DYNAMIC BUILDINFO PROGRAM FILE !!!

CD %~dp0
ECHO     File path is %CD%
SET prgfile=".\buildinfo.st"
SET varfile=".\buildinfo.var"
SET reftrue=ref:
SET commitinfo=N/A
ECHO     Program file is %prgfile%
ECHO     Variablefile is %varfile%

REM Replace \Logical with ~ then remove the rest of the string
SET CDtemp=%CD%
SET CDtemp=%CDtemp:\Logical=~%
FOR /f "delims=~" %%a IN ("%CDtemp%") DO (set root=%%a)

REM Check for the git folder in the project
IF NOT EXIST %root%\.git (goto NOGIT)

REM Read the information in the HEAD file of the .git folder
FOR /f "delims=" %%a IN (%root%\.git\HEAD) DO (set commit=%%a)
FOR /f "delims= " %%a IN ("%commit%") DO (set checkref=%%a)

REM Check if the information is a file path or a commit number
if %checkref% == %reftrue% (goto IFCODE) else (goto PRINTFILES)

REM If its a file path, grab the commit number from the file path
:IFCODE
SET committemp=%commit%
SET committemp=%committemp:heads/=~%
FOR /f "tokens=1,2 delims=~" %%a IN ("%committemp%") DO (set reffilepath=%%b)
REM Check for the file in the git folder
IF NOT EXIST %root%\.git\refs\heads\%reffilepath% (goto NOGIT)
FOR /f "delims=" %%a IN (%root%\.git\refs\heads\%reffilepath%) DO (set commit=%%a)
GOTO:PRINTFILES

REM No branch info if there isn't a git directory or commit files
:NOGIT
SET commit= 
SET commitinfo= 
GOTO:PRINTFILES

REM create the buildinfo program and variables in the logical folder
:PRINTFILES
FOR /f "tokens=2-4 delims=/ " %%a IN ('date /t') DO (set mydate=%%c.%%a.%%b)
FOR /f "tokens=1-2 delims=/:" %%a IN ('time /t') DO (set mytime=%%a:%%b)
IF (%commit% NEQ %commitinfo%) (SET commitinfo=%commit:~0,7%)

ECHO (**********************************************************************) > %varfile%
ECHO (*   Dynamic Build Info header file generated: %mydate% %mytime%    *) >> %varfile%
ECHO (**********************************************************************) >> %varfile%
ECHO (**********************************************************************) >> %varfile%
ECHO (*    This file is generated with each build by the batch file*) >> %varfile%
ECHO (**********************************************************************) >> %varfile%
ECHO. >> %prgfile%
ECHO VAR >> %varfile%
ECHO BUILDINFO : STRING[200] := '%2::%3 by %1 %mydate% %mytime% %commitinfo%'; >> %varfile%
ECHO BUILDNAME : STRING[200] := '%2_%3'; >> %varfile%
ECHO USERNAME : STRING[50] := '%1'; >> %varfile%
ECHO PROJNAME : STRING[50] := '%2'; >> %varfile%
ECHO COMMIT: STRING[50] := '%commit%'; >> %varfile%
ECHO COMMIT_INFO: STRING[50] := '%commitinfo%'; >> %varfile%
ECHO END_VAR >> %varfile%


ECHO (**********************************************************************) > %prgfile%
ECHO (*   Dynamic Build Info header file generated: %mydate% %mytime%    *) >> %prgfile%
ECHO (**********************************************************************) >> %prgfile%
ECHO (**********************************************************************) >> %prgfile%
ECHO (*    This file is generated with each build by the batch file*) >> %prgfile%
ECHO (**********************************************************************) >> %prgfile%

ECHO. >> %prgfile%
ECHO PROGRAM _INIT >> %prgfile%
ECHO BUILDINFO:=BUILDINFO; >> %prgfile%
ECHO BUILDNAME:=BUILDNAME; >> %prgfile%
ECHO USERNAME:=USERNAME; >> %prgfile%
ECHO PROJNAME:=PROJNAME; >> %prgfile%
ECHO COMMIT:=COMMIT; >> %prgfile%
ECHO COMMIT_INFO:=COMMIT_INFO; >> %prgfile%
ECHO END_PROGRAM >> %prgfile%
ECHO PROGRAM _CYCLIC >> %prgfile%
ECHO brsmemset(ADR(G_BUILDNAME),0,SIZEOF(G_BUILDNAME)); >> %prgfile%
ECHO brsstrcpy(ADR(G_BUILDNAME),ADR(BUILDNAME)); >> %prgfile%
ECHO brsmemset(ADR(G_BUILDINFO),0,SIZEOF(G_BUILDINFO)); >> %prgfile%
ECHO brsstrcpy(ADR(G_BUILDINFO),ADR(BUILDINFO)); >> %prgfile%
ECHO brsmemset(ADR(G_USERNAME),0,SIZEOF(G_USERNAME)); >> %prgfile%
ECHO brsstrcpy(ADR(G_USERNAME),ADR(USERNAME)); >> %prgfile%
ECHO brsmemset(ADR(G_PROJNAME),0,SIZEOF(G_PROJNAME)); >> %prgfile%
ECHO brsstrcpy(ADR(G_PROJNAME),ADR(PROJNAME)); >> %prgfile%
ECHO brsmemset(ADR(G_COMMIT),0,SIZEOF(G_COMMIT)); >> %prgfile%
ECHO brsstrcpy(ADR(G_COMMIT),ADR(COMMIT)); >> %prgfile%
ECHO brsmemset(ADR(G_COMMIT_INFO),0,SIZEOF(G_COMMIT_INFO)); >> %prgfile%
ECHO brsstrcpy(ADR(G_COMMIT_INFO),ADR(COMMIT_INFO)); >> %prgfile%
ECHO END_PROGRAM >> %prgfile%
