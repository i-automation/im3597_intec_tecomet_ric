(**********************************************************************) 
(*   Dynamic Build Info header file generated: 2020.08.21 12:06 PM    *) 
(**********************************************************************) 
(**********************************************************************) 
(*    This file is generated with each build by the batch file*) 
(**********************************************************************) 
 
PROGRAM _INIT 
BUILDINFO:=BUILDINFO; 
BUILDNAME:=BUILDNAME; 
USERNAME:=USERNAME; 
PROJNAME:=PROJNAME; 
COMMIT:=COMMIT; 
COMMIT_INFO:=COMMIT_INFO; 
END_PROGRAM 
PROGRAM _CYCLIC 
brsmemset(ADR(G_BUILDNAME),0,SIZEOF(G_BUILDNAME)); 
brsstrcpy(ADR(G_BUILDNAME),ADR(BUILDNAME)); 
brsmemset(ADR(G_BUILDINFO),0,SIZEOF(G_BUILDINFO)); 
brsstrcpy(ADR(G_BUILDINFO),ADR(BUILDINFO)); 
brsmemset(ADR(G_USERNAME),0,SIZEOF(G_USERNAME)); 
brsstrcpy(ADR(G_USERNAME),ADR(USERNAME)); 
brsmemset(ADR(G_PROJNAME),0,SIZEOF(G_PROJNAME)); 
brsstrcpy(ADR(G_PROJNAME),ADR(PROJNAME)); 
brsmemset(ADR(G_COMMIT),0,SIZEOF(G_COMMIT)); 
brsstrcpy(ADR(G_COMMIT),ADR(COMMIT)); 
brsmemset(ADR(G_COMMIT_INFO),0,SIZEOF(G_COMMIT_INFO)); 
brsstrcpy(ADR(G_COMMIT_INFO),ADR(COMMIT_INFO)); 
END_PROGRAM 
