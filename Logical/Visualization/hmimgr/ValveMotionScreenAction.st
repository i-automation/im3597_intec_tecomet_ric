
ACTION ValveMotionScreenAction:
	
	// Check to be sure that we are on the manual valve screen and not in auto mode and not setting up
	IF (HMI.Page.Current = PAGE_SERVICE8) AND (Machine.Stat.Mode = MODE_MANUAL) AND NOT Machine.Stat.SettingUp THEN
		IF NOT(WasOnValveScreenAndInManual) THEN
			WasOnValveScreenAndInManual:= TRUE;
			HMI.PB.Valves[0]:= i_Rivet_Singulator_Ext_213x[0];
			HMI.PB.Valves[1]:= i_Rivet_Head_Stopper_Latch_2130;
			HMI.PB.Valves[2]:= i_Rivet_Singulator_Ext_213x[1];
			HMI.PB.Valves[3]:= i_Rivet_Nest_Horz_Ext_212x[0];
			HMI.PB.Valves[4]:= o_Press_Support_Vert_Up_2508A; //can't use the sensor feedback since vert could be set low to sit on horz support, but sensor would still be high
			HMI.PB.Valves[5]:= i_Rivet_Nest_Horz_Ext_212x[1];
			HMI.PB.Valves[6]:= i_Anvil_Plt_Clp_L_Latched_2023;
			HMI.PB.Valves[7]:= i_Press_Support_Horz_Ext_2029;
			HMI.PB.Valves[8]:= i_Anvil_Plt_Clp_R_Latched_2025;
			HMI.PB.Valves[9]:= o_Rivet_Vac_On_3507;
			HMI.PB.Valves[10]:= o_Rivet_Feeder_L_On_3505;
			HMI.PB.Valves[11]:= o_Rivet_Feeder_R_On_3506;
		END_IF;
		
		// Good to actuate all valves
		o_Rivet_Singulator_Ext_251xA[0]		:= HMI.PB.Valves[0];
		o_Rivet_Singulator_Ret_251xB[0]		:= NOT(HMI.PB.Valves[0]);
		
		o_Rivet_Head_Stopper_Latch_2510A	:= HMI.PB.Valves[1];
		o_Rivet_Head_Stopper_UnL_2510B		:= NOT(HMI.PB.Valves[1]);
		
		o_Rivet_Singulator_Ext_251xA[1]		:= HMI.PB.Valves[2];
		o_Rivet_Singulator_Ret_251xB[1]		:= NOT(HMI.PB.Valves[2]);
		
		o_Rivet_Nest_Horz_Ext_251xA[0]		:= HMI.PB.Valves[3];
		o_Rivet_Nest_Horz_Ret_251xB[0]		:= NOT(HMI.PB.Valves[3]);
		
		o_Press_Support_Vert_Up_2508A		:= HMI.PB.Valves[4];
		o_Press_Support_Vert_Down_2508B		:= NOT(HMI.PB.Valves[4]);
		
		o_Rivet_Nest_Horz_Ext_251xA[1]		:= HMI.PB.Valves[5];
		o_Rivet_Nest_Horz_Ret_251xB[1]		:= NOT(HMI.PB.Valves[5]);
		
		o_Anvil_Plate_Latch_L_Lch_2506A		:= HMI.PB.Valves[6];
		o_Anvil_Plate_Latch_L_Unl_2506B		:= NOT(HMI.PB.Valves[6]);
		
		o_Press_Support_Horz_Ext_2509A		:= HMI.PB.Valves[7];
		o_Press_Support_Horz_Ret_2509B		:= NOT(HMI.PB.Valves[7]);
		
		o_Anvil_Plate_Latch_R_Lch_2507A		:= HMI.PB.Valves[8];
		o_Anvil_Plate_Latch_R_Unl_2507B		:= NOT(HMI.PB.Valves[8]);
		
		o_Rivet_Vac_On_3507					:= HMI.PB.Valves[9];
//			o_Rivet_Vac_Rel_3508				:= HMI.PB.Valves[9];
		
		o_Rivet_Feeder_L_On_3505			:= HMI.PB.Valves[10];
		
		o_Rivet_Feeder_R_On_3506			:= HMI.PB.Valves[11];
		
		
		// Input Status
		HMI.IO.ValvePositions[0]			:= i_Rivet_Singulator_Ext_213x[0];
		HMI.IO.ValvePositions[1]			:= i_Rivet_Singulator_Ret_213x[0];
		HMI.IO.ValvePositions[2]			:= i_Rivet_Head_Stopper_Latch_2130;
		HMI.IO.ValvePositions[3]			:= i_Rivet_Head_Stopper_Unl_2131;
		HMI.IO.ValvePositions[4]			:= i_Rivet_Singulator_Ext_213x[1];
		HMI.IO.ValvePositions[5]			:= i_Rivet_Singulator_Ret_213x[1];
		HMI.IO.ValvePositions[6]			:= i_Rivet_Nest_Horz_Ext_212x[0];
		HMI.IO.ValvePositions[7]			:= i_Rivet_Nest_Horz_Ret_212x[0];
		HMI.IO.ValvePositions[8]			:= i_Press_Support_Vert_Up_2031;
		HMI.IO.ValvePositions[9]			:= i_Press_Support_Vert_Down_2032;
		HMI.IO.ValvePositions[10]			:= i_Rivet_Nest_Horz_Ext_212x[1];
		HMI.IO.ValvePositions[11]			:= i_Rivet_Nest_Horz_Ret_212x[1];
		HMI.IO.ValvePositions[12]			:= i_Anvil_Plt_Clp_L_Latched_2023;
		HMI.IO.ValvePositions[13]			:= i_Anvil_Plt_Clp_L_Unlatched_2024;
		HMI.IO.ValvePositions[14]			:= i_Press_Support_Horz_Ext_2029;
		HMI.IO.ValvePositions[15]			:= i_Press_Support_Horz_Ret_2030;
		HMI.IO.ValvePositions[16]			:= i_Anvil_Plt_Clp_R_Latched_2025;
		HMI.IO.ValvePositions[17]			:= i_Anvil_Plt_Clp_R_Unatched_2026;
		HMI.IO.ValvePositions[18]			:= i_Rivet_Press_Vac_Ack_2129;
		HMI.IO.ValvePositions[19]			:= NOT(i_Rivet_Press_Vac_Ack_2129);
		
		
		//Some restricted motion which was causing trouble when below
		// Nests
		// When one nest is extended, the other nest cannot extend
		// When the Head Stopper is not extended, neither nest can extend
		UNLOCK(ADR(HMI.SDP.Valves[3]));
		UNLOCK(ADR(HMI.SDP.Valves[5]));
		
		IF ABS(Press.Data.Position < Press.Par.NestPenetrateThreshold) THEN
			IF (i_Rivet_Nest_Horz_Ext_212x[0]) THEN
				o_Rivet_Nest_Horz_Ext_251xA[1]		:= FALSE;
				o_Rivet_Nest_Horz_Ret_251xB[1]		:= TRUE;
				HMI.PB.Valves[5]					:= FALSE;
				LOCK(ADR(HMI.SDP.Valves[5]));
			ELSIF (i_Rivet_Nest_Horz_Ext_212x[1]) THEN
				o_Rivet_Nest_Horz_Ext_251xA[0]		:= FALSE;
				o_Rivet_Nest_Horz_Ret_251xB[0]		:= TRUE;
				HMI.PB.Valves[3]					:= FALSE;
				LOCK(ADR(HMI.SDP.Valves[3]));
			END_IF
		ELSE
			IF (i_Rivet_Nest_Horz_Ret_212x[0]) THEN
				o_Rivet_Nest_Horz_Ext_251xA[0]		:= FALSE;
				o_Rivet_Nest_Horz_Ret_251xB[0]		:= TRUE;
				HMI.PB.Valves[3]					:= FALSE;
				LOCK(ADR(HMI.SDP.Valves[3]));
			END_IF
			IF (i_Rivet_Nest_Horz_Ret_212x[1]) THEN
				o_Rivet_Nest_Horz_Ext_251xA[1]		:= FALSE;
				o_Rivet_Nest_Horz_Ret_251xB[1]		:= TRUE;
				HMI.PB.Valves[5]					:= FALSE;
				LOCK(ADR(HMI.SDP.Valves[5]));
			END_IF
		END_IF
		
	ELSE
		IF WasOnValveScreenAndInManual THEN
			WasOnValveScreenAndInManual:= FALSE;
			Machine.Cmd.KissBlock.RaiseVert:= o_Press_Support_Vert_Up_2508A;
			Machine.Cmd.KissBlock.LowerVert:= o_Press_Support_Vert_Down_2508B;
			Machine.Cmd.KissBlock.ExtendHor:= o_Press_Support_Horz_Ext_2509A;
			Machine.Cmd.KissBlock.RetractHor:= o_Press_Support_Horz_Ret_2509B;
			
			Machine.Stat.KissBlock.VertUp:= i_Press_Support_Vert_Up_2031;
			IF Machine.Stat.KissBlock.VertUp THEN
				Machine.Cmd.KissBlock.Engage:= TRUE;
			END_IF;
			
			RivetHead.Cmd.VacuumOn:= o_Rivet_Vac_On_3507;
			
			RivetHead.Cmd.ExtendSing[0]:= i_Rivet_Singulator_Ext_213x[0];
			RivetHead.Cmd.RetractSing[0]:= i_Rivet_Singulator_Ret_213x[0];
			RivetHead.Cmd.ExtendSing[1]:= i_Rivet_Singulator_Ext_213x[1];
			RivetHead.Cmd.RetractSing[1]:= i_Rivet_Singulator_Ret_213x[1];
			
			RivetHead.Cmd.ExtendNest[0]:= i_Rivet_Nest_Horz_Ext_212x[0];
			RivetHead.Cmd.RetractNest[0]:= i_Rivet_Nest_Horz_Ret_212x[0];
			RivetHead.Cmd.ExtendNest[1]:= i_Rivet_Nest_Horz_Ext_212x[1];
			RivetHead.Cmd.RetractNest[1]:= i_Rivet_Nest_Horz_Ret_212x[1];
			
			Feeder[0].On:= o_Rivet_Feeder_L_On_3505;
			Feeder[1].On:= o_Rivet_Feeder_R_On_3506;
		END_IF
		
		o_Rivet_Feeder_L_On_3505:= Feeder[0].On;
		o_Rivet_Feeder_R_On_3506:= Feeder[1].On;
			
		o_Press_Support_Vert_Up_2508A		:= Machine.Cmd.KissBlock.RaiseVert;
		o_Press_Support_Vert_Down_2508B		:= Machine.Cmd.KissBlock.LowerVert;
		o_Press_Support_Horz_Ext_2509A		:= Machine.Cmd.KissBlock.ExtendHor;
		o_Press_Support_Horz_Ret_2509B		:= Machine.Cmd.KissBlock.RetractHor;
			
		o_Rivet_Singulator_Ext_251xA[0]		:= RivetHead.Cmd.ExtendSing[0];
		o_Rivet_Singulator_Ext_251xA[1]		:= RivetHead.Cmd.ExtendSing[1];
		o_Rivet_Singulator_Ret_251xB[0]		:= RivetHead.Cmd.RetractSing[0];
		o_Rivet_Singulator_Ret_251xB[1]		:= RivetHead.Cmd.RetractSing[1];
			
		o_Rivet_Nest_Horz_Ext_251xA[0]		:= RivetHead.Cmd.ExtendNest[0];
		o_Rivet_Nest_Horz_Ext_251xA[1]		:= RivetHead.Cmd.ExtendNest[1];
		o_Rivet_Nest_Horz_Ret_251xB[0]		:= RivetHead.Cmd.RetractNest[0];
		o_Rivet_Nest_Horz_Ret_251xB[1]		:= RivetHead.Cmd.RetractNest[1];
			
		o_Rivet_Vac_On_3507					:= RivetHead.Cmd.VacuumOn;
			
		Machine.Stat.KissBlock.VertUp		:= i_Press_Support_Vert_Up_2031;
		Machine.Stat.KissBlock.VertDown		:= i_Press_Support_Vert_Down_2032;
		Machine.Stat.KissBlock.HorExtended	:= i_Press_Support_Horz_Ext_2029;
		Machine.Stat.KissBlock.HorRetracted	:= i_Press_Support_Horz_Ret_2030;
			
		RivetHead.Stat.SingExtended[0]		:= i_Rivet_Singulator_Ext_213x[0];
		RivetHead.Stat.SingExtended[1]		:= i_Rivet_Singulator_Ext_213x[1];
		RivetHead.Stat.SingRetracted[0]		:= i_Rivet_Singulator_Ret_213x[0];
		RivetHead.Stat.SingRetracted[1]		:= i_Rivet_Singulator_Ret_213x[1];
		
		RivetHead.Stat.NestExtended[0]		:= i_Rivet_Nest_Horz_Ext_212x[0];
		RivetHead.Stat.NestExtended[1]		:= i_Rivet_Nest_Horz_Ext_212x[1];
		RivetHead.Stat.NestRetracted[0]		:= i_Rivet_Nest_Horz_Ret_212x[0];
		RivetHead.Stat.NestRetracted[1]		:= i_Rivet_Nest_Horz_Ret_212x[1];
		
		RivetHead.Stat.RivetSuctioned		:= i_Rivet_Press_Vac_Ack_2129;
	END_IF
	
	
	
	// TIMEOUTS
	TON_LeftLatchTimeout.PT:= T#1s;
	TON_LeftUnlatchTimeout.PT:= T#1s;
	TON_RightLatchTimeout.PT:= T#1s;
	TON_RightUnlatchTimeout.PT:= T#1s;
	TON_LeftSingulatorExtTimeout.PT:= T#1s;
	TON_LeftSingulatorRetTimeout.PT:= T#1s;
	TON_RightSingulatorExtTimeout.PT:= T#1s;
	TON_RightSingulatorRetTimeout.PT:= T#1s;
	TON_LeftNestExtTimeout.PT:= T#1s;
	TON_LeftNestRetTimeout.PT:= T#1s;
	TON_RightNestExtTimeout.PT:= T#1s;
	TON_RightNestRetTimeout.PT:= T#1s;
	TON_StopperLatchTimeout.PT:= T#1s;
	TON_StopperUnlatchTimeout.PT:= T#1s;
	TON_HorzSuppExtTimeout.PT:= T#1s;
	TON_HorzSuppRetTimeout.PT:= T#1s;
	TON_VertSuppUpTimeout.PT:= T#2s;
	TON_VertSuppDownTimeout.PT:= T#2s;
	
	//LEFT ANVIL CLAMP
	IF EDGEPOS(o_Anvil_Plate_Latch_L_Lch_2506A) THEN
		//LATCH TIMER SET
		TON_LeftLatchTimeout.IN:= TRUE;
		//UNLATCH TIMER RESET
		TON_LeftUnlatchTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Anvil_Plate_Latch_L_Unl_2506B) THEN
		//UNLACTCH TIMER SET
		TON_LeftUnlatchTimeout.IN:= TRUE;
		//LACTCH TIMER RESET
		TON_LeftLatchTimeout.IN:= FALSE;
	END_IF
	IF (i_Anvil_Plt_Clp_L_Latched_2023) THEN
		//LATCH TIMER RESET
		TON_LeftLatchTimeout.IN:= FALSE;
	END_IF
	IF (i_Anvil_Plt_Clp_L_Unlatched_2024) THEN
		//UNLATCH TIMER RESET
		TON_LeftUnlatchTimeout.IN:= FALSE;
	END_IF
	IF TON_LeftLatchTimeout.Q THEN
		//RESET LATCH TIMER
		TON_LeftLatchTimeout.IN:= FALSE;
		//SET LATCH TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_LEFT_LATCH_TIMEOUT;
	END_IF
	IF TON_LeftUnlatchTimeout.Q THEN
		//RESET UNLATCH TIMER
		TON_LeftUnlatchTimeout.IN:= FALSE;
		//SET UNLATCH TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_LEFT_UNLATCH_TIMEOUT;
	END_IF
	
	//RIGHT ANVIL CLAMP
	IF EDGEPOS(o_Anvil_Plate_Latch_R_Lch_2507A) THEN
		//LATCH TIMER SET
		TON_RightLatchTimeout.IN:= TRUE;
		//UNLATCH TIMER RESET
		TON_RightUnlatchTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Anvil_Plate_Latch_R_Unl_2507B) THEN
		//UNLACTCH TIMER SET
		TON_RightUnlatchTimeout.IN:= TRUE;
		//LACTCH TIMER RESET
		TON_RightLatchTimeout.IN:= FALSE;
	END_IF
	IF (i_Anvil_Plt_Clp_R_Latched_2025) THEN
		//LATCH TIMER RESET
		TON_RightLatchTimeout.IN:= FALSE;
	END_IF
	IF (i_Anvil_Plt_Clp_R_Unatched_2026) THEN
		//UNLATCH TIMER RESET
		TON_RightUnlatchTimeout.IN:= FALSE;
	END_IF
	IF TON_RightLatchTimeout.Q THEN
		//RESET LATCH TIMER
		TON_RightLatchTimeout.IN:= FALSE;
		//SET LATCH TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_RIGHT_LATCH_TIMEOUT;
	END_IF
	IF TON_RightUnlatchTimeout.Q THEN
		//RESET UNLATCH TIMER
		TON_RightUnlatchTimeout.IN:= FALSE;
		//SET UNLATCH TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_RIGHT_UNLATCH_TIMEOUT;
	END_IF
	
	//LEFT SINGULATOR
	IF EDGEPOS(o_Rivet_Singulator_Ext_251xA[0]) THEN
		//EXTEND TIMER SET
		TON_LeftSingulatorExtTimeout.IN:= TRUE;
		//RETRACT TIMER RESET
		TON_LeftSingulatorRetTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Rivet_Singulator_Ret_251xB[0]) THEN
		//RETRACT TIMER SET
		TON_LeftSingulatorRetTimeout.IN:= TRUE;
		//EXTEND TIMER RESET
		TON_LeftSingulatorExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Singulator_Ext_213x[0]) THEN
		//EXTEND TIMER RESET
		TON_LeftSingulatorExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Singulator_Ret_213x[0]) THEN
		//RETRACT TIMER RESET
		TON_LeftSingulatorRetTimeout.IN:= FALSE;
	END_IF
	IF TON_LeftSingulatorExtTimeout.Q THEN
		//RESET EXTEND TIMER
		TON_LeftSingulatorExtTimeout.IN:= FALSE;
		//SET EXTEND TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_LEFT_SING_EXT_TIMEOUT;
	END_IF
	IF TON_LeftSingulatorRetTimeout.Q THEN
		//RESET RETRACT TIMER
		TON_LeftSingulatorRetTimeout.IN:= FALSE;
		//SET RETRACT TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_LEFT_SING_RET_TIMEOUT;
	END_IF
	
	//RIGHT SINGULATOR
	IF EDGEPOS(o_Rivet_Singulator_Ext_251xA[1]) THEN
		//EXTEND TIMER SET
		TON_RightSingulatorExtTimeout.IN:= TRUE;
		//RETRACT TIMER RESET
		TON_RightSingulatorRetTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Rivet_Singulator_Ret_251xB[1]) THEN
		//RETRACT TIMER SET
		TON_RightSingulatorRetTimeout.IN:= TRUE;
		//EXTEND TIMER RESET
		TON_RightSingulatorExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Singulator_Ext_213x[1]) THEN
		//EXTEND TIMER RESET
		TON_RightSingulatorExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Singulator_Ret_213x[1]) THEN
		//RETRACT TIMER RESET
		TON_RightSingulatorRetTimeout.IN:= FALSE;
	END_IF
	IF TON_RightSingulatorExtTimeout.Q THEN
		//RESET EXTEND TIMER
		TON_RightSingulatorExtTimeout.IN:= FALSE;
		//SET EXTEND TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_RIGHT_SING_EXT_TIMEOUT;
	END_IF
	IF TON_RightSingulatorRetTimeout.Q THEN
		//RESET RETRACT TIMER
		TON_RightSingulatorRetTimeout.IN:= FALSE;
		//SET RETRACT TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_RIGHT_SING_RET_TIMEOUT;
	END_IF
	
	//LEFT NEST
	IF EDGEPOS(o_Rivet_Nest_Horz_Ext_251xA[0]) THEN
		//EXTEND TIMER SET
		TON_LeftNestExtTimeout.IN:= TRUE;
		//RETRACT TIMER RESET
		TON_LeftNestRetTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Rivet_Nest_Horz_Ret_251xB[0]) THEN
		//RETRACT TIMER SET
		TON_LeftNestRetTimeout.IN:= TRUE;
		//EXTEND TIMER RESET
		TON_LeftNestExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Nest_Horz_Ext_212x[0]) THEN
		//EXTEND TIMER RESET
		TON_LeftNestExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Nest_Horz_Ret_212x[0]) THEN
		//RETRACT TIMER RESET
		TON_LeftNestRetTimeout.IN:= FALSE;
	END_IF
	IF TON_LeftNestExtTimeout.Q THEN
		//RESET EXTEND TIMER
		TON_LeftNestExtTimeout.IN:= FALSE;
		//SET EXTEND TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_LEFT_NEST_EXT_TIMEOUT;
	END_IF
	IF TON_LeftNestRetTimeout.Q THEN
		//RESET RETRACT TIMER
		TON_LeftNestRetTimeout.IN:= FALSE;
		//SET RETRACT TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_LEFT_NEST_RET_TIMEOUT;
	END_IF
	
	//RIGHT NEST
	IF EDGEPOS(o_Rivet_Nest_Horz_Ext_251xA[1]) THEN
		//EXTEND TIMER SET
		TON_RightNestExtTimeout.IN:= TRUE;
		//RETRACT TIMER RESET
		TON_RightNestRetTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Rivet_Nest_Horz_Ret_251xB[1]) THEN
		//RETRACT TIMER SET
		TON_RightNestRetTimeout.IN:= TRUE;
		//EXTEND TIMER RESET
		TON_RightNestExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Nest_Horz_Ext_212x[1]) THEN
		//EXTEND TIMER RESET
		TON_RightNestExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Nest_Horz_Ret_212x[1]) THEN
		//RETRACT TIMER RESET
		TON_RightNestRetTimeout.IN:= FALSE;
	END_IF
	IF TON_RightNestExtTimeout.Q THEN
		//RESET EXTEND TIMER
		TON_RightNestExtTimeout.IN:= FALSE;
		//SET EXTEND TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_RIGHT_NEST_EXT_TIMEOUT;
	END_IF
	IF TON_RightNestRetTimeout.Q THEN
		//RESET RETRACT TIMER
		TON_RightNestRetTimeout.IN:= FALSE;
		//SET RETRACT TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_RIGHT_NEST_RET_TIMEOUT;
	END_IF
	
	//RIVETHEAD STOPPER
	IF EDGEPOS(o_Rivet_Head_Stopper_Latch_2510A) THEN
		//LATCH TIMER SET
		TON_StopperLatchTimeout.IN:= TRUE;
		//UNLATCH TIMER RESET
		TON_StopperUnlatchTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Rivet_Head_Stopper_UnL_2510B) THEN
		//UNLATCH TIMER SET
		TON_StopperUnlatchTimeout.IN:= TRUE;
		//LATCH TIMER RESET
		TON_StopperLatchTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Head_Stopper_Latch_2130) THEN
		//LATCH TIMER RESET
		TON_StopperLatchTimeout.IN:= FALSE;
	END_IF
	IF (i_Rivet_Head_Stopper_Unl_2131) THEN
		//UNLATCH TIMER RESET
		TON_StopperUnlatchTimeout.IN:= FALSE;
	END_IF
	IF TON_StopperLatchTimeout.Q THEN
		//RESET LATCH TIMER
		TON_StopperLatchTimeout.IN:= FALSE;
		//SET LATCH TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_STOPPER_LATCH_TIMEOUT;
	END_IF
	IF TON_StopperUnlatchTimeout.Q THEN
		//RESET UNLATCH TIMER
		TON_StopperUnlatchTimeout.IN:= FALSE;
		//SET UNLATCH TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_STOPPER_UNLATCH_TIMEOUT;
	END_IF
	
	//HORIZONTAL SUPPORT
	IF EDGEPOS(o_Press_Support_Horz_Ext_2509A) THEN
		//EXTEND TIMER SET
		TON_HorzSuppExtTimeout.IN:= TRUE;
		//RETRACT TIMER RESET
		TON_HorzSuppRetTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Press_Support_Horz_Ret_2509B) THEN
		//RETRACT TIMER SET
		TON_HorzSuppRetTimeout.IN:= TRUE;
		//EXTEND TIMER RESET
		TON_HorzSuppExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Press_Support_Horz_Ext_2029) THEN
		//EXTEND TIMER RESET
		TON_HorzSuppExtTimeout.IN:= FALSE;
	END_IF
	IF (i_Press_Support_Horz_Ret_2030) THEN
		//RETRACT TIMER RESET
		TON_HorzSuppRetTimeout.IN:= FALSE;
	END_IF
	IF TON_HorzSuppExtTimeout.Q THEN
		//RESET EXTEND TIMER
		TON_HorzSuppExtTimeout.IN:= FALSE;
		//SET EXTEND TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_HORZ_SUPP_EXT_TIMEOUT;
	END_IF
	IF TON_HorzSuppRetTimeout.Q THEN
		//RESET RETRACT TIMER
		TON_HorzSuppRetTimeout.IN:= FALSE;
		//SET RETRACT TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_HORZ_SUPP_RET_TIMEOUT;
	END_IF
	
	//VERTICAL SUPPORT
	IF EDGEPOS(o_Press_Support_Vert_Up_2508A) THEN
		//RAISE TIMER SET
		TON_VertSuppUpTimeout.IN:= TRUE;
		//LOWER TIMER RESET
		TON_VertSuppDownTimeout.IN:= FALSE;
	END_IF
	IF EDGEPOS(o_Press_Support_Vert_Down_2508B) AND i_Press_Support_Horz_Ret_2030 THEN //otherwise would get a timeout since vert would remain up on top of horz support
		//LOWER TIMER SET
		TON_VertSuppDownTimeout.IN:= TRUE;
		//RAISE TIMER RESET
		TON_VertSuppUpTimeout.IN:= FALSE;
	END_IF
	IF (i_Press_Support_Vert_Up_2031) THEN //don't want an edgepos here since the vert could already be up when actuated off of the horizontal support
		//RAISE TIMER RESET
		TON_VertSuppUpTimeout.IN:= FALSE;
	END_IF
	IF (i_Press_Support_Vert_Down_2032) THEN
		//LOWER TIMER RESET
		TON_VertSuppDownTimeout.IN:= FALSE;
	END_IF
	IF TON_VertSuppUpTimeout.Q THEN
		//RESET RAISE TIMER
		TON_VertSuppUpTimeout.IN:= FALSE;
		//SET RAISE TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_VERT_SUPP_UP_TIMEOUT;
	END_IF
	IF TON_VertSuppDownTimeout.Q THEN
		//RESET LOWER TIMER
		TON_VertSuppDownTimeout.IN:= FALSE;
		//SET LOWER TIMEOUT FAULT
		Machine.Stat.Error:= MACHINE_VERT_SUPP_DOWN_TIMEOUT;
	END_IF
	
	//TIMEOUT TIMER CALLS
	TON_LeftLatchTimeout();
	TON_LeftUnlatchTimeout();
	TON_RightLatchTimeout();
	TON_RightUnlatchTimeout();
	TON_LeftSingulatorExtTimeout();
	TON_LeftSingulatorRetTimeout();
	TON_RightSingulatorExtTimeout();
	TON_RightSingulatorRetTimeout();
	TON_LeftNestExtTimeout();
	TON_LeftNestRetTimeout();
	TON_RightNestExtTimeout();
	TON_RightNestRetTimeout();
	TON_StopperLatchTimeout();
	TON_StopperUnlatchTimeout();
	TON_HorzSuppExtTimeout();
	TON_HorzSuppRetTimeout();
	TON_VertSuppUpTimeout();
	TON_VertSuppDownTimeout();
	
	
	
	// Restricted Motion

	// Lock vibratory track if singulator is extended
	IF i_Rivet_Singulator_Ext_213x[0] THEN
		LOCK(ADR(HMI.SDP.Valves[10]));
	ELSE
		UNLOCK(ADR(HMI.SDP.Valves[10]));
	END_IF
	IF i_Rivet_Singulator_Ext_213x[1] THEN
		LOCK(ADR(HMI.SDP.Valves[11]));
	ELSE
		UNLOCK(ADR(HMI.SDP.Valves[11]));
	END_IF
	
	// Vertical Press Support
	UNLOCK(ADR(HMI.SDP.Valves[4]));
	UNLOCK(ADR(HMI.SDP.Valves[7]));
	
	
	// Restrict valve outputs if out of zone
	IF ((Axis[AX_Y].Data.Position < REAL_TO_LREAL(SystemSettings.YAxisRestriction_NegLim)) OR (Axis[AX_Y].Data.Position > REAL_TO_LREAL(SystemSettings.YAxisRestriction_PosLim)) OR NOT(Axis[AX_Y].Status.Homed)) THEN
		IF (i_Press_Support_Horz_Ret_2030) THEN
			o_Press_Support_Vert_Up_2508A		:= FALSE;
			o_Press_Support_Vert_Down_2508B		:= TRUE;
			HMI.PB.Valves[4]					:= FALSE;
		ELSE
			o_Press_Support_Horz_Ext_2509A		:= FALSE;
			o_Press_Support_Horz_Ret_2509B		:= TRUE;
			o_Press_Support_Vert_Up_2508A		:= TRUE;
			o_Press_Support_Vert_Down_2508B		:= FALSE;
			HMI.PB.Valves[7]					:= FALSE;
		END_IF
		
		LOCK(ADR(HMI.SDP.Valves[4]));
		LOCK(ADR(HMI.SDP.Valves[7]));
	
	ELSIF (i_Press_Support_Vert_Down_2032) THEN
		o_Press_Support_Horz_Ext_2509A		:= FALSE;
		o_Press_Support_Horz_Ret_2509B		:= TRUE;
		HMI.PB.Valves[7]					:= FALSE;
		LOCK(ADR(HMI.SDP.Valves[7]));
	ELSIF (i_Press_Support_Vert_Up_2031) THEN
		// restrict movement by y axis if we are in zone
//		SystemSettings.AxisConfig[AX_Y].Mapp.Axis.SoftwareLimitPositions.LowerLimit	:= YAxisRestriction_NegLim;
//		LimitY
	END_IF
			
END_ACTION
