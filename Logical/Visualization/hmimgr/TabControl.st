(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Program: hmimgr
 * File: TabControl.st
 * Author: SSwindells
 * Created: April 11, 2012
 ********************************************************************
 * Implementation of program hmimgr
 ********************************************************************) 

ACTION TabControl: 
	// MAIN TAB LAYER
	// set all tab backgrounds to the secondary (not visible) color
	// and hide the seperating line
	FOR i:= 0 TO ( (SIZEOF(HMI.Tabs.MainTabColorCDP)/SIZEOF(HMI.Tabs.MainTabColorCDP[0]))-1) DO
		HMI.Tabs.MainTabColorCDP[i]:= HMIColors.InactiveTabColor + 256*0;
		SHOW(ADR(HMI.Tabs.MainTabLineSDP[i]));
	END_FOR
		
	CASE HMI.Page.Current OF
		PAGE_MAIN:
			HMI.Tabs.MainTabColorCDP[0]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.MainTabLineSDP[0]));
				
		PAGE_SETUP:
			HMI.Tabs.MainTabColorCDP[1]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.MainTabLineSDP[1]));
				
		PAGE_SERVICE0..PAGE_SERVICE7:
			HMI.Tabs.MainTabColorCDP[2]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.MainTabLineSDP[2]));
				
		PAGE_SYS0..PAGE_SYS7:
			HMI.Tabs.MainTabColorCDP[3]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.MainTabLineSDP[3]));
	END_CASE
				
	// service TAB LAYER
	// set all tab backgrounds to the secondary (not visible) color
	// and hide the seperating line
	FOR i:= 0 TO ( (SIZEOF(HMI.Tabs.ServiceTabColorCDP)/SIZEOF(HMI.Tabs.ServiceTabColorCDP[0]))-1) DO
		HMI.Tabs.ServiceTabColorCDP[i]:= HMIColors.InactiveTabColor + 256*0;
		SHOW(ADR(HMI.Tabs.ServiceTabLineSDP[i]));
	END_FOR
		
	CASE HMI.Page.Current OF
		PAGE_SERVICE0:
			HMI.Tabs.ServiceTabColorCDP[0]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[0]));
				
		PAGE_SERVICE1:
			HMI.Tabs.ServiceTabColorCDP[1]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[1]));
				
		PAGE_SERVICE2:
			HMI.Tabs.ServiceTabColorCDP[2]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[2]));
				
		PAGE_SERVICE3:
			HMI.Tabs.ServiceTabColorCDP[3]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[3]));
			
		PAGE_SERVICE4:
			HMI.Tabs.ServiceTabColorCDP[4]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[4]));
		
		PAGE_SERVICE5:
			HMI.Tabs.ServiceTabColorCDP[5]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[5]));
			
		PAGE_SERVICE6,PAGE_SERVICE8:
			HMI.Tabs.ServiceTabColorCDP[6]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[6]));
			
		PAGE_SERVICE7:
			HMI.Tabs.ServiceTabColorCDP[7]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.ServiceTabLineSDP[7]));
		
	END_CASE
	
	// system TAB LAYER
	// set all tab backgrounds to the secondary (not visible) color
	// and hide the seperating line
	FOR i:= 0 TO ( (SIZEOF(HMI.Tabs.SysTabColorCDP)/SIZEOF(HMI.Tabs.SysTabColorCDP[0]))-1) DO
		HMI.Tabs.SysTabColorCDP[i]:= HMIColors.InactiveTabColor + 256*0;
		SHOW(ADR(HMI.Tabs.SysTabLineSDP[i]));
	END_FOR
		
	CASE HMI.Page.Current OF
		PAGE_SYS0:
			HMI.Tabs.SysTabColorCDP[0]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[0]));
				
		PAGE_SYS1:
			HMI.Tabs.SysTabColorCDP[1]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[1]));
				
		PAGE_SYS2:
			HMI.Tabs.SysTabColorCDP[2]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[2]));
				
		PAGE_SYS3:
			HMI.Tabs.SysTabColorCDP[3]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[3]));
			
		PAGE_SYS4:
			HMI.Tabs.SysTabColorCDP[4]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[4]));
				
		PAGE_SYS5:
			HMI.Tabs.SysTabColorCDP[5]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[5]));
				
		PAGE_SYS6:
			HMI.Tabs.SysTabColorCDP[6]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[6]));
				
		PAGE_SYS7:
			HMI.Tabs.SysTabColorCDP[7]:= HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.Tabs.SysTabLineSDP[7]));
		
	END_CASE			
END_ACTION