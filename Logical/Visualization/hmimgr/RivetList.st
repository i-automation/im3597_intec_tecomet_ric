ACTION UpdateRivetList:
	
	//Caution: This action is called within another loop, use j index var to avoid conflict
	FOR j:= 0 TO ((SIZEOF(SystemSettings.Rivet)/SIZEOF(SystemSettings.Rivet[0]))-1) DO
		itoa(INT_TO_DINT(j+1),ADR(jstr));
		strcpy(ADR(RivetNameList[j]),ADR(jstr));
		strcat(ADR(RivetNameList[j]),ADR(' -   '));
		strcat(ADR(RivetNameList[j]),ADR(SystemSettings.Rivet[j].Name));
	END_FOR 

END_ACTION
