
ACTION SafetyScreenAction:
	
	IF (HMI.Page.Current = PAGE_SERVICE9) THEN
		IF EnteringPage THEN
			FOR DoorIndex:= 0 TO 5 DO
				HMI.PB.SafetyDoors[DoorIndex]:= i_Safety_Door_Locked[DoorIndex];
			END_FOR
		END_IF;
		// Check to be sure that we are not in auto mode
//		IF (Machine.Stat.Mode <> MODE_AUTO) THEN
			// Good to lock/unlock all safety doors (may need other checks here too)
			
			FOR DoorIndex:= 0 TO 5 DO
				IF HMI.PB.SafetyDoors[DoorIndex] THEN
					o_Safety_Door_Lock[DoorIndex]:= TRUE;
					o_Safety_Door_Unlock[DoorIndex]:= FALSE;
				ELSE
					o_Safety_Door_Lock[DoorIndex]:= FALSE;
					o_Safety_Door_Unlock[DoorIndex]:= TRUE;
				END_IF;
			END_FOR
			
			// Input Status (might want a status for the door too - whether it is locked or not)
			//			HMI.IO.ValvePositions[0]			:= i_Rivet_Singulator_Ext_213x[0];
			//			HMI.IO.ValvePositions[1]			:= i_Rivet_Singulator_Ret_213x[0];
			//			HMI.IO.ValvePositions[2]			:= i_Rivet_Head_Stopper_Latch_2130;
			//			HMI.IO.ValvePositions[3]			:= i_Rivet_Head_Stopper_Unl_2131;
			//			HMI.IO.ValvePositions[4]			:= i_Rivet_Singulator_Ext_213x[1];
			//			HMI.IO.ValvePositions[5]			:= i_Rivet_Singulator_Ret_213x[1];
			//			HMI.IO.ValvePositions[6]			:= i_Rivet_Nest_Horz_Ext_212x[0];
			//			HMI.IO.ValvePositions[7]			:= i_Rivet_Nest_Horz_Ret_212x[0];
			//			HMI.IO.ValvePositions[8]			:= i_Press_Support_Vert_Up_2031;
			//			HMI.IO.ValvePositions[9]			:= i_Press_Support_Vert_Down_2032;
			//			HMI.IO.ValvePositions[10]			:= i_Rivet_Nest_Horz_Ext_212x[1];
			//			HMI.IO.ValvePositions[11]			:= i_Rivet_Nest_Horz_Ret_212x[1];
			//			HMI.IO.ValvePositions[12]			:= i_Anvil_Plt_Clp_L_Latched_2023;
			//			HMI.IO.ValvePositions[13]			:= i_Anvil_Plt_Clp_L_Unlatched_2024;
			//			HMI.IO.ValvePositions[14]			:= i_Press_Support_Horz_Ext_2029;
			//			HMI.IO.ValvePositions[15]			:= i_Press_Support_Horz_Ret_2030;
			//			HMI.IO.ValvePositions[16]			:= i_Anvil_Plt_Clp_R_Latched_2025;
			//			HMI.IO.ValvePositions[17]			:= i_Anvil_Plt_Clp_R_Unatched_2026;
			//			HMI.IO.ValvePositions[18]			:= i_Rivet_Press_Vac_Ack_2129;
			//			HMI.IO.ValvePositions[19]			:= NOT(i_Rivet_Press_Vac_Ack_2129);
//		END_IF
	END_IF
	
END_ACTION
