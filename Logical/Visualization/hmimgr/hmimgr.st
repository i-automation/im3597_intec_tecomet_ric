(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: hmimgr
 * File: hmimgr.st
 * Author: ?
 * Created: long ago
 * Version: 1.00.0
 * Modified Date: 07/13/2016
 * Modified BY:	phouston
 ********************************************************************
 * Implementation of program hmimgr

 * Description:

	This program is created to handle various functions related to
	the HMI. As it has evolved, it has grown to include recipe handling,
	USB browsing, system settings import/export, and ethernet
	configuration.

 * Options:

 * Version History:
 ********************************************************************)
PROGRAM _CYCLIC

	IF (DataMgrInit) THEN

		IF (vcHandle = 0) THEN
			vcHandle:= VA_Setup(1, VisObjName);
		END_IF

		// look for loss of connection so we can re-initialize the vcHandle
		IF (vcHandle <> 0) THEN
			IF (VA_Saccess(1, vcHandle) = vaERR_CONNECTION_LOST) THEN
				vcHandle:= FALSE;
			ELSE
				VA_Srelease(1,vcHandle);
			END_IF
		END_IF

		// monitor HMI touch status
		IF (vcHandle <> 0) THEN
			IF (VA_Saccess(1, vcHandle) = 0) THEN
				VA_GetTouchAction(1,vcHandle,1, ADR(Touch));
				VA_Srelease(1,vcHandle);
			END_IF
		END_IF

		hwinitialized:= AllAxesInit AND AllIOInit;

		// State Trace;
		IF (State <> StateTrace[0]) THEN
			FOR i:= ((SIZEOF(StateTrace)/SIZEOF(StateTrace[0]))-1) TO 1 BY (-1) DO
				StateTrace[i]:= StateTrace[i-1];
			END_FOR
			StateTrace[0]:= State;
		END_IF

		CASE State OF

			IDLE:
				IF ( (HMI.Page.Current = PAGE_INIT) AND (HMI.Page.Change <> PAGE_SPLASH) ) THEN
					TON_Init.IN:= TRUE;
					HMI.Page.Change:= PAGE_SPLASH;

				ELSIF ( (HMI.Page.Current = PAGE_SPLASH) AND (HMI.InitStatus <> 1) AND hwinitialized) THEN
					TON_Init.IN:= FALSE;
					HMI.InitStatus:= 1;   // initialized  - XXX use CONSTANTS!!
					HMI.Page.Change:= PAGE_MAIN;

				ELSIF (TON_Init.Q) THEN
					TON_Init.IN:= FALSE;
					HMI.InitStatus:= 2;    // timeout - XXX use CONSTANTS!!
					IF (HMI.Page.Current = PAGE_INIT) THEN
						HMI.Page.Change:= PAGE_SERVICE1;
					END_IF

				//Change Language
				ELSIF ((SystemSettings.Display.Language <> HMI.Language.Current) AND (SystemSettings.Display.Language <> HMI.Language.Change)) THEN
					HMI.Language.Change:= SystemSettings.Display.Language;

				//Change Measurement Units
				ELSIF ((SystemSettings.Display.UnitType <> HMI.Units.LinearAccelerationUnits.Current) AND (SystemSettings.Display.UnitType <> HMI.Units.LinearAccelerationUnits.Change)) THEN
					HMI.Units.LinearAccelerationUnits.Change:= SystemSettings.Display.UnitType;

				ELSIF ((SystemSettings.Display.UnitType <> HMI.Units.LinearDisplacementUnits.Current) AND (SystemSettings.Display.UnitType <> HMI.Units.LinearDisplacementUnits.Change)) THEN
					HMI.Units.LinearDisplacementUnits.Change:= SystemSettings.Display.UnitType;

				ELSIF ((SystemSettings.Display.UnitType <> HMI.Units.LinearVelocityUnits.Current) AND (SystemSettings.Display.UnitType <> HMI.Units.LinearVelocityUnits.Change)) THEN
					HMI.Units.LinearVelocityUnits.Change:= SystemSettings.Display.UnitType;

				ELSIF ((SystemSettings.Display.UnitType <> HMI.Units.TemperatureUnits.Current) AND (SystemSettings.Display.UnitType <> HMI.Units.TemperatureUnits.Change)) THEN
					HMI.Units.TemperatureUnits.Change:= SystemSettings.Display.UnitType;

				ELSIF (HMI.Network.EthernetSettingsLoad) THEN
					HMI.Network.EthernetSettingsLoad:= FALSE;
					HMI.Network.EthernetSettingsStatus:= FALSE;
					State:= ETHERNET_SETTINGS_LOAD_MODE;

				ELSIF (HMI.Network.EthernetSettingsApply) THEN
					HMI.Network.EthernetSettingsApply:= FALSE;
					IF (HMI.Network.EthernetConfigMode = 0) THEN //Static IP  - XXX USE CONSTANTS!
						// IP valid range "1.0.0.1" - "247.255.255.255" 
						IF ( (HMI.Network.EthernetIPAddress[0] > 0) AND (HMI.Network.EthernetIPAddress[0] < 248) AND (HMI.Network.EthernetIPAddress[3] > 0) ) THEN
							// subnet mask valid range "1.0.0.1" - "255.255.255.254" 
							IF ( (HMI.Network.EthernetSubnetMask[0] > 0) AND (HMI.Network.EthernetSubnetMask[3] < 255) ) THEN
								// gateway valid range "1.0.0.1" - "247.255.255.255" 
								IF ( (HMI.Network.EthernetGatewayAddress[0] > 0) AND (HMI.Network.EthernetGatewayAddress[0] < 248) AND (HMI.Network.EthernetGatewayAddress[3] > 0) ) THEN
									IF ( (HMI.Network.EthernetINANode > 0) AND (HMI.Network.EthernetINANode < 255) ) THEN
										IP2String(pIPString:= ADR(IPAddrString) , Octet1:=  HMI.Network.EthernetIPAddress[0], Octet2:=  HMI.Network.EthernetIPAddress[1],
										Octet3:= HMI.Network.EthernetIPAddress[2] , Octet4:=  HMI.Network.EthernetIPAddress[3]);
										State:= ETHERNET_SETTINGS_SAVE_IP;
									ELSE // main controller INA node is NOT valid
										HMI.Network.EthernetSettingsStatus:= 5; //XXX CONSTANTS
									END_IF
								ELSE // main controller gateway ip is bad
									HMI.Network.EthernetSettingsStatus:= 4;
								END_IF
							ELSE // main controller subnet mask is bad
								HMI.Network.EthernetSettingsStatus:= 3;
							END_IF
						ELSE // main controller  IP bad
							HMI.Network.EthernetSettingsStatus:= 2;
						END_IF
					ELSE // DHCP mode
						State:= ETHERNET_SETTINGS_SAVE_MODE;
					END_IF
				
				// OEM system settings functions
				ELSIF (HMI.OEM.OEMImportPB) THEN
					HMI.OEM.OEMImportPB:= FALSE;
					NextState:= IMPORT_SYS;
					State:= USB_BROWSE0;
				ELSIF (HMI.OEM.OEMExportPB) THEN
					HMI.OEM.OEMExportPB:= FALSE;
					NextState:= EXPORT_SYS;
					State:= USB_BROWSE0;
				ELSIF (HMI.OEM.OEMExportDiagnosticsPB) THEN
					HMI.OEM.OEMExportDiagnosticsPB:= FALSE;
					NextState:= EXPORT_SDM;
					State:= USB_BROWSE0;
				END_IF
			
			USB_BROWSE0:
				IF (NOT USBBrowseRequired) THEN
					State:= NextState;
				ELSE
					State:= USB_BROWSE1;
				END_IF
			
			USB_BROWSE1:
				UsbNodeListGet_0.enable:= TRUE;
				UsbNodeListGet_0.pBuffer:= ADR(node_id_buffer);
				UsbNodeListGet_0.bufferSize:= SIZEOF(node_id_buffer);
				UsbNodeListGet_0.filterInterfaceClass:= asusb_CLASS_MASS_STORAGE;
				UsbNodeListGet_0.filterInterfaceSubClass:= FALSE;
				IF (UsbNodeListGet_0.status = ERR_OK) THEN
					UsbNodeListGet_0.enable:= FALSE;
					node:= FALSE;
					brsmemset(ADR(usb_data_buffer), 0, SIZEOF(usb_data_buffer));
					State:= USB_BROWSE2;
				ELSIF ( (UsbNodeListGet_0.status <> ERR_FUB_BUSY) AND (UsbNodeListGet_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					UsbNodeListGet_0.enable:= FALSE;
					HMI.OEM.OEMSysSettingsStatus:= 6; //XXX- Use constants!!
					State:= IDLE;
				END_IF
			
			USB_BROWSE2:
				UsbNodeGet_0.enable:= TRUE;
				UsbNodeGet_0.nodeId:= node_id_buffer[node];
				UsbNodeGet_0.pBuffer:= ADR(usb_data_buffer[node]);
				UsbNodeGet_0.bufferSize:= SIZEOF(usb_data_buffer[node]); 
				IF (UsbNodeGet_0.status = ERR_OK) THEN
					node:= node + TRUE;
					IF (node = UsbNodeListGet_0.listNodes) THEN
						UsbNodeGet_0.enable:= FALSE;
						node:= FALSE;
						brsmemset(ADR(device_descriptor), 0, SIZEOF(device_descriptor));
						State:= USB_BROWSE3;
					ELSE
						UsbNodeGet_0.nodeId:= node_id_buffer[node];
						UsbNodeGet_0.pBuffer:= ADR(usb_data_buffer[node]);
					END_IF
				ELSIF ( (UsbNodeGet_0.status <> ERR_FUB_BUSY) AND (UsbNodeGet_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					UsbNodeGet_0.enable:= FALSE;
					HMI.OEM.OEMSysSettingsStatus:= 6;  //XXX- Use constants!!
					State:= IDLE;
				END_IF
			
			USB_BROWSE3:
				UsbDescriptorGet_0.enable:= TRUE;
				UsbDescriptorGet_0.requestType:= FALSE;
				UsbDescriptorGet_0.descriptorType:= TRUE;
				UsbDescriptorGet_0.languageId:= FALSE;
				UsbDescriptorGet_0.nodeId:= node_id_buffer[node];
				UsbDescriptorGet_0.pBuffer:= ADR(device_descriptor[node]);
				UsbDescriptorGet_0.bufferSize:= SIZEOF(device_descriptor[node]);
				IF (UsbDescriptorGet_0.status = ERR_OK) THEN
					node:= node + TRUE;
					IF (node = UsbNodeListGet_0.listNodes) THEN
						UsbDescriptorGet_0.enable:= FALSE;
						IF (DevLinkHandle <> 0) THEN
							State:= USB_BROWSE4; 
						ELSE
							State:= USB_BROWSE5;
						END_IF
					ELSE
						UsbDescriptorGet_0.nodeId:= node_id_buffer[node];
						UsbDescriptorGet_0.pBuffer:= ADR(device_descriptor[node]);
					END_IF
				ELSIF ( (UsbDescriptorGet_0.status <> ERR_FUB_BUSY) AND (UsbDescriptorGet_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					UsbDescriptorGet_0.enable:= FALSE;
					HMI.OEM.OEMSysSettingsStatus:= 6;//XXX - USE CONSTANTS!!
					State:= IDLE;
				END_IF
	
			USB_BROWSE4:
				DevUnlink_0.enable:= TRUE;
				DevUnlink_0.handle:= DevLinkHandle;
				IF (DevUnlink_0.status = ERR_OK) THEN
					DevUnlink_0.enable:= FALSE;
					DevLink_0.enable:= TRUE;
					DevLink_0.pDevice:= ADR('usbdrive');
					DevLink_0.pParam:= ADR(FileDeviceParam);
					State:= USB_BROWSE5;
				ELSIF ( (DevUnlink_0.status <> ERR_FUB_BUSY) AND (DevUnlink_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					DevUnlink_0.enable:= FALSE;
					HMI.OEM.OEMSysSettingsStatus:= 6;//XXX - USE CONSTANTS!!
					State:= IDLE;
				END_IF
			
			USB_BROWSE5: // link to file device
				// NOTE: this only links to the 1st discoved USB device!!!!!
				brsmemset(ADR(FileDeviceParam),0,SIZEOF(FileDeviceParam));
				strcpy(ADR(device_name), ADR('usbdrive'));
				strcpy(ADR(FileDeviceParam), ADR('/DEVICE='));
				strcat(ADR(FileDeviceParam), ADR(usb_data_buffer[0].ifName));
				DevLink_0.enable:= TRUE;
				DevLink_0.pDevice:= ADR('usbdrive');
				DevLink_0.pParam:= ADR(FileDeviceParam);
				IF (DevLink_0.status = ERR_OK) THEN
					DevLinkHandle:= DevLink_0.handle;
					DevLink_0.enable:= FALSE;
					State:= NextState;
				ELSIF ( (DevLink_0.status <> ERR_FUB_BUSY) AND (DevLink_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					DevLink_0.enable:= FALSE;
					DevLinkHandle:= FALSE;
					HMI.OEM.OEMSysSettingsStatus:= 6;//XXX - USE CONSTANTS!!
					State:= IDLE;
				END_IF
			
			IMPORT_SYS:
				IF (RecipesCyclic.Status = IARECIPE_STATUS_READY) THEN
					RecipesCyclic.cmd:= IARECIPE_CMD_IMPORT_SYS;
					HMI.OEM.OEMSysSettingsStatus:= TRUE;   // importing.... //XXX - USE CONSTANTS!!
					State:= IMPORT_SYS_WAIT;
				END_IF

			IMPORT_SYS_WAIT:
				IF (RecipesCyclic.Status = IARECIPE_STATUS_ERROR) THEN
					RecipesCyclic.cmd:= IARECIPE_CMD_NONE;
					HMI.OEM.OEMSysSettingsStatus:= 3;  // import failure... //XXX - USE CONSTANTS!!
					State:= IDLE;
				ELSIF (RecipesCyclic.Status = IARECIPE_STATUS_DONE) THEN
					RecipesCyclic.cmd:= IARECIPE_CMD_NONE;
					HMI.OEM.OEMSysSettingsStatus:= 2; // import success //XXX - USE CONSTANTS!!
					State:= IDLE;
				END_IF

			EXPORT_SYS:
				IF (RecipesCyclic.Status = IARECIPE_STATUS_READY) THEN
					RecipesCyclic.cmd:= IARECIPE_CMD_EXPORT_SYS;
					HMI.OEM.OEMSysSettingsStatus:= 4; // exporting
					State:= EXPORT_SYS_WAIT;
				END_IF

			EXPORT_SYS_WAIT:
				IF (RecipesCyclic.Status = IARECIPE_STATUS_ERROR) THEN
					RecipesCyclic.cmd:= IARECIPE_CMD_NONE;
					HMI.OEM.OEMSysSettingsStatus:= 6; // export failure //XXX - USE CONSTANTS!!
					State:= IDLE;
				ELSIF (RecipesCyclic.Status = IARECIPE_STATUS_DONE) THEN
					RecipesCyclic.cmd:= IARECIPE_CMD_NONE;
					HMI.OEM.OEMSysSettingsStatus:= 5; // export success //XXX - USE CONSTANTS!!
					State:= IDLE;
				END_IF

			EXPORT_SDM:
				SysDump.cmd:= TRUE;
				IF SysDump.inProgress THEN
					HMI.OEM.OEMSysSettingsStatus:= 4; // exporting  //XXX - USE CONSTANTS!!
					State:= EXPORT_SDM_WAIT;
				END_IF

			EXPORT_SDM_WAIT:
				IF (SysDump.inError) THEN
					SysDump.inError:= FALSE;
					HMI.OEM.OEMSysSettingsStatus:= 9; // export failure //XXX - USE CONSTANTS!!
					State:= IDLE;
				ELSIF (SysDump.complete) THEN
					SysDump.complete:= FALSE;
					HMI.OEM.OEMSysSettingsStatus:= 8; // export success  //XXX - USE CONSTANTS!!
					State:= IDLE;
				END_IF

			ETHERNET_SETTINGS_LOAD_MODE:
				GetEthConfigMode.pDevice:= ADR(EthernetIF);
				GetEthConfigMode.enable:= TRUE;
				IF (GetEthConfigMode.status = ERR_OK) THEN
					GetEthConfigMode.enable:= FALSE;
					HMI.Network.EthernetConfigMode:= GetEthConfigMode.ConfigMode;
					State:= ETHERNET_SETTINGS_LOAD_IP;
				ELSIF ( (GetEthConfigMode.status <> ERR_FUB_BUSY) AND (GetEthConfigMode.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					GetEthConfigMode.enable:= FALSE;
					HMI.Network.EthernetSettingsStatus:= 6; // function not available  //XXX - USE CONSTANTS!
					State:= ETHERNET_SETTINGS_LOAD_IP;
				END_IF

			ETHERNET_SETTINGS_LOAD_IP:
				GetIPAddr.enable:= TRUE;
				GetIPAddr.pDevice:= ADR(EthernetIF);
				GetIPAddr.pIPAddr:= ADR(IPAddrString);
				GetIPAddr.Len:= SIZEOF(IPAddrString);
				IF (GetIPAddr.status = ERR_OK) THEN
					GetIPAddr.enable:= FALSE;
					IF (strcmp( ADR(IPAddrString), ADR('')) <> 0) THEN
						String2IP.IPAddress:= ADR(IPAddrString);
						String2IP();
						HMI.Network.EthernetIPAddress[0]:= String2IP.Octet1;
						HMI.Network.EthernetIPAddress[1]:= String2IP.Octet2;
						HMI.Network.EthernetIPAddress[2]:= String2IP.Octet3;
						HMI.Network.EthernetIPAddress[3]:= String2IP.Octet4;
					ELSE
						HMI.Network.EthernetIPAddress[0]:= FALSE;
						HMI.Network.EthernetIPAddress[1]:= FALSE;
						HMI.Network.EthernetIPAddress[2]:= FALSE;
						HMI.Network.EthernetIPAddress[3]:= FALSE;
					END_IF
					State:= ETHERNET_SETTINGS_LOAD_SNM;
				ELSIF ( (GetIPAddr.status <> ERR_FUB_BUSY) AND (GetIPAddr.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					GetIPAddr.enable:= FALSE;
					HMI.Network.EthernetSettingsStatus:= 6; // function not available //XXX - USE CONSTANTS!
					State:= ETHERNET_SETTINGS_LOAD_SNM;
					
				END_IF

			ETHERNET_SETTINGS_LOAD_SNM:
				GetSubnetMask.enable:= TRUE;
				GetSubnetMask.pDevice:= ADR(EthernetIF);
				GetSubnetMask.pSubnetMask:= ADR(SubnetString);
				GetSubnetMask.Len:= SIZEOF(SubnetString);
				IF (GetSubnetMask.status = ERR_OK) THEN
					GetSubnetMask.enable:= FALSE;
					IF (strcmp( ADR(SubnetString), ADR('')) <> 0) THEN
						String2IP.IPAddress:= ADR(SubnetString);
						String2IP ();
						HMI.Network.EthernetSubnetMask[0]:= String2IP.Octet1;
						HMI.Network.EthernetSubnetMask[1]:= String2IP.Octet2;
						HMI.Network.EthernetSubnetMask[2]:= String2IP.Octet3;
						HMI.Network.EthernetSubnetMask[3]:= String2IP.Octet4;
					ELSE
						HMI.Network.EthernetSubnetMask[0]:= FALSE;
						HMI.Network.EthernetSubnetMask[1]:= FALSE;
						HMI.Network.EthernetSubnetMask[2]:= FALSE;
						HMI.Network.EthernetSubnetMask[3]:= FALSE;
					END_IF
					State:= ETHERNET_SETTINGS_LOAD_GW;
				ELSIF ( (GetSubnetMask.status <> ERR_FUB_BUSY) AND (GetSubnetMask.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					GetSubnetMask.enable:= FALSE;
					HMI.Network.EthernetSettingsStatus:= 6; // function not available//XXX - USE CONSTANTS!
					State:= ETHERNET_SETTINGS_LOAD_GW;
				END_IF

			ETHERNET_SETTINGS_LOAD_GW:
				GetDefaultGateway.enable:= TRUE;
				GetDefaultGateway.pDevice:= ADR(EthernetIF);
				GetDefaultGateway.pGateway:= ADR(GatewayString);
				GetDefaultGateway.Len:= SIZEOF(GatewayString);
				IF (GetDefaultGateway.status = ERR_OK) THEN
					GetDefaultGateway.enable:= FALSE;
					IF (strcmp( ADR(GatewayString), ADR('')) <> 0) THEN
						String2IP.IPAddress:= ADR(GatewayString);
						String2IP();
						HMI.Network.EthernetGatewayAddress[0]:= String2IP.Octet1;
						HMI.Network.EthernetGatewayAddress[1]:= String2IP.Octet2;
						HMI.Network.EthernetGatewayAddress[2]:= String2IP.Octet3;
						HMI.Network.EthernetGatewayAddress[3]:= String2IP.Octet4;
					ELSE
						HMI.Network.EthernetGatewayAddress[0]:= FALSE;
						HMI.Network.EthernetGatewayAddress[1]:= FALSE;
						HMI.Network.EthernetGatewayAddress[2]:= FALSE;
						HMI.Network.EthernetGatewayAddress[3]:= FALSE;
					END_IF
					State:= ETHERNET_SETTINGS_LOAD_INA;
				ELSIF ( (GetDefaultGateway.status <> ERR_FUB_BUSY) AND (GetDefaultGateway.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					GetDefaultGateway.enable:= FALSE;
					HMI.Network.EthernetSettingsStatus:= 6; // function not available//XXX - USE CONSTANTS!
					State:= ETHERNET_SETTINGS_LOAD_INA;
				END_IF


			ETHERNET_SETTINGS_LOAD_INA:
				GetInaNode.pDevice:= ADR(EthernetIF);
				GetInaNode.enable:= TRUE;
				IF (GetInaNode.status = ERR_OK) THEN
					GetInaNode.enable:= FALSE;
					HMI.Network.EthernetINANode:= GetInaNode.InaNode;
					State:= IDLE;
				ELSIF ( (GetInaNode.status <> ERR_FUB_BUSY) AND (GetInaNode.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					GetInaNode.enable:= FALSE;
					HMI.Network.EthernetSettingsStatus:= 6; // function not available//XXX - USE CONSTANTS!
					State:= IDLE;
				END_IF

			ETHERNET_SETTINGS_SAVE_IP:
				SetIPAddr.enable:= TRUE;
				SetIPAddr.pDevice:= ADR(EthernetIF);
				SetIPAddr.pIPAddr:= ADR(IPAddrString);
				SetIPAddr.Option:= cfgOPTION_NON_VOLATILE;
				IF (SetIPAddr.status = ERR_OK) THEN
					SetIPAddr.enable:= FALSE;
					IP2String(pIPString:= ADR(SubnetString) , Octet1:=  HMI.Network.EthernetSubnetMask[0], Octet2:=  HMI.Network.EthernetSubnetMask[1],
					Octet3:= HMI.Network.EthernetSubnetMask[2] , Octet4:=  HMI.Network.EthernetSubnetMask[3]);
					State:= ETHERNET_SETTINGS_SAVE_SNM;
				ELSIF ( (SetIPAddr.status <> ERR_FUB_BUSY) AND (SetIPAddr.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					SetIPAddr.enable:= FALSE;
					State:= ETHERNET_SETTINGS_SAVE_SNM;
				END_IF

			ETHERNET_SETTINGS_SAVE_SNM:
				SetSubnetMask.enable:= TRUE;
				SetSubnetMask.pDevice:= ADR(EthernetIF);
				SetSubnetMask.pSubnetMask:= ADR(SubnetString);
				SetSubnetMask.Option:= cfgOPTION_NON_VOLATILE;
				IF (SetSubnetMask.status = ERR_OK) THEN
					SetSubnetMask.enable:= FALSE;
					IP2String(pIPString:= ADR(GatewayString) , Octet1:=  HMI.Network.EthernetGatewayAddress[0], Octet2:=  HMI.Network.EthernetGatewayAddress[1],
					Octet3:= HMI.Network.EthernetGatewayAddress[2] , Octet4:=  HMI.Network.EthernetGatewayAddress[3]);
					State:= ETHERNET_SETTINGS_SAVE_GW;
				ELSIF ( (SetSubnetMask.status <> ERR_FUB_BUSY) AND (SetSubnetMask.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					SetSubnetMask.enable:= FALSE;
					State:= ETHERNET_SETTINGS_SAVE_GW;
				END_IF

			ETHERNET_SETTINGS_SAVE_GW:
				SetDefaultGateway.enable:= TRUE;
				SetDefaultGateway.pDevice:= ADR(EthernetIF);
				SetDefaultGateway.pGateway:= ADR(GatewayString);
				SetDefaultGateway.Option:= cfgOPTION_NON_VOLATILE;
				IF (SetDefaultGateway.status = ERR_OK) THEN
					SetDefaultGateway.enable:= FALSE;
					State:= ETHERNET_SETTINGS_SAVE_INA;
				ELSIF ( (SetDefaultGateway.status <> ERR_FUB_BUSY) AND (SetDefaultGateway.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					SetDefaultGateway.enable:= FALSE;
					State:= ETHERNET_SETTINGS_SAVE_INA;
					
				END_IF

			ETHERNET_SETTINGS_SAVE_INA:
				SetInaNode.enable:= TRUE;
				SetInaNode.pDevice:= ADR(EthernetIF);
				SetInaNode.Option:= cfgOPTION_NON_VOLATILE;
				SetInaNode.InaNode:= HMI.Network.EthernetINANode;
				IF (SetInaNode.status = ERR_OK) THEN
					SetInaNode.enable:= FALSE;
					State:= ETHERNET_SETTINGS_SAVE_MODE;
				ELSIF ( (SetInaNode.status <> ERR_FUB_BUSY) AND (SetInaNode.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					SetInaNode.enable:= FALSE;
					State:= IDLE;
				END_IF
			
			ETHERNET_SETTINGS_SAVE_MODE:
				SetEthConfigMode.enable:= TRUE;
				SetEthConfigMode.pDevice:= ADR(EthernetIF);
				SetEthConfigMode.Option:= cfgOPTION_NON_VOLATILE;
				IF (HMI.Network.EthernetConfigMode = 0) THEN
					SetEthConfigMode.ConfigMode:= cfgCONFIGMODE_MANUALLY;
				ELSE
					SetEthConfigMode.ConfigMode:= cfgCONFIGMODE_DHCPCLIENT;
				END_IF
				IF (SetEthConfigMode.status = ERR_OK) THEN
					SetEthConfigMode.enable:= TRUE;
					HMI.Network.EthernetSettingsStatus:= TRUE;
					State:= IDLE;
				ELSIF ( (SetEthConfigMode.status <> ERR_FUB_BUSY) AND (SetEthConfigMode.status <> ERR_FUB_ENABLE_FALSE) ) THEN
					SetEthConfigMode.enable:= FALSE;
					State:= IDLE;
				END_IF
				
		END_CASE // end of main state machine
		
		//login
		IF (HMI.Login.LoginPromptPB) THEN
			HMI.Login.LoginPromptPB:= FALSE;
			HMI.Login.LoginStatus:= FALSE;
			SHOW(ADR(HMI.Login.LoginPromptSDP));
					
		ELSIF (HMI.Login.LoginCancelPB) THEN
			HMI.Login.LoginCancelPB:= FALSE;
			HIDE(ADR(HMI.Login.LoginPromptSDP));
					
		ELSIF (HMI.Login.LoginPB) THEN
			HMI.Login.LoginPB:= FALSE;
			HMI.Login.LoginStatus:= FALSE;
			IF (strcmp(ADR(HMI.Login.LoginPassword), ADR(SystemSettings.Display.Reserved[HMI.Login.LoginUser])) = 0) THEN
				HIDE(ADR(HMI.Login.LoginPromptSDP));
				HMI.Login.LoginLevel:= HMI.Login.LoginUser + 1;// +1 because LoginLevel 0 (operator) is default level, no PW needed
			ELSE
				HMI.Login.LoginStatus:= TRUE;
			END_IF
			strcpy(ADR(HMI.Login.LoginPassword), ADR(''));
		
		ELSIF (HMI.Login.LogoutPB) THEN
			HMI.Login.LogoutPB:= FALSE;
			HMI.Login.LoginLevel:= LOGINLEVEL_OPERATOR;
			HMI.Login.LoginStatus:= FALSE;
			HIDE(ADR(HMI.Login.LoginPromptSDP));
		END_IF
		
		// TAB COLOR / VISIBILITY
		TabControl;
		
		//Rivet Editor
		IF (HMI.Page.Current = PAGE_SETUP) THEN
			UpdateRivetList;
			RivetConfig ACCESS ADR(SystemSettings.Rivet[RivetConfigIndex]);
		END_IF
		
		//Feeder Control on main page
		IF (HMI.Page.Current = PAGE_MAIN) THEN
			FOR i:= 0 TO MAX_FEEDER DO
				IF (strcmp(ADR(Feeder[i].RivetType),ADR(''))=0)THEN
					strcpy(ADR(FeederRivetTypeDisp[i]),ADR('{EMPTY}'));
					SHOW(ADR(HMI.Main.FeederLoadSDP[i]));
					HIDE(ADR(HMI.Main.FeederPurgeSDP[i]));
				ELSE
					strcpy(ADR(FeederRivetTypeDisp[i]),ADR(Feeder[i].RivetType));
					HIDE(ADR(HMI.Main.FeederLoadSDP[i]));
					SHOW(ADR(HMI.Main.FeederPurgeSDP[i]));
				END_IF
				IF HMI.Main.FeederPurgePB[i] THEN
					HMI.Main.FeederPurgePB[i]:= FALSE;
					SHOW(ADR(HMI.Main.PurgeLayerSDP));
					SelectedFeeder:= INT_TO_USINT(i);
				ELSIF HMI.Main.FeederLoadPB[i] THEN
					UpdateRivetList;
					HMI.Main.FeederLoadPB[i]:= FALSE;
					SHOW(ADR(HMI.Main.LoadLayerSDP));
					SelectedFeeder:= INT_TO_USINT(i);
				END_IF
			END_FOR
			IF HMI.Main.FeederLoadConfirmPB THEN
				HMI.Main.FeederLoadConfirmPB:= FALSE;
				strcpy(ADR(Feeder[SelectedFeeder].RivetType),ADR(SystemSettings.Rivet[RivetConfigIndex].Name));
				//XXX - Run some sequence
				HIDE(ADR(HMI.Main.LoadLayerSDP));
			END_IF
			IF HMI.Main.FeederPurgeConfirmPB THEN
				HMI.Main.FeederPurgeConfirmPB:= FALSE;
				HIDE(ADR(HMI.Main.PurgeLayerSDP));
				Machine.Cmd.Purge[SelectedFeeder]:= TRUE;
			END_IF
			IF Machine.Stat.PurgeComplete[SelectedFeeder] THEN
				Machine.Stat.PurgeComplete[SelectedFeeder]:= FALSE;
				Machine.Cmd.Purge[SelectedFeeder]:= FALSE;
				strcpy(ADR(Feeder[SelectedFeeder].RivetType),ADR(''));
			END_IF
			IF HMI.Main.FeederClearConfirmPB THEN // just used to clear the field in case the operator just selects the wrong rivet type for feeder 1 or 2
				HMI.Main.FeederClearConfirmPB:= FALSE;
				HIDE(ADR(HMI.Main.PurgeLayerSDP));
				strcpy(ADR(Feeder[SelectedFeeder].RivetType),ADR(''));
			END_IF
			IF HMI.Main.LoadPurgeCancelPB THEN
				HMI.Main.LoadPurgeCancelPB:= FALSE;
				HIDE(ADR(HMI.Main.PurgeLayerSDP));
				HIDE(ADR(HMI.Main.LoadLayerSDP));
			END_IF
		END_IF
		
		IF Machine.Stat.Purging AND (SelectedFeeder = 0) THEN
			LeftPurging:= TRUE;
			RightPurging:= FALSE;
		ELSIF Machine.Stat.Purging AND (SelectedFeeder = 1) THEN
			LeftPurging:= FALSE;
			RightPurging:= TRUE;
		ELSIF NOT(Machine.Stat.Purging) THEN
			LeftPurging:= FALSE;
			RightPurging:= FALSE;
		END_IF;
		
		// AXIS SETUP SCREENS
		// limit axis selection box indices since the VC up/down datapoint doesn't have a maximum
		axisconfigindex:= LIMIT(0, axisconfigindex, MAX_AXIS_INDEX);
		
		// these variables are used so we can map different unit groups to each
		// and show the appropriate layer for the selected axis
		AxisConfig_notype ACCESS ADR(SystemSettings.AxisConfig[axisconfigindex]);
		AxisConfig_linear ACCESS ADR(SystemSettings.AxisConfig[axisconfigindex]);
		AxisConfig_revs ACCESS ADR(SystemSettings.AxisConfig[axisconfigindex]);
		AxisConfig_degrees ACCESS ADR(SystemSettings.AxisConfig[axisconfigindex]);
		
		AxisDiag_notype ACCESS ADR(Axis[axisconfigindex]);
		AxisDiag_linear ACCESS ADR(Axis[axisconfigindex]);
		AxisDiag_revs ACCESS ADR(Axis[axisconfigindex]);
		AxisDiag_degrees ACCESS ADR(Axis[axisconfigindex]);
		
		// axis setup - basic & limits screen layers
		CASE axisconfigindex OF
			AX_X:
				unitgroup:= UNITGROUP_LINEAR;
			AX_Y:
				unitgroup:= UNITGROUP_LINEAR;
		END_CASE
		
		HIDE(ADR(HMI.Axis.AxisSetupBasicLinearSDP));
		HIDE(ADR(HMI.Axis.AxisSetupBasicRevsSDP));
		HIDE(ADR(HMI.Axis.AxisSetupBasicDegreesSDP));
		HIDE(ADR(HMI.Axis.AxisSetupLimitsVelAccLinearSDP));		
		HIDE(ADR(HMI.Axis.AxisSetupLimitsVelAccRevsSDP));
		HIDE(ADR(HMI.Axis.AxisSetupLimitsVelAccDegSDP));
		HIDE(ADR(HMI.Axis.AxisSetupLimitsLagErrorLinearSDP));
		HIDE(ADR(HMI.Axis.AxisSetupLimitsLagErrorRevsSDP));
		HIDE(ADR(HMI.Axis.AxisSetupLimitsLagErrorDegSDP));
		HIDE(ADR(HMI.Axis.AxisSetupLimitsSWLinearSDP));	
		HIDE(ADR(HMI.Axis.AxisSetupLimitsSWRevsSDP));
		HIDE(ADR(HMI.Axis.AxisSetupLimitsSWDegSDP));
		HIDE(ADR(HMI.Axis.AxisDiagLinearSDP));
		HIDE(ADR(HMI.Axis.AxisDiagRevsSDP));
		HIDE(ADR(HMI.Axis.AxisDiagDegreesSDP));
		
		CASE unitgroup OF
			UNITGROUP_LINEAR:
				SHOW(ADR(HMI.Axis.AxisDiagLinearSDP));
				SHOW(ADR(HMI.Axis.AxisSetupBasicLinearSDP));
				SHOW(ADR(HMI.Axis.AxisSetupLimitsVelAccLinearSDP));
				CASE AxisConfig_notype.Motor.Type OF
					MOTOR_UNDEFINED,
					MOTOR_NOTREQUIRED,
					MOTOR_AC:

					ELSE // all servos need to have lag error window
						SHOW(ADR(HMI.Axis.AxisSetupLimitsLagErrorLinearSDP));
						
				END_CASE
				
				// XXX1 to be dealt with
				//				IF (Axis[axisconfigindex].data.UnitBasis = UNITBASIS_FINITE) THEN
				//					SHOW(ADR(HMI.AxisSetupLimitsSWLinearSDP));
				//				END_IF
			
			
			UNITGROUP_REVS:
				SHOW(ADR(HMI.Axis.AxisDiagRevsSDP));
				SHOW(ADR(HMI.Axis.AxisSetupBasicRevsSDP));
				SHOW(ADR(HMI.Axis.AxisSetupLimitsVelAccRevsSDP));
				CASE AxisConfig_notype.Motor.Type OF
					MOTOR_UNDEFINED,
					MOTOR_NOTREQUIRED,
					MOTOR_AC:
						
					ELSE // all servos need to have lag error window
						SHOW(ADR(HMI.Axis.AxisSetupLimitsLagErrorRevsSDP));
						
				END_CASE
				
				// XXX1 to be dealt with
				//				IF (Axis[axisconfigindex].data.UnitBasis = UNITBASIS_FINITE) THEN
				//					SHOW(ADR(HMI.AxisSetupLimitsSWRevsSDP));
				//				END_IF	
			
			
			UNITGROUP_DEGREES:
				SHOW(ADR(HMI.Axis.AxisDiagDegreesSDP));
				SHOW(ADR(HMI.Axis.AxisSetupBasicDegreesSDP));
				SHOW(ADR(HMI.Axis.AxisSetupLimitsVelAccDegSDP));
				
				CASE AxisConfig_notype.Motor.Type OF
					MOTOR_UNDEFINED,
					MOTOR_NOTREQUIRED,
					MOTOR_AC:
						
					ELSE // all servos need to have lag error window
						SHOW(ADR(HMI.Axis.AxisSetupLimitsLagErrorDegSDP));
						
				END_CASE
				
			// XXX1
			//				IF (Axis[axisconfigindex].data.UnitBasis = UNITBASIS_FINITE) THEN
			//					SHOW(ADR(HMI.AxisSetupLimitsSWDegSDP));
			//				END_IF
			
		END_CASE
			
		// axis setup - motor screen layers
		// XXX the logic here needs to consider more than just the motor type (e.g. SDC axis)
		CASE AxisConfig_notype.Motor.Type OF
			MOTOR_UNDEFINED,
			MOTOR_NOTREQUIRED:
				HIDE(ADR(HMI.Axis.AxisSetupMotorACSDP));
				HIDE(ADR(HMI.Axis.AxisSetupMotorServoSDP));
				HIDE(ADR(HMI.Axis.AxisSetupMotorRealSDP));
				HIDE(ADR(HMI.Axis.AxisSetupMotorCommOffsetSDP));
				
			MOTOR_AC:
				SHOW(ADR(HMI.Axis.AxisSetupMotorACSDP));
				HIDE(ADR(HMI.Axis.AxisSetupMotorServoSDP));
				SHOW(ADR(HMI.Axis.AxisSetupMotorRealSDP));
				HIDE(ADR(HMI.Axis.AxisSetupMotorCommOffsetSDP));
				
			ELSE // should be a servo
				HIDE(ADR(HMI.Axis.AxisSetupMotorACSDP));
				SHOW(ADR(HMI.Axis.AxisSetupMotorServoSDP));
				SHOW(ADR(HMI.Axis.AxisSetupMotorRealSDP));
				IF (AxisConfig_notype.Motor.CommutationOverride > 0) THEN
					SHOW(ADR(HMI.Axis.AxisSetupMotorCommOffsetSDP));
				ELSE
					HIDE(ADR(HMI.Axis.AxisSetupMotorCommOffsetSDP));
				END_IF
		END_CASE
		
		// axis setup - tuning screen layers
		CASE AxisConfig_notype.Motor.Type OF
			MOTOR_UNDEFINED,
			MOTOR_NOTREQUIRED,
			MOTOR_AC:	
				HIDE(ADR(HMI.Axis.AxisSetupTuningSDP));
				HIDE(ADR(HMI.Axis.AxisSetupTuningNotchSDP));
				
			ELSE // should be a servo
				SHOW(ADR(HMI.Axis.AxisSetupTuningSDP));
				// XXX1 need to address
				//				IF (AxisConfig_notype.Tuning.NOTCH_MODE > 0) THEN
				//					SHOW(ADR(HMI.AxisSetupTuningNotchSDP));
				//				ELSE
				//					HIDE(ADR(HMI.AxisSetupTuningNotchSDP));
				//				END_IF
		END_CASE
		
		// auto tuning
		Machine.Cmd.Manual[axisconfigindex].AutoTune:= HMI.Axis.AxisSetupAutoTuningPB;
		HMI.Axis.AxisSetupAutoTuningPB:= FALSE;
		
		//Axis diag page - XXX need to update for MappAxis compatibility
		//HIDE(ADR(HMI.AxisDiagCurrentsSDP));
		//HIDE(ADR(HMI.AxisDiagDriveIOSDP));
		//	CASE AxisDiag_notype.data.nc_obj_typ OF
		//			ncAXIS:
		//		SHOW(ADR(HMI.AxisDiagDriveIOSDP));
		//		CASE AxisDiag_notype.data.acp_typ OF
		//			ncACP_TYP_INV,
		//			ncACP_TYP_PS,
		//			ncACP_TYP_PPS,
		//			ncACP_TYP_SIM:
		//				SHOW(ADR(HMI.AxisDiagCurrentsSDP));
		//		END_CASE
		//	
		//	ncV_AXIS:
		//		// don't show currents or drive IO if virtual axis
		//END_CASE	
			
		// only auto-logout admin or above
		TON_Logout.IN:= ( (HMI.Login.LoginLevel >= LOGINLEVEL_ADMIN) AND (Touch.status = 2) );
		IF (TON_Logout.Q) THEN
			HMI.Login.LoginLevel:= LOGINLEVEL_OPERATOR;
		END_IF
		
		//Percent Complete Display
		IF ((Machine.Stat.Mode = MODE_AUTO) AND Machine.Stat.Rivetting AND NOT(AlarmMgr.ErrMessage)) THEN
			SHOW(ADR(HMI.Common.PercentCompleteSDP));
			IF (RivetList.NumRivets>0) AND RivetList.Valid THEN
				HMI.Common.PercentComplete:= REAL_TO_UINT((UINT_TO_REAL(CurrentRivet+1) / UINT_TO_REAL(RivetList.NumRivets))*65535.0);
			ELSE
				HMI.Common.PercentComplete:= 0;	
			END_IF
		ELSE
			HIDE(ADR(HMI.Common.PercentCompleteSDP));
			HMI.Common.PercentComplete:= 0;
		END_IF
		
		IF AlarmMgr.ErrMessage OR AlarmMgr.ErrCritical OR AlarmMgr.ErrFatal THEN
			SHOW(ADR(HMI.Common.NotificationSDP));
		ELSE;
			HIDE(ADR(HMI.Common.NotificationSDP));
		END_IF
		
		//PushButton Lights
		o_PBLT_PowerOn_3504;
		o_PBLT_Reset_3503;
		o_PBLT_Start_3502;
		
		
		IF oldPage <> HMI.Page.Current THEN
			EnteringPage:= TRUE;
		ELSE
			EnteringPage:= FALSE;
		END_IF;
		oldPage:= HMI.Page.Current;
		
		// Valve Motion Screen
		ValveMotionScreenAction;
		
		// Safety Screen
		SafetyScreenAction;
	
		
		// blinking variable
		IF (TON_BlinkON.Q) THEN
			TON_BlinkON.IN:= FALSE;
			TON_BlinkOFF.IN:= TRUE;
			Blink:= FALSE;
		ELSIF (TON_BlinkOFF.Q) THEN
			TON_BlinkOFF.IN:= FALSE;
			TON_BlinkON.IN:= TRUE;
			Blink:= TRUE;
		END_IF
		IF (TON_BlinkFastON.Q) THEN
			TON_BlinkFastON.IN:= FALSE;
			TON_BlinkFastOFF.IN:= TRUE;
			BlinkFast:= FALSE;
		ELSIF (TON_BlinkFastOFF.Q) THEN
			TON_BlinkFastOFF.IN:= FALSE;
			TON_BlinkFastON.IN:= TRUE;
			BlinkFast:= TRUE;
		END_IF
		
		TON_BlinkON();
		TON_BlinkOFF();
		TON_BlinkFastON();
		TON_BlinkFastOFF();
		TON_Init();
		TON_Logout();
		
		GetEthConfigMode();
		GetIPAddr();
		GetSubnetMask();
		GetDefaultGateway();
		GetInaNode();
		SetEthConfigMode();
		SetIPAddr();
		SetSubnetMask();
		SetDefaultGateway();
		SetInaNode();
		UsbNodeListGet_0();
		UsbNodeGet_0();
		UsbDescriptorGet_0();
		DevLink_0();
		DevUnlink_0();
		
		// show/hide/lock/unlock various screens based on mode;
		IF (Machine.Stat.Mode = MODE_MANUAL) THEN
			HIDE(ADR(HMI.Common.StopSDP));
			HIDE(ADR(HMI.Common.PauseSDP));
			IF (NOT AlarmMgr.ErrCritical) THEN
				SHOW(ADR(HMI.Common.StartSDP));
			ELSE
				HIDE(ADR(HMI.Common.StartSDP));
			END_IF
		ELSIF (Machine.Stat.Mode = MODE_AUTO) THEN
			HIDE(ADR(HMI.Common.StartSDP));
			SHOW(ADR(HMI.Common.StopSDP));
			IF Machine.Stat.Rivetting THEN //show pause/continue button if auto sequence, but not in purging sequence
				SHOW(ADR(HMI.Common.PauseSDP));
			ELSE
				HIDE(ADR(HMI.Common.PauseSDP));
			END_IF;
		ELSE
			HIDE(ADR(HMI.Common.StartSDP));
			HIDE(ADR(HMI.Common.StopSDP));
			HIDE(ADR(HMI.Common.PauseSDP));
		END_IF
		
		IF (AlarmMgr.ErrCritical OR AlarmMgr.ErrFatal OR NOT AlarmMgr.EStopOK OR DevLinkFailed OR DevLinkTimeout) THEN //since I want not devlinked to be treated as a message but don't want all messages to show reset button
			SHOW(ADR(HMI.Common.ResetSDP));
		ELSE
			HIDE(ADR(HMI.Common.ResetSDP));
		END_IF
		
		//Alarm Banner - Allow easy return to previous page by pressing banner again
		IF HMI.Common.AlarmBannerPB THEN
			HMI.Common.AlarmBannerPB:= FALSE;
			IF ((HMI.Page.Current = PAGE_SERVICE1) OR (HMI.Page.Current = PAGE_SERVICE2)) AND (AlarmReturnPage>0) THEN
				HMI.Page.Change:= AlarmReturnPage;
			ELSE
				AlarmReturnPage:= HMI.Page.Current;
				HMI.Page.Change:= PAGE_SERVICE1;
			END_IF
		END_IF
	
	
		//DateTime String
		GetTime_0.enable:= TRUE;
		GetTime_0();
		TimeDate:= GetTime_0.DT1;
		ascDT(TimeDate,ADR(HMI.Common.DateTimeStr),SIZEOF(HMI.Common.DateTimeStr));
		
	
	END_IF
	
	
	
	
	
END_PROGRAM

PROGRAM _INIT
// initialize timers for keeping track of blinking on hmi lights
	TON_BlinkON.IN:= FALSE;
	TON_BlinkON.PT:= T#1s;
	TON_BlinkOFF.IN:= TRUE;
	TON_BlinkOFF.PT:= T#1s;
	TON_BlinkFastON.IN:= FALSE;
	TON_BlinkFastON.PT:= T#200ms;
	TON_BlinkFastOFF.IN:= TRUE;
	TON_BlinkFastOFF.PT:= T#200ms;
	
	// auto logout timer
	TON_Logout.PT:= T#10m;
	
	HIDE(ADR(HMI.Login.LoginPromptSDP));
	HIDE(ADR(HMI.SDP.LoadConfirmPopup));
	HIDE(ADR(HMI.SDP.ChoiceToResume));
	
	HMI.Network.EthernetINANode:= 5;//Hard coded here since it's not accessible on the HMI and needs to be non-zero.
	
	TON_Init.PT:= T#15s;
	TON_Init.IN:= TRUE;
	HMI.InitStatus:= FALSE;
	
	HMIColors.PageBackColor:= 248;
	HMIColors.ActiveTabColor:= 252; // this is the color the background of the tab button should take when in the foreground
	HMIColors.InactiveTabColor:= 250;  // this is the color the background of the tab button should take when not in the foreground
	
	HIDE(ADR(HMI.Main.LoadLayerSDP));
	HIDE(ADR(HMI.Main.PurgeLayerSDP));
	
	IF (strcmp( ADR(EthernetIF), ADR('')) = 0) THEN
		ERRxfatal(0,0,ADR('EthernetIF Device Not Defined'));
	END_IF
	
	IF (strcmp (ADR(VisObjName), ADR('')) = 0) THEN
		ERRxfatal(0,0,ADR('VisObjName Not Defined'));
	END_IF
	
END_PROGRAM