
TYPE
	HMI_typ : 	STRUCT 
		Login : HMI_Login_typ;
		Axis : HMI_Axis_typ;
		Network : HMI_Network_typ;
		VarWatch : HMI_VarWatch_typ;
		OEM : HMI_OEM_typ;
		Common : HMI_Common_typ;
		Tabs : HMI_Tabs_typ;
		Units : HMI_Units_typ;
		Main : HMI_Main_typ;
		IO : HMI_IO_typ;
		PB : HMI_PB_typ;
		SDP : HMI_SDP_typ;
		InitStatus : UINT;
		TouchStatus : USINT;
		Page : HMI_Runtime_typ;
		Language : HMI_Runtime_typ;
		KeyLevel : HMI_Runtime_typ;
	END_STRUCT;
	HMI_Main_typ : 	STRUCT 
		ClearLastRunPB : BOOL;
		ContinueLastRunPB : BOOL;
		SkipLastRivetPB : BOOL;
		RivetToContinueOn : UINT;
		LoadConfirmedPB : BOOL;
		LoadCancelledPB : BOOL;
		FeederClearConfirmPB : BOOL;
		FeederPurgeConfirmPB : BOOL;
		FeederLoadConfirmPB : BOOL;
		LoadPurgeCancelPB : BOOL;
		FeederLoadPB : ARRAY[0..MAX_FEEDER]OF BOOL;
		FeederPurgePB : ARRAY[0..MAX_FEEDER]OF BOOL;
		LoadLayerSDP : UINT;
		PurgeLayerSDP : UINT;
		FeederPurgeSDP : ARRAY[0..MAX_FEEDER]OF UINT;
		FeederLoadSDP : ARRAY[0..MAX_FEEDER]OF UINT;
	END_STRUCT;
	HMI_Tabs_typ : 	STRUCT 
		MainTabColorCDP : ARRAY[0..3]OF UINT;
		MainTabLineSDP : ARRAY[0..3]OF UINT;
		ServiceTabColorCDP : ARRAY[0..7]OF UINT;
		ServiceTabLineSDP : ARRAY[0..7]OF UINT;
		SysTabColorCDP : ARRAY[0..8]OF UINT;
		SysTabLineSDP : ARRAY[0..8]OF UINT;
	END_STRUCT;
	HMI_OEM_typ : 	STRUCT 
		OEMImportPB : BOOL;
		OEMExportPB : BOOL;
		OEMRestoreDefaultsPB : BOOL;
		OEMExportDiagnosticsPB : BOOL;
		OEMSysSettingsStatus : UINT;
	END_STRUCT;
	HMI_Common_typ : 	STRUCT 
		StartPB : BOOL;
		ResetPB : BOOL;
		PercentComplete : UINT;
		PercentCompleteSDP : UINT;
		StopPB : BOOL;
		StartSDP : UINT;
		StopSDP : UINT;
		PauseSDP : UINT;
		ResetSDP : UINT;
		AlarmBannerPB : BOOL;
		NotificationSDP : UINT;
		DateTimeStr : STRING[80];
	END_STRUCT;
	HMI_VarWatch_typ : 	STRUCT 
		VarMonitorPB : BOOL;
		VarReqPB : BOOL;
		VarClearPB : BOOL;
	END_STRUCT;
	HMI_Units_typ : 	STRUCT 
		TemperatureUnits : HMI_Runtime_typ;
		LinearAccelerationUnits : HMI_Runtime_typ;
		LinearVelocityUnits : HMI_Runtime_typ;
		LinearDisplacementUnits : HMI_Runtime_typ;
	END_STRUCT;
	HMI_Axis_typ : 	STRUCT 
		AxisSetupBasicLinearSDP : UINT;
		AxisSetupBasicRevsSDP : UINT;
		AxisSetupBasicDegreesSDP : UINT;
		AxisSetupMotorACSDP : UINT;
		AxisSetupMotorRealSDP : UINT;
		AxisSetupMotorServoSDP : UINT;
		AxisSetupMotorCommOffsetSDP : UINT;
		AxisSetupLimitsVelAccLinearSDP : UINT;
		AxisSetupLimitsVelAccRevsSDP : UINT;
		AxisSetupLimitsVelAccDegSDP : UINT;
		AxisSetupLimitsLagErrorLinearSDP : UINT;
		AxisSetupLimitsLagErrorRevsSDP : UINT;
		AxisSetupLimitsLagErrorDegSDP : UINT;
		AxisSetupLimitsSWLinearSDP : UINT;
		AxisSetupLimitsSWRevsSDP : UINT;
		AxisSetupLimitsSWDegSDP : UINT;
		AxisSetupTuningSDP : UINT;
		AxisSetupTuningNotchSDP : UINT;
		AxisSetupAutoTuningPB : BOOL;
		AxisDiagLinearSDP : UINT;
		AxisDiagDegreesSDP : UINT;
		AxisDiagRevsSDP : UINT;
		AxisDiagDriveIOSDP : UINT;
		AxisDiagCurrentsSDP : UINT;
	END_STRUCT;
	HMI_Network_typ : 	STRUCT 
		EthernetSettingsLoad : BOOL;
		EthernetSettingsApply : BOOL;
		EthernetConfigMode : UDINT; (*0=cfgCONFIGMODE_MANUALLY	Manual configuration, 1=cfgCONFIGMODE_DHCPCLIENT	DHCP client*)
		EthernetIPAddress : ARRAY[0..3]OF USINT;
		EthernetSubnetMask : ARRAY[0..3]OF USINT;
		EthernetGatewayAddress : ARRAY[0..3]OF USINT;
		EthernetINANode : USINT;
		EthernetSettingsStatus : USINT;
	END_STRUCT;
	HMI_Runtime_typ : 	STRUCT 
		Change : UINT;
		Current : UINT;
	END_STRUCT;
	HMI_IO_typ : 	STRUCT 
		IOForce : ARRAY[0..11]OF HMI_IOForce_typ;
		IOEnable_SDP : UINT;
		IOLayer_SDP : ARRAY[0..19]OF UINT;
		IOPointVis : ARRAY[0..11]OF USINT;
		IOTextVis : ARRAY[0..11]OF USINT;
		IOModuleSelectDown : BOOL;
		IOModuleSelectUp : BOOL;
		IOModuleSelectDP : USINT;
		IONodeSwitch16 : USINT;
		IONodeSwitch1 : USINT;
		IOBaseModule_SDP : UINT;
		IOModuleSelected : UINT;
		IOModule : ARRAY[0..199]OF HMI_IOModule_typ;
		ValvePositions : ARRAY[0..19]OF BOOL;
	END_STRUCT;
	HMI_IOModule_typ : 	STRUCT 
		Status : UINT;
		Selected : BOOL;
		ModuleImageIndex : USINT;
		ModuleView_SDP : UINT;
	END_STRUCT;
	HMI_IOForce_typ : 	STRUCT 
		Enable : REFERENCE TO BOOL;
		ForceValue : REFERENCE TO DINT;
		PhysicalValue : REFERENCE TO DINT;
	END_STRUCT;
	HMI_PB_typ : 	STRUCT 
		Valves : ARRAY[0..11]OF BOOL;
		SafetyDoors : ARRAY[0..5]OF BOOL;
	END_STRUCT;
	HMI_SDP_typ : 	STRUCT 
		Valves : ARRAY[0..11]OF UINT;
		JogY : ARRAY[0..5]OF UINT;
		LoadConfirmPopup : UINT;
		ChoiceToResume : UINT;
	END_STRUCT;
	HMI_Login_typ : 	STRUCT 
		LoginPromptPB : BOOL;
		LoginCancelPB : BOOL;
		LoginPromptSDP : UINT;
		LoginPB : BOOL;
		LogoutPB : BOOL;
		LoginUser : USINT; (*see LOGINLEVEL_ constants*)
		LoginLevel : USINT;
		LoginStatus : USINT;
		LoginPassword : STRING[6];
	END_STRUCT;
END_TYPE
