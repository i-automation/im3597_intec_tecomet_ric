
TYPE
	UnitBasis_enum : 
		(
		UNITBASIS_FINITE,
		UNITBASIS_ENDLESS_PERIODIC,
		UNITBASIS_ENDLESS_NONPERIODIC
		);
	Axis_Cmd_typ : 	STRUCT 
		Init : BOOL; (*Initialize the axis with all required settings (taken from SystemSettings.AxisConfig). *)
		On : BOOL; (*Power on the axis. *)
		Home : BOOL; (*Home the axis. *)
		MoveVelocity : BOOL; (*Initiate velocity move.*)
		MoveAbsolute : BOOL; (*Initiate absolute move. *)
		MoveAdditive : BOOL; (*Initiate additive move. *)
		AutoTune : BOOL; (*Autotune the axis. *)
		ErrAck : BOOL; (*Acknowledge all axis errors. *)
		UpdateEncoderScaling : BOOL;
		OverrideSWEndLimits : BOOL; (*Allows forcing of SW limits from external task. *)
		EnableSWEndLimits : BOOL; (*Enabling/disabling SW limits from external task. NOTE: will only be taken into account if 'SWEndLimitsOverride' is set to TRUE.*)
	END_STRUCT;
	Axis_Par_typ : 	STRUCT 
		Basic : MpAxisBasicParType;
	END_STRUCT;
	Axis_Data_typ : 	STRUCT 
		ErrorID : UINT;
		ErrorText : ARRAY[0..3]OF STRING[79];
		Position : LREAL;
		Velocity : REAL;
		TorqueRaw : REAL;
		TorqueCont : REAL;
		TorquePeak : REAL;
		CurrentRaw : REAL;
		CurrentCont : REAL;
		CurrentPeak : REAL;
		LagError : LREAL;
		MotorSensorTemp : LREAL;
		AxisPeriod : UDINT;
		PLCopenAxisPeriod : REAL;
		ActualScalingEncoderCounts : UDINT;
		ActualScalingMotorRevs : UDINT;
		MaxWindingTemp : REAL;
		MotorPeakCurrent : REAL;
		MotorRatedCurrent : REAL;
	END_STRUCT;
	Axis_Status_typ : 	STRUCT 
		Init : BOOL;
		On : BOOL;
		Idle : BOOL;
		Homing : BOOL;
		Homed : BOOL;
		HomingError : BOOL;
		SWEndLimitsEnabled : BOOL;
		ContinuousMotion : BOOL;
		DiscreteMotion : BOOL;
		AutotuneActive : BOOL;
		AutotuneDone : BOOL;
		Error : BOOL;
	END_STRUCT;
	Axis_Info_typ : 	STRUCT  (*Diagnostic structure (not referenced in mm).*)
		Basic : MpAxisBasicInfoType;
		Config : MpAxisInfoType;
	END_STRUCT;
	Axis_typ : 	STRUCT 
		Cmd : Axis_Cmd_typ;
		Par : Axis_Par_typ;
		Status : Axis_Status_typ;
		Data : Axis_Data_typ;
		Info : Axis_Info_typ;
	END_STRUCT;
END_TYPE

(**)
(*Press*)

TYPE
	Press_Cmd_typ : 	STRUCT 
		On : BOOL;
		Home : BOOL; (*Home the axis. *)
		LoadProgram : BOOL;
		JogPos : BOOL;
		JogNeg : BOOL;
		Move : BOOL;
		StartCycle : BOOL;
		Abort : BOOL; (*Stop an auto move (press cycle)*)
		Stop : BOOL; (*Stop a manual move (absolute, relative, jog moves)*)
		ResetComm : BOOL;
		ErrAck : BOOL; (*Acknowledge all axis errors. *)
	END_STRUCT;
	Press_Par_typ : 	STRUCT 
		Mode : USINT; (*=0: Off Mode; =1: Manual Mode; =2: Auto Mode*)
		Program : USINT;
		MoveType : USINT; (*0=jog, 1=relative, 2=absolute*)
		Position : REAL;
		Velocity : REAL;
		NestPenetrateThreshold : REAL;
	END_STRUCT;
	Press_Status_typ : 	STRUCT 
		ReadyToPowerOn : BOOL;
		On : BOOL;
		Homed : BOOL;
		Idle : BOOL;
		InOperation : BOOL;
		MoveSuccessful : BOOL;
		MoveFailed : BOOL;
		BelowThreshold : BOOL;
		LoadedProgram : UINT;
		ProgramLoaded : BOOL;
		CommOK : BOOL;
		Error : BOOL;
	END_STRUCT;
	Press_Data_typ : 	STRUCT 
		Load : REAL;
		Position : REAL;
		Velocity : REAL;
	END_STRUCT;
	Press_typ : 	STRUCT 
		Cmd : Press_Cmd_typ;
		Par : Press_Par_typ;
		Status : Press_Status_typ;
		Data : Press_Data_typ;
	END_STRUCT;
END_TYPE

(**)
(*Press (full type in case we get festo drive working through communication other than hard IO)*)

TYPE
	Press_Cmd_typ1 : 	STRUCT 
		Init : BOOL; (*Initialize the axis with all required settings (taken from SystemSettings.AxisConfig). *)
		On : BOOL;
		Home : BOOL; (*Home the axis. *)
		MoveAbsolute : BOOL; (*Initiate absolute move. *)
		MoveAdditive : BOOL;
		JogPositive : BOOL;
		JogNegative : BOOL;
		Halt : BOOL;
		Stop : BOOL; (*Cancel mvoe and stop with emergency ramp*)
		ReleaseBrake : BOOL; (*Cancel move and halt with braking ramp*)
		ErrAck : BOOL; (*Acknowledge all axis errors. *)
	END_STRUCT;
	Press_Par_typ1 : 	STRUCT 
		ControlMode : USINT; (*=0: position mode; =1: force mode; =2: velocity mode*)
		Position : REAL;
		Velocity : REAL; (*used in position mode*)
		Torque : REAL; (*used in force mode*)
		TorqueRamp : REAL; (*used in force mode*)
	END_STRUCT;
	Press_Data_typ1 : 	STRUCT 
		Position : REAL;
		Velocity : REAL;
		Torque : REAL;
	END_STRUCT;
	Press_Status_typ1 : 	STRUCT 
		Init : BOOL;
		ReadyToPowerOn : USINT;
		On : BOOL;
		Homing : BOOL;
		Homed : BOOL;
		Idle : BOOL;
		MoveActive : BOOL;
		MoveComplete : BOOL;
		StopActive : BOOL;
		HaltActive : BOOL;
		Warning : BOOL;
		Error : BOOL;
	END_STRUCT;
	Press_typ1 : 	STRUCT 
		Cmd : Press_Cmd_typ1;
		Par : Press_Par_typ1;
		Status : Press_Status_typ1;
		Data : Press_Data_typ1;
	END_STRUCT;
END_TYPE
