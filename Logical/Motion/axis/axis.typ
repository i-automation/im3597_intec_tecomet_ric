
TYPE
	MC_typ : 	STRUCT 
		Status : AxisStatus_enum;
		EStatus : AxisStatus_enum;
		StatusTrace : ARRAY[0..99]OF AxisStatus_enum;
		FB : MC_FB_typ;
		Data : MC_Data_typ;
		Internal : iAAxis_Internal_typ;
	END_STRUCT;
	MC_FB_typ : 	STRUCT 
		MpAxisBasic : MpAxisBasic;
		iAAxisCyclic : iAAxisCyclic;
		TON_HomingTimeout : TON;
		MC_BR_InitAxisPar : MC_BR_InitAxisPar;
	END_STRUCT;
	MC_Data_typ : 	STRUCT 
		UDCNominal : REAL;
		ACMotor : iAAxis_ACMotor_typ;
		MpLinkName : STRING[30];
		pAxis : UDINT;
		szAxis : UDINT;
		pMpLink : UDINT;
		szMpLink : UDINT;
	END_STRUCT;
	AxisStatus_enum : 
		( (*Essentially dupliate info from the Axis[...].Status structure, mostly for diagnostics purposes. *)
		A_STATUS_NOT_INIT := 0,
		A_STATUS_INIT_DONE := 1,
		A_STATUS_POWER_ON := 10,
		A_STATUS_POWER_OFF := 11,
		A_STATUS_IDLE := 20,
		A_STATUS_HOMING := 30,
		A_STATUS_HOMING_DONE := 31,
		A_STATUS_NOT_HOMED := 32,
		A_STATUS_HOMING_ERROR := 33,
		A_STATUS_SW_LIMITS_ENABLED := 40,
		A_STATUS_SW_LIMITS_DISABLED := 41,
		A_STATUS_CONTINUOUS_MOTION := 50,
		A_STATUS_DISCRETE_MOTION := 51,
		A_STATUS_GEARING_ACTIVE := 60,
		A_STATUS_CAMMING_ACTIVE := 61,
		A_STATUS_AUTOTUNE_ACTIVE := 70,
		A_STATUS_AUTOTUNE_DONE := 71,
		A_STATUS_OFFSET_ACTIVATED := 80,
		A_STATUS_OFFSET_DEACTIVATED := 81,
		A_STATUS_OFFSET_DONE := 82,
		A_STATUS_PHASE_ACTIVATED := 83,
		A_STATUS_PHASE_DEACTIVATED := 84,
		A_STATUS_PHASE_DONE := 85,
		A_STATUS_CAM_SEQ_ACTIVATED := 90,
		A_STATUS_CAM_SEQ_DEACTIVATED := 91,
		A_STATUS_CAM_SEQ_STANDBY := 92,
		A_STATUS_ERROR := 500
		);
END_TYPE
