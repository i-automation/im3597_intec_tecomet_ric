﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.4.67 SP?>
<Program SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File>axis.st</File>
    <File Private="true">axis.var</File>
    <File Private="true">axis.typ</File>
    <File>DataInit.st</File>
    <File>CyclicFUBInput.st</File>
    <File>CyclicFUBOutput.st</File>
    <File>RecordAxisStatus.st</File>
    <File>CyclicPostProcessing.st</File>
  </Files>
</Program>