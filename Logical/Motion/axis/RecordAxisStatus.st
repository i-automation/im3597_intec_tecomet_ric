
// This action monitors the Axis[x].Status substructure for changes in BOOLs, and updates the axis status accordingly
ACTION RecordAxisStatus: 
	
	IF (brsmemcmp(ADR(Axis[AxisIndex].Status),ADR(OldAxisStatusStruct[AxisIndex]),SIZEOF(Axis[AxisIndex].Status)) <> 0) THEN
		// shifting StatusTrace elements up by one
		FOR i:= 99 TO 1 BY (-1) DO
			MC[AxisIndex].StatusTrace[i]:= MC[AxisIndex].StatusTrace[i-1];
		END_FOR
		// Recording the new entry in Status enum
		IF (Axis[AxisIndex].Status.Init > OldAxisStatusStruct[AxisIndex].Init) THEN
			MC[AxisIndex].Status:= A_STATUS_INIT_DONE;
		ELSIF (Axis[AxisIndex].Status.Init < OldAxisStatusStruct[AxisIndex].Init) THEN
			MC[AxisIndex].Status:= A_STATUS_NOT_INIT;
		ELSIF (Axis[AxisIndex].Status.On > OldAxisStatusStruct[AxisIndex].On) THEN
			MC[AxisIndex].Status:= A_STATUS_POWER_ON;
		ELSIF (Axis[AxisIndex].Status.On < OldAxisStatusStruct[AxisIndex].On) THEN
			MC[AxisIndex].Status:= A_STATUS_POWER_OFF;
		ELSIF (Axis[AxisIndex].Status.Idle > OldAxisStatusStruct[AxisIndex].Idle) THEN
			MC[AxisIndex].Status:= A_STATUS_IDLE;
		ELSIF (Axis[AxisIndex].Status.Homing > OldAxisStatusStruct[AxisIndex].Homing) THEN
			MC[AxisIndex].Status:= A_STATUS_HOMING;
		ELSIF (Axis[AxisIndex].Status.Homed > OldAxisStatusStruct[AxisIndex].Homed) THEN
			MC[AxisIndex].Status:= A_STATUS_HOMING_DONE;
		ELSIF (Axis[AxisIndex].Status.Homed < OldAxisStatusStruct[AxisIndex].Homed) THEN
			MC[AxisIndex].Status:= A_STATUS_NOT_HOMED;
		ELSIF (Axis[AxisIndex].Status.HomingError > OldAxisStatusStruct[AxisIndex].HomingError) THEN
			MC[AxisIndex].Status:= A_STATUS_HOMING_ERROR;
		ELSIF (Axis[AxisIndex].Status.SWEndLimitsEnabled > OldAxisStatusStruct[AxisIndex].SWEndLimitsEnabled) THEN
			MC[AxisIndex].Status:= A_STATUS_SW_LIMITS_ENABLED;
		ELSIF (Axis[AxisIndex].Status.SWEndLimitsEnabled < OldAxisStatusStruct[AxisIndex].SWEndLimitsEnabled) THEN
			MC[AxisIndex].Status:= A_STATUS_SW_LIMITS_DISABLED;
		ELSIF (Axis[AxisIndex].Status.ContinuousMotion > OldAxisStatusStruct[AxisIndex].ContinuousMotion) THEN
			MC[AxisIndex].Status:= A_STATUS_CONTINUOUS_MOTION;
		ELSIF (Axis[AxisIndex].Status.DiscreteMotion > OldAxisStatusStruct[AxisIndex].DiscreteMotion) THEN
			MC[AxisIndex].Status:= A_STATUS_DISCRETE_MOTION;
		ELSIF (Axis[AxisIndex].Status.AutotuneActive > OldAxisStatusStruct[AxisIndex].AutotuneActive) THEN
			MC[AxisIndex].Status:= A_STATUS_AUTOTUNE_ACTIVE;
		ELSIF (Axis[AxisIndex].Status.AutotuneDone > OldAxisStatusStruct[AxisIndex].AutotuneDone) THEN
			MC[AxisIndex].Status:= A_STATUS_AUTOTUNE_DONE;
		ELSIF (Axis[AxisIndex].Status.Error > OldAxisStatusStruct[AxisIndex].Error) THEN
			MC[AxisIndex].Status:= A_STATUS_ERROR;
		END_IF
		MC[AxisIndex].StatusTrace[0]:= MC[AxisIndex].Status;
		OldAxisStatusStruct[AxisIndex]:= Axis[AxisIndex].Status;
	END_IF
		
END_ACTION