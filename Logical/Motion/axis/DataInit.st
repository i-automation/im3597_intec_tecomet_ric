
// Data initialization code (to be run once after DataMgrInit goes TRUE). 
ACTION DataInit:
	
	// Look for rising edge on DataMgrInit
	IF ((DataMgrInit) AND (NOT OldDataMgrInit)) THEN
		// Create name of MpLink PV dynamically
		MC[AxisIndex].Data.MpLinkName:= 'MpLink';
		brsstrcat(ADR(MC[AxisIndex].Data.MpLinkName),ADR(SystemSettings.AxisConfig[AxisIndex].Mapp.AxisName));
		// Find address of Axis handle and MpLink PV in memory
		Status:= PV_xgetadr(ADR(SystemSettings.AxisConfig[AxisIndex].Mapp.AxisName),ADR(MC[AxisIndex].Data.pAxis),ADR(MC[AxisIndex].Data.szAxis));
		IF (Status <> 0) THEN
			// XXX1 some kind of error message
		END_IF
		Status:= PV_xgetadr(ADR(MC[AxisIndex].Data.MpLinkName),ADR(MC[AxisIndex].Data.pMpLink),ADR(MC[AxisIndex].Data.szMpLink));
		IF (Status <> 0) THEN
			// XXX1 some kind of error message
		END_IF
		IF ((MC[AxisIndex].Data.pMpLink <> 0) AND (MC[AxisIndex].Data.pAxis <> 0)) THEN
			// MpAxisBasic setup
			MC[AxisIndex].FB.MpAxisBasic.MpLink:= MC[AxisIndex].Data.pMpLink;
			MC[AxisIndex].FB.MpAxisBasic.Axis:= MC[AxisIndex].Data.pAxis;
			MC[AxisIndex].FB.MpAxisBasic.Enable:= TRUE;
			// iAAxisCyclic setup
			MC[AxisIndex].FB.iAAxisCyclic.pMpLink:= MC[AxisIndex].Data.pMpLink;
			MC[AxisIndex].FB.iAAxisCyclic.pAxis:= MC[AxisIndex].Data.pAxis;
			MC[AxisIndex].FB.iAAxisCyclic.Enable:= TRUE;			
		END_IF
		// Assign 'constant' values
		MC[AxisIndex].Internal.Constants.UI1_0:= 0;
		MC[AxisIndex].Internal.Constants.UI1_1:= 1;
		MC[AxisIndex].Internal.Constants.UI1_2:= 2;
		MC[AxisIndex].Internal.Constants.UI1_3:= 3;
		MC[AxisIndex].Internal.Constants.UI2_0:= 0;
		MC[AxisIndex].Internal.Constants.UI2_1:= 1;
		MC[AxisIndex].Internal.Constants.UI2_2:= 2;
		MC[AxisIndex].Internal.Constants.UI2_514:= 514;
		MC[AxisIndex].Internal.Constants.R4_0:= 0;
	END_IF
		
END_ACTION