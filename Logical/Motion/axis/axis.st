(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: axis
 * File: axis.st
 * Author: amusser
 * Created: 8/10/2017
 * Version: v1.0
 * Modified Date:
 * Modified BY:
 ********************************************************************
 * Implementation of program Axis

 * Description:

	This task controls our axes.

 * Options:

 * Version History:
 ********************************************************************)

(* XXX1 additional things TO deal with
	1. When stopping a movevelocity, PLCopen state goes from continuous to discrete briefly (and our Axis.Status.xxx follows suit). 
	2. When moveadditive is interrupted by moveabsolute, you want to reset the first command immediately (right now it doesn't reset until full move completes). 
	3. Ensure offset and phase inputs/outputs work for CamSequencer as well. 
	4. Look for duplicate input parameters (i.e. Accel under Basic and Coupling, for instance), and find a smart way not to require mm to assign each of these individually. 
	5. Do we really still want to disable SW limits by default on startup? This was done back in the day to allow jogging prior to homing... but MpAxis may allow this another way. 
	6. Need to ensure that limits, tuning, etc can be reapplied dynamically when you power off/power on (or should we require a reinitialization?). 
	7. May need to couple this developemtn with the ArEventLog library, because oftentimes MpAxis gives obscure messages (error #1002, parameter outside range), but can't point to culprit except through logger...
	8. Two ParIDs were giving me trouble and not writing correctly in simulation... ACP10PAR_UDC_NOMINAL and ACP10PAR_ENCOD_LINE_CHK_IGNORE. To be investigated on hardware. 
	9. Need to be able to change the set velocity and have MpAxisBasic update automatically. 
	10. Need to be able to pass in negative numbers for velocity for move in negative direction...
	11. Need to fill in description column for all Axis_typ structures and substructures. 
	12. If the axis has initialized but then later gets an error, we're resetting the AxisInitialized flag. Not sure this is what we want to do. 


*)

PROGRAM _CYCLIC

	IF (DataMgrInit) THEN
		
		// Defaulting axis initialization flag to TRUE, and then resetting in the loop if any axis is not initialized. 
		AllAxesInit:= TRUE;

		FOR AxisIndex:= 0 TO MAX_AXIS_INDEX DO
			
			// Data initialization code (to be run once after DataMgrInit goes TRUE). 
			DataInit;
			
			IF ((NOT SystemSettings.AxisConfig[AxisIndex].Disabled) AND (MC[AxisIndex].Data.pAxis <> 0) AND (MC[AxisIndex].Data.pMpLink <> 0)) THEN
				
				// Setting Axis Stop Decels here
				Axis[AxisIndex].Par.Basic.Stop.Deceleration:= SystemSettings.AxisConfig[AxisIndex].Move.Decel;
				
				
				// Cyclic FUB assignments.
				CyclicFUBInput;
				
				// Calling all FUBs. 
				MC[AxisIndex].FB.MpAxisBasic();
				MC[AxisIndex].FB.iAAxisCyclic();
				
				// Cyclic assignments to global structures from FUBs.
				CyclicFUBOutput;
				
				// Recording the status info
				RecordAxisStatus;
				
				// Resetting the global init flag if any enabled axis is not initialized. 
				IF (NOT Axis[AxisIndex].Status.Init AND NOT SystemSettings.AxisConfig[AxisIndex].Disabled) THEN
					AllAxesInit:= FALSE;
				END_IF
				
			END_IF
			
		END_FOR
		
		// Cyclic post-processing
		CyclicPostProcessing;
	
	END_IF
	OldDataMgrInit:= DataMgrInit;

END_PROGRAM
	
	
PROGRAM _INIT;

	GetCycleTime.enable:= TRUE;
	GetCycleTime();

	(* init program *);
	IF ( ((SIZEOF(Axis)/SIZEOF(Axis[0])) <> (SIZEOF(MC)/SIZEOF(MC[0]))) OR
		((SIZEOF(Axis)/SIZEOF(Axis[0])) <> (SIZEOF(SystemSettings.AxisConfig)/SIZEOF(SystemSettings.AxisConfig[0]))) ) THEN
		ERRxfatal(0,0,ADR('range of Axis <> range of MC <> range of SystemSettings.AxisConfig'));
	END_IF
	
	// Assigning one-time inputs to FUBs (structures, enables, etc).  
	FOR AxisIndex:= 0 TO MAX_AXIS_INDEX DO
		Axis[AxisIndex].Par.Basic.CyclicRead.LagErrorMode:= mpAXIS_READ_OFF;
		Axis[AxisIndex].Par.Basic.CyclicRead.TorqueMode:= mpAXIS_READ_OFF;
		Axis[AxisIndex].Par.Basic.CyclicRead.MotorTempMode:= mpAXIS_READ_OFF;
		Axis[AxisIndex].Par.Basic.CyclicRead.UserChannelMode:= mpAXIS_READ_OFF;
		MC[AxisIndex].FB.iAAxisCyclic.pErrTxt:= ADR(Axis[AxisIndex].Data.ErrorText[0]);
		MC[AxisIndex].FB.iAAxisCyclic.ErrorNumType:= IAAXIS_ERR_NONE;
	END_FOR

END_PROGRAM


PROGRAM _EXIT
  	//Force re-init after transfer
    FOR AxisIndex:= 0 TO MAX_AXIS_INDEX DO
		MC[AxisIndex].FB.iAAxisCyclic.Enable:= FALSE;
		MC[AxisIndex].FB.iAAxisCyclic();
		MC[AxisIndex].FB.MpAxisBasic.Enable:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic();
	END_FOR
	OldDataMgrInit:= FALSE;
	
END_PROGRAM
      
