
// This action maps all FUB outputs to PVs. 
ACTION CyclicFUBOutput:
	
	// XXX1 need to fill in all blanks here!
	// Status information
	Axis[AxisIndex].Status.Init:= MC[AxisIndex].FB.iAAxisCyclic.AxisInitialized;
	Axis[AxisIndex].Status.On:= MC[AxisIndex].FB.MpAxisBasic.PowerOn;
	Axis[AxisIndex].Status.Homing:= (MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_HOMING);
	Axis[AxisIndex].Status.Homed:= MC[AxisIndex].FB.MpAxisBasic.IsHomed;
	// Axis[AxisIndex].Status.HomingError:= 
	Axis[AxisIndex].Status.SWEndLimitsEnabled:= MC[AxisIndex].FB.iAAxisCyclic.SWEndLimitsEnabled;
	Axis[AxisIndex].Status.ContinuousMotion:= (MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_CONTINUOUS_MOTION);
	Axis[AxisIndex].Status.DiscreteMotion:= (MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_DISCRETE_MOTION);
	// Axis[AxisIndex].Status.AutotuneActive:= 
	Axis[AxisIndex].Status.AutotuneDone:= MC[AxisIndex].FB.MpAxisBasic.TuningDone;
	// XXX1 verify that this catches drive faults AND FUB faults. 
	Axis[AxisIndex].Status.Error:= ((MC[AxisIndex].FB.MpAxisBasic.Error) OR (MC[AxisIndex].FB.iAAxisCyclic.Error));
	Axis[AxisIndex].Status.Idle:= ((MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_STANDSTILL) AND (OldMC[AxisIndex].FB.MpAxisBasic.PowerOn) AND (NOT Axis[AxisIndex].Status.Error) AND NOT(MC[AxisIndex].FB.MpAxisBasic.MoveAbsolute));
	
	// Data information
	IF (Axis[AxisIndex].Status.Error) THEN
		// Here we identify for each FUB whether the error is FUB related or drive related. 
		IF (MC[AxisIndex].FB.MpAxisBasic.Error) THEN
			IF (MC[AxisIndex].FB.MpAxisBasic.Info.Diag.StatusID.ID = mpAXIS_ERR_PLC_OPEN) THEN
				Axis[AxisIndex].Data.ErrorID:= MC[AxisIndex].FB.MpAxisBasic.Info.Diag.Internal.Code;
				MC[AxisIndex].FB.iAAxisCyclic.ErrorNumType:= mcAXIS_ERROR;
			ELSE
				Axis[AxisIndex].Data.ErrorID:= MC[AxisIndex].FB.MpAxisBasic.Info.Diag.StatusID.Code;
				MC[AxisIndex].FB.iAAxisCyclic.ErrorNumType:= mcFB_ERROR;
			END_IF
		ELSE
			MC[AxisIndex].FB.iAAxisCyclic.ErrorNumType:= mcFB_ERROR;
		END_IF
	ELSE
		Axis[AxisIndex].Data.ErrorID:= 0;
		MC[AxisIndex].FB.iAAxisCyclic.ErrorNumType:= IAAXIS_ERR_NONE;
	END_IF
	// Axis[AxisIndex].Data.ErrorText ==> this is being written to from the iAAxisCyclic FB. 
	Axis[AxisIndex].Data.Position:= MC[AxisIndex].FB.MpAxisBasic.Position;
	Axis[AxisIndex].Data.Velocity:= MC[AxisIndex].FB.MpAxisBasic.Velocity;
	IF ((MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.UserChannelParameterID.Valid) AND (SystemSettings.AxisConfig[AxisIndex].Motor.Type <> MOTOR_NOTREQUIRED)) THEN
		Axis[AxisIndex].Data.CurrentRaw:= LREAL_TO_REAL(MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.UserChannelParameterID.Value);
		Axis[AxisIndex].Data.CurrentCont:= ABS(Axis[AxisIndex].Data.CurrentRaw);
	ELSE
		Axis[AxisIndex].Data.CurrentRaw:= 0.0;
		Axis[AxisIndex].Data.CurrentCont:= 0.0;
	END_IF
	IF (Axis[AxisIndex].Data.CurrentCont > Axis[AxisIndex].Data.CurrentPeak) THEN
		Axis[AxisIndex].Data.CurrentPeak:= Axis[AxisIndex].Data.CurrentCont;
	END_IF 
	IF (MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.Torque.Valid) THEN
		Axis[AxisIndex].Data.TorqueRaw:= LREAL_TO_REAL(MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.Torque.Value);
		Axis[AxisIndex].Data.TorqueCont:= ABS(Axis[AxisIndex].Data.TorqueRaw);
	ELSE
		Axis[AxisIndex].Data.TorqueRaw:= 0.0;
		Axis[AxisIndex].Data.TorqueCont:= 0.0;
	END_IF
	IF (Axis[AxisIndex].Data.TorqueCont > Axis[AxisIndex].Data.TorquePeak) THEN
		Axis[AxisIndex].Data.TorquePeak:= Axis[AxisIndex].Data.TorqueCont;
	END_IF 
	IF (MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.LagError.Valid) THEN
		Axis[AxisIndex].Data.LagError:= MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.LagError.Value;
	ELSE
		Axis[AxisIndex].Data.LagError:= 0.0;
	END_IF
	IF (MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.MotorTemperature.Valid) THEN
		Axis[AxisIndex].Data.MotorSensorTemp:= MC[AxisIndex].FB.MpAxisBasic.Info.CyclicRead.MotorTemperature.Value;
	ELSE
		Axis[AxisIndex].Data.MotorSensorTemp:= 0.0;
	END_IF
	// Axis[AxisIndex].Data.AxisPeriod:= 
	// Axis[AxisIndex].Data.PLCopenAxisPeriod:= 
	// Axis[AxisIndex].Data.ActualScalingEncoderCounts:= 
	// Axis[AxisIndex].Data.ActualScalingMotorRevs:= 
	// Axis[AxisIndex].Data.SlavePosition:= 
	// Axis[AxisIndex].Data.MaxWindingTemp:= 
	// Axis[AxisIndex].Data.MotorPeakCurrent:= 
	// 1Axis[AxisIndex].Data.MotorRatedCurrent:= 
	
	// Info sub-structures from FUBs. 
	Axis[AxisIndex].Info.Basic:= MC[AxisIndex].FB.MpAxisBasic.Info;
	Axis[AxisIndex].Info.Config:= MC[AxisIndex].FB.iAAxisCyclic.MappInfo;
	
	// Resetting commands if the action has completed
	IF ((NOT MC[AxisIndex].FB.MpAxisBasic.MoveActive) AND (OldMC[AxisIndex].FB.MpAxisBasic.MoveActive)) THEN
		Axis[AxisIndex].Cmd.MoveVelocity:= FALSE;
		Axis[AxisIndex].Cmd.MoveAbsolute:= FALSE;
		Axis[AxisIndex].Cmd.MoveAdditive:= FALSE;
	END_IF
	IF ((Axis[AxisIndex].Cmd.Home) AND (Axis[AxisIndex].Status.Homed) AND (NOT OldAxis[AxisIndex].Status.Homed)) THEN
		Axis[AxisIndex].Cmd.Home:= FALSE;
	END_IF
	IF ((Axis[AxisIndex].Cmd.ErrAck) AND (NOT Axis[AxisIndex].Status.Error)) THEN
		Axis[AxisIndex].Cmd.ErrAck:= FALSE;
	END_IF


END_ACTION