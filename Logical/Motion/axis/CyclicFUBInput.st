
// This action maps PVs to all FUB inputs. Some come directly from global structures (Axis or SystemSettings), while others are manipulated. 
ACTION CyclicFUBInput:
	
	// Cyclic parameter assignments
	MC[AxisIndex].FB.MpAxisBasic.Parameters:= ADR(Axis[AxisIndex].Par.Basic);
	MC[AxisIndex].FB.iAAxisCyclic.Internal:= ADR(MC[AxisIndex].Internal);
	MC[AxisIndex].FB.iAAxisCyclic.ErrorNum:= Axis[AxisIndex].Data.ErrorID;
	
	// Error reset commands
	MC[AxisIndex].FB.iAAxisCyclic.ErrorReset:= Axis[AxisIndex].Cmd.ErrAck;
	MC[AxisIndex].FB.MpAxisBasic.ErrorReset:= Axis[AxisIndex].Cmd.ErrAck;
	
	// Cyclic assignments that need to happen regardless of whether we're initilialized or there's an error.
	MC[AxisIndex].FB.iAAxisCyclic.ErrorReset:= Axis[AxisIndex].Cmd.ErrAck;
	MC[AxisIndex].FB.iAAxisCyclic.InitializeAxis:= Axis[AxisIndex].Cmd.Init;
	MC[AxisIndex].FB.iAAxisCyclic.BasicInfo:= MC[AxisIndex].FB.MpAxisBasic.Info;
	MC[AxisIndex].FB.iAAxisCyclic.AxisConfig:= SystemSettings.AxisConfig[AxisIndex];
	MC[AxisIndex].FB.iAAxisCyclic.MotorList:= SystemSettings.MotorList;
	MC[AxisIndex].FB.iAAxisCyclic.pMotorRatedCurrent:= ADR(Axis[AxisIndex].Data.MotorRatedCurrent);
	MC[AxisIndex].FB.iAAxisCyclic.pMotorPeakCurrent:= ADR(Axis[AxisIndex].Data.MotorPeakCurrent);
	MC[AxisIndex].FB.iAAxisCyclic.pMaxWindingTemp:= ADR(Axis[AxisIndex].Data.MaxWindingTemp);
	MC[AxisIndex].FB.iAAxisCyclic.UDCNominal:= SQRT(2.0) * UINT_TO_REAL(SystemSettings.LineVoltage);
	MC[AxisIndex].FB.iAAxisCyclic.PhaseMonIgnore:= SystemSettings.PhaseMonIgnore;
	MC[AxisIndex].FB.iAAxisCyclic.MotorSimulationActivated:= MC[AxisIndex].FB.MpAxisBasic.Simulation;
//	IF (NOT Axis[AxisIndex].Cmd.OverrideSWEndLimits) THEN
//		MC[AxisIndex].FB.iAAxisCyclic.EnableSWEndLimits:= ((MC[AxisIndex].FB.MpAxisBasic.IsHomed) AND (MC[AxisIndex].FB.MpAxisBasic.Info.AxisInitialized) AND 
//														((SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.BaseType = mpAXIS_LIMITED_LINEAR) OR 
//														(SystemSettings.AxisConfig[AxisIndex].Mapp.Axis.BaseType = mpAXIS_LIMITED_ROTARY))); 
//	ELSE // the override is active for SW limits
//		MC[AxisIndex].FB.iAAxisCyclic.EnableSWEndLimits:= ((MC[AxisIndex].FB.MpAxisBasic.IsHomed) AND (MC[AxisIndex].FB.MpAxisBasic.Info.AxisInitialized) AND (Axis[AxisIndex].Cmd.EnableSWEndLimits));
//	END_IF	
	
	MC[AxisIndex].FB.MpAxisBasic.Simulate:= MC[AxisIndex].FB.iAAxisCyclic.ActivateMotorSimulation;
		
	// Require that axis be fully initialized with no errors present before other commands can be processed
	IF ((Axis[AxisIndex].Status.Init) AND (NOT Axis[AxisIndex].Status.Error)) THEN
		// XXX1 need to fill in all blanks here		
		
		// MpAxisBasic assignments.		
		// MC[AxisIndex].FB.MpAxisBasic.Update:= 
		MC[AxisIndex].FB.MpAxisBasic.Power:= Axis[AxisIndex].Cmd.On;
		MC[AxisIndex].FB.MpAxisBasic.Home:= Axis[AxisIndex].Cmd.Home;
		MC[AxisIndex].FB.MpAxisBasic.MoveVelocity:= Axis[AxisIndex].Cmd.MoveVelocity;
		MC[AxisIndex].FB.MpAxisBasic.MoveAbsolute:= Axis[AxisIndex].Cmd.MoveAbsolute;
		MC[AxisIndex].FB.MpAxisBasic.MoveAdditive:= Axis[AxisIndex].Cmd.MoveAdditive;
		// XXX1 this Stop command needs to be updated
		MC[AxisIndex].FB.MpAxisBasic.Stop:= (((NOT Axis[AxisIndex].Cmd.MoveVelocity) AND (OldAxis[AxisIndex].Cmd.MoveVelocity)) OR 
												((NOT Axis[AxisIndex].Cmd.MoveAdditive) AND (OldAxis[AxisIndex].Cmd.MoveAdditive)) OR 
												((NOT Axis[AxisIndex].Cmd.MoveAbsolute) AND (OldAxis[AxisIndex].Cmd.MoveAbsolute)));
		// MC[AxisIndex].FB.MpAxisBasic.JogPositive:=
		// MC[AxisIndex].FB.MpAxisBasic.JogNegative:= 
		MC[AxisIndex].FB.MpAxisBasic.Autotune:= Axis[AxisIndex].Cmd.AutoTune;
		// MC[AxisIndex].FB.MpAxisBasic.TorqueLimit:= 
		// MC[AxisIndex].FB.MpAxisBasic.ReleaseBrake:= 
						
	ELSE // if not initialized or error present, reset all commands				
		// Forcing the axis to stop if it isn't already stopped. 
		IF ((MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_DISCRETE_MOTION) OR (MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_CONTINUOUS_MOTION) OR
			(MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_SYNCHRONIZED_MOTION) OR (MC[AxisIndex].FB.MpAxisBasic.Info.PLCopenState = mpAXIS_HOMING)) THEN
			MC[AxisIndex].FB.MpAxisBasic.Stop:= TRUE;
		ELSE
			MC[AxisIndex].FB.MpAxisBasic.Stop:= FALSE;
		END_IF 
		MC[AxisIndex].FB.MpAxisBasic.Update:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.Power:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.Home:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.MoveVelocity:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.MoveAbsolute:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.MoveAdditive:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.JogPositive:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.JogNegative:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.Autotune:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.TorqueLimit:= FALSE;
		MC[AxisIndex].FB.MpAxisBasic.ReleaseBrake:= FALSE;
		// Resetting all commands from Axis structure as well. 
		Axis[AxisIndex].Cmd.On:= FALSE;
		Axis[AxisIndex].Cmd.Home:= FALSE;
		Axis[AxisIndex].Cmd.MoveVelocity:= FALSE;
		Axis[AxisIndex].Cmd.MoveAbsolute:= FALSE;
		Axis[AxisIndex].Cmd.MoveAdditive:= FALSE;
		Axis[AxisIndex].Cmd.AutoTune:= FALSE;
		Axis[AxisIndex].Cmd.UpdateEncoderScaling:= FALSE;
	END_IF

END_ACTION