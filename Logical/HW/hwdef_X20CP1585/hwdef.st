
PROGRAM _INIT

	// require presence of IO modules when using real target
	IO_ModuleRequired[0]:= TRUE; // IO fault DI9371
	IO_ModuleRequired[1]:= TRUE; // IO fault DO9322
	IO_ModuleRequired[2]:= TRUE; // IO fault DS438A
	IO_ModuleRequired[3]:= TRUE; // IO fault IF10D1-1
	IO_ModuleRequired[4]:= TRUE; // IO fault CV_X300
	IO_ModuleRequired[5]:= TRUE; // IO fault SR 750
	IO_ModuleRequired[6]:= TRUE; // IO fault EX260
	IO_ModuleRequired[7]:= TRUE; // IO Link Channel 1
	IO_ModuleRequired[8]:= TRUE; // IO Link Channel 2
	IO_ModuleRequired[9]:= TRUE; //IO Link Stack Light
	IO_ModuleRequired[10]:= TRUE; // IO fault DO9322a
	IO_ModuleRequired[11]:= TRUE; // IO fault Festo Press
	
	// usb browse flag
	USBBrowseRequired:= FALSE;

	// set ethernet device for AsARCfg functions
	strcpy( ADR(EthernetIF), ADR('IF2') );

	// set visualization name
	strcpy( ADR(VisObjName), ADR('VisWSV') );
	
	// set windows share file device parameter strng
	strcpy( ADR(FileDeviceParam), ADR('/SIP=192.168.2.123 /PROTOCOL=cifs /SHARE=GCODE /USER=tecomet /PASSWORD=tecomet'));
	
	//Define hardware configuration
	HwCfg:= X20CP1585;
	
	//'use' other configs' constants to avoid nuisance unused var warnings
	ARSIM;
	
	//Modbus testing
	ModbusTest_Coil1;
	ModbusTest_Coil2;
	ModbusTest_InputReg1;
	ModbusTest_HoldingReg1;
	ModbusTest_HoldingReg2;
	
	
END_PROGRAM


PROGRAM _CYCLIC


END_PROGRAM
