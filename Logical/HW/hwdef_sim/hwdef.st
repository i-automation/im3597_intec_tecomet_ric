PROGRAM _INIT

	// simulation mode doesn't require the IO modules to be present
	IO_ModuleRequired[0]:= FALSE;
	IO_ModuleRequired[1]:= FALSE;
	IO_ModuleRequired[2]:= FALSE; 
	IO_ModuleRequired[3]:= FALSE; 
	IO_ModuleRequired[4]:= FALSE; 
	IO_ModuleRequired[5]:= FALSE; 
	IO_ModuleRequired[6]:= FALSE; 
	IO_ModuleRequired[7]:= FALSE;  
	IO_ModuleRequired[8]:= FALSE; 
	IO_ModuleRequired[9]:= FALSE; 
	
	// fake e-stop being ready
	i_EStop_OK:= TRUE;
	
	// usb browse flag
	USBBrowseRequired:= FALSE;
	
	// set ethernet device for AsARCfg functions
	strcpy( ADR(EthernetIF), ADR('IF1') );
	
	// set visualization name
	strcpy( ADR(VisObjName), ADR('VisWSV') );
	
	// set usb drive file device parameter strng
	strcpy( ADR(FileDeviceParam), ADR('/DEVICE=C:\\Temp'));
	
	//Define hardware configuration
	HwCfg:= ARSIM;
	
	//Simulate output from barcode reader until it's implemented
	strcpy(ADR(Barcode.Data),ADR('85-4718.tfi'));
	Barcode.Stat.Valid:= TRUE;
	IO_ModuleOK[5]:= TRUE;
	IO_ModuleOK[3]:= TRUE;
	
	//Simulate Vision result until it's implemented
	Vision.Pass:= TRUE;
	
	i_Main_Air_Pressure_OK_2028:= TRUE;
	
	//Force feeder types until HMI/Settings are implemented
	//strcpy(ADR(Feeder[0].RivetType),ADR('0143-11'));
	//strcpy(ADR(Feeder[1].RivetType),ADR('0143-8'));
	
	//'use' other configs' constants to avoid nuisance unused var warnings
	X20CP1585;
	
END_PROGRAM


PROGRAM _CYCLIC

	Data[AX_X]:= LREAL_TO_REAL(Axis[AX_X].Data.Position)*400+500;
	Data[AX_Y]:= LREAL_TO_REAL(Axis[AX_Y].Data.Position)*400+2500;

END_PROGRAM
