
GENERAL OVERVIEW
This project is meant to be a starting point for all new projects taken on by iAutomation. This project contains 
prewritten code, visualization objects, and hardware configurations that are common across machine platforms and
otherwise repetitive to write for every new project.


FOLDERS
 1. Libraries 		- Contains all libraries used in the project
 2. Programs 		- Contains all base project programs
 3. Visualization 	- Contains all VC (Visual Components) obects
 4. Motion Objects 	- Contains all initialization tables, parameter tables, and any other motion related objects other than a program
 5. Documentation 	- Contains README, RELEASE, and other standards-related documents. 


VARIABLE AND TYPE DECLARATION FILES (anything with a file extension '.var' or '.typ')
 1. Global.typ 	- Contains structures (including Recipe and SystemSettings structures) to be used by any task excluding HMI and motion structures.
 2. HMI.typ	- Contains structures to be used in the HMI.
 3. Motion.typ	- Contains structures to be used by any motion components.
 4. Global.var	- Contains all global variable declarations (including Recipes and SystemSettings) that are not related to Motion, HMI, or IO.
 5. Motion.var	- Contains all global variable declarations related to motion.
 6. HMI.var	- Contains all global variable declarations related to the HMI.
 7. IO.var	- Contains all global variable declarations related to IO.


KEY VARIABLES
 1. Recipe (MIGHT BE ABLE TO DELETE)
	A structure of retained variables that are modifiable by an operator. These variables may change depending on the product being run.
	Storage of these variables is handled in the program 'datamgr.st'. Use of enumerations should be avoided.
 2. SystemSettings
	A structure of retained variables that are not modifiable by an operator level user, but may be modifiable by an administrator or OEM.
	These variables typically do not change based on the product being run, but may change depending on machine configuration. Use of enumeraitons should be avoided here as well.
 3. DataMgrInit
	A boolean flag produced when the program has access to the recipes. An IF statement using this variable as a condition typically surrounds the entirety of most programs in 
	the	Base Project, and will prevent the program from running while recipe information is unavailable.
 4. AlarmMgr
	A structure of information to be passed into or out of alarmmgr.st, including alarm conditions and banner texts. 
 5. TempMgr (DELETED)
	A structure of information to be passed into or out of tempmgr.st, including on commands/status, tuning commands/status, temperature, and temperature zone status. 
 6. HMI
	A structure containing variables to be mapped to the VC4 visualization.
 7. MM
	A structure of variables used by the motion manager task mm.st . This is typically divided up into two substructures of data and commands.
 8. Axis
	A large structure of variables used by the axis manager task axismgr.st . This is divided up into two substructures of commands and data, and these structures should
	not need to be changed for most applications.

	
PROGRAM DESCRIPTIONS
 1. buildinfo
	This program automatically populates a string upon building a project. This string contains information such as the .apj name, configuration name, building engineer, 
	and a timestamp.
 2. mm (WILL NEED TO REPLACE WITH APP-SPECIFIC CODE)
	Short for "Motion Manager", this task is the main sequencer for the application. It contains an example state machine for controlling several axes of different types.
 3. axis
	The axis manager. This task allows any possible ACP10 command to be fed into it for any axis, and will provide feedback on that axis. This code should not need to be changed.
 4. alarmmgr
	The alarm manager. This task sets and resets alarms based on conditions you give it. It also defines fault levels to be anywhere from a message to fatal.
 5. datamgr (SHOULDN'T NEED EXCEPT FOR SYSTEM SETTINGS)
	The data manager. This task interfaces with the iArecipe library and monitors the retrieval and saving of recipe and system settings data.
 6. hmimgr
	The HMI manager. This task handles the all interfaces between an operator and the machine. This includes visualization, file devices, and much of the logic behind recipe 
	handling.
 7. ioforce_XXX
 	Allows for the forcing of IO points. A separate task must be created for each configuration, and the configuration name should be put in place of 'XXX' (e.g. ioforce_pp65).
	Cyclic code rarely needs revision, but init code needs to be rewritten for each configuration.
 8. alarmsmsg
 	Reads the currently active alarm text and cycles through all active alarms in the alarm banner.
 9. tempmgr (DELETED)
 	The temperature manager. This manages PID heating/cooling zones.
 10.defaults
 	Allows for the storage and restoration of default system settings. This is where the programmer defines the system defaults. These should be defined for each project.
 11.hwdef_XXX
 	The hardware definition. This defines the required vs optional IO modules, required USB browsing, ethernet interface card slot, visualization object name,
	and file device parameters. Each configuration should have their own hardware definition.
 12.sysDump
 	Handles the creation and exporting of the system dump file after pressing "Export System Diagnostics". There should be no need to change this task aside from a software bug.
 13.safetymgr (COULD PROBABLY REPLACE THIS WITH SAFETY LOGIC FROM AB SAFETY COMPONENTS SINCE SAFETY IS NOT HANDLED VIA B&R HW)
 	This program handles gives the user the ability to commission the safety PLC from the HMI. This includes reading safety PLC status, downloading the application, rebooting
	the safety PLC, and clearing out the memory on the safety PLC. There should be no need to change this task aside from a software bug.
14.safetyhmi (PROBABLY DELETE THIS, OR REPLACE HEAVILY DUE TO ABOVE)
	This program manages the changing of pages and security lockouts for the safety commissioning screens.
15.serialmgr (DON'T THINK WE ARE USING ANY SERIAL COMMUNICATION)
	This handles serial communication with the iASerial library. To configure your own safety communictaion, look to the Init portion of the task as well as the SerialCyclic
	action.
16.varRead
	This gives the user the ability to read the value of process values on the "Variable Watch" page of the HMI. There should be no need to change this task aside from a software bug.
17.smtpmgr
	This is the manager for the email notification feature. It handles the sending and triggering of status email notifications.
	
	
VISUALIZATION
 1. VisVGA (DELETED)
	A 640x480 landscape visualization template containing interfaces for base project functionality.
 2. VisQVGA (DELETED)
 	A 320x240 landscape visualization template containing interfaces for base project functionality.
 2. VisWVGA (RESIZED TO 1024x600, THEN DELETED)
 	A 800x480 landscape visualization template containing interfaces for base project functionality.
	
	
MOTION OBJECTS
	This folder contains all motion objects required in the logical view. Init par tables, ACOPOS par tables for different motor types, error texts, and cam profiles all reside here.
	
	
DOCUMENTATION
	Base project documentation, including this readme and a release log.
	
	
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


AXIS CONFIGURATION GUIDE
	Explination of key variables:
		 MC[AxisIndex].config.UnitFactor (Axis init task): This is your resolution per unit of travel. For example, if your unit of travel is inches, and your unit factor is 256,
		 	you will be able to move to 256 unique locations between 0" and 1". Minimum value = 256, Maximum value = EncoderCounts/UnitOfTravel. The higher, the better with this one.
			must be an increment of 2^n
			***Note: For this example, 256 in the test window corresponds to 1"
			
		 MC[AxisIndex].config.UnitBasis (Axis init task): Specify here whether your axis is periodic, endless and non periodic, or finite.
		 
		 SystemSettings.AxisConfig[AxisIndex].Motor.ScalingEncoderCounts & SystemSettings.AxisConfig[AxisIndex].Motor.ScalingMotorRevs (defaults): These deserve to be explained together.
		 	these varibles define the physical axis scaling similar to what is specified in the axis init table. Continuing the example from the MC[AxisIndex].config.UnitFactor explination;
			the axis is connected to a 10"/rev ballscrew. Therefore, these two values would be:
				SystemSettings.AxisConfig[AxisIndex].Motor.ScalingEncoderCounts	:= REAL_TO_UDINT(10 * Axis[X_AXIS].data.UnitFactor); //10"
				SystemSettings.AxisConfig[AxisIndex].Motor.ScalingMotorRevs		:= REAL_TO_UDINT(1.0);//per one rev
	
	Once these variables are set up correctly, the example will allow you to input [Axis[AxisIndex].cmd.Position = 1] and your load will move one inch.

	
IO FORCE TASK CONFIGURATION GUIDE
	This section is a guide for updating the ioforce.st task with proper channel names. Be sure to use a separate ioforce program for each configuration!
	1) Create your variables and map them to your IO slices.
	2) Under Configuration View, right click IoMap.iom and select "open as text".
	3) Copy each IO point into the init portion of the ioforce program. Each IO point should have a line of the form:
		strcpy(ADR(Channel[0].Datapoint), ADR('%IX.IF4.ST1.IF1.ST2.DigitalInput01'));
	   Copy the text for each channel from the IoMap.iom file (after and including [%] to before and excluding [;] ) and paste between the '' marks of such a line of code.
	4) Label each module with a comment for readability. For example, group the IO points for a DI9371 together and label them as //DI9371.
	
	Each channel type has a prescribed prefix.
		%IX. is a digital input
		%QX. is a digital output
		%IW. is an analog input
		%QW. is an analog output

		
ALARM MANAGER TASK
	Alarms are broken down into 5 categories. ( Alarm Groups and Alarm Bit arrays )
		- Application => this should contain most of the alarms related to the operation of the machine	
		- Axis Alarms => Alarms for each axis The Logic for these is in place the appropriate alrms just need to be added for each axis to the Axis Alarm Group.
							- There should be 6 Alarms per Axis. See Preconfigured alarms for details. 
		- Safety Alarms => Alarms related to saftey programs of hardware. 
		- Communication => Alarms related to communication protocols. 
		- Hardware => Module Alarms for needed IO modules. 
		
	The Logic for determining Axis Parameter Faults is contained in the alarmmgr.axisparam.st action. This should not need to be edited if axes are configured correctly.
		- If a Parameter fault occurs the text is pulled from the AxisConfig.tmx localized text object using the ArTextSys Library. This works similar to a VC text group.
		- The .tmx file is referenced using a "TextID" and LanguageCode to pull a translateable text into a string variable. 
		- This allows us to have a translateable dynamic text in the alarm text. (Dynamic VC texts cannot be referenced in the alarm text)
		- If a project needs to be translated this Localized text needs to be exported. Along with the text from the visualization.
				 
		  	  
SMTP/EMAIL SETUP
		Email setup can be done in two ways. From the Defaults program or from the HMI during commisioning.
			- From the HMI
				1) From the System>Network>EmailCfg Page go into create a test email.
						This allows you to enter the IP address of the host email server, the sender address which is used as the user, and the password. 
						You can also enter up to 5 recipients. 
						Any information entered here will be saved into the system settings and be used to send each email.
				2) Once the account information is set up select which messages are enabled and disabled from the System>Network>EmailCfg Page
						Each Message has a custom subject and body text that can be entered. Along with timers for some interval dependent messages. 
						Also enter a machine name that the machine will be reffered to in the email subject.
				3) One thing that has to be set in code will be the Machine Model. This can be set in the defaults task and the idea is that the machine model is something
						that will be constant for a line of machines independent of the end user. The machine name will be different for each end user. 	
											
			- In Defaults 
				1) All the information set using the config pages in the HMI can be done in the defaults task if desired. The only down side to this is that anyone with access
						to the source code will have access to the email account information. 
				2) The information in SystemSettings.Email needs to be assigned.
				3) The information in SystemSettings.SMTP.MessageData[] contains specific information for each message type. 
						For which Array index corresponds to each message look in the SMTPmgr task varaibles. 
				
		Adding more messages.
				If there arrises the need to add another message type there are a few things that need to be done. 
					1) Increase the array size of SystemSettings.SMTP.MessageData[].
					2) Add a new case state in the SMTPmgr and Copy/Pase the assignments from another state using hte the new SystemSettings.SMTP.MessageData[] index.
					3) change the data that is attached to the end of the email if needed. 
					4) Add a trigger to send the manager to the new case state to send the email.
					5) Add an On/Off and Configure button in the HMI for the new message type.  
			
		Account Setup
				In order for an email account to allow access from a remote application a few things need to be done. (All links are for gmail account)
					1) Allow Less Secure Apps 
						https://support.google.com/accounts/answer/6010255?hl=en
					2) Incorrect Password workaround
						https://support.google.com/accounts/answer/6009563
					3) Login Allow 
						https://accounts.google.com/DisplayUnlockCaptcha

											
DISABLING UNNEEDED PORTIONS OF THE BASE PROJECT
	This section is a guide to disabling unneeded portions of the base project.
		
	Below are some commonly disabled features, and how to disable them fully.
	
	PROGRAMS
		To delete an unneeded program, find it in logical view and remove it. When you remove it, check "Adjust Software Configuration Files".
		Many programs come in groups to fully capture a feature. For example, if motion is not required the axis task should be deleted.
		
	MOTION
		To remove all motion from the program delete the following:
			Logical View:
			- axis
			- MotionObjects
			- Motion Libraries
			
			Configuration View:
			- Motion folder (less mm)
			
			Physical View:
			- Any motion conponents such as a servo drive
			
			Visualization Objects:
			- _44ServiceAxisData 		page
			- _45ServiceManualMotion 	page
			- _82SysAxisSetupBasic 		page
			- _83SysAxisSetupMotor 		page
			- _84SysAxisSetupLimits 	page
			- _85SysAxisSetupTuning 	page
			- navigation tabs to the above pages
			
			System Settings (in Global.typ):
			- SystemSettings_Motor_typ
			- SystemSettings_Tuning_typ
			- SystemSettings_Limits_typ
			- SystemSettings_AxisConfig_typ
			- SystemSettings_typ.AxisConfig
			
	TEMPERATURE
		To remove temerature management from the program delete the following:
			Logical View:
			- tempmgr
			
			Configuration View:
			- tempmgr in cpu.sw
			
			Physical View:
			- Any temperature sensor modules (e.g. X20AT4222)
			
			Visualization Objects:
			- N/A
			
			System Settings (in Global.typ):
			- TempMgr_typ
			- TempMgr_data_typ
			- TempMgr_cmd_typ
			- SystemSettings_typ.TemperatureLimit
			- SystemSettings_typ.TemperatureZone
			
	DEFAULT CONFIGURATIONS
		To remove any default configuration from the program, delete the following.
			Logical View:
			- ioforce_XXX*
			- hwdef_XXX*
				*(Where XXX is the designation for that particular configuration)
				
			Configuration View:
			- The entire configuration
				Note: A currently active configuration may not be deleted.