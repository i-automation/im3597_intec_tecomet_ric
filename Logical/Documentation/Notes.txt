Commit notes: 

 
 Drawing Updates (from iA to Intec)
 - IF10D1-1 has its own IP address - 192.168.0.22 

 HMI:
 - Is init screen waiting for all devices to init? or just dataman?
 - Fix IOlink wiring on device diagram
 - Add SMC valvebank to device diagram
 - Reboot required alarm not working?
 
 Machine:
 - On first run after reboot, immediate filenotfound error instead of barcodetimeout (sim-only problem?)
 - Add recipe for camera in file (See Shiwei Email)
 - Add vision system light output (See Shiwei Email)
 - Add safety controller interface (See Shiwei Email)
 - Watchdog for Hammer to be UP/parked prior to any X/Y moves
 - Hammer Torque watchdog
 - IO Forcing (including non-B&R devices)
 - Rivet purge & other manual rivet commands

 Data:
 - Implement windows file share (For initial testing just use local filedevice for simplicity)

 Misc:
 - Search and correct "XXX" comments
 - Duplicate barcode settings from demo case config to x20cpu config
 - Fix File Headers
 - Fix Buildinfo
 - Deploy SW into X20 config and setup timing - setup feeder and other permanent vars
 
 Extras:
 - Show riveting progress with stacklight level mode?
 - Display actual filename when filenotfound error occurs
 - Add Warning: Same rivet type loaded in both feeders (Is this a valid mode? maybe run off of one until the other has an error?)
 - Fix alpha- pad to better show case (pull from sample project)
 - "Setup mode" (manual move w/ part clamped)
 - Add job names/times/stats to logger
 
 Discuss/Setup/Test at Checkout:
 - Ensure buttons are locked while running when need be (i.e. don't allow feeder load/purge while running in auto)
 - Which tabs hidden with OEM login level
 - Which settings locked with which login level
 - Stacklight behavior
 - Update defaults program with final systemsettings at end of checkout
 - remove error # columns in alarm history list (turned on for troubleshooting blank alarms)
 - Needed rivet type label in needed rivet type missing alarm not always appearing or is incorrect? Have seen this a few times but can't reproduce reliably