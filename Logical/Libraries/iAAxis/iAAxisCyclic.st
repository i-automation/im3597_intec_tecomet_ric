FUNCTION_BLOCK iAAxisCyclic
(*
	iA Supplementary Axis Manager Init function block

	Version: 		1.0.0

	Description:	This FUNCTION block covers the axis initialization, along with misc. cyclic functions that are NOT supported BY MpAxis. 
					There is no single state machine--rather, each covered function has its own state machine (or simple IF statement), and 
					the calling task takes care of knowing when to issue each type of command. 
					For the initialization, the FUB receives a single 'Init' command AND runs through all OF its init states in order TO prepare
					the axis FOR operation. This state machine is largely recycled from the existing axis task, although only the init portion 
					has been used. 
					The rest of the commands are misc. additions to the MpAxis portfolio. 

	Date Created:	9/5/2017
	Created BY:		Musser, Andrew

	Date Modified:	
	Modified BY:	

	Inputs:

	
	Outputs:
	
	
	Commands:

		
	Version History:
	1.0.0		First release. 
	
*)	
	IF (Enable) THEN 
		Active:= 1;
		
		// Internal assignments that need to 'summarize' the info that's passed in
		IsPhysicalInverter:= ((BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOS) OR (BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOSmulti65m) OR
								(BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOSmulti) OR (BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOSmicro) OR
								(BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOSmulti65) OR (BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOS_P3));
		
		// Look for drive faults, and abort routine if necessary
		IF ((BasicInfo.Diag.StatusID.ID = mpAXIS_ERR_PLC_OPEN) AND (InitStep <> SI_ERROR)) THEN
			Internal.FB.ACMotorParameterCalc.Enable:= 0;
			Internal.FB.MpAxisBasicConfig.Save:= 0;
			Internal.FB.MC_BR_InitParList.Execute:= 0;
			Internal.FB.MC_BR_InitParTabObj.Execute:= 0;
			Internal.FB.MC_BR_ReadParID.Execute:= 0;
			Internal.FB.MC_BR_WriteParID.Execute:= 0;
			Error:= 1;
			ErrorID:= BasicInfo.Diag.Internal.Code;
			InitErrorStep:= InitStep;
			InitStep:= SI_ERROR;
		END_IF
		
		// Look for error reset, and handle resetting all commands to proper state
		IF ((ErrorReset) AND (Error)) THEN
			Error:= 0;
			ErrorID:= 0;
			InitStep:= SI_NOT_INIT;
		END_IF
		
		
		(******* AXIS INITIALIZATION *******)
		// Recording Init StepTrace
		IF (InitStep <> InitStepTrace[0]) THEN
			FOR i:= 99 TO 1 BY (-1) DO
				InitStepTrace[i]:= InitStepTrace[i-1];
			END_FOR
			InitStepTrace[0]:= InitStep;
		END_IF
		
		// Main state machine for axis initialization
		CASE InitStep OF
			
			SI_NOT_INIT: // 0
				InitErrorStep:= SI_NOT_INIT;
				IF (InitializeAxis) THEN
					InitStep:= SI_MOTOR1;
				END_IF
			
			
			SI_MOTOR1:  // 10 download motor parameter table;
				ActivateMotorSimulation:= 0;
				// Verify that network is initialized and wait for devicetype to no longer be unknown
				IF ((BasicInfo.BootState = mpAXIS_BLP_DONE) AND (BasicInfo.HardwareInfo.DeviceType <> mpAXIS_DEVICE_UNKNOWN)) THEN
					// check to see if we've done our initialization routine;
					IF (AxisInitialized = 1) THEN
						InitStep:= SI_DONE;
					ELSE
						IF (BasicInfo.HardwareInfo.DeviceType <> mpAXIS_VIRTUAL) THEN
							// skip motor section if SDC axis	
							IF (BasicInfo.HardwareInfo.DeviceType <> ncACP_TYP_SDC) THEN
										
								CASE AxisConfig.Motor.Type OF	
									IAAXIS_MOTOR_UNDEFINED: // not configured;			
								
									IAAXIS_MOTOR_AC: 
										IF ((AxisConfig.Motor.Voltage >= 115) AND (AxisConfig.Motor.Voltage <= 480) AND
											(AxisConfig.Motor.Frequency >= 50) AND (AxisConfig.Motor.Frequency <= 60) AND
											(AxisConfig.Motor.Current >= 0) AND
											(AxisConfig.Motor.Speed > 0) AND
											(AxisConfig.Motor.PowerFactor > 0) AND (AxisConfig.Motor.PowerFactor < 1) AND
											(AxisConfig.Motor.Power > 0) ) THEN
		
											Internal.FB.ACMotorParameterCalc.Enable:= 1;
											Internal.FB.ACMotorParameterCalc.RatedVoltage:= AxisConfig.Motor.Voltage;
											Internal.FB.ACMotorParameterCalc.RatedFrequency:= AxisConfig.Motor.Frequency;
											Internal.FB.ACMotorParameterCalc.RatedCurrent:= AxisConfig.Motor.Current;
											Internal.FB.ACMotorParameterCalc.RatedSpeed:= AxisConfig.Motor.Speed;
											Internal.FB.ACMotorParameterCalc.PowerFactor:= AxisConfig.Motor.PowerFactor;
											Internal.FB.ACMotorParameterCalc.RatedPower:= AxisConfig.Motor.Power;
											Internal.FB.ACMotorParameterCalc.nParallel:= AxisConfig.Motor.nParallel;
											Internal.FB.ACMotorParameterCalc.isDelta:= AxisConfig.Motor.isDelta;
											Internal.FB.ACMotorParameterCalc();
											ACMotor.Par.Voltage := Internal.FB.ACMotorParameterCalc.Voltage;
											ACMotor.Par.RatedSpeed:= Internal.FB.ACMotorParameterCalc.Speed;
											ACMotor.Par.MaxSpeed:= Internal.FB.ACMotorParameterCalc.Speed * 1.1;		
											ACMotor.Par.Rs := Internal.FB.ACMotorParameterCalc.Rs;
											ACMotor.Par.Rr := Internal.FB.ACMotorParameterCalc.Rr;
											ACMotor.Par.Lm := Internal.FB.ACMotorParameterCalc.Lm;
											ACMotor.Par.Ls := Internal.FB.ACMotorParameterCalc.Ls;
											ACMotor.Par.Lr := Internal.FB.ACMotorParameterCalc.Lr;
											ACMotor.Par.zp := Internal.FB.ACMotorParameterCalc.zp;
											ACMotor.Par.I_stall := Internal.FB.ACMotorParameterCalc.I_stall;
											ACMotor.Par.I_rated:= Internal.FB.ACMotorParameterCalc.I_rated;
											ACMotor.Par.I_mag := Internal.FB.ACMotorParameterCalc.I_mag;
											ACMotor.Par.I_max := Internal.FB.ACMotorParameterCalc.I_max;
											ACMotor.Par.T_stall := Internal.FB.ACMotorParameterCalc.T_stall;
											ACMotor.Par.T_rated := Internal.FB.ACMotorParameterCalc.T_rated;
											ACMotor.Par.T_max := Internal.FB.ACMotorParameterCalc.T_max;
											ACMotor.Par.Kt := Internal.FB.ACMotorParameterCalc.Kt;
											ACMotor.Par.Acu:= Internal.FB.ACMotorParameterCalc.Acu;
											ACMotor.Par.Temp_max:= Internal.FB.ACMotorParameterCalc.Temp_max;

											Internal.FB.MC_BR_InitParList.DataAddress:= ADR(ACMotor.ParSeq);
											Internal.FB.MC_BR_InitParList.Execute:= 1;
											InitStep:= SI_MOTOR2;
										END_IF
									
									IAAXIS_MOTOR_STEPPER: // don't need to do any of the ParID stuff
										InitStep:= SI_PAR_TABLE_DOWNLOAD;
									
									IAAXIS_MOTOR_NOTREQUIRED: // at this point probably a power supply axis in either real or simulated mode - acknowledges that there won't be a motor
										InitStep:= SI_UDC_NOMINAL;

									ELSE // otherwise we expect to have an actual motor specified, and proceed to download
										strcpy( ADR(Internal.FB.MC_BR_InitParTabObj.DataObjectName), ADR(MotorList[AxisConfig.Motor.Type]) );
										Internal.FB.MC_BR_InitParTabObj.Execute:= 1;
										InitStep:= SI_MOTOR2;

								END_CASE
										
							ELSE // it is an SDC axis - no need to mess with any motor/voltage parameters
								InitStep:= SI_PAR_TABLE_DOWNLOAD;
								
							END_IF // end OF sdc check;
									
						ELSIF (BasicInfo.HardwareInfo.DeviceType = mpAXIS_VIRTUAL) THEN
							InitStep:= SI_PAR_TABLE_DOWNLOAD;
							
						END_IF // end of axis real or virtual type check;
					END_IF // end of axis init check;
				END_IF // end of network init check;
					
			
			SI_MOTOR2: // 11 download motor parameter table;
				IF ((Internal.FB.MC_BR_InitParList.Done) OR (Internal.FB.MC_BR_InitParTabObj.Done)) THEN
					Internal.FB.MC_BR_InitParList.Execute:= 0 ;
					Internal.FB.MC_BR_InitParTabObj.Execute:= 0;
						
					InitStep:= SI_MOTOR_READ_MAX_TEMP;
				// check for condition where table is missing, and feed this back up the chain. 
				ELSIF (Internal.FB.MC_BR_InitParTabObj.Error) THEN 
					Internal.FB.MC_BR_InitParTabObj.Execute:= 0;
					Error:= 1;
					ErrorID:= Internal.FB.MC_BR_InitParTabObj.ErrorID;
					InitErrorStep:= InitStep;
					InitStep:= SI_ERROR;					
				END_IF
		
			
			SI_MOTOR_READ_MAX_TEMP: // 20
				Internal.FB.MC_BR_ReadParID.ParID:= ACP10PAR_MOTOR_WIND_TEMP_MAX;
				Internal.FB.MC_BR_ReadParID.DataAddress:= pMaxWindingTemp;
				Internal.FB.MC_BR_ReadParID.DataType:= ncPAR_TYP_REAL;
				Internal.FB.MC_BR_ReadParID.Execute:= 1;
						
				IF (Internal.FB.MC_BR_ReadParID.Done) THEN
					Internal.FB.MC_BR_ReadParID.Execute:= 0;	
					InitStep:= SI_MOTOR_READ_RATED_CURR;
				END_IF	
			
			
			SI_MOTOR_READ_RATED_CURR: // 21
				Internal.FB.MC_BR_ReadParID.ParID:= ACP10PAR_MOTOR_CURR_RATED;
				Internal.FB.MC_BR_ReadParID.DataAddress:= pMotorRatedCurrent;
				Internal.FB.MC_BR_ReadParID.DataType:= ncPAR_TYP_REAL;
				Internal.FB.MC_BR_ReadParID.Execute:= 1;
						
				IF (Internal.FB.MC_BR_ReadParID.Done) THEN
					Internal.FB.MC_BR_ReadParID.Execute:= 0;	
					InitStep:= SI_MOTOR_READ_PEAK_CURR;
				END_IF	
		
			
			SI_MOTOR_READ_PEAK_CURR: // 22
				Internal.FB.MC_BR_ReadParID.ParID:= ACP10PAR_MOTOR_CURR_MAX;
				Internal.FB.MC_BR_ReadParID.DataAddress:= pMotorPeakCurrent;
				Internal.FB.MC_BR_ReadParID.DataType:= ncPAR_TYP_REAL;
				Internal.FB.MC_BR_ReadParID.Execute:= 1;
						
				IF (Internal.FB.MC_BR_ReadParID.Done) THEN
					Internal.FB.MC_BR_ReadParID.Execute:= 0;													
					// disable temperate model - have to write parid twice;
					// commutation offset override
					IF ((AxisConfig.Motor.CommutationOverride = 1) AND (AxisConfig.Motor.Type <> IAAXIS_MOTOR_AC) AND
						(AxisConfig.Motor.CommutationOffset >= -2*IAAXIS_PI) AND (AxisConfig.Motor.CommutationOffset <= 2*IAAXIS_PI)) THEN
						InitStep:= SI_MOTOR_COMMUT_OFFSET;
					ELSIF (AxisConfig.Motor.TemperatureModelDisable = 1) THEN
						InitStep:= SI_MOTOR_TEMP_MODEL1;
					ELSIF (AxisConfig.Motor.EncoderCheckBypass = 1) THEN
						InitStep:= SI_MOTOR_LINE_CHK_IGNORE;
					ELSE
						InitStep:= SI_UDC_NOMINAL;
					END_IF
				END_IF
	
						
			SI_MOTOR_COMMUT_OFFSET: // 30		
				Internal.FB.MC_BR_WriteParID.ParID:= ACP10PAR_MOTOR_COMMUT_OFFSET;
				Internal.FB.MC_BR_WriteParID.DataAddress:= ADR(AxisConfig.Motor.CommutationOffset);
				Internal.FB.MC_BR_WriteParID.DataType:= ncPAR_TYP_REAL;
				Internal.FB.MC_BR_WriteParID.Execute:= 1;
						
				IF (Internal.FB.MC_BR_WriteParID.Done) THEN
					Internal.FB.MC_BR_WriteParID.Execute:= 0;
					// disable temperate model - have TO write parid twice
					IF (AxisConfig.Motor.TemperatureModelDisable = 1) THEN
						InitStep:= SI_MOTOR_TEMP_MODEL1;
					// encoder line check
					ELSIF (AxisConfig.Motor.EncoderCheckBypass = 1) THEN
						InitStep:= SI_MOTOR_LINE_CHK_IGNORE;
					ELSE
						InitStep:= SI_UDC_NOMINAL;
					END_IF
				END_IF			

			
			SI_MOTOR_TEMP_MODEL1: // 40		
				Internal.FB.MC_BR_WriteParID.ParID:= ACP10PAR_TEMP_MOTOR_MODEL_MODE;
				Internal.FB.MC_BR_WriteParID.DataAddress:= ADR(Internal.Constants.UI2_0);
				Internal.FB.MC_BR_WriteParID.DataType:= ncPAR_TYP_UINT;
				Internal.FB.MC_BR_WriteParID.Execute:= 1;
				
				IF (Internal.FB.MC_BR_WriteParID.Done) THEN
					Internal.FB.MC_BR_WriteParID.Execute:= 0;
					InitStep:= SI_MOTOR_TEMP_MODEL2;
				END_IF
					
		
			SI_MOTOR_TEMP_MODEL2: // 41		
				// Second write to disable temp model
				Internal.FB.MC_BR_WriteParID.Execute:= 1;
				
				IF (Internal.FB.MC_BR_WriteParID.Done) THEN
					Internal.FB.MC_BR_WriteParID.Execute:= 0;
					IF (AxisConfig.Motor.EncoderCheckBypass = 1) THEN
						InitStep:= SI_MOTOR_LINE_CHK_IGNORE;
					ELSE
						InitStep:= SI_UDC_NOMINAL;
					END_IF
				END_IF
					
			
			SI_MOTOR_LINE_CHK_IGNORE: // 50		
				Internal.FB.MC_BR_WriteParID.ParID:= ACP10PAR_ENCOD_LINE_CHK_IGNORE;
				Internal.FB.MC_BR_WriteParID.DataAddress:= ADR(Internal.Constants.UI2_1);
				Internal.FB.MC_BR_WriteParID.DataType:= ncPAR_TYP_UINT;
				Internal.FB.MC_BR_WriteParID.Execute:= 1;
				
				IF (Internal.FB.MC_BR_WriteParID.Done) THEN
					Internal.FB.MC_BR_WriteParID.Execute:= 0;
					InitStep:= SI_UDC_NOMINAL;
				END_IF

						
			SI_UDC_NOMINAL: // 60			
				IF (UDCNominal > 0) THEN
					Internal.FB.MC_BR_WriteParID.ParID:= ACP10PAR_UDC_NOMINAL;
					Internal.FB.MC_BR_WriteParID.DataAddress:= ADR(UDCNominal);
					Internal.FB.MC_BR_WriteParID.DataType:= ncPAR_TYP_REAL;
					Internal.FB.MC_BR_WriteParID.Execute:= 1;
									
					IF (Internal.FB.MC_BR_WriteParID.Done) THEN
						Internal.FB.MC_BR_WriteParID.Execute:= 0;						
						InitStep:= SI_PHASE_MON;
					END_IF
				ELSE
					InitStep:= SI_PHASE_MON;
				END_IF
					
			
			SI_PHASE_MON: // 70			
				IF (PhaseMonIgnore = 1) THEN
					Internal.FB.MC_BR_WriteParID.ParID:= ACP10PAR_PHASE_MON_IGNORE;
					Internal.FB.MC_BR_WriteParID.DataAddress:= ADR(Internal.Constants.UI2_1);
					Internal.FB.MC_BR_WriteParID.DataType:= ncPAR_TYP_UINT;
					Internal.FB.MC_BR_WriteParID.Execute:= 1;		
			
					IF (Internal.FB.MC_BR_WriteParID.Done) THEN
						Internal.FB.MC_BR_WriteParID.Execute:= 0;		
						InitStep:= SI_PAR_TABLE_DOWNLOAD;
					END_IF	
				ELSE
					InitStep:= SI_PAR_TABLE_DOWNLOAD;
				END_IF
			
						
			SI_PAR_TABLE_DOWNLOAD:  // 80			
				IF (strcmp(ADR(AxisConfig.Motor.ParObjName), ADR("")) <> 0) THEN
					// assign additional parameter name
					strcpy(ADR(Internal.FB.MC_BR_InitParTabObj.DataObjectName), ADR(AxisConfig.Motor.ParObjName));
					Internal.FB.MC_BR_InitParTabObj.Execute:= 1;
		
					IF (Internal.FB.MC_BR_InitParTabObj.Done) THEN
						Internal.FB.MC_BR_InitParTabObj.Execute:= 0;
						InitStep:= SI_GLOBAL;
					ELSIF (Internal.FB.MC_BR_InitParTabObj.Error) THEN
						Internal.FB.MC_BR_InitParTabObj.Execute:= 0;
						Error:= 1;
						ErrorID:= Internal.FB.MC_BR_InitParTabObj.ErrorID;
						InitErrorStep:= InitStep;
						InitStep:= SI_ERROR;
					END_IF
				ELSE
					InitStep:= SI_GLOBAL;
				END_IF
					
		
			SI_GLOBAL: // 90			
				// ################################################################################;
				// this is where the majority of ncAXIS parameters need to be set so they'll be
				// globally initialized
				// ################################################################################;
				// We don't do any extra error checking on the parameters here, as this is done in the alarmmgr task.
				// We move all data from the input 'AxisConfig' structure to the Internal.AppliedAxisConfig, since this
				// then gives the flexibility to modify it before applying and gives visibility later on into what was
				// actually applied. 
				Internal.AppliedAxisConfig:= AxisConfig;
				// Modifying the BaseType because we don't want software limits to be active until after we've homed. 
				IF (Internal.AppliedAxisConfig.Mapp.Axis.BaseType = mpAXIS_LIMITED_LINEAR) THEN
					Internal.AppliedAxisConfig.Mapp.Axis.BaseType:= mpAXIS_LINEAR;
				ELSIF (Internal.AppliedAxisConfig.Mapp.Axis.BaseType = mpAXIS_LIMITED_ROTARY) THEN
					Internal.AppliedAxisConfig.Mapp.Axis.BaseType:= mpAXIS_ROTARY;
				END_IF					
				// Transfer the parameters to the drive (these are already stored in the AxisConfig structure). 
				Internal.FB.MpAxisBasicConfig.Save:= 1;
				InitStep:= SI_GLOBAL_WAIT;

					
			SI_GLOBAL_WAIT: // 91	
				IF ((Internal.FB.MpAxisBasicConfig.CommandDone) OR (NOT Internal.FB.MpAxisBasicConfig.Save)) THEN
					Internal.FB.MpAxisBasicConfig.Save:= 0;						
					// XXX1 perhaps back calculate these based on Mapp scaling parameters...
					// Axis[AxisIndex].data.ActualScalingEncoderCounts:=  AX.encoder_if.parameter.scaling.load.units; 	
					// Axis[AxisIndex].data.ActualScalingMotorRevs:= AX.encoder_if.parameter.scaling.load.rev_motor;											
					
					// virtual axis doesn't have software end limits or need any cyclic reads setup
					IF (BasicInfo.HardwareInfo.DeviceType = mpAXIS_VIRTUAL) THEN						
						AxisInitialized:= 1;
						InitStep:= SI_DONE;						
					ELSE	
						// we don't want to switch on simulation for these guys.
						IF ((IsPhysicalInverter) OR (AxisConfig.Motor.Type = IAAXIS_MOTOR_STEPPER) OR 
							(BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOSmulti_PS) OR (BasicInfo.HardwareInfo.DeviceType = mpAXIS_ACOPOSmulti_PPS)) THEN
							AxisInitialized:= 1;
							InitStep:= SI_DONE;
						ELSE
							InitStep:= SI_SIMULATION_MODE;	
						END_IF
					END_IF
				ELSIF (Internal.FB.MpAxisBasicConfig.Error) THEN
					Internal.FB.MpAxisBasicConfig.Save:= 0;
					Error:= 1;
					ErrorID:= Internal.FB.MpAxisBasicConfig.Info.Diag.StatusID.Code;
					InitErrorStep:= InitStep;
					InitStep:= SI_ERROR;					
				END_IF
					
					
			SI_SIMULATION_MODE: // 100
				IF (BasicInfo.PLCopenState = mpAXIS_DISABLED) THEN
					ActivateMotorSimulation:= 1;					
					InitStep:= SI_SIMULATION_MODE_WAIT;
				END_IF
			
			
			SI_SIMULATION_MODE_WAIT: // 101
				IF (MotorSimulationActivated) THEN
					AxisInitialized:= 1;
					InitStep:= SI_DONE;
				END_IF
			
			
			SI_ERROR: // 400
				AxisInitialized:= 0;
				// Logic for returning to SI_NOT_INIT is handled outside of the state machine. 
			
			
			SI_DONE: // 500
				// This is our flag to reinitialize
				IF (NOT InitializeAxis) THEN
					AxisInitialized:= 0;
					InitStep:= SI_NOT_INIT;
				END_IF
				
		END_CASE 
		(******* END OF AXIS INITIALIZATION SECTION *******)
	
		
		(******* HANDLING SOFTWARE LIMITS *******)
		// Enabling/disabling software limits
		IF (((EnableSWEndLimits) AND (NOT SWEndLimitsEnabled)) OR
			((NOT EnableSWEndLimits) AND (SWEndLimitsEnabled)) AND (NOT Error)) THEN
			Internal.FB.MC_BR_WriteParID.ParID:= ACP10PAR_SGEN_SW_END_IGNORE;
			IF (EnableSWEndLimits) THEN
				Internal.FB.MC_BR_WriteParID.DataAddress:= ADR(Internal.Constants.UI1_0);
			ELSE
				Internal.FB.MC_BR_WriteParID.DataAddress:= ADR(Internal.Constants.UI1_1);
			END_IF
			Internal.FB.MC_BR_WriteParID.DataType:= ncPAR_TYP_USINT;
			Internal.FB.MC_BR_WriteParID.Execute:= 1;		
		
			IF (Internal.FB.MC_BR_WriteParID.Done) THEN
				Internal.FB.MC_BR_WriteParID.Execute:= 0;
				SWEndLimitsEnabled:= EnableSWEndLimits;
			ELSIF (Internal.FB.MC_BR_WriteParID.Error) THEN
				Internal.FB.MC_BR_WriteParID.Execute:= 0;
				Error:= 1;
				ErrorID:= Internal.FB.MC_BR_WriteParID.ErrorID;
			END_IF	
		END_IF
		(******* END OF HANDLING SOFTWARE LIMITS SECTION *******)

		
		(******* RECORDING AXIS ERRORS *******)
		// Retrieving the error text cyclically
		// Only calling this if an actual error is present, and needs to be processed
		CASE ErrorNumType OF
			mcAXIS_ERROR,
			mcFB_ERROR:
				// only updating parameters if we're not in the middle of a function call
				IF ((NOT Internal.FB.MC_BR_GetErrorText.Busy) AND (NOT Internal.FB.MC_BR_GetErrorText.Error)) THEN
					Internal.FB.MC_BR_GetErrorText.Configuration.Format:= mcBLANK;
					Internal.FB.MC_BR_GetErrorText.Configuration.LineLength:= 80;
					Internal.FB.MC_BR_GetErrorText.Configuration.DataLength:= 4 * 80;
					Internal.FB.MC_BR_GetErrorText.Configuration.DataAddress:= pErrTxt;
					Internal.FB.MC_BR_GetErrorText.Configuration.DataObjectName:= 'acp10etxen';
					Internal.FB.MC_BR_GetErrorText.ErrorRecord.Number:= ErrorNum;
					Internal.FB.MC_BR_GetErrorText.ErrorRecord.Type:= ErrorNumType;
					Internal.FB.MC_BR_GetErrorText.ErrorRecord.ParID:= 0;
					Internal.FB.MC_BR_GetErrorText.ErrorRecord.Info:= 0;
					Internal.FB.MC_BR_GetErrorText.Execute:= 1;
				END_IF
			ELSE
				// otherwise we clear the error text
				Internal.FB.MC_BR_GetErrorText.Execute:= 0;
				brsmemset(pErrTxt,0,4*80);
		END_CASE
		Internal.FB.MC_BR_GetErrorText();
		IF (Internal.FB.MC_BR_GetErrorText.Done) THEN
			Internal.FB.MC_BR_GetErrorText.Execute:= 0;
		ELSIF (Internal.FB.MC_BR_GetErrorText.Error) THEN // if the FB itself has an error, display a custom error message
			Internal.FB.MC_BR_GetErrorText.Execute:= 0;
			Error:= 1;
			ErrorID:= Internal.FB.MC_BR_GetErrorText.ErrorID;
			brsmemset(pErrTxt,0,4*80);
			brsstrcpy(pErrTxt,ADR('Error executing MC_BR_GetErrorText FB.'));
		END_IF	
		(******* END OF RECORDING AXIS ERRORS SECTION *******)

		
		// MpAxisBasicConfig setup
		Internal.FB.MpAxisBasicConfig.Enable:= 1;
		Internal.FB.MpAxisBasicConfig.ErrorReset:= ErrorReset;
		Internal.FB.MpAxisBasicConfig.MpLink:= pMpLink;
		Internal.FB.MpAxisBasicConfig.Configuration:= ADR(Internal.AppliedAxisConfig.Mapp);
		
		// PLCopen FUB setup
		Internal.FB.MC_BR_InitParList.Axis:= pAxis;
		Internal.FB.MC_BR_InitParTabObj.Axis:= pAxis;		
		Internal.FB.MC_BR_ReadParID.Axis:= pAxis;
		Internal.FB.MC_BR_WriteParID.Axis:= pAxis;
		
		// Parameter setup for MC_BR_InitParList call related to ACMotor parameters.
		IF (ACMotor.ParSeq.parameter.data_adr = 0) THEN
			ACMotor.ParSeq.parameter.data_adr:= ADR(ACMotor.ParAdrRec[0]);	
			ACMotor.ParSeq.parameter.data_len:= SIZEOF(ACMotor.ParAdrRec[0]) * 40 ;
			ACMotor.ParAdrRec[0].data_adr:= ADR(Internal.Constants.UI2_1);
			ACMotor.ParAdrRec[1].data_adr:= ADR(Internal.Constants.UI2_514);
			ACMotor.ParAdrRec[2].data_adr:= ADR(Internal.Constants.UI2_1);
			ACMotor.ParAdrRec[3].data_adr:= ADR(ACMotor.Par.zp);
			ACMotor.ParAdrRec[4].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[5].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[6].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[7].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[8].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[9].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[10].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[11].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[12].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[13].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[14].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[15].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[16].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[17].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[18].data_adr:= ADR(Internal.Constants.R4_0);
			ACMotor.ParAdrRec[19].data_adr:= ADR(ACMotor.Par.Voltage); // rated voltage
			ACMotor.ParAdrRec[20].data_adr:= ADR(Internal.Constants.R4_0);  // voltage constant (0 for AC motor)
			ACMotor.ParAdrRec[21].data_adr:= ADR(ACMotor.Par.RatedSpeed); // rated speed
			ACMotor.ParAdrRec[22].data_adr:= ADR(ACMotor.Par.MaxSpeed); // max speed
			ACMotor.ParAdrRec[23].data_adr:= ADR(ACMotor.Par.T_stall); // stall torque
			ACMotor.ParAdrRec[24].data_adr:= ADR(ACMotor.Par.T_rated); // rated torque
			ACMotor.ParAdrRec[25].data_adr:= ADR(ACMotor.Par.T_max); // peak torque
			ACMotor.ParAdrRec[26].data_adr:= ADR(ACMotor.Par.Kt); // torque constant
			ACMotor.ParAdrRec[27].data_adr:= ADR(ACMotor.Par.I_stall); // stall current
			ACMotor.ParAdrRec[28].data_adr:= ADR(ACMotor.Par.I_rated); // rated current
			ACMotor.ParAdrRec[29].data_adr:= ADR(ACMotor.Par.I_max); // peak current
			ACMotor.ParAdrRec[30].data_adr:= ADR(ACMotor.Par.Acu); // winding cross section
			ACMotor.ParAdrRec[31].data_adr:= ADR(ACMotor.Par.Rs); // stator resistance
			ACMotor.ParAdrRec[32].data_adr:= ADR(ACMotor.Par.Ls); // stator inductance
			ACMotor.ParAdrRec[33].data_adr:= ADR(Internal.Constants.R4_0); // rotor inertia
			ACMotor.ParAdrRec[34].data_adr:= ADR(Internal.Constants.R4_0); // commutation OFfset
			ACMotor.ParAdrRec[35].data_adr:= ADR(ACMotor.Par.Rr); // rotor resistance
			ACMotor.ParAdrRec[36].data_adr:= ADR(ACMotor.Par.Lr); // rotor inductance
			ACMotor.ParAdrRec[37].data_adr:= ADR(ACMotor.Par.Lm); // mutual inductance
			ACMotor.ParAdrRec[38].data_adr:= ADR(ACMotor.Par.I_mag); // magnetizing current
			ACMotor.ParAdrRec[39].data_adr:= ADR(ACMotor.Par.Temp_max); // max winding temp
			ACMotor.ParAdrRec[0].par_id:= ACP10PAR_MOTOR_TYPE;
			ACMotor.ParAdrRec[1].par_id:= ACP10PAR_MOTOR_COMPATIBILITY;
			ACMotor.ParAdrRec[2].par_id:= ACP10PAR_MOTOR_WIND_CONNECT;
			ACMotor.ParAdrRec[3].par_id:= ACP10PAR_MOTOR_POLEPAIRS;
			ACMotor.ParAdrRec[4].par_id:= ACP10PAR_MOTOR_BRAKE_CURR_RATED;
			ACMotor.ParAdrRec[5].par_id:= ACP10PAR_MOTOR_BRAKE_TORQ_RATED;
			ACMotor.ParAdrRec[6].par_id:= ACP10PAR_MOTOR_BRAKE_ON_TIME;
			ACMotor.ParAdrRec[7].par_id:= ACP10PAR_MOTOR_BRAKE_OFF_TIME;
			ACMotor.ParAdrRec[8].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR1;
			ACMotor.ParAdrRec[9].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR2;
			ACMotor.ParAdrRec[10].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR3;
			ACMotor.ParAdrRec[11].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR4;
			ACMotor.ParAdrRec[12].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR5;
			ACMotor.ParAdrRec[13].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR6;
			ACMotor.ParAdrRec[14].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR7;
			ACMotor.ParAdrRec[15].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR8;
			ACMotor.ParAdrRec[16].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR9;
			ACMotor.ParAdrRec[17].par_id:= ACP10PAR_MOTOR_TEMPSENS_PAR10;
			ACMotor.ParAdrRec[18].par_id:= ACP10PAR_MOTOR_THERMAL_CONST;
			ACMotor.ParAdrRec[19].par_id:= ACP10PAR_MOTOR_VOLTAGE_RATED;
			ACMotor.ParAdrRec[20].par_id:= ACP10PAR_MOTOR_VOLTAGE_CONST;
			ACMotor.ParAdrRec[21].par_id:= ACP10PAR_MOTOR_SPEED_RATED;
			ACMotor.ParAdrRec[22].par_id:= ACP10PAR_MOTOR_SPEED_MAX;
			ACMotor.ParAdrRec[23].par_id:= ACP10PAR_MOTOR_TORQ_STALL;
			ACMotor.ParAdrRec[24].par_id:= ACP10PAR_MOTOR_TORQ_RATED;
			ACMotor.ParAdrRec[25].par_id:= ACP10PAR_MOTOR_TORQ_MAX;
			ACMotor.ParAdrRec[26].par_id:= ACP10PAR_MOTOR_TORQ_CONST;
			ACMotor.ParAdrRec[27].par_id:= ACP10PAR_MOTOR_CURR_STALL;
			ACMotor.ParAdrRec[28].par_id:= ACP10PAR_MOTOR_CURR_RATED;
			ACMotor.ParAdrRec[29].par_id:= ACP10PAR_MOTOR_CURR_MAX;
			ACMotor.ParAdrRec[30].par_id:= ACP10PAR_MOTOR_WIND_CROSS_SECT;
			ACMotor.ParAdrRec[31].par_id:= ACP10PAR_MOTOR_STATOR_RESISTANCE;
			ACMotor.ParAdrRec[32].par_id:= ACP10PAR_MOTOR_STATOR_INDUCTANCE;
			ACMotor.ParAdrRec[33].par_id:= ACP10PAR_MOTOR_INERTIA;
			ACMotor.ParAdrRec[34].par_id:= ACP10PAR_MOTOR_COMMUT_OFFSET;
			ACMotor.ParAdrRec[35].par_id:= ACP10PAR_MOTOR_ROTOR_RESISTANCE;
			ACMotor.ParAdrRec[36].par_id:= ACP10PAR_MOTOR_ROTOR_INDUCTANCE;
			ACMotor.ParAdrRec[37].par_id:= ACP10PAR_MOTOR_MUTUAL_INDUCTANCE;
			ACMotor.ParAdrRec[38].par_id:= ACP10PAR_MOTOR_MAGNETIZING_CURR;
			ACMotor.ParAdrRec[39].par_id:= ACP10PAR_MOTOR_WIND_TEMP_MAX;
		END_IF
		
		// Calling all FBs
		Internal.FB.ACMotorParameterCalc();
		Internal.FB.MpAxisBasicConfig();
		Internal.FB.MC_BR_InitParList();
		Internal.FB.MC_BR_InitParTabObj();
		Internal.FB.MC_BR_ReadParID();
		Internal.FB.MC_BR_WriteParID();
		
		// Passing MpAxisBasicConfig info back out
		MappInfo:= Internal.FB.MpAxisBasicConfig.Info;
		
	ELSE
		Active:= 0;
		
	END_IF
	
END_FUNCTION_BLOCK
