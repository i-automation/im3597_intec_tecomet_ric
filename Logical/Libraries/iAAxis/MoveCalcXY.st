FUNCTION_BLOCK MoveCalcXY

	//PART 1 - Calculate the time each move would take at full speed/accel to determine which move takes the longest
	FOR i:= 0 TO ((SIZEOF(Axis)/SIZEOF(Axis[0])) - 1) DO
		IF ((Axis[i].MaxVelocity = 0) OR (Axis[i].Accel = 0)) THEN
				//XXX Parameter Error
			ELSE
				MoveDistance_int[i]:= LREAL_TO_REAL(ABS(Axis[i].CurrentPos - Axis[i].NewPos));
				//Calculate max speed of trianglar move. If the calculated max speed of the move exceeds the axis max speed then the move is trapezoidal.

				//Final velocity squared equals initial velocity squared plus 2*(acceleration * displacement)
				//v^2 = u^2 + 2as, where v= Final velocity, u= initial velocity, a=acceleration, and s= displacement

				//Simplified for starting from standstill...
				//v^2 = 0 * 2as 
				//v^2 = 2as 
				//v = SQRT(2as)

				IF (SQRT(2.0 * (Axis[i].Accel * (MoveDistance_int[i] / 2.0))) > Axis[i].MaxVelocity) THEN
					MoveType_int[i]:= TRAPEZOIDAL;
				ELSE
					MoveType_int[i]:= TRIANGULAR;
				END_IF

				CASE MoveType_int[i] OF
					TRIANGULAR:
					//Move time is t = v/a; getting v from the previous calc of max speed and multiplying by 2 for symmetric decel.
						MoveTime_int[i]:= ((SQRT(2.0 * (Axis[i].Accel * (MoveDistance_int[i] / 2.0)))) / Axis[i].Accel) * 2.0;

					TRAPEZOIDAL:
					//- Calculate distance moved getting to the specified speed at specified accel
						AccelTime_int[i]:= Axis[i].MaxVelocity / Axis[i].Accel;
						AccelDistance_int[i]:= 0.5*Axis[i].MaxVelocity * AccelTime_int[i];

					//- Subtract 2x the accel distance (symmetric decel) from move distance
						ConstVelDistance_int[i]:= MoveDistance_int[i] - (2.0 * AccelDistance_int[i]); //XXX - sanity check if result is <0?

					//- Remaining move distance is the constant velocity section, calculate the time at constant velocity to move this distance
						ConstVelTime_int[i]:= ConstVelDistance_int[i] / Axis[i].MaxVelocity;

					//- The move time is the sum of 2x the acceleration time and the constant velocity time
						MoveTime_int[i]:= (2.0 * AccelTime_int[i]) + ConstVelTime_int[i];

				END_CASE
			END_IF
		END_FOR
		MoveTime:= MAX(MoveTime_int[0], MoveTime_int[1]); //XXX - For support of >2ax this will need to be rewritten

		//PART 2 - Calculate new moves for the other axes, slowing them to match the axis with the longest move time.
		FOR i:= 0 TO ((SIZEOF(Axis)/SIZEOF(Axis[0])) - 1) DO
			IF (MoveTime_int[i] = MoveTime) THEN
				//This was the limiting axis, just copy parameters from input
				Accel[i]:= Axis[i].Accel;
			ELSE
				//We need to slow the axis down. Calculate a triangular move to cover the specified distance in the movetime
				//a = 2s/(t^2)
				IF MoveDistance_int[i] <> 0 THEN
					Accel[i]:= (2.0 * (MoveDistance_int[i] / 2.0)) / (EXPT((MoveTime / 2.0), 2.0));//XXX sanity check accel is less than input accel
				ELSIF MoveDistance_int[i] = 0 THEN
					Accel[i]:= 1; //to get around an error on a "standstill move"
				END_IF
			END_IF
		END_FOR

END_FUNCTION_BLOCK
