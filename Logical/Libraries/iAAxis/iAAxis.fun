
FUNCTION_BLOCK iAAxisCyclic (* *) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL; (*Enables/Disables the function block.*)
		pMpLink : {REDUND_UNREPLICABLE} UDINT; (*Incoming communiaction handle (mimicing mapp standard interface).*)
		pAxis : UDINT; (*Axis reference.*)
		ErrorReset : BOOL; (*Resets function block errors.*)
		InitializeAxis : BOOL; (*Command to run through all steps and initialize axis fully. *)
		BasicInfo : MpAxisBasicInfoType; (*Info directly taken from MpAxisBasic FUB. *)
		AxisConfig : iAAxis_Config_typ; (*All Axis Configuration data (comes from SystemSettings). *)
		MotorList : ARRAY[0..MAX_MOTORS_INDEX] OF STRING[10]; (*Array of motor data object names (comes from SystemSettings). *)
		pMotorRatedCurrent : UDINT; (*Pointer to Axis structure for storing MotorRatedCurrent.*)
		pMotorPeakCurrent : UDINT; (*Pointer to Axis structure for storing MotorPeakCurrent. *)
		pMaxWindingTemp : UDINT; (*Pointer to Axis structure for storing MotorWindingTemp*)
		UDCNominal : REAL; (*Nominal DC bus voltage. *)
		PhaseMonIgnore : BOOL; (*Flag to ignore single-phase operation warnings. *)
		ErrorNum : UINT; (*The error number to be evaluated.*)
		ErrorNumType : USINT; (*The type of the error number to be evaluated (0 = drive fault, 2 = FB error). *)
		pErrTxt : UDINT; (*Reference to an external string where the error text will be written. *)
		EnableSWEndLimits : BOOL; (*Command to enable SW limits.*)
		MotorSimulationActivated : BOOL;
		Internal : REFERENCE TO iAAxis_Internal_typ; (*Internal structure that stores FUB instances, etc.*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Indicates whether or not the function block is active. *)
		Error : BOOL; (*Indicates that the function block is in an error state or a command was not executed correctly. *)
		ErrorID : UINT; (*ErrorID reflecting the nature of the FUB's active error. *)
		AxisInitialized : BOOL; (*Axis fully initialized. *)
		ActivateMotorSimulation : BOOL; (*Command sent to external FUB (MpAxisBasic) to activate simulation. *)
		SWEndLimitsEnabled : BOOL; (*Status bit indicating SW limits are enabled. *)
		MappInfo : MpAxisInfoType; (*Info from MpAxisBasicConfig FUB. *)
	END_VAR
	VAR
		i : UINT; (*Internal counter. *)
		InitErrorStep : AxisInitSteps_enum; (*Step where the error occurred. *)
		InitStep : AxisInitSteps_enum; (*Step for main state machine. *)
		InitStepTrace : ARRAY[0..99] OF AxisInitSteps_enum; (*Step trace for main state machine. *)
		IsPhysicalInverter : BOOL; (*Records whether DeviceType is a physical drive of some sort. *)
		ACMotor : iAAxis_ACMotor_typ; (*Structure for handling parameter calculation and storage for AC Motors. *)
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK MoveCalcXY (* *) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Axis : {REDUND_UNREPLICABLE} ARRAY[0..1] OF iAAxis_MoveCalcXY_par_typ;
	END_VAR
	VAR_OUTPUT
		MoveTime : {REDUND_UNREPLICABLE} REAL;
		Accel : {REDUND_UNREPLICABLE} ARRAY[0..1] OF REAL;
	END_VAR
	VAR
		i : INT;
		ConstVelTime_int : ARRAY[0..1] OF REAL;
		ConstVelDistance_int : ARRAY[0..1] OF REAL;
		AccelDistance_int : ARRAY[0..1] OF REAL;
		AccelTime_int : ARRAY[0..1] OF REAL;
		MoveDistance_int : ARRAY[0..1] OF REAL;
		MoveType_int : ARRAY[0..1] OF iAAxis_MoveCalcXY_MoveType_enm;
		MoveTime_int : ARRAY[0..1] OF REAL;
	END_VAR
END_FUNCTION_BLOCK
