
TYPE
	iAAxis_Internal_FB_typ : 	STRUCT 
		MpAxisBasicConfig : MpAxisBasicConfig;
		MC_BR_GetErrorText : MC_BR_GetErrorText;
		MC_BR_InitParList : MC_BR_InitParList;
		MC_BR_ReadParID : MC_BR_ReadParID;
		MC_BR_WriteParID : MC_BR_WriteParID;
		MC_BR_InitParTabObj : MC_BR_InitParTabObj;
		ACMotorParameterCalc : ACMotorParameterCalc;
	END_STRUCT;
	iAAxis_Internal_typ : 	STRUCT 
		FB : iAAxis_Internal_FB_typ;
		Constants : iAAxis_Internal_Constants_typ;
		AppliedAxisConfig : iAAxis_Config_typ;
	END_STRUCT;
	iAAxis_Internal_Constants_typ : 	STRUCT 
		UI2_0 : UINT;
		UI2_1 : UINT;
		UI2_2 : UINT;
		UI2_514 : UINT;
		UI1_0 : USINT;
		UI1_1 : USINT;
		UI1_2 : USINT;
		UI1_3 : USINT;
		R4_0 : REAL;
	END_STRUCT;
	AxisInitSteps_enum : 
		(
		SI_NOT_INIT := 0,
		SI_MOTOR1 := 10,
		SI_MOTOR2 := 11,
		SI_MOTOR_READ_MAX_TEMP := 20,
		SI_MOTOR_READ_RATED_CURR := 21,
		SI_MOTOR_READ_PEAK_CURR := 22,
		SI_MOTOR_COMMUT_OFFSET := 30,
		SI_MOTOR_TEMP_MODEL1 := 40,
		SI_MOTOR_TEMP_MODEL2 := 41,
		SI_MOTOR_LINE_CHK_IGNORE := 50,
		SI_UDC_NOMINAL := 60,
		SI_PHASE_MON := 70,
		SI_PAR_TABLE_DOWNLOAD := 80,
		SI_GLOBAL := 90,
		SI_GLOBAL_WAIT := 91,
		SI_SIMULATION_MODE := 100,
		SI_SIMULATION_MODE_WAIT := 101,
		SI_ERROR := 400,
		SI_DONE := 500
		);
	iAAxis_Config_typ : 	STRUCT 
		Disabled : BOOL;
		Move : iAAxis_Config_Move_typ;
		Motor : iAAxis_Config_Motor_typ;
		Mapp : MpAxisBasicConfigType;
	END_STRUCT;
	iAAxis_ACMotor_typ : 	STRUCT 
		ParSeq : ACP10DATBL_typ;
		ParAdrRec : ARRAY[0..40]OF ACP10PRADR_typ;
		Par : iAAxis_ACMotor_Par_typ;
	END_STRUCT;
	iAAxis_ACMotor_Par_typ : 	STRUCT 
		Voltage : REAL;
		RatedSpeed : REAL;
		MaxSpeed : REAL;
		Rs : REAL;
		Rr : REAL;
		Lm : REAL;
		Ls : REAL;
		Lr : REAL;
		zp : INT;
		I_stall : REAL;
		I_rated : REAL;
		I_mag : REAL;
		I_max : REAL;
		T_stall : REAL;
		T_rated : REAL;
		T_max : REAL;
		Kt : REAL;
		Temp_max : REAL;
		Acu : REAL;
	END_STRUCT;
	iAAxis_Config_Move_typ : 	STRUCT 
		Accel : REAL;
		Decel : REAL;
		JogSpeedHigh : REAL;
		JogSpeed : REAL;
		JogIncrement : REAL;
		HomeOffset : REAL;
	END_STRUCT;
	iAAxis_Config_Motor_typ : 	STRUCT 
		Type : UINT;
		ParObjName : STRING[12];
		EncoderCheckBypass : UINT;
		TemperatureModelDisable : UINT;
		CommutationOverride : UINT;
		CommutationOffset : REAL;
		nParallel : USINT;
		isDelta : BOOL;
		Voltage : INT;
		Frequency : INT;
		Current : REAL;
		Speed : INT;
		PowerFactor : REAL;
		Power : INT;
	END_STRUCT;
	iAAxis_MoveCalcXY_par_typ : 	STRUCT 
		Accel : REAL;
		MaxVelocity : REAL;
		CurrentPos : LREAL;
		NewPos : LREAL;
	END_STRUCT;
	iAAxis_MoveCalcXY_MoveType_enm : 
		(
		TRIANGULAR,
		TRAPEZOIDAL
		);
END_TYPE
