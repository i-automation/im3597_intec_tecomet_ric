
FUNCTION_BLOCK ACMotorParameterCalc
	VAR_INPUT
		Enable : BOOL;
		RatedVoltage : INT;
		RatedFrequency : INT;
		RatedCurrent : REAL;
		RatedSpeed : INT;
		RatedPower : INT;
		PowerFactor : REAL;
		nParallel : USINT;
		isDelta : BOOL;
	END_VAR
	VAR_OUTPUT
		Voltage : REAL;
		Speed : REAL;
		Rs : REAL;
		Rr : REAL;
		Lm : REAL;
		Ls : REAL;
		Lr : REAL;
		zp : INT;
		I_stall : REAL;
		I_rated : REAL;
		I_mag : REAL;
		I_max : REAL;
		T_stall : REAL;
		T_rated : REAL;
		T_max : REAL;
		Kt : REAL;
		Acu : REAL;
		Temp_max : REAL;
	END_VAR
	VAR
		isqnp_ : REAL;
		Xs_ : REAL;
		isdnp_ : REAL;
		Xh_ : REAL;
		p_ : REAL;
		sigma_ : REAL;
		wrn_ : REAL;
		sigma : REAL;
		pi : REAL;
		p_temp : REAL;
		stardeltacompensation : REAL;
		np : REAL;
	END_VAR
END_FUNCTION_BLOCK
