(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: fputil2
 * File: fputil2.fun
 * Author: SSwindells
 * Created: September 15, 2010
 ********************************************************************
 * Functions and function blocks of library fputil2
 ********************************************************************)

FUNCTION fpu2eq : BOOL
	VAR_INPUT
		A : LREAL;
		B : LREAL;
		epsilon : LREAL;
	END_VAR
END_FUNCTION

FUNCTION fpu2lt : BOOL
	VAR_INPUT
		A : LREAL;
		B : LREAL;
		epsilon : LREAL;
	END_VAR
END_FUNCTION

FUNCTION fpu2le : BOOL
	VAR_INPUT
		A : LREAL;
		B : LREAL;
		epsilon : LREAL;
	END_VAR
END_FUNCTION

FUNCTION fpu2gt : BOOL
	VAR_INPUT
		A : LREAL;
		B : LREAL;
		epsilon : LREAL;
	END_VAR
END_FUNCTION

FUNCTION fpu2ge : BOOL
	VAR_INPUT
		A : LREAL;
		B : LREAL;
		epsilon : LREAL;
	END_VAR
END_FUNCTION
