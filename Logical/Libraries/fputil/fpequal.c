#include <stdlib.h>
#include "fputil.h"


plcbit fpequal(float A, float B, signed long maxUlps) {

	signed long aInt, bInt, intDiff;

	aInt = *(signed long*)&A;
	/* Make aInt lexicographically ordered as a twos-complement int */
	if (aInt < 0){
		aInt = 0x80000000 - aInt;
	}
	
	bInt = *(signed long*)&B;
	/* Make bInt lexicographically ordered as a twos-complement int */
	if (bInt < 0){
		bInt = 0x80000000 - bInt;
	}
	
	intDiff = abs(aInt - bInt);

    if (intDiff <= maxUlps)
        return 1;
    return 0;

}


