PROGRAM _INIT
	 
END_PROGRAM

PROGRAM _CYCLIC
	(* SAFETY SYSTEM CONTROLLER
	ALLEN BRADLEY
	AB 440R-ENETR
	ENET/IP 192.168.0.11*)

	IF DataMgrInit THEN
		Enable:= TRUE;
	END_IF;
	
	//Get status of safety controller and its inputs, used to annunciate errors via the HMI alarm system.
	//get status from safety relay
	//get attribute single, class 4, instanc 104, attribute 3, 320 bytes
	FB_CIP_Message_Safety_Relay_In.enable := Enable;
	FB_CIP_Message_Safety_Relay_In.pDevice := ADR('SS1.IF1');     //Path to	X20IF10D1-1
	FB_CIP_Message_Safety_Relay_In.pIpAddress := ADR('192.168.0.11');     //IP	Address OF EthIP Adapter
	FB_CIP_Message_Safety_Relay_In.service:= 16#0E;     //CIP Service (from	Adapter documentation)
	FB_CIP_Message_Safety_Relay_In.class:= 4;     //CIP Class (from Adapter	documentation)
	FB_CIP_Message_Safety_Relay_In.instance:= 104;     //CIP Instance (from	Adapter documentation)
	FB_CIP_Message_Safety_Relay_In.attribute:= 3;     //CIP Attribute (from	Adapter documentation)
	FB_CIP_Message_Safety_Relay_In.dataCount:= 0;     //Size of data to	be sent
	FB_CIP_Message_Safety_Relay_In.pData:= ADR(eipsenddata);     //Pointer to (address	OF) data TO be sent
	FB_CIP_Message_Safety_Relay_In.maxResDataCount := 320;	//Maximum size of data to be received
	FB_CIP_Message_Safety_Relay_In.pResData:= ADR(eiprecvdata);//Pointer to	(address OF) location TO receive data
		
	FB_CIP_Message_Safety_Relay_In(); //FUB call
	
	//status mapping
	
	i_EStop_OK := eiprecvdata[9].4;
	i_Safety_Interlock_OK := eiprecvdata[61].4;
	
	i_EStop_PB_OK[0] := eiprecvdata[24].0;
	i_EStop_PB_OK[1] := eiprecvdata[24].1;
	i_EStop_PB_OK[2] := eiprecvdata[24].2;
	i_EStop_PB_OK[3] := eiprecvdata[24].3;
	
	i_Safety_Door_Locked[0] := eiprecvdata[76].0;
	i_Safety_Door_Locked[1] := eiprecvdata[76].1;
	i_Safety_Door_Locked[2] := eiprecvdata[76].2;
	i_Safety_Door_Locked[3] := eiprecvdata[76].3;
	i_Safety_Door_Locked[4] := eiprecvdata[76].4;
	i_Safety_Door_Locked[5] := eiprecvdata[76].5;
	
	
	//safety door lock/unlock cmd mapping
	eipsenddata[40].0 := o_Safety_Door_Unlock[0];
	eipsenddata[40].1 := o_Safety_Door_Unlock[1];
	eipsenddata[40].2 := o_Safety_Door_Unlock[2];
	eipsenddata[40].3 := o_Safety_Door_Unlock[3];
	eipsenddata[40].4 := o_Safety_Door_Unlock[4];
	eipsenddata[40].5 := o_Safety_Door_Unlock[5];
	
	eipsenddata[44].0 := o_Safety_Door_Lock[0];
	eipsenddata[44].1 := o_Safety_Door_Lock[1];
	eipsenddata[44].2 := o_Safety_Door_Lock[2];
	eipsenddata[44].3 := o_Safety_Door_Lock[3];
	eipsenddata[44].4 := o_Safety_Door_Lock[4];
	eipsenddata[44].5 := o_Safety_Door_Lock[5];
	

	//send safety lock/unlock cmd to safety relay
	//set attribute single, class 4, instance 152, attribute 3, 216 bytes	
	
	FB_CIP_Message_Safety_Relay_Out.enable := Enable;
	FB_CIP_Message_Safety_Relay_Out.pDevice := ADR('SS1.IF1');     //Path to	X20IF10D1-1
	FB_CIP_Message_Safety_Relay_Out.pIpAddress := ADR('192.168.0.11');     //IP	Address OF EthIP Adapter
	FB_CIP_Message_Safety_Relay_Out.service:= 16#10;     //CIP Service (from	Adapter documentation)
	FB_CIP_Message_Safety_Relay_Out.class:= 4;     //CIP Class (from Adapter	documentation)
	FB_CIP_Message_Safety_Relay_Out.instance:= 152;     //CIP Instance (from	Adapter documentation)
	FB_CIP_Message_Safety_Relay_Out.attribute:= 3;     //CIP Attribute (from	Adapter documentation)
	FB_CIP_Message_Safety_Relay_Out.dataCount:= 216;     //Size of data to	be sent
	FB_CIP_Message_Safety_Relay_Out.pData:= ADR(eipsenddata);     //Pointer to (address	OF) data TO be sent
	FB_CIP_Message_Safety_Relay_Out.maxResDataCount := 0;	//Maximum size of data to be received
	FB_CIP_Message_Safety_Relay_Out.pResData:= ADR(eiprecvdata);//Pointer to	(address OF) location TO receive data
	
	
	FB_CIP_Message_Safety_Relay_Out(); //FUB call
	
END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

