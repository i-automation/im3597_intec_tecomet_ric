PROGRAM _INIT
	TON_Reset.PT:= T#160ms;
	TON_ScanTimeout.PT:= T#2s;
	
END_PROGRAM

PROGRAM _CYCLIC
	(*KEYENCE
	BARCODE READER
	SR-752
	ENET/IP 192.168.0.14*)

	ReadInputs;

	CASE State OF
		IDLE:
			IF NOT(IO_ModuleOK[5]) THEN
				Barcode.Stat.Error:= BARCODE_COMM_LOST;
				State:= ERROR;
			ELSIF (Barcode.Cmd.Scan) THEN
				Barcode_IO.Out.Read_Request:= TRUE;
				TON_ScanTimeout.IN:= TRUE;
				State:= SCANNING;
			END_IF
			Barcode.Cmd.ErrAck:= FALSE;
			
		SCANNING:
			IF Barcode_IO.In.Read_Failure THEN
				Barcode.Stat.Error:= BARCODE_READ_FAILURE;
				State:= ERROR;
			ELSIF Barcode_IO.In.Read_Complete THEN
				TON_ScanTimeout.IN:= FALSE;
				Barcode_IO.Out.Read_Request:= FALSE;
				strcpy(ADR(Barcode.Data),ADR(Barcode_IO.In.Result_Data));
				memset(ADR(Barcode.Data)+strlen(ADR(Barcode.Data))-1,0,1);//clear the $r char
				Barcode.Stat.Valid:= TRUE;
				State:= SCANNED;				
			ELSIF (Barcode_IO.In.General_Error OR Barcode_IO.In.Error) THEN
				Barcode.Stat.Error:= BARCODE_GENERAL_FAILURE;
				Barcode_IO.In.General_Error_Code;
				State:= ERROR;
			ELSIF TON_ScanTimeout.Q THEN
				TON_ScanTimeout.IN:= FALSE;
				Barcode.Stat.Error:= BARCODE_READ_TIMEOUT;
				State:= ERROR;
			END_IF
				
		SCANNED:
			//Wait for command to clear before resetting data
			IF NOT(Barcode.Cmd.Scan) THEN
				Barcode.Stat.Valid:= FALSE;
				Barcode_IO.Out.Read_Complete_Clear:= TRUE;
				memset(ADR(Barcode.Data),SIZEOF(Barcode.Data),0);
				State:= RESET; //Go through reset state so complete_clear has time to process
			END_IF
			
		ERROR:
			TON_ScanTimeout.IN:= FALSE;
			Barcode_IO.Out.Read_Request:= FALSE;
			Barcode.Cmd.Scan:= FALSE;
			Barcode.Stat.Valid:= FALSE;
			IF (Barcode.Cmd.ErrAck) OR ((Barcode.Stat.Error = BARCODE_COMM_LOST) AND IO_ModuleOK[5]) THEN
				Barcode.Cmd.ErrAck:= FALSE;
				Barcode.Stat.Error:= BARCODE_OK;
				Barcode_IO.Out.Read_Complete_Clear:= TRUE;
				Barcode_IO.Out.Error_Clear:= TRUE;
				State:= RESET;
			END_IF
		
		RESET:
			TON_Reset.IN:= TRUE;
			IF TON_Reset.Q THEN
				TON_Reset.IN:= FALSE;
				Barcode_IO.Out.Read_Complete_Clear:= FALSE;
				Barcode_IO.Out.Error_Clear:= FALSE;
				State:= IDLE;
			END_IF
		
	END_CASE
	TON_ScanTimeout();
	TON_Reset();
	
	SetOutputs;

END_PROGRAM

PROGRAM _EXIT

END_PROGRAM
