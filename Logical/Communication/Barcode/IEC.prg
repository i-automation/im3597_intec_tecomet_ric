﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.4.67 SP?>
<Program SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Init, cyclic, exit code">Barcode.st</File>
    <File Description="Local data types" Private="true">Barcode.typ</File>
    <File Description="Local variables" Private="true">Barcode.var</File>
    <File>ReadInputs.st</File>
    <File>SetOutputs.st</File>
  </Files>
</Program>