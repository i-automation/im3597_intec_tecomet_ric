//Mapp the IO into a struct. 
// - The EDS file has all this information, but mapping the IO to vars is outside the scope of what the the IF10D1-1 and DTM do
// - B&R BOOL type is 1BYTE in length which does not map directly to the single bits from the input/output image
// - The Keyance input/output images have padding gaps, preventing looping through the struct cleanly
// - All this means it's best just to map it manually...

ACTION ReadInputs: 
	Barcode_IO.In.Error:=						     BarcodeInputImage[0].0;
	Barcode_IO.In.Result_Data_Available:=		     BarcodeInputImage[0].1;
	Barcode_IO.In.Result_Data_Strobe:=			     BarcodeInputImage[0].2;
	Barcode_IO.In.Buffer_Overflow_Error:=		     BarcodeInputImage[0].6;
	Barcode_IO.In.General_Error:=				     BarcodeInputImage[0].7;
	Barcode_IO.In.BUSY:=						     BarcodeInputImage[1].0;
	Barcode_IO.In.TRG_BUSY:=					     BarcodeInputImage[1].1;
	Barcode_IO.In.LOCK_BUSY:=					     BarcodeInputImage[1].2;
	Barcode_IO.In.MODE_BUSY:=					     BarcodeInputImage[1].3;
	Barcode_IO.In.ERR_BUSY:=					     BarcodeInputImage[1].4;
	Barcode_IO.In.Read_Complete:=				     BarcodeInputImage[2].0;
	Barcode_IO.In.Preset_Complete:=				     BarcodeInputImage[2].1;
	Barcode_IO.In.Register_Preset_Data_Complete:=    BarcodeInputImage[2].2;
	Barcode_IO.In.Tune_Complete:=				     BarcodeInputImage[2].3;
	Barcode_IO.In.EXT_Request_Complete:=		     BarcodeInputImage[2].7;
	Barcode_IO.In.Read_Failure:=				     BarcodeInputImage[3].0;
	Barcode_IO.In.Preset_Failure:=				     BarcodeInputImage[3].1;
	Barcode_IO.In.Register_Preset_Data_Failure:=     BarcodeInputImage[3].2;
	Barcode_IO.In.Tune_Failure:=				     BarcodeInputImage[3].3;
	Barcode_IO.In.EXT_Request_Failure:=			     BarcodeInputImage[3].7;
	Barcode_IO.In.IN1_Status:=					     BarcodeInputImage[4].0;
	Barcode_IO.In.IN2_Status:=					     BarcodeInputImage[4].1;
	Barcode_IO.In.OUT1_Status:=					     BarcodeInputImage[4].4;
	Barcode_IO.In.OUT2_Status:=					     BarcodeInputImage[4].5;
	Barcode_IO.In.OUT3_Status:=					     BarcodeInputImage[4].6;
	Barcode_IO.In.Unstable:=					     BarcodeInputImage[5].0;
	Barcode_IO.In.Matching_Level_Unstable:=		     BarcodeInputImage[5].1;
	Barcode_IO.In.ISO_IEC15415_Unstable:=		     BarcodeInputImage[5].2;
	Barcode_IO.In.AIM_DPM_Unstable:=			     BarcodeInputImage[5].3;
	Barcode_IO.In.SAE_AS9132_Unstable:=		 	     BarcodeInputImage[5].4;
	memcpy(ADR(Barcode_IO.In.Matching_Level),ADR(BarcodeInputImage[8]),2);//2bytes 8&9
	memcpy(ADR(Barcode_IO.In.ISO_IEC15415_Grade),ADR(BarcodeInputImage[10]),2);//2bytes 10&11
	memcpy(ADR(Barcode_IO.In.AIM_DPM_Grade),ADR(BarcodeInputImage[12]),2);//2bytes 12&13
	memcpy(ADR(Barcode_IO.In.Read_Result_Code),ADR(BarcodeInputImage[16]),2);//2bytes 16&17
	memcpy(ADR(Barcode_IO.In.Preset_Result_Code),ADR(BarcodeInputImage[18]),2);//2bytes 18&19
	memcpy(ADR(Barcode_IO.In.Register_Preset_Data_Result_Code),ADR(BarcodeInputImage[20]),2);//2bytes 20&21
	memcpy(ADR(Barcode_IO.In.Tune_Result_Code),ADR(BarcodeInputImage[22]),2);//2bytes 22&23
	memcpy(ADR(Barcode_IO.In.EXT_Request_Result_Code),ADR(BarcodeInputImage[30]),2);//2bytes 30&31
	memcpy(ADR(Barcode_IO.In.General_Error_Code),ADR(BarcodeInputImage[32]),2);//2bytes 32&33
	memcpy(ADR(Barcode_IO.In.Result_Data_Ready_Count),ADR(BarcodeInputImage[36]),2);//2bytes 36&37
	memcpy(ADR(Barcode_IO.In.Result_Data_Update_Count),ADR(BarcodeInputImage[38]),2);//2bytes 38&39
	memcpy(ADR(Barcode_IO.In.Result_Data_Size),ADR(BarcodeInputImage[42]),2);//2bytes 42&43
	memset(ADR(Barcode_IO.In.Result_Data),0,SIZEOF(Barcode_IO.In.Result_Data));
	memcpy(ADR(Barcode_IO.In.Result_Data),ADR(BarcodeInputImage[44]),MIN(Barcode_IO.In.Result_Data_Size,128));
END_ACTION
