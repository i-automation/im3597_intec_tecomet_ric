//Mapp the IO into a struct. 
// - The EDS file has all this information, but mapping the IO to vars is outside the scope of what the the IF10D1-1 and DTM do
// - B&R BOOL type is 1BYTE in length which does not map directly to the single bits from the input/output image
// - The Keyance input/output images have padding gaps, preventing looping through the struct cleanly
// - All this means it's best just to map it manually...

ACTION SetOutputs: 
	BarcodeOutputImage[0].1:= Barcode_IO.Out.Result_Data_Latch;
	BarcodeOutputImage[0].7:= Barcode_IO.Out.Error_Clear;
	BarcodeOutputImage[1].0:= Barcode_IO.Out.Read_Request;
	BarcodeOutputImage[1].1:= Barcode_IO.Out.Preset_Request;
	BarcodeOutputImage[1].2:= Barcode_IO.Out.Register_Preset_Data_Request;
	BarcodeOutputImage[1].3:= Barcode_IO.Out.Tune_Request;
	BarcodeOutputImage[2].0:= Barcode_IO.Out.Read_Complete_Clear;
	BarcodeOutputImage[2].1:= Barcode_IO.Out.Preset_Complete_Clear;
	BarcodeOutputImage[2].2:= Barcode_IO.Out.RegstrPreset_Data_Complete_Clear;
	BarcodeOutputImage[2].3:= Barcode_IO.Out.Tune_Complete_Clear;
	BarcodeOutputImage[2].7:= Barcode_IO.Out.EXT_Request_Complete_Clear;
	memcpy(ADR(BarcodeOutputImage[4]),ADR(Barcode_IO.Out.Bank_Number),2); //Bytes 4&5
	memcpy(ADR(BarcodeOutputImage[10]),ADR(Barcode_IO.Out.User_Data_Size),2); //Bytes 10&11
	memcpy(ADR(BarcodeOutputImage[12]),ADR(Barcode_IO.Out.User_Data),1);
END_ACTION
