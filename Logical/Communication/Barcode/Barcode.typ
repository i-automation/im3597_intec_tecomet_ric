
TYPE
	Barcode_IO_Out_typ : 	STRUCT 
		Result_Data_Latch : BOOL;
		Error_Clear : BOOL;
		Read_Request : BOOL;
		Preset_Request : BOOL;
		Register_Preset_Data_Request : BOOL;
		Tune_Request : BOOL;
		Read_Complete_Clear : BOOL;
		Preset_Complete_Clear : BOOL;
		RegstrPreset_Data_Complete_Clear : BOOL;
		Tune_Complete_Clear : BOOL;
		EXT_Request_Complete_Clear : BOOL;
		Bank_Number : UINT;
		User_Data_Size : UINT;
		User_Data : USINT;
	END_STRUCT;
	Barcode_IO_In_typ : 	STRUCT 
		Error : BOOL;
		Result_Data_Available : BOOL;
		Result_Data_Strobe : BOOL;
		Buffer_Overflow_Error : BOOL;
		General_Error : BOOL;
		BUSY : BOOL;
		TRG_BUSY : BOOL;
		LOCK_BUSY : BOOL;
		MODE_BUSY : BOOL;
		ERR_BUSY : BOOL;
		Read_Complete : BOOL;
		Preset_Complete : BOOL;
		Register_Preset_Data_Complete : BOOL;
		Tune_Complete : BOOL;
		EXT_Request_Complete : BOOL;
		Read_Failure : BOOL;
		Preset_Failure : BOOL;
		Register_Preset_Data_Failure : BOOL;
		Tune_Failure : BOOL;
		EXT_Request_Failure : BOOL;
		IN1_Status : BOOL;
		IN2_Status : BOOL;
		OUT1_Status : BOOL;
		OUT2_Status : BOOL;
		OUT3_Status : BOOL;
		Unstable : BOOL;
		Matching_Level_Unstable : BOOL;
		ISO_IEC15415_Unstable : BOOL;
		AIM_DPM_Unstable : BOOL;
		SAE_AS9132_Unstable : BOOL;
		Matching_Level : UINT;
		ISO_IEC15415_Grade : UINT;
		AIM_DPM_Grade : UINT;
		Read_Result_Code : UINT;
		Preset_Result_Code : UINT;
		Register_Preset_Data_Result_Code : UINT;
		Tune_Result_Code : UINT;
		EXT_Request_Result_Code : UINT;
		General_Error_Code : UINT;
		Result_Data_Ready_Count : UINT;
		Result_Data_Update_Count : UINT;
		Result_Data_Size : UINT;
		Result_Data : STRING[128];
	END_STRUCT;
	Barcode_IO_typ : 	STRUCT 
		In : Barcode_IO_In_typ;
		Out : Barcode_IO_Out_typ;
	END_STRUCT;
	States_enm : 
		(
		IDLE,
		SCANNING,
		SCANNED,
		ERROR,
		RESET
		);
END_TYPE
