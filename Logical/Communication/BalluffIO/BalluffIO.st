PROGRAM _CYCLIC

	//Module 0
	IO_ModuleOK[7]:= (BalluffChannelStatus[0] = CHANNEL_STAT_OK);
	i_Anvil_Plt_Clp_L_Latched_2023:= 	BalluffInputImage[0].0;
	i_Anvil_Plt_Clp_L_Unlatched_2024:= 	BalluffInputImage[0].1;
	i_Anvil_Plt_Clp_R_Latched_2025:= 	BalluffInputImage[0].2;
	i_Anvil_Plt_Clp_R_Unatched_2026:= 	BalluffInputImage[0].3;
	i_Anvil_Plate_Presence_2027:= 		BalluffInputImage[0].4;
	i_Main_Air_Pressure_OK_2028:= 		BalluffInputImage[0].5;
	i_Press_Support_Horz_Ext_2029:= 	BalluffInputImage[0].6;
	i_Press_Support_Horz_Ret_2030:=		BalluffInputImage[0].7;
	i_Press_Support_Vert_Up_2031:= 		BalluffInputImage[0].8;
	i_Press_Support_Vert_Down_2032:= 	BalluffInputImage[0].9;
	//Spare := 							BalluffInputImage[0].10;
	//Spare := 							BalluffInputImage[0].11;
	//Spare := 							BalluffInputImage[0].12;
	//Spare := 							BalluffInputImage[0].13;
	//Spare := 							BalluffInputImage[0].14;
	//Spare := 							BalluffInputImage[0].15;
	
	//Module 1
	IO_ModuleOK[8]:= (BalluffChannelStatus[1] = CHANNEL_STAT_OK);
	i_Rivet_Nest_Horz_Ext_212x[0]:= 		BalluffInputImage[1].0;
	i_Rivet_Nest_Horz_Ret_212x[0]:= 		BalluffInputImage[1].1;
	i_Rivet_Nest_Horz_Ext_212x[1]:= 		BalluffInputImage[1].2;
	i_Rivet_Nest_Horz_Ret_212x[1]:= 		BalluffInputImage[1].3;
	i_Rivet_In_Rail_Presence_212x[0]:= 	BalluffInputImage[1].4;
	i_Rivet_In_Rail_Presence_212x[1]:= 	BalluffInputImage[1].5;
	i_Rivet_Press_Vac_Ack_2129:= 		BalluffInputImage[1].6;
	i_Rivet_Head_Stopper_Latch_2130:= 	BalluffInputImage[1].7;
	i_Rivet_Head_Stopper_Unl_2131:= 	BalluffInputImage[1].8;
	i_Rivet_Singulator_Ext_213x[0]:= 	BalluffInputImage[1].9;
	i_Rivet_Singulator_Ret_213x[0]:= 	BalluffInputImage[1].10;
	i_Rivet_Singulator_Ext_213x[1]:= 	BalluffInputImage[1].11;
	i_Rivet_Singulator_Ret_213x[1]:= 	BalluffInputImage[1].12;
	//Spare	:= 							BalluffInputImage[1].13;
	//Spare := 							BalluffInputImage[1].14;
	//Spare := 							BalluffInputImage[1].15;
	
END_PROGRAM
