
TYPE
	UDT_Festo_YJKP_Acyclic_Write_Res : 	STRUCT 
		Service_ID : BYTE; (*Service ID related to the request sent.*)
		Message_ID : UDINT; (*Message ID related to the request sent.*)
		Data_Length : UDINT; (*must be 0x04*)
		Ack : BYTE; (*0x00 OK
0x01 Service not supported
0x02 Data length invalid
0x03 Object index invalid
0x04 Access type invalid
0x05 Data type invalid
0x06 Value invalid
0x07 REad type invalid
0x08 Write type invalid
0x09 Values invalid*)
		Write_Type : UDINT; (*write type related to the request sent.*)
		Index : UINT; (*Index of object to be written*)
		Sub_Index : USINT; (*Sub-Index of object to be written*)
		Data_Type : USINT; (*Data Type of the Object to be written
0x01 = BOOL
0x02 = SINT
0x03 = USINT
0x04 = INT
0x05 = UINT
0x06 = DINT
0x07 = UDINT
0x08 = BYTE
0x09 = WORD
0x0A = DWORD
0x0B = REAL*)
		Reserved : ARRAY[0..85]OF USINT;
	END_STRUCT;
	UDT_Festo_YJKP_Acyclic_Write_Req : 	STRUCT 
		Service_ID : BYTE; (*0x11 = Write object acyclic*)
		Message_ID : UDINT; (*Free usable message ID. To be matched with message ID on the response*)
		Data_Length : UDINT; (*0x04 + length of data in data bytes*)
		Ack : BYTE; (*set to 0x00*)
		Write_Type : UDINT; (*set to 0000*)
		Index : UINT; (*Index of object to be written*)
		Sub_Index : USINT; (*Sub-Index of object to be written*)
		Data_Type : USINT; (*Data Type of the Object to be written
0x01 = BOOL
0x02 = SINT
0x03 = USINT
0x04 = INT
0x05 = UINT
0x06 = DINT
0x07 = UDINT
0x08 = BYTE
0x09 = WORD
0x0A = DWORD
0x0B = REAL*)
		Data_Byte : ARRAY[0..85]OF BYTE;
	END_STRUCT;
	UDT_Festo_YJKP_Cyclic_In : 	STRUCT 
		Service_ID : BYTE; (*Service ID related to the request sent.*)
		Data_Length : UINT; (*must be 0x2B,  = 43*)
		Ack : BYTE; (*0x00 OK
0x01 Service not supported
0x02 Data length invalid
0x03 Object index invalid
0x04 Access type invalid
0x05 Data type invalid
0x06 Value invalid
0x07 REad type invalid
0x08 Write type invalid
0x09 Values invalid*)
		Message_ID : USINT; (*Message ID related to the request sent.*)
		Status_Manual_Mode : BOOL;
		Status_Auto_Mode : BOOL;
		Status_Homing_Required : BOOL;
		Status_Program_Loaded : BOOL;
		Status_Step_Mode : BOOL;
		Status_In_Operation : BOOL;
		Status_Step_Done : BOOL;
		Status_Result_OK : BOOL;
		Status_Result_NOK : BOOL;
		Status_Press_Enabled : BOOL;
		Status_Tared : BOOL;
		Status_Ready : BOOL;
		Status_Servo_Press_Ready : BOOL;
		Status_Error : BOOL;
		Status_Warning : BOOL;
		Status_Safety : BOOL;
		Status_Hardware_Config : BOOL;
		Status_Logging_USB : BOOL;
		Status_Logging_SD_Card : BOOL;
		Status_Reserved : ARRAY[0..12]OF BOOL;
		Comm_Mode : BYTE; (*0 = Acyclic, 1 = Cyclic*)
		Offset_Force_Sensor : DINT; (*100 = 1 N*)
		Motion_Mode : BYTE; (*0 = jog, 1 = move absolute, 2 = move relative*)
		Motion_Velocity : DINT; (*100 = 1 mm/s*)
		Motion_Pos_Distance : DINT; (*100 = 1 mm*)
		Loaded_Program : UINT;
		Digital_Outputs : BYTE; (*Bit 0 = Output 1, Bit 7 = Output 8*)
		Actual_Pos : DINT; (*100 = 1 mm*)
		Actual_Force : DINT; (*100 = 1 N*)
		Actual_Velocity : DINT; (*100 = 1 mm/s*)
		Maximum_Pos : DINT; (*100 = 1 mm*)
		Maximum_Force : DINT; (*100 = 1 N*)
		NOK_Reason : WORD;
	END_STRUCT;
	UDT_Festo_YJKP_Cyclic_Out : 	STRUCT 
		Service_ID : BYTE; (*0x12,  = 18 for cyclic comm*)
		Data_Length : UINT; (*must be 0x2B,  = 43*)
		Ack : BYTE; (*set to 0x00*)
		Message_ID : USINT; (*Free usable message ID. To be matched with message ID on the response*)
		Command_Manual_Mode : BOOL;
		Command_Auto_Mode : BOOL;
		Command_Start_Homing : BOOL;
		Command_Start_Press : BOOL;
		Command_Abort_Press : BOOL; (*Low Active*)
		Command_Load_Program : BOOL;
		Command_Reset_Error : BOOL;
		Command_Enable_Press : BOOL;
		Command_Tare : BOOL;
		Command_Move : BOOL;
		Command_Stop_Move : BOOL;
		Command_Jog_Pos : BOOL;
		Command_Jog_Neg : BOOL;
		Command_Load_Hardware_Config : BOOL;
		Command_Logging_USB : BOOL;
		Command_Logging_SD_Card : BOOL;
		Command_System_Reset : BOOL;
		Command_Reset_Statistic : BOOL;
		Command_Step_Mode : BOOL;
		Command_Reserved : ARRAY[0..12]OF BOOL;
		Comm_Mode : BYTE; (*0 = Acyclic, 1 = Cyclic*)
		Offset_Force_Sensor : DINT; (*100 = 1 N*)
		Motion_Mode : USINT; (*0 = jog, 1 = move absolute, 2 = move relative*)
		Motion_Velocity : DINT; (*100 = 1 mm/s*)
		Motion_Pos_Distance : DINT; (*100 = 1 mm*)
		Program_Selection : UINT;
		Digital_Inputs : BYTE; (*Bit 0 = Input 1, Bit 7 = Input 8*)
	END_STRUCT;
END_TYPE

(**)
(*Old types (from modbus implementation straight to drive)*)

TYPE
	FestoPress_typ : 	STRUCT 
		control : control_typ;
		status : status_typ;
	END_STRUCT;
	control_typ : 	STRUCT 
		EnableDrive : BOOL;
		Stop : BOOL;
		ReleaseBrake : BOOL;
		FaultReset : BOOL;
		LockSoftwareAccess : BOOL;
		OperatingMode1 : BOOL;
		OperatingMode2 : BOOL;
		Halt : BOOL;
		StartPositioning : BOOL;
		Home : BOOL;
		JogPos : BOOL;
		JogNeg : BOOL;
		TeachActPos : BOOL; (*On Falling Edge, drops current actual pos into nominal pos register for currently selected record (if in record mode)*)
		ClearRemainingPos : BOOL; (*While in halt state, a rising edge causes the positioning task to be deleted and a transition to READY state*)
		Relative : BOOL; (*=1: position is treated as relative position; =0: position is treated as an absolute position*)
		ControlMode1 : BOOL;
		ControlMode2 : BOOL;
		Setpoint1 : USINT; (*if position mode selected = velocity as % of the base value; if force mode selected = torque ramp*)
		Setpoint2 : REAL; (*if position mode selected = position in positioning unit; if force mode selected = torque setpoint as % of nominal torque*)
	END_STRUCT;
	status_typ : 	STRUCT 
		DriveEnabled : BOOL;
		StopActive : BOOL;
		Warning : BOOL;
		Fault : BOOL;
		EnableReady : BOOL;
		SoftwareAccessLocked : BOOL; (*=1: device control through fieldbus not possible; =0: device control through fieldbus possible*)
		OperatingMode1 : BOOL;
		OperatingMode2 : BOOL;
		HaltActive : BOOL;
		StartAcknowledged : BOOL; (*=1: start executed (homing/jogging/positioning); =0: ready for start*)
		MotionComplete : BOOL; (*=1: positioning job complete; =0: positioning job active; set after device switched on*)
		TeachAcknowledged : BOOL; (*=1: teaching carried out - actual value has been transferred; =0: ready for teaching*)
		AxisMoving : BOOL; (*Homing/Moving (at velocity)*)
		FollowingError : BOOL; (*On Falling Edge, drops current actual pos into nominal pos register for currently selected record (if in record mode)*)
		StandstillControl : BOOL; (*=1: axis has left tolerance window after motion complete; =0: axis still within tolerance window after motion complete*)
		Homed : BOOL;
		Relative : BOOL;
		ControlMode1 : BOOL;
		ControlMode2 : BOOL;
		Actual1 : USINT; (*if position mode selected = velocity as % of the base value; if force mode selected = torque ramp*)
		Actual2 : REAL; (*if position mode selected = position in positioning unit; if force mode selected = position in positioning unit*)
	END_STRUCT;
	State_enum : 
		(
		IDLE,
		MOVE_ABSOLUTE,
		MOVE_ADDITIVE,
		MOVE_ACTIVE,
		STOPPED,
		ERROR
		);
END_TYPE
