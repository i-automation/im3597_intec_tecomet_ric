
ACTION PressForwarding:
	
	//Command forwarding
	FestoPress.control.EnableDrive:= Press.Cmd.On;
	FestoPress.control.Stop:= Press.Cmd.Stop;
	FestoPress.control.ReleaseBrake:= Press.Cmd.ReleaseBrake;
	FestoPress.control.FaultReset:= Press.Cmd.ErrAck;
	FestoPress.control.Halt:= Press.Cmd.Halt;
	FestoPress.control.Home:= Press.Cmd.Home;
	FestoPress.control.JogPos:= Press.Cmd.JogPositive;
	FestoPress.control.JogNeg:= Press.Cmd.JogNegative;
	
	CASE State OF
		IDLE: //waiting for a command
			IF Press.Cmd.MoveAbsolute AND Press.Status.Idle THEN
				State:= MOVE_ABSOLUTE;
			ELSIF Press.Cmd.MoveAdditive AND Press.Status.Idle THEN
				State:= MOVE_ADDITIVE;
			END_IF;
				
		MOVE_ABSOLUTE:
			Press.Cmd.MoveAbsolute:= FALSE;
			FestoPress.control.Relative:= FALSE;
			FestoPress.control.StartPositioning:= TRUE;
			IF Press.Status.MoveActive THEN
				State
			END_IF;
			
		MOVE_ADDITIVE:
			Press.Cmd.MoveAdditive:= FALSE;
			FestoPress.control.Relative:= TRUE;
			FestoPress.control.StartPositioning:= TRUE;
		
		MOVE_ACTIVE:
			IF Press.Status.MoveComplete THEN
				FestoPress.control.StartPositioning:= FALSE;
			END_IF;
		
		STOPPED:
			//might need to include a clear remaining position command here too
			Press.Cmd.MoveAbsolute:= FALSE;
			Press.Cmd.MoveAdditive:= FALSE;
			FestoPress.control.StartPositioning:= FALSE;
			IF NOT Press.Status.StopActive AND NOT Press.Status.HaltActive THEN
				State:= IDLE;
			END_IF;
			
		ERROR:
			//might need to include a clear remaining position command here too
			Press.Cmd.MoveAbsolute:= FALSE;
			Press.Cmd.MoveAdditive:= FALSE;
			FestoPress.control.StartPositioning:= FALSE;
			IF NOT Press.Status.Error THEN
				State:= IDLE;
			END_IF;
	END_CASE
	
	IF Press.Status.Error THEN
		State:= ERROR;
	ELSIF Press.Status.StopActive OR Press.Status.HaltActive THEN
		State:= STOPPED;
	END_IF;
	
	
	//Status forwarding
	Press.Status.ReadyToPowerOn:= FestoPress.status.EnableReady;
	Press.Status.On:= FestoPress.status.DriveEnabled;
	Press.Status.Homed:= FestoPress.status.Homed;
	Press.Status.MoveActive:= FestoPress.status.AxisMoving; //homing/jogging/positioning
	Press.Status.MoveComplete:= FestoPress.status.MotionComplete;
	Press.Status.StopActive:= FestoPress.status.StopActive;
	Press.Status.HaltActive FestoPress.status.HaltActive;
	Press.Status.Warning:= FestoPress.status.Warning;
	Press.Status.Error:= FestoPress.status.Error OR FestoPress.status.FollowingError OR FestoPress.status.StandstillControl; //may want to break up at some point
	
	Press.Status.Idle:= NOT Press.Status.StopActive AND NOT Press.Status.HaltActive AND Press.Status.On AND Press.Status.Homed AND NOT Press.Status.MoveActive AND NOT Press.Status.Error;
	
	
	//Control Mode parameters
	IF Press.Par.ControlMode = POSITION THEN// 0
		FestoPress.control.ControlMode1:= FALSE;
		FestoPress.control.ControlMode2:= FALSE;
		FestoPress.control.Setpoint1:= Press.Par.Velocity / BaseVelocity) * 100; //NEED TO KNOW BASE VELOCITY PARAMETER
		FestoPress.control.Setpoint2:= Press.Par.Position;
		Press.Data.Velocity := (FestoPress.status.Actual1 / 100) * BaseVelocity; //NEED TO KNOW BASE VELOCITY PARAMETER
		Press.Data.Position := FestoPress.status.Actual2;
	ELSIF Press.Par.ControlMode = FORCE THEN //1
		FestoPress.control.ControlMode1:= TRUE;
		FestoPress.control.ControlMode2:= FALSE;
		FestoPress.control.Setpoint1:= Press.Par.TorqueRamp / BaseForceRamp) * 100; //NEED TO KNOW BASE FORCE RAMP PARAMETER
		FestoPress.control.Setpoint2:= Press.Par.Torque / BaseForce) * 100; //NEED TO KNOW BASE FORCE PARAMETER
		Press.Data.Torque := (FestoPress.status.Actual1 / 100) * RatedTorque; //NEED TO KNOW RATED TORQUE PARAMETER
		Press.Data.Position := FestoPress.status.Actual2;
	END_IF;
			
	
	//Unused commands/statuses
//	FestoPress.control.LockSoftwareAccess;
//	FestoPress.status.SoftwareAccessLocked;
//	FestoPress.control.OperatingMode1;
//	FestoPress.control.OperatingMode2;
//	FestoPress.status.OperatingMode1;
//	FestoPress.status.OperatingMode2;
//	FestoPress.control.TeachActPos;
//	FestoPress.status.TeachAcknowledged;
//	FestoPress.status.ControlMode1;
//	FestoPress.status.ControlMode2;
//	FestoPress.control.ClearRemainingPos;
//	FestoPress.status.Relative:= statuswords.16;
//	FestoPress.status.StartAcknowledged; //goes back to 0 when ready for command
	
END_ACTION
