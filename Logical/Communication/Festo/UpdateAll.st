//Drive Command/Status Word Configuration
//8 bytes output - CCON(1), CPOS(2), CDIR(3), Setpoint value 1(4), Setpoint value 2(5-8)
//8 bytes input - SCON(1), SPOS(2), SDIR(3), Actual value 1(4), Actual value 2(5-8)
//CCON B0: 		Enable Drive -> 2^0 (+1)
//CCON B1: 		Stop (1=enable operation, 0=stop active - cancel positioning job and stop with emergency ramp) -> 2^1 (+2)
//CCON B2: 		Open Brake (1=release brake, 0=activate brake) -> 2^2 (+4)
//CCON B3: 		Reset Fault -> 2^3 (+8)
//CCON B4: 		RESERVED (0) -> 2^4 (+16)
//CCON B5: 		Lock Software Access - controls access to integrated parameterization interface of the drive (1=interface blocked, 0=can modify parameters or control inputs) -> 2^5 (+32)
//CCON B6/7: 	Select Operating Mode - Record Selection or Direct Mode (B6=0/B7=0: record selection mode, B6=1/B7=0: direct mode) -> 2^6 & 2^7 (+64 & +128)
//CPOS B0: 		Halt (1=halt not requested, 0=halt activated - cancel positioning job and halt with braking ramp) -> 2^8 (+256)
//CPOS B1: 		Start Positioning Task - rising edge tranfers current nominal data and starts a positioning process -> 2^9 (+512)
//CPOS B2: 		Start Homing - rising edge starts homing with set parameters -> 2^10 (+1024)
//CPOS B3: 		Jog Positive - jogs with rising edge, stops with falling edge -> 2^11 (+2048)
//CPOS B4: 		Jog Negative - jogs with rising edge, stops with falling edge -> 2^12 (+4096)
//CPOS B5: 		Teach Actual Value - on a falling edge, the current actual value is transferred to the nominal value register of the currently addressed positioning record -> 2^13 (+8192)
//CPOS B6: 		Clear Remaining Position - in the halt state, a rising edge causes the positioning task to be deleted and a transition to the READY state -> 2^14 (+16384)
//CPOS B7: 		RESERVED (0) -> 2^15 (+32768)
//CDIR B0: 		Absolute/Relative (1=nominal value is relative to the last nominal value, 0=nominal value is absolute) -> 2^16 (+65536)
//CDIR B1/B2: 	Control Mode (B1=0/B2=0: position control, B1=1/B2=0: force mode, B1=0/B2=1: velocity control) -> 2^17 & 2^18 (+131072 & +262144)
//CDIR B3/B4: 	Camming -> 2^19 & 2^20 (+524288 & +1048576)
//CDIR B5/B6: 	Camming -> 2^21 & 2^22 (+2097152 & +
//CDIR B7: 		Camming (1=execute cam function, 0=normal job) -> 2^23
//Set Val 1:	Velocity - velocity as % of the base value, or if force mode selected above, then this is torque ramp -> 2^24 - 2^31
//Set Val 2:	Position - position in positioning unit, or if force mode selected above then torque setpoint as % of nominal torque -> 2^32 - 2^63
//SCON B0: 		Drive Enabled -> 2^1
//SCON B1: 		Operation Enabled (1=positioning possible, 0=stop active) -> 2^1
//SCON B2: 		Warning
//SCON B3: 		Fault
//SCON B4: 		Enable Ready
//SCON B5: 		Software Access (1=device control through fieldbus not possible, 0=device control through fieldbus possible)
//SCON B6/7: 	Operating Mode (0/0=record selection, 1/0=direct mode)
//SPOS B0: 		Halt (1=halt not active - axis can be moved, 0=halt active)
//SPOS B1: 		Start Acknowledged (1=start executed - homing/jogging/positioning, 0=ready for start - homing/jogging/positioning)
//SPOS B2: 		Motion Complete (1=positioning job completed, 0=positioning job active) - set after device switched on
//SPOS B3: 		Teach Acknowledged (1=teaching carried out - actual value has been transferred, 0=ready for teaching) - other status available depending on setting
//SPOS B4: 		AxisMoving (1=velocity of axis >= limit value, 0=velocity of axis < limit value)
//SPOS B5: 		Drag/Deviation Error (1=following error active, 0=no following error)
//SPOS B6: 		Standstill Control (1=axis has left tolerance window after Motion Complete, 0=after Motion Complete axis remains in tolerance window)
//SPOS B7: 		Axis Referenced (1=homed, 0=not homed)
//SDIR B0: 		Absolute/Relative (1=nominal value is relative to the last nominal value, 0=nominal value is absolute)
//SDIR B1/2: 	Control Mode (0/0=position control, 1/0=force mode, 0/1=velocity control)	
//SDIR B3/4: 	Camming
//SDIR B5/6: 	Camming
//SDIR B7: 		Camming (1=cam executed, 0=normal job)
//Act Val 1:	Velocity - velocity as % of the base value, or if force mode selected above, then this is torque ramp
//Act Val 2:	Position - position in positioning unit, or if force mode selected above then position same


ACTION UpdateAll: 
	controlwords.0:= FestoPress.control.EnableDrive;
	controlwords.1:= FestoPress.control.Stop;
	controlwords.2:= FestoPress.control.ReleaseBrake;
	controlwords.3:= FestoPress.control.FaultReset;
	controlwords.5:= FestoPress.control.LockSoftwareAccess;
	controlwords.6:= FestoPress.control.OperatingMode1;
	controlwords.7:= FestoPress.control.OperatingMode2;
	controlwords.8:= FestoPress.control.Halt;
	controlwords.9:= FestoPress.control.StartPositioning;
	controlwords.10:= FestoPress.control.Home;
	controlwords.11:= FestoPress.control.JogPos;
	controlwords.12:= FestoPress.control.JogNeg;
	controlwords.13:= FestoPress.control.TeachActPos;
	controlwords.14:= FestoPress.control.ClearRemainingPos;
	controlwords.16:= FestoPress.control.Relative;
	controlwords.17:= FestoPress.control.ControlMode1;
	controlwords.18:= FestoPress.control.ControlMode2;
	memcpy(ADR(controlwords) + 3, ADR(FestoPress.control.Setpoint1), 1);
	memcpy(ADR(controlwords) + 4, ADR(FestoPress.control.Setpoint2), 4);
	
	FestoPress.status.DriveEnabled:= statuswords.0;
	FestoPress.status.StopActive:= NOT statuswords.1;
	FestoPress.status.Warning:= statuswords.2; //fault type
	FestoPress.status.Error:= statuswords.3; //fault type
	FestoPress.status.SoftwareAccessLocked:= statuswords.4;
	FestoPress.status.OperatingMode1:= statuswords.5;
	FestoPress.status.OperatingMode2:= statuswords.6;
	FestoPress.status.EnableReady:= statuswords.7;
	FestoPress.status.HaltActive:= NOT statuswords.8;
	FestoPress.status.StartAcknowledged:= statuswords.9; //goes back to 0 when ready for command
	FestoPress.status.MotionComplete:= statuswords.10;
	FestoPress.status.TeachAcknowledged:= statuswords.11;
	FestoPress.status.AxisMoving:= statuswords.12; //homing/jogging/positioning
	FestoPress.status.FollowingError:= statuswords.13; //fault type
	FestoPress.status.StandstillControl:= statuswords.14; //fault type
	FestoPress.status.Homed:= statuswords.15;
	FestoPress.status.Relative:= statuswords.16;
	FestoPress.status.ControlMode1:= statuswords.17;
	FestoPress.status.ControlMode2:= statuswords.18;
	memcpy(ADR(FestoPress.status.Actual1), ADR(statuswords) + 3, 1);
	memcpy(ADR(FestoPress.status.Actual2), ADR(statuswords) + 4, 4);
END_ACTION
