(*FESTO SERVO PRESS CONTROLLER
	CECC-X-M1-YS
	ENET/IP 192.168.0.12 
	
	
	FESTO SERVO PRESS DRIVE
	CMMP-AS
	ETHERNET 192.168.0.19*)

PROGRAM _INIT
	Press.Cmd.ResetComm:= 1;
	v_Festo_Acyclic_Msg_ID:= 123; //just has to be nonzero number
	 
END_PROGRAM

PROGRAM _CYCLIC
	
	//////////////////////////////////////
	////////ETH/IP IMPLEMENTATION/////////
	//////////////////////////////////////
	
	// Festo comm cycle pulse bit
	T_Festo_Comm_Cycle.IN := NOT T_Festo_Comm_Cycle.Q;
	T_Festo_Comm_Cycle.PT := T#50ms;
	T_Festo_Comm_Cycle();
	
	v_Festo_Comm_Pulse := T_Festo_Comm_Cycle.Q;
	
	
	// Local Acyclic input mapping
	memcpy(ADR(i_FestoAcyclicWriteResp.Service_ID), ADR(FestoInput[0]), 1);
	memcpy(ADR(i_FestoAcyclicWriteResp.Message_ID), ADR(FestoInput[1]), 4);
	memcpy(ADR(i_FestoAcyclicWriteResp.Data_Length), ADR(FestoInput[5]), 4);
	memcpy(ADR(i_FestoAcyclicWriteResp.Ack), ADR(FestoInput[9]), 1);
	memcpy(ADR(i_FestoAcyclicWriteResp.Write_Type), ADR(FestoInput[10]), 4);
	memcpy(ADR(i_FestoAcyclicWriteResp.Index), ADR(FestoInput[14]), 2);
	memcpy(ADR(i_FestoAcyclicWriteResp.Sub_Index), ADR(FestoInput[16]), 1);
	memcpy(ADR(i_FestoAcyclicWriteResp.Data_Type), ADR(FestoInput[17]), 1);

	// Local Cyclic input mapping
	memcpy(ADR(i_FestoCyclic.Service_ID), ADR(FestoInput[0]), 1);
	memcpy(ADR(i_FestoCyclic.Data_Length), ADR(FestoInput[1]), 2);
	memcpy(ADR(i_FestoCyclic.Ack), ADR(FestoInput[3]), 1);
	memcpy(ADR(i_FestoCyclic.Message_ID), ADR(FestoInput[4]), 1);
	i_FestoCyclic.Status_Manual_Mode 		:= 	FestoInput[5].0;
	i_FestoCyclic.Status_Auto_Mode 			:= 	FestoInput[5].1;
	i_FestoCyclic.Status_Homing_Required 	:= 	FestoInput[5].2;
	i_FestoCyclic.Status_Program_Loaded		:= 	FestoInput[5].3;
	i_FestoCyclic.Status_Step_Mode	 		:= 	FestoInput[5].4;
	i_FestoCyclic.Status_In_Operation	 	:= 	FestoInput[5].5;
	i_FestoCyclic.Status_Step_Done	 		:= 	FestoInput[5].6;
	i_FestoCyclic.Status_Result_OK	 		:= 	FestoInput[5].7;
	i_FestoCyclic.Status_Result_NOK	 		:= 	FestoInput[6].0;
	i_FestoCyclic.Status_Press_Enabled 		:= 	FestoInput[6].1;
	i_FestoCyclic.Status_Tared		 		:= 	FestoInput[6].2;
	i_FestoCyclic.Status_Ready	 			:= 	FestoInput[6].3;
	i_FestoCyclic.Status_Servo_Press_Ready	:= 	FestoInput[6].4;
	i_FestoCyclic.Status_Error		 		:= 	FestoInput[6].5;
	i_FestoCyclic.Status_Warning	 		:= 	FestoInput[6].6;
	i_FestoCyclic.Status_Safety		 		:= 	FestoInput[6].7;
	i_FestoCyclic.Status_Hardware_Config	:= 	FestoInput[7].0;
	i_FestoCyclic.Status_Logging_USB 		:= 	FestoInput[7].1;
	i_FestoCyclic.Status_Logging_SD_Card	:= 	FestoInput[7].2;
	memcpy(ADR(i_FestoCyclic.Comm_Mode), ADR(FestoInput[9]), 1);
	memcpy(ADR(i_FestoCyclic.Offset_Force_Sensor), ADR(FestoInput[10]), 4);
	memcpy(ADR(i_FestoCyclic.Motion_Mode), ADR(FestoInput[14]), 1);
	memcpy(ADR(i_FestoCyclic.Motion_Velocity), ADR(FestoInput[15]), 4);
	memcpy(ADR(i_FestoCyclic.Motion_Pos_Distance), ADR(FestoInput[19]), 4);
	memcpy(ADR(i_FestoCyclic.Loaded_Program), ADR(FestoInput[23]), 2);
	memcpy(ADR(i_FestoCyclic.Digital_Outputs), ADR(FestoInput[25]), 1);
	memcpy(ADR(i_FestoCyclic.Actual_Pos), ADR(FestoInput[26]), 4);
	memcpy(ADR(i_FestoCyclic.Actual_Force), ADR(FestoInput[30]), 4);
	memcpy(ADR(i_FestoCyclic.Actual_Velocity), ADR(FestoInput[34]), 4);
	memcpy(ADR(i_FestoCyclic.Maximum_Pos), ADR(FestoInput[38]), 4);
	memcpy(ADR(i_FestoCyclic.Maximum_Force), ADR(FestoInput[42]), 4);
	memcpy(ADR(i_FestoCyclic.NOK_Reason), ADR(FestoInput[46]), 2);
	
	// Global Input Mapping
	Press.Status.Error:= i_FestoCyclic.Status_Error;
	Press.Status.Homed:= NOT(i_FestoCyclic.Status_Homing_Required);
	Press.Status.MoveSuccessful:= EDGEPOS(i_FestoCyclic.Status_Result_OK);
	Press.Status.MoveFailed:= EDGEPOS(i_FestoCyclic.Status_Result_NOK);
	Press.Status.Idle:= i_FestoCyclic.Status_Servo_Press_Ready;
	Press.Status.InOperation:= i_FestoCyclic.Status_In_Operation;
	Press.Status.ReadyToPowerOn:= i_FestoCyclic.Status_Ready;
	Press.Status.On:= i_FestoCyclic.Status_Press_Enabled;
	Press.Status.LoadedProgram:= i_FestoCyclic.Loaded_Program;
	Press.Status.ProgramLoaded:= i_FestoCyclic.Status_Program_Loaded;
	Press.Data.Load:= DINT_TO_REAL(i_FestoCyclic.Actual_Force) / 100; //since scalled in 100s N (for now, leaving in N, but may want to change to lb-f to match PLC unit)
	Press.Data.Position:= DINT_TO_REAL(i_FestoCyclic.Actual_Pos) / 100; //since scalled in 100s mm (for now, leaving in mm, but may want to change to inch to match PLC unit)
	Press.Data.Velocity:= DINT_TO_REAL(i_FestoCyclic.Actual_Velocity) / 100; //since scalled in 100s mm (for now, leaving in mm, but may want to change to inch to match PLC unit)
	
	Press.Status.CommOK:= v_Festo_Comm_Mode;
	
	// Reset Festo Comm
	T_Festo_Comm_Reset.IN := Press.Cmd.ResetComm; // OR ((T_Festo_Comm_Reset.ET > 0) AND (NOT T_Festo_Comm_Reset.Q));
	T_Festo_Comm_Reset.PT := T#3s;
	T_Festo_Comm_Reset();
	
	v_Festo_Comm_Reset_Active := T_Festo_Comm_Reset.ET > 0;
	
	IF T_Festo_Comm_Reset.Q THEN
		Press.Cmd.ResetComm := 0;
	END_IF;
	
	IF v_Festo_Comm_Reset_Active THEN
		v_Festo_Comm_Mode :=0;
		v_Festo_Comm_Mode_Switch_Active := 0;
		//////
		////	fill festo output with 0
		//// FLL 0 o_Festo_Acyclic 1 FLL 0 o_Festo_Cyclic 1
		///???????????????????????????????????????????????????????????????????????????????????????????????????????????????
		//check the following lines
		o_FestoAcyclicWriteReq.Service_ID := 0;
		o_FestoAcyclicWriteReq.Message_ID := 0;
		o_FestoAcyclicWriteReq.Data_Length := 0;
		o_FestoAcyclicWriteReq.Ack := 0;
		o_FestoAcyclicWriteReq.Write_Type := 0;
		o_FestoAcyclicWriteReq.Index := 0;
		o_FestoAcyclicWriteReq.Sub_Index := 0;
		o_FestoAcyclicWriteReq.Data_Type := 0;
		o_FestoAcyclicWriteReq.Data_Byte[0] := 0;
		
		
		o_FestoCyclic.Service_ID := 0;
		o_FestoCyclic.Data_Length := 0;
		o_FestoCyclic.Ack := 0;
		o_FestoCyclic.Comm_Mode := 0;
	END_IF;
	
	
	// After comm reset, in acyclic mode, send msg to switch to cyclic mode, then wait for response
	
	IF NOT v_Festo_Comm_Reset_Active AND NOT v_Festo_Comm_Mode THEN
		o_FestoAcyclicWriteReq.Service_ID := 16#11;
		o_FestoAcyclicWriteReq.Message_ID := v_Festo_Acyclic_Msg_ID;
		o_FestoAcyclicWriteReq.Data_Length := 5;
		o_FestoAcyclicWriteReq.Ack := 0;
		o_FestoAcyclicWriteReq.Write_Type := 0;
		o_FestoAcyclicWriteReq.Index := 16#2002;
		o_FestoAcyclicWriteReq.Sub_Index := 0;
		o_FestoAcyclicWriteReq.Data_Type := 16#08;
		o_FestoAcyclicWriteReq.Data_Byte[0] := 1;
		v_Festo_Comm_Mode_Switch_Active := 1;
	END_IF;
	
	IF v_Festo_Comm_Mode_Switch_Active AND 
		i_FestoAcyclicWriteResp.Service_ID = 16#11 AND
		i_FestoAcyclicWriteResp.Message_ID = v_Festo_Acyclic_Msg_ID AND
		i_FestoAcyclicWriteResp.Data_Length = 4 AND
		i_FestoAcyclicWriteResp.Ack = 0 AND
		i_FestoAcyclicWriteResp.Write_Type = 0 AND
		i_FestoAcyclicWriteResp.Index = 16#2002 AND
		i_FestoAcyclicWriteResp.Sub_Index = 0 AND
		i_FestoAcyclicWriteResp.Data_Type = 8 THEN
		
		v_Festo_Comm_Mode_Switch_Active := 0;
		v_Festo_Comm_Mode := 1;
	END_IF;
	
	
	// Festo Cyclic Comm
	IF v_Festo_Comm_Pulse AND v_Festo_Comm_Mode AND (i_FestoCyclic.Message_ID = o_FestoCyclic.Message_ID) THEN
		o_FestoCyclic.Message_ID := o_FestoCyclic.Message_ID + 1;
		IF o_FestoCyclic.Message_ID >= 100 THEN 
			o_FestoCyclic.Message_ID := 1;
		END_IF;
	END_IF;
	
	T_Festo_Comm_Cyclic_Check( IN := (i_FestoCyclic.Message_ID <> o_FestoCyclic.Message_ID) AND NOT v_Festo_Comm_Mode_Switch_Active, PT := T#2s);
	
	IF T_Festo_Comm_Cyclic_Check.Q THEN
		v_Festo_Comm_Mode := 0;
	END_IF;
	
	IF v_Festo_Comm_Mode THEN
		o_FestoCyclic.Service_ID := 16#12;
		o_FestoCyclic.Data_Length := 16#15;
		o_FestoCyclic.Ack := 0;
		o_FestoCyclic.Comm_Mode := 1;
	END_IF;
	
	
	// Updating press move modes
	IF EDGEPOS(Press.Cmd.JogNeg) OR EDGEPOS(Press.Cmd.JogPos) THEN
		Press.Par.MoveType:= PRESS_MOVE_JOG;
		Press.Par.Mode:= PRESS_MODE_MANUAL;
	END_IF;
	IF Press.Cmd.Home OR Press.Cmd.Move THEN
		Press.Par.Mode:= PRESS_MODE_MANUAL;
	END_IF;
	IF Press.Cmd.StartCycle THEN
		Press.Par.Mode:= PRESS_MODE_AUTO;
	END_IF;
	
	
	// Global Output Mapping
	o_FestoCyclic.Command_Enable_Press:= Press.Cmd.On;
	o_FestoCyclic.Command_Start_Homing:= Press.Cmd.Home;
	o_FestoCyclic.Command_Start_Press:= Press.Cmd.StartCycle;
	o_FestoCyclic.Command_Move:= Press.Cmd.Move;
	o_FestoCyclic.Command_Jog_Pos:= Press.Cmd.JogPos;
	o_FestoCyclic.Command_Jog_Neg:= Press.Cmd.JogNeg;
	o_FestoCyclic.Command_Abort_Press:= NOT(Press.Cmd.Abort);
	o_FestoCyclic.Command_Stop_Move:= Press.Cmd.Stop;
	o_FestoCyclic.Command_Load_Program:= Press.Cmd.LoadProgram;
	o_FestoCyclic.Command_Reset_Error:= Press.Cmd.ErrAck;
	
	
	o_FestoCyclic.Motion_Velocity:= REAL_TO_DINT(Press.Par.Velocity * 100); //since scalled in 100s mm (for now, leaving in mm, but may want to change to inch to match PLC unit)
	o_FestoCyclic.Motion_Pos_Distance:= REAL_TO_DINT(Press.Par.Position * 100); //since scalled in 100s mm (for now, leaving in mm, but may want to change to inch to match PLC unit)
	o_FestoCyclic.Program_Selection:= Press.Par.Program;
	o_FestoCyclic.Motion_Mode:= Press.Par.MoveType;
	
	CASE Press.Par.Mode OF
		PRESS_MODE_OFF:
			o_FestoCyclic.Command_Manual_Mode:= FALSE;
			o_FestoCyclic.Command_Auto_Mode:= FALSE;
		
		PRESS_MODE_MANUAL:
			o_FestoCyclic.Command_Manual_Mode:= TRUE;
			o_FestoCyclic.Command_Auto_Mode:= FALSE;
		
		PRESS_MODE_AUTO:
			o_FestoCyclic.Command_Manual_Mode:= FALSE;
			o_FestoCyclic.Command_Auto_Mode:= TRUE;
	END_CASE;
	
	// Local Output mapping
	IF v_Festo_Comm_Mode THEN
		memcpy(ADR(FestoOutput[0]), ADR(o_FestoCyclic.Service_ID), 1);
		memcpy(ADR(FestoOutput[1]), ADR(o_FestoCyclic.Data_Length), 2);
		memcpy(ADR(FestoOutput[3]), ADR(o_FestoCyclic.Ack), 1);
		memcpy(ADR(FestoOutput[4]), ADR(o_FestoCyclic.Message_ID), 1);
		FestoOutput[5].0 := o_FestoCyclic.Command_Manual_Mode;
		FestoOutput[5].1 := o_FestoCyclic.Command_Auto_Mode;
		FestoOutput[5].2 := o_FestoCyclic.Command_Start_Homing;
		FestoOutput[5].3 := o_FestoCyclic.Command_Start_Press;
		FestoOutput[5].4 := o_FestoCyclic.Command_Abort_Press; //abort during a press = 0
		FestoOutput[5].5 := o_FestoCyclic.Command_Load_Program;
		FestoOutput[5].6 := o_FestoCyclic.Command_Reset_Error;
		FestoOutput[5].7 := o_FestoCyclic.Command_Enable_Press;
		FestoOutput[6].0 := o_FestoCyclic.Command_Tare;
		FestoOutput[6].1 := o_FestoCyclic.Command_Move;
		FestoOutput[6].2 := o_FestoCyclic.Command_Stop_Move;
		FestoOutput[6].3 := o_FestoCyclic.Command_Jog_Pos;
		FestoOutput[6].4 := o_FestoCyclic.Command_Jog_Neg;
		FestoOutput[6].5 := o_FestoCyclic.Command_Load_Hardware_Config;
		FestoOutput[6].6 := o_FestoCyclic.Command_Logging_USB;
		FestoOutput[6].7 := o_FestoCyclic.Command_Logging_SD_Card;
		FestoOutput[7].0 := o_FestoCyclic.Command_System_Reset;
		FestoOutput[7].1 := o_FestoCyclic.Command_Reset_Statistic;
		FestoOutput[7].2 := o_FestoCyclic.Command_Step_Mode;
		memcpy(ADR(FestoOutput[9]), ADR(o_FestoCyclic.Comm_Mode), 1);
		memcpy(ADR(FestoOutput[10]), ADR(o_FestoCyclic.Offset_Force_Sensor), 4);
		memcpy(ADR(FestoOutput[14]), ADR(o_FestoCyclic.Motion_Mode), 1);
		memcpy(ADR(FestoOutput[15]), ADR(o_FestoCyclic.Motion_Velocity), 4);
		memcpy(ADR(FestoOutput[19]), ADR(o_FestoCyclic.Motion_Pos_Distance), 4);
		memcpy(ADR(FestoOutput[23]), ADR(o_FestoCyclic.Program_Selection), 2);
		memcpy(ADR(FestoOutput[25]), ADR(o_FestoCyclic.Digital_Inputs), 1);
	ELSE
		memcpy(ADR(FestoOutput[0]), ADR(o_FestoAcyclicWriteReq.Service_ID), 1);
		memcpy(ADR(FestoOutput[1]), ADR(o_FestoAcyclicWriteReq.Message_ID), 4);
		memcpy(ADR(FestoOutput[5]), ADR(o_FestoAcyclicWriteReq.Data_Length), 4);
		memcpy(ADR(FestoOutput[9]), ADR(o_FestoAcyclicWriteReq.Ack), 1);
		memcpy(ADR(FestoOutput[10]), ADR(o_FestoAcyclicWriteReq.Write_Type), 4);
		memcpy(ADR(FestoOutput[14]), ADR(o_FestoAcyclicWriteReq.Index), 2);
		memcpy(ADR(FestoOutput[16]), ADR(o_FestoAcyclicWriteReq.Sub_Index), 1);
		memcpy(ADR(FestoOutput[17]), ADR(o_FestoAcyclicWriteReq.Data_Type), 1);
		memcpy(ADR(FestoOutput[18]), ADR(o_FestoAcyclicWriteReq.Data_Byte[0]), 86);
	END_IF;
	
	
	//Fub calls
	
	
	
	///////////////////////////////////
	////////DIO IMPLEMENTATION/////////
	///////////////////////////////////

////	//Inputs mapped
////	Press.Status.Error:= i_Press_Fault;
////	Press.Status.Homed:= NOT(i_Press_HomingRequired);
////	Press.Status.MoveSuccessful:= EDGEPOS(i_Press_Result_OK);
////	Press.Status.MoveFailed:= EDGEPOS(i_Press_Result_NotOK);
////	Press.Status.Idle:= i_Press_Ready;
////	Press.Status.On:= i_Press_Activated;
////	
////	//Outputs mapped
////	CASE Press.Par.Mode OF
////		PRESS_MODE_OFF:
////			o_Press_Manual_Mode:= FALSE;
////			o_Press_Auto_Mode:= FALSE;
////		
////		PRESS_MODE_MANUAL:
////			o_Press_Manual_Mode:= TRUE;
////			o_Press_Auto_Mode:= FALSE;
////		
////		PRESS_MODE_AUTO:
////			o_Press_Manual_Mode:= FALSE;
////			o_Press_Auto_Mode:= TRUE;
////	END_CASE;
////		
////	o_Press_Start_Cycle:= Press.Cmd.StartCycle;
////	o_Press_Home:= Press.Cmd.Home;
////	o_Press_Err_Ack:= Press.Cmd.ErrAck;
////	o_Press_Program_Bit_0:= Press.Par.Program.0;
////	o_Press_Program_Bit_1:= Press.Par.Program.1;
////	o_Press_Program_Bit_2:= Press.Par.Program.2;
////	o_Press_Program_Bit_3:= Press.Par.Program.3;
	
	
	//////////////////////////////////////
	////////MODBUS IMPLEMENTATION/////////
	//////////////////////////////////////
	
	(* UPDATE INPUTS/OUTPUTS *)
//	UpdateAll;
	
	
	(* FORWARD LOW-LEVEL FESTO PRESS COMMANDS/STATUSES TO HIGH-LEVEL APPLICATION PRESS STRUCTURE *)
//	PressForwarding;
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

