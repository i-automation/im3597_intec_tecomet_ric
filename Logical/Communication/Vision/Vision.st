PROGRAM _INIT
	TON_Reset.PT:= T#160ms;
	TON_ScanTimeout.PT:= T#10s;
	Camera_IO.Out.CommandNumber:= 24; //specific to the camera operation when changing programs 
	Camera_IO.Out.CommandPar1:= 1; //same
	
END_PROGRAM

PROGRAM _CYCLIC
	(* VISION SYSTEM CONTROLLER
	KEYENCE
	CV-X352F
	ENET/IP 192.168.0.13*)

// Sequence (rough draft)
// #1 - Turn on lighting
// #2 - Send recipe # and trigger capture
// #3 - Get pass/fail/error result
// #4 - Turn off lighting
	
	CASE State OF
		IDLE:
			IF NOT(IO_ModuleOK[4]) THEN
				Vision.Stat.Error:= VISION_COMM_LOST;
				State:= ERROR;
			ELSIF (Vision.Cmd.ChangeProgram) THEN
				TON_ScanTimeout.IN:= TRUE;
				State:= SET_PROGRAM;
			ELSIF (Vision.Cmd.Scan) THEN
				Camera_IO.Out.Trg1:= TRUE;
				TON_ScanTimeout.IN:= TRUE;
				State:= SCANNING;
			END_IF
			Vision.Cmd.ErrAck:= FALSE;
			
		SET_PROGRAM:
			Camera_IO.Out.CommandPar2:= RivetList.CameraProg;
			IF Camera_IO.In.Command_Ready_Flag THEN
				Camera_IO.Out.Command_Request_Flag:= TRUE;
				TON_ScanTimeout.IN:= FALSE;
				State:= SET_PROGRAM_CONFIRM;
			ELSIF TON_ScanTimeout.Q THEN
				TON_ScanTimeout.IN:= FALSE;
				Vision.Stat.Error:= VISION_SET_TIMEOUT;
				State:= ERROR;
			END_IF;
			
		SET_PROGRAM_CONFIRM:
			TON_ScanTimeout.IN:= TRUE;
			IF Camera_IO.In.Command_Complete_Flag THEN
				IF (Camera_IO.In.Command_Result = 0) THEN
					Camera_IO.Out.Command_Request_Flag:= FALSE;
					Vision.Stat.ProgramChanged:= TRUE;
				ELSE //may want to add specific check for =3 (does not exist)
					TON_ScanTimeout.IN:= FALSE;
					Vision.Stat.Error:= VISION_PROG_NOT_EXIST;
					State:= ERROR;
				END_IF;
			END_IF
			IF NOT(Vision.Cmd.ChangeProgram) THEN
				TON_ScanTimeout.IN:= FALSE;
				Vision.Stat.ProgramChanged:= FALSE;
				State:= IDLE;
			ELSIF TON_ScanTimeout.Q THEN
				TON_ScanTimeout.IN:= FALSE;
				Vision.Stat.Error:= VISION_SET_TIMEOUT;
				State:= ERROR;
			END_IF;
			
		SCANNING:
			IF Camera_IO.In.Ack1 THEN
				TON_ScanTimeout.IN:= FALSE;
				Camera_IO.Out.Trg1:= FALSE;
				State:= RESULT_READY;	
			ELSIF TON_ScanTimeout.Q THEN
				TON_ScanTimeout.IN:= FALSE;
				Vision.Stat.Error:= VISION_READ_TIMEOUT;
				State:= ERROR;
			END_IF
			
		RESULT_READY:
			IF (NOT(Camera_IO.In.Ack1) AND Camera_IO.In.Result_Ready_Flag) THEN
				Camera_IO.Out.Result_Ack_Flag:= TRUE;
				Vision.Stat.Data:= Camera_IO.In.Tool_Judge_Value0;
				Vision.Stat.Valid:= TRUE;
				IF Vision.Stat.Data THEN
					Vision.Data.Pass:= TRUE;
					Vision.Data.Fail:= FALSE;
				ELSE
					Vision.Data.Pass:= FALSE;
					Vision.Data.Fail:= TRUE;
				END_IF;
				State:= RESULT;
			END_IF;
				
		RESULT:
			IF NOT(Camera_IO.In.Result_Ready_Flag) THEN
				Camera_IO.Out.Result_Ack_Flag:= FALSE;
			END_IF;
			//Wait for command to clear before resetting data
			IF NOT(Vision.Cmd.Scan) THEN
				Vision.Stat.Valid:= FALSE;
				Vision.Stat.Data := FALSE;
				State:= IDLE;
			END_IF
			
		ERROR:
			TON_ScanTimeout.IN:= FALSE;
			Vision.Cmd.ChangeProgram:= FALSE;
			Camera_IO.Out.Command_Request_Flag:= FALSE;
			Camera_IO.Out.Trg1:= FALSE;
			Vision.Cmd.Scan:= FALSE;
			Vision.Stat.Valid:= FALSE;
			IF (Vision.Cmd.ErrAck) OR ((Vision.Stat.Error = VISION_COMM_LOST) AND IO_ModuleOK[4]) THEN
				Vision.Cmd.ErrAck:= FALSE;
				Vision.Stat.Error:= VISION_OK;
				State:= IDLE;
			END_IF
		
//		RESET:
//			TON_Reset.IN:= TRUE;
//			IF TON_Reset.Q THEN
//				TON_Reset.IN:= FALSE;
//				Barcode_IO.Out.Read_Complete_Clear:= FALSE;
//				Barcode_IO.Out.Error_Clear:= FALSE;
//				State:= IDLE;
//			END_IF
		
	END_CASE
	
	//Inputs
	Camera_IO.In.Ack1:= cvxInput[2].0; //ACK1 - Byte2/Bit0
	Camera_IO.In.Tool_Judge_Value0:= cvxInput[4].0; //Tool Judge Value0 - Byte4/Bit0
	Camera_IO.In.Result_Ready_Flag:= cvxInput[0].3; //Result Ready Flag - Byte0/Bit3
	Camera_IO.In.Command_Ready_Flag:= cvxInput[0].2; //Command Ready Flag - Byte0/Bit2
	Camera_IO.In.Command_Complete_Flag:= cvxInput[0].0;
	memcpy(ADR(Camera_IO.In.Command_Result),ADR(cvxInput[20]),4); //result from camera program load - 0=success, 3=does not exist, 22=out of range
		
	//Outputs
	cvxOutput[1].0:= Camera_IO.Out.Trg1; //TRG1 - Byte1/Bit0
	cvxOutput[0].3:= Camera_IO.Out.Result_Ack_Flag; //Result ack flag - Byte0/Bit3
	memcpy(ADR(cvxOutput[16]),ADR(Camera_IO.Out.CommandNumber),4);
	memcpy(ADR(cvxOutput[20]),ADR(Camera_IO.Out.CommandPar1),4);
	memcpy(ADR(cvxOutput[24]),ADR(Camera_IO.Out.CommandPar2),4);
	cvxOutput[0].0:= Camera_IO.Out.Command_Request_Flag;
	
	o_Camera_Lights_3509:= Vision.Cmd.Lights;
	
	TON_ScanTimeout();

END_PROGRAM

PROGRAM _EXIT
	 
END_PROGRAM

