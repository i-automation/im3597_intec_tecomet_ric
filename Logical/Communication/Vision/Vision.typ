
TYPE
	Camera_IO_Out_typ : 	STRUCT 
		Trg1 : BOOL;
		Result_Ack_Flag : BOOL;
		CommandNumber : DINT; (*should not change*)
		CommandPar1 : DINT; (*should not change*)
		CommandPar2 : DINT; (*vision program number*)
		Command_Request_Flag : BOOL;
	END_STRUCT;
	Camera_IO_In_typ : 	STRUCT 
		Ack1 : BOOL;
		Tool_Judge_Value0 : BOOL;
		Result_Ready_Flag : BOOL;
		Command_Ready_Flag : BOOL;
		Command_Complete_Flag : BOOL;
		Command_Result : DINT;
	END_STRUCT;
	Camera_IO_typ : 	STRUCT 
		In : Camera_IO_In_typ;
		Out : Camera_IO_Out_typ;
	END_STRUCT;
	States_enm : 
		(
		IDLE,
		SET_PROGRAM,
		SET_PROGRAM_CONFIRM,
		SCANNING,
		RESULT_READY,
		RESULT,
		ERROR,
		RESET
		);
END_TYPE
