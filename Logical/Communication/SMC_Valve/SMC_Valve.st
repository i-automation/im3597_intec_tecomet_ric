PROGRAM _CYCLIC
	(*SMC EX260-SPL1*)
	
	//BYTE1
	EX260_IO_Out[0].0:= o_Anvil_Plate_Latch_L_Unl_2506B;
	EX260_IO_Out[0].1:= o_Anvil_Plate_Latch_L_Lch_2506A;
	EX260_IO_Out[0].2:= o_Anvil_Plate_Latch_R_Unl_2507B;
	EX260_IO_Out[0].3:= o_Anvil_Plate_Latch_R_Lch_2507A;
	EX260_IO_Out[0].4:= o_Press_Support_Vert_Up_2508A;
	EX260_IO_Out[0].5:= o_Press_Support_Vert_Down_2508B;
	EX260_IO_Out[0].6:= o_Press_Support_Horz_Ext_2509A;
	EX260_IO_Out[0].7:= o_Press_Support_Horz_Ret_2509B;
	HMI_OutputValue[0]			:= o_Anvil_Plate_Latch_L_Unl_2506B;
	HMI_OutputValue[1]			:= o_Anvil_Plate_Latch_L_Lch_2506A;
	HMI_OutputValue[2]			:= o_Anvil_Plate_Latch_R_Unl_2507B;
	HMI_OutputValue[3]			:= o_Anvil_Plate_Latch_R_Lch_2507A;
	HMI_OutputValue[4]			:= o_Press_Support_Vert_Up_2508A;
	HMI_OutputValue[5]			:= o_Press_Support_Vert_Down_2508B;
	HMI_OutputValue[6]			:= o_Press_Support_Horz_Ext_2509A;
	HMI_OutputValue[7]			:= o_Press_Support_Horz_Ret_2509B;
	
	//BYTE2
	EX260_IO_Out[1].0:= o_Rivet_Head_Stopper_Latch_2510A;
	EX260_IO_Out[1].1:= o_Rivet_Head_Stopper_UnL_2510B;
	EX260_IO_Out[1].2:= o_Rivet_Singulator_Ext_251xA[0];
	EX260_IO_Out[1].3:= o_Rivet_Singulator_Ret_251xB[0];
	EX260_IO_Out[1].4:= o_Rivet_Singulator_Ext_251xA[1];
	EX260_IO_Out[1].5:= o_Rivet_Singulator_Ret_251xB[1];
	EX260_IO_Out[1].6:= o_Rivet_Nest_Horz_Ext_251xA[0];
	EX260_IO_Out[1].7:= o_Rivet_Nest_Horz_Ret_251xB[0];
	HMI_OutputValue[8]			:= o_Rivet_Head_Stopper_Latch_2510A;
	HMI_OutputValue[9]			:= o_Rivet_Head_Stopper_UnL_2510B;
	HMI_OutputValue[10]			:= o_Rivet_Singulator_Ext_251xA[0];
	HMI_OutputValue[11]			:= o_Rivet_Singulator_Ret_251xB[0];
	HMI_OutputValue[12]			:= o_Rivet_Singulator_Ext_251xA[1];
	HMI_OutputValue[13]			:= o_Rivet_Singulator_Ret_251xB[1];
	HMI_OutputValue[14]			:= o_Rivet_Nest_Horz_Ext_251xA[0];
	HMI_OutputValue[15]			:= o_Rivet_Nest_Horz_Ret_251xB[0];
	
	//BYTE3
	EX260_IO_Out[2].0:= o_Rivet_Nest_Horz_Ext_251xA[1];
	EX260_IO_Out[2].1:= o_Rivet_Nest_Horz_Ret_251xB[1];
	HMI_OutputValue[16]			:= o_Rivet_Nest_Horz_Ext_251xA[1];
	HMI_OutputValue[17]			:= o_Rivet_Nest_Horz_Ret_251xB[1];

END_PROGRAM