
ACTION UpdateIO: 
	//Map the IO into a struct. 
	// - The IODD file has all this information, but mapping the IO to vars is outside the scope of what the IO-Link Module does
	// - B&R BOOL type is 1BYTE in length which does not map directly to the single bits from the input/output image
	// - The Balluff input/output images have padding gaps and mode-dependant types, preventing looping through the struct cleanly
	// - All this means it's best just to map it manually...
	
	IF (StackLightChannelStatus = CHANNEL_STAT_OK) THEN
		
		//Error
		StackLight_IO.In.Error:= StackLightInputImage;
		
		//Mode - input image bits are divided differently below based on mode setting
		memcpy(ADR(StackLightOutputImage[3]),ADR(StackLight_IO.Out.Mode),1);

		CASE StackLight_IO.Out.Mode OF
		
			SEGMENT:
				//Num Segments
				memcpy(ADR(StackLightOutputImage[4]),ADR(StackLight_IO.Out.Num_Segs),1);
			
				//Color & Blink data
				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[0].Color),1); //Copy the color enum to a byte first so the ".n" bit access operator can be used.
				StackLightOutputImage[0].0:= TmpByte.0;
				StackLightOutputImage[0].1:= TmpByte.1;
				StackLightOutputImage[0].2:= TmpByte.2;
				StackLightOutputImage[0].3:= StackLight_IO.Out.Segment[0].Blink;
				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[1].Color),1);
				StackLightOutputImage[0].4:= TmpByte.0;
				StackLightOutputImage[0].5:= TmpByte.1;
				StackLightOutputImage[0].6:= TmpByte.2;
				StackLightOutputImage[0].7:= StackLight_IO.Out.Segment[1].Blink;
				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[2].Color),1);
				StackLightOutputImage[1].0:= TmpByte.0;
				StackLightOutputImage[1].1:= TmpByte.1;
				StackLightOutputImage[1].2:= TmpByte.2;
				StackLightOutputImage[1].3:= StackLight_IO.Out.Segment[2].Blink;
				//	(Originally Tested w/ 5seg light, not needed here)
				//				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[3].Color),1);
				//				StackLightOutputImage[1].4:= TmpByte.0;
				//				StackLightOutputImage[1].5:= TmpByte.1;
				//				StackLightOutputImage[1].6:= TmpByte.2;
				//				StackLightOutputImage[1].7:= StackLight_IO.Out.Segment[3].Blink;
				//				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[4].Color),1);
				//				StackLightOutputImage[2].0:= TmpByte.0;
				//				StackLightOutputImage[2].1:= TmpByte.1;
				//				StackLightOutputImage[2].2:= TmpByte.2;
				//				StackLightOutputImage[2].3:= StackLight_IO.Out.Segment[4].Blink;
				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Buzzer.BuzzType),1);
				StackLightOutputImage[2].4:= TmpByte.0;
				StackLightOutputImage[2].5:= TmpByte.1;
				//StackLightOutputImage[2].6:= Unused
				StackLightOutputImage[2].7:= StackLight_IO.Out.Buzzer.Buzz;
			
				//Blink Mode
				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[0].BlinkMode),1);
				StackLightOutputImage[5].0:= TmpByte.0;
				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[1].BlinkMode),1);
				StackLightOutputImage[5].1:= TmpByte.0;
				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[2].BlinkMode),1);
				StackLightOutputImage[5].2:= TmpByte.0;
				//	(Originally Tested w/ 5seg light, not needed here)
				//				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[3].BlinkMode),1);
				//				StackLightOutputImage[5].3:= TmpByte.0;
				//				memcpy(ADR(TmpByte),ADR(StackLight_IO.Out.Segment[4].BlinkMode),1);
				//				StackLightOutputImage[5].4:= TmpByte.0;
				
				//Blink Frequency
				memcpy(ADR(StackLightOutputImage[6]),ADR(StackLight_IO.Out.Blink_Freq),1);
							
				//Buzzer Volume
				StackLightOutputImage[7]:= StackLight_IO.Out.Buzzer.Volume;
	
			LEVEL:
				StackLight_IO.In.Error:= MODE_NOT_IMPLEMENTED;
				
			RUNLIGHT:
				StackLight_IO.In.Error:= MODE_NOT_IMPLEMENTED;
			
			FLEXIBLE:
				StackLight_IO.In.Error:= MODE_NOT_IMPLEMENTED;
			
		END_CASE
		
	ELSE
		StackLight_IO.In.Error:= NOT_CONNECTED;
	END_IF

END_ACTION
