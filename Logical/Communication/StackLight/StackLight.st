PROGRAM _INIT

	StackLight_IO.Out.Mode:= SEGMENT;
	StackLight_IO.Out.Num_Segs:= 3;
	StackLight_IO.Out.Blink_Freq:= FREQ_1HZ;
	StackLight_IO.Out.Segment[2].Color:= RED;
	StackLight_IO.Out.Segment[1].Color:= YELLOW;
	StackLight_IO.Out.Segment[0].Color:= GREEN;

END_PROGRAM


PROGRAM _CYCLIC

	//XXX - Need clarification on exact stacklight behavior, but it should be quick to re-arrange conditions here as needed. (maybe the whole light should be used instead of 3seg, blinking behavior, etc.)
	IF AlarmMgr.ErrCritical OR AlarmMgr.ErrFatal THEN
		StackLight_IO.Out.Segment[2].Color:= RED;
		StackLight_IO.Out.Segment[2].Blink:= TRUE;
		StackLight_IO.Out.Segment[1].Color:= OFF;		
		StackLight_IO.Out.Segment[0].Color:= OFF;
	ELSIF Machine.Stat.Mode = MODE_AUTO THEN
		StackLight_IO.Out.Segment[2].Color:= OFF;
		StackLight_IO.Out.Segment[1].Color:= OFF;		
		StackLight_IO.Out.Segment[0].Color:= GREEN;
	ELSE
		StackLight_IO.Out.Segment[2].Color:= OFF;
		StackLight_IO.Out.Segment[1].Color:= YELLOW;
		StackLight_IO.Out.Segment[0].Color:= OFF;
	END_IF
	
	UpdateIO;

	IO_ModuleOK[9]:= (StackLight_IO.In.Error = NO_ERROR);

END_PROGRAM

