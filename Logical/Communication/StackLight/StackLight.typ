
TYPE
	StackLight_IO_Out_typ : 	STRUCT 
		Mode : Stacklight_Mode_enm;
		Num_Segs : USINT;
		Segment : ARRAY[0..MAX_SEGMENTS]OF StackLight_IO_Out_Segments_typ;
		Buzzer : StackLight_IO_Out_Buzzer_typ;
		Blink_Freq : Stacklight_BlinkFreq_enm;
	END_STRUCT;
	StackLight_IO_Out_Buzzer_typ : 	STRUCT 
		Buzz : BOOL;
		BuzzType : Stacklight_BuzzType_enm;
		Volume : USINT;
	END_STRUCT;
	StackLight_IO_Out_Segments_typ : 	STRUCT 
		Color : Stacklight_Colors_enm;
		Blink : BOOL;
		BlinkMode : Stacklight_BlinkMode_enm;
	END_STRUCT;
	Stacklight_IO_In_typ : 	STRUCT 
		Error : Stacklight_Errors_enm;
	END_STRUCT;
	StackLight_IO_typ : 	STRUCT 
		In : Stacklight_IO_In_typ;
		Out : StackLight_IO_Out_typ;
	END_STRUCT;
	Stacklight_BuzzType_enm : 
		(
		CONTINUOUS := 0,
		CHOP_1HZ := 1,
		CHOP_5Z := 2,
		BEEP_3X := 3
		);
	Stacklight_BlinkFreq_enm : 
		(
		INVALID := 0,
		FREQ_HALF_1_HZ := 1,
		FREQ_1HZ := 2,
		FREQ_2HZ := 3,
		FREQ_5HZ := 4,
		FREQ_10HZ := 5
		);
	Stacklight_BlinkMode_enm : 
		(
		BLINKING := 0,
		FLASHING := 1
		);
	Stacklight_Errors_enm : 
		(
		NO_ERROR := 0,
		WRONG_MODE := 1,
		LEVEL_OUT_OF_RANGE := 2,
		WRONG_NUM_SEGMENTS := 4, (*3 skipped intentionally per Balluff docs*)
		WRONG_FREQUENCY := 5,
		WRONG_SPEED := 6,
		WRONG_BUZZER_FUNCTION := 7,
		NOT_CONNECTED := 98, (*Error from application, all others come direct from light*)
		MODE_NOT_IMPLEMENTED := 99 (*Error from application, all others come direct from light*)
		);
	Stacklight_Colors_enm : 
		(
		OFF := 0,
		GREEN := 1,
		RED := 2,
		YELLOW := 3,
		BLUE := 4,
		ORANGE := 5,
		USER_DEF := 6,
		WHITE := 7
		);
	Stacklight_Mode_enm : 
		( (*This is actually bit-coded, but doing it this way lets us just copy the whole area and handle setting the mode as in enum*)
		SEGMENT := 1,
		LEVEL := 2,
		RUNLIGHT := 4,
		FLEXIBLE := 8
		);
END_TYPE
