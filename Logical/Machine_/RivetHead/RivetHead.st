PROGRAM _INIT
	TON_Hammer_Delay.PT:= T#1s;
	TON_Press_Move_Timeout.PT:= T#5s;
	CommDelay_TON.PT:= T#250ms;
	NestRetractPosition:= 30; //30mm
	PurgeDownPosition:= 30; //50mm
	HammerMoveVel:= 100; //100mm/s
	
	
END_PROGRAM

PROGRAM _CYCLIC
	IF DataMgrInit THEN
	
		//Overview:	
//			a.	The vibratory feeders will be turned on FOR either OR both rivet bowls, depending on which are used in the current workpiece.
//			b.	Once the track proximity sensor is seen, the XY table will move to the first rivet position, such that the hole is directly underneath the hammer. The singulator for the appropriate rivet type simultaneously will extend so that the rivet is under the hammer.
//			c.	The vacuum FOR the hammer will turn on, AND THEN hammer will move so that contact is made with the top OF the rivet head. While the rivet is being held BY the hammer, the vacuum switch FOR the hammer will be monitored TO ensure that the rivet has NOT been dropped. 
//			d.	The hammer, with a fast MOVE, will MOVE towards the workpiece TO predetermined position.
//			e.	The hammer, with a slow MOVE, will THEN push the rivet into the appropriate hole. The target position FOR this MOVE will be specified with the path information, AND the load cell force will be monitored TO ensure that it is within a certain range. If when at the target position the force reading is either too low OR too high, motion will stop AND a fault will be shown on the HMI.
//			f.	Once the rivet has been successfully inserted within the correct force range, the vacuum FOR the hammer will turn off, AND the hammer will RETURN TO its zero position at the top OF its motion range. Note: WHILE the hammer has been below the singulator, this singulator head will retract TO collect the next rivet FOR the next hole.
//			g.	So long as the track proximity sensor is tripped AND the last rivet target has NOT been reached, the XY table will MOVE TO the next position in the riveting path AND will REPEAT steps 8b � 8f. If the last rivet FOR the current tray has been inserted, proceed TO the next step.		
		
		// State Trace;
		IF (State <> StateTrace[0]) THEN
			FOR i:= ((SIZEOF(StateTrace)/SIZEOF(StateTrace[0]))-1) TO 1 BY (-1) DO
				StateTrace[i]:= StateTrace[i-1];
			END_FOR
			StateTrace[0]:= State;
		END_IF
		
		
		//RivetHead sm
		CASE State OF
			//Waiting for a start command from the Machine sm
			IDLE:
				RivetHead.Stat.Running:= FALSE;
				RivetHead.Stat.Ready:= FALSE;
				
				//Start set from either Riveting or Purging sequence steps in Machine sm
				IF RivetHead.Cmd.Start THEN
					//Determine which Feeders to use - (The list has already been validated, so we're sure to find a match)
					FOR i:= 0 TO MAX_FEEDER DO
						FOR j:= 0 TO (UINT_TO_INT(RivetList.NumRivets)-1) DO
							IF (strcmp(ADR(RivetList.Rivet[j].Type),ADR(Feeder[i].RivetType))=0) THEN
								Feeder[i].On:= TRUE;
								EXIT;
							END_IF
						END_FOR
					END_FOR
					RivetHead.Cmd.Start:= FALSE;
					Press.Cmd.Move:= FALSE;
					Press.Cmd.StartCycle:= FALSE;
					State:= RUNNING;
				ELSIF (RivetHead.Cmd.Purge OR RivetHead.Cmd.Rivet) THEN
					//Error, not running
					RivetHead.Stat.Error:= RIVETHEAD_NOT_RUNNING;
					State:= ERROR;
				END_IF
		
			//Once the sm has been started, it will wait here for a Rivet or a Purge command in order to know where to go (or if stopped, it will just reset and go back to IDLE)
			RUNNING:
				RivetHead.Stat.Running:= TRUE;
				RivetHead.Stat.Ready:= TRUE;
				
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF RivetHead.Cmd.Rivet THEN
					RivetHead.Stat.Ready:= FALSE;
					Press.Cmd.Move:= FALSE;
					Press.Cmd.StartCycle:= FALSE;
					//Determine which Rivet to use 
					FOR i:= 0 TO MAX_FEEDER DO
						IF (strcmp(ADR(RivetList.Rivet[CurrentRivet].Type),ADR(Feeder[i].RivetType))=0) THEN
							ActiveFeeder:= INT_TO_USINT(i);
							EXIT;
						END_IF
					END_FOR
					State:= AUTO_SINGULATE_EXT;
				ELSIF RivetHead.Cmd.Purge THEN
					Feeder[SelectedFeeder].On:= TRUE;
					RivetHead.Stat.Ready:= FALSE;
					Press.Cmd.Move:= FALSE;
					Press.Cmd.StartCycle:= FALSE;
					State:= PURGE_SINGULATE_EXT;
				END_IF

				
			//Rivetting Sequence
			//Singulator extends and waits for a specified time before moving to next state and retracting
			//Start a program load here so that it will be loaded by the time the hammer needs to move (program will depend on rivet type)
			AUTO_SINGULATE_EXT:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					Press.Par.Program:= 7; //SystemSettings.Rivet[CurrentRivetTypeIdx].PressProgram; // 7 = generic no press program
					Press.Cmd.LoadProgram:= TRUE;
					RivetHead.Cmd.ExtendSing[ActiveFeeder]:= TRUE;
					RivetHead.Cmd.RetractSing[ActiveFeeder]:= FALSE;
					TON_Singulator_Ext.IN:= TRUE;
					TON_Singulator_Ext.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].SingulatorExtTime);
					TON_Singulator_Timeout.IN:= TRUE;
					TON_Singulator_Timeout.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].SingulatorTimeoutTime);
					IF RivetHead.Stat.SingExtended[ActiveFeeder] AND TON_Singulator_Ext.Q THEN //leave singulator out for so long so that rivet can drop down
						TON_Singulator_Ext.IN:= FALSE;
						TON_Singulator_Timeout.IN:= FALSE;
						State:= AUTO_SINGULATE_RET;
					ELSIF TON_Singulator_Timeout.Q THEN
						TON_Singulator_Ext.IN:= FALSE;
						TON_Singulator_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_SINGULATOR_TIMEOUT;
						State:= ERROR;
					END_IF
				END_IF
					
			//Singulator retracts
			AUTO_SINGULATE_RET:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					RivetHead.Cmd.ExtendSing[ActiveFeeder]:= FALSE;
					RivetHead.Cmd.RetractSing[ActiveFeeder]:= TRUE;
					TON_Singulator_Timeout.IN:= TRUE;
					IF RivetHead.Stat.SingRetracted[ActiveFeeder] THEN
						TON_Singulator_Timeout.IN:= FALSE;
						TON_Singulator_To_Nest_Gap.IN:= TRUE;
						TON_Singulator_To_Nest_Gap.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].SingulatorToNestGapTime);
						RivetHead.Cmd.RetractSing[ActiveFeeder]:= FALSE;
						State:= AUTO_SINGULATOR_TO_NEST_GAP;
					ELSIF TON_Singulator_Timeout.Q THEN
						RivetHead.Cmd.RetractSing[ActiveFeeder]:= FALSE;
						TON_Singulator_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_SINGULATOR_TIMEOUT;
						State:= ERROR;
					END_IF
				END_IF
				
			//Wait for a specified amount of time before continuing and extending the nest to allow time for the rivet to get through the singulator and down into the nest
			AUTO_SINGULATOR_TO_NEST_GAP:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF TON_Singulator_To_Nest_Gap.Q THEN
						TON_Singulator_To_Nest_Gap.IN:= FALSE;
						State:= AUTO_NEST_EXT;
				END_IF
				
			//Extend the nest to the press (and go ahead and turn the vacuum on to get ready for press movement down to meet the rivet)
			AUTO_NEST_EXT:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					RivetHead.Cmd.ExtendNest[ActiveFeeder]:= TRUE;
					RivetHead.Cmd.RetractNest[ActiveFeeder]:= FALSE;
					TON_Nest_Timeout.IN:= TRUE;
					TON_Nest_Timeout.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].NestTimeoutTime);
					RivetHead.Cmd.VacuumOn:= TRUE;
					IF RivetHead.Stat.NestExtended[ActiveFeeder] THEN
						TON_Nest_Timeout.IN:= FALSE;
						State:= AUTO_MOVE_HAMMER_DOWN;
					ELSIF TON_Nest_Timeout.Q THEN
						TON_Nest_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_NEST_TIMEOUT;
						State:= ERROR;
					END_IF
				END_IF
				
			//Once the nest has been extended so that the rivet is under the press, move the hammer down with a auto press program specified for the current rivet type
			AUTO_MOVE_HAMMER_DOWN:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF (Press.Status.ProgramLoaded AND (Press.Status.LoadedProgram = Press.Par.Program)) THEN
					Press.Cmd.LoadProgram:= FALSE;
					Press.Par.Mode:= PRESS_MODE_AUTO;
					Press.Cmd.StartCycle:= TRUE;
					TON_Press_Move_Timeout.IN:= TRUE;
					State:= AUTO_MOVING_HAMMER_DOWN;
				END_IF;
				
			//If the press movement has finished with a successful move status, then continue (turn the vacuum off when the press either gets to the bottom or an error occurs)
			AUTO_MOVING_HAMMER_DOWN:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF Press.Status.MoveSuccessful THEN
					TON_Press_Move_Timeout.IN:= FALSE;
					RivetHead.Cmd.VacuumOn:= FALSE;
					Press.Cmd.StartCycle:= FALSE;
					State:= AUTO_NEST_RET;
				ELSIF ((Press.Data.Position > 50) AND (Press.Data.Position < 100) AND NOT(RivetHead.Stat.RivetSuctioned)) THEN //XXX - maybe definitely don't hard code later
					RivetHead.Cmd.VacuumOn:= FALSE;
					Press.Cmd.StartCycle:= FALSE;
					RivetHead.Stat.Error:= RIVETHEAD_LOST_RIVET_SUCTION;
					State:= ERROR;
				ELSIF Press.Status.MoveFailed THEN
					RivetHead.Cmd.VacuumOn:= FALSE;
					Press.Cmd.StartCycle:= FALSE;
					RivetHead.Stat.Error:= RIVETHEAD_PRESS_MOVE_FAILED;
					State:= ERROR;
				ELSIF TON_Press_Move_Timeout.Q THEN
					RivetHead.Cmd.VacuumOn:= FALSE;
					Press.Cmd.StartCycle:= FALSE;
					RivetHead.Stat.Error:= RIVETHEAD_PRESS_MOVE_TIMEOUT;
					State:= ERROR;
				END_IF

			//While the press is at the bottom of its move, retract the nest before moving the press back up
			AUTO_NEST_RET:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					RivetHead.Cmd.ExtendNest[ActiveFeeder]:= FALSE;
					RivetHead.Cmd.RetractNest[ActiveFeeder]:= TRUE;
					TON_Nest_Timeout.IN:= TRUE;
					IF RivetHead.Stat.NestRetracted[ActiveFeeder] THEN
						RivetHead.Cmd.RetractNest[ActiveFeeder]:= FALSE;
						TON_Nest_Timeout.IN:= FALSE;
						TON_Press_Move_Timeout.IN:= TRUE;
						State:= AUTO_MOVE_HAMMER_UP;
					ELSIF TON_Nest_Timeout.Q THEN
						RivetHead.Cmd.RetractNest[ActiveFeeder]:= FALSE;
						TON_Nest_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_NEST_TIMEOUT;
						State:= ERROR;		
					END_IF
				END_IF
				
			//Once the nest is retracted, move the press back up to 0 with a manual move with the specified velocity
			AUTO_MOVE_HAMMER_UP:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF TON_Press_Move_Timeout.Q THEN
					Press.Cmd.Move:= FALSE;
					RivetHead.Stat.Error:= RIVETHEAD_PRESS_MOVE_TIMEOUT;
					State:= ERROR;
				ELSE
					Press.Par.Mode:= PRESS_MODE_MANUAL;
					Press.Par.MoveType:= PRESS_MOVE_ABS;
					Press.Par.Velocity:= HammerMoveVel;
					Press.Par.Position:= 0;
					Press.Cmd.Move:= TRUE;
					IF Press.Status.InOperation THEN
						TON_Press_Move_Timeout.IN:= FALSE;
						State:= AUTO_MOVING_HAMMER_UP;
					END_IF;
				END_IF
				
			//Once the move has finished, go back to RUNNING state and wait for next Rivet command
			AUTO_MOVING_HAMMER_UP:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF Press.Status.Idle THEN
					Press.Cmd.Move:= FALSE;
					RivetHead.Cmd.Rivet:= FALSE;
					IF Press.Data.Position < 5 THEN //XXX - hard-coded, should change
						State:= RUNNING;
					ELSE
						RivetHead.Stat.Error:= RIVETHEAD_PRESS_NOT_UP;
						State:= ERROR;
					END_IF
				END_IF
				
				
			//Purge Sequence
			//Singulator extends and waits for a specified time before moving to next state and retracting
			PURGE_SINGULATE_EXT:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					RivetHead.Cmd.ExtendSing[SelectedFeeder]:= TRUE;
					RivetHead.Cmd.RetractSing[SelectedFeeder]:= FALSE;
					TON_Singulator_Ext.IN:= TRUE;
					TON_Singulator_Ext.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].SingulatorExtTime);
					TON_Singulator_Timeout.IN:= TRUE;
					TON_Singulator_Timeout.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].SingulatorTimeoutTime);
					IF RivetHead.Stat.SingExtended[SelectedFeeder] AND TON_Singulator_Ext.Q THEN //leave singulator out for so long so that rivet can drop down
						TON_Singulator_Ext.IN:= FALSE;
						TON_Singulator_Timeout.IN:= FALSE;
						State:= PURGE_SINGULATE_RET;
					ELSIF TON_Singulator_Timeout.Q THEN
						TON_Singulator_Ext.IN:= FALSE;
						TON_Singulator_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_SINGULATOR_TIMEOUT;
						State:= ERROR;
					END_IF
				END_IF
					
			//Singulator retracts
			PURGE_SINGULATE_RET:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					RivetHead.Cmd.ExtendSing[SelectedFeeder]:= FALSE;
					RivetHead.Cmd.RetractSing[SelectedFeeder]:= TRUE;
					TON_Singulator_Timeout.IN:= TRUE;
					IF RivetHead.Stat.SingRetracted[SelectedFeeder] THEN
						RivetHead.Cmd.RetractSing[SelectedFeeder]:= FALSE;
						TON_Singulator_Timeout.IN:= FALSE;
						TON_Singulator_To_Nest_Gap.IN:= TRUE;
						TON_Singulator_To_Nest_Gap.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].SingulatorToNestGapTime);
						State:= PURGE_SINGULATOR_TO_NEST_GAP;
					ELSIF TON_Singulator_Timeout.Q THEN
						RivetHead.Cmd.RetractSing[SelectedFeeder]:= FALSE;
						TON_Singulator_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_SINGULATOR_TIMEOUT;
						State:= ERROR;
					END_IF
				END_IF
				
			//Wait for a specified amount of time before continuing and extending the nest to allow time for the rivet to get through the singulator and down into the nest
			PURGE_SINGULATOR_TO_NEST_GAP:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF TON_Singulator_To_Nest_Gap.Q THEN
					TON_Singulator_To_Nest_Gap.IN:= FALSE;
					State:= PURGE_NEST_EXT;
				END_IF
				
			//Extend the nest to the press
			PURGE_NEST_EXT:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					RivetHead.Cmd.ExtendNest[SelectedFeeder]:= TRUE;
					RivetHead.Cmd.RetractNest[SelectedFeeder]:= FALSE;
					TON_Nest_Timeout.IN:= TRUE;
					TON_Nest_Timeout.PT:= UINT_TO_TIME(SystemSettings.Rivet[CurrentRivetTypeIdx].NestTimeoutTime);
					IF RivetHead.Stat.NestExtended[SelectedFeeder] THEN
						TON_Nest_Timeout.IN:= FALSE;
						TON_Press_Move_Timeout.IN:= TRUE;
						State:= PURGE_MOVE_HAMMER_DOWN;
					ELSIF TON_Nest_Timeout.Q THEN
						TON_Nest_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_NEST_TIMEOUT;
						State:= ERROR;
					END_IF
				END_IF
				
			//Once the nest has been extended so that the rivet is under the press, move the hammer down with a manual move to just under the nest
			PURGE_MOVE_HAMMER_DOWN:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF TON_Press_Move_Timeout.Q THEN
					Press.Cmd.Move:= FALSE;
					RivetHead.Stat.Error:= RIVETHEAD_PRESS_MOVE_TIMEOUT;
					State:= ERROR;
				ELSE
					Press.Par.Mode:= PRESS_MODE_MANUAL;
					Press.Par.MoveType:= PRESS_MOVE_ABS;
					Press.Par.Velocity:= HammerMoveVel;
					Press.Par.Position:= PurgeDownPosition;
					Press.Cmd.Move:= TRUE;
					IF Press.Status.InOperation THEN
						TON_Press_Move_Timeout.IN:= FALSE;
						State:= PURGE_MOVING_HAMMER_DOWN;
					END_IF;
				END_IF;
				
			//When move has completed, move on
			PURGE_MOVING_HAMMER_DOWN:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF Press.Status.Idle THEN
					Press.Cmd.Move:= FALSE;
					State:= PURGE_NEST_RET;
				END_IF
				
			//Retract the nest before the press moves back up to start
			PURGE_NEST_RET:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSE
					RivetHead.Cmd.ExtendNest[SelectedFeeder]:= FALSE;
					RivetHead.Cmd.RetractNest[SelectedFeeder]:= TRUE;
					TON_Nest_Timeout.IN:= TRUE;
					IF RivetHead.Stat.NestRetracted[SelectedFeeder] THEN
						RivetHead.Cmd.RetractNest[SelectedFeeder]:= FALSE;
						TON_Nest_Timeout.IN:= FALSE;
						TON_Press_Move_Timeout.IN:= TRUE;
						State:= PURGE_MOVE_HAMMER_UP;
					ELSIF TON_Nest_Timeout.Q THEN
						RivetHead.Cmd.RetractNest[SelectedFeeder]:= FALSE;
						TON_Nest_Timeout.IN:= FALSE;
						RivetHead.Stat.Error:= RIVETHEAD_NEST_TIMEOUT;
						State:= ERROR;		
					END_IF
				END_IF

			//Move hammer back up to 0 with another manual move
			PURGE_MOVE_HAMMER_UP:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF TON_Press_Move_Timeout.Q THEN
					Press.Cmd.Move:= FALSE;
					RivetHead.Stat.Error:= RIVETHEAD_PRESS_MOVE_TIMEOUT;
					State:= ERROR;
				ELSE
					Press.Par.Mode:= PRESS_MODE_MANUAL;
					Press.Par.MoveType:= PRESS_MOVE_ABS;
					Press.Par.Velocity:= HammerMoveVel;
					Press.Par.Position:= 0;
					Press.Cmd.Move:= TRUE;
					IF Press.Status.InOperation THEN
						TON_Press_Move_Timeout.IN:= FALSE;
						State:= PURGE_MOVING_HAMMER_UP;
					END_IF;
				END_IF
				
			//When finished moving, go back to RUNNING state, and wait for next Purge command
			PURGE_MOVING_HAMMER_UP:
				IF RivetHead.Cmd.Stop THEN
					State:= STOP;
				ELSIF Press.Status.Idle THEN
					Press.Cmd.Move:= FALSE;
					RivetHead.Cmd.Purge:= FALSE;
					IF Press.Data.Position < 5 THEN //XXX - hard-coded, should change
						State:= RUNNING;
					ELSE
						RivetHead.Stat.Error:= RIVETHEAD_PRESS_NOT_UP;
						State:= ERROR;
					END_IF
				END_IF
				
				
			STOP:
				RivetHead.Cmd.Stop:= FALSE;
				Press.Cmd.Move:= FALSE;
				Press.Cmd.StartCycle:= FALSE;
				Press.Cmd.LoadProgram:= FALSE;
				RivetHead.Cmd.Rivet:= FALSE;
				RivetHead.Cmd.Purge:= FALSE;
				RivetHead.Cmd.VacuumOn:= FALSE;
				RivetHead.Stat.Running:= FALSE;
				RivetHead.Stat.Ready:= FALSE;
				TON_Singulator_Ext.IN:= FALSE;
				TON_Singulator_Timeout.IN:= FALSE;
				TON_Nest_Timeout.IN:= FALSE;
				TON_Singulator_To_Nest_Gap.IN:= FALSE;
				TON_Press_Move_Timeout.IN:= FALSE;
				FOR i:= 0 TO MAX_FEEDER DO
					Feeder[i].On:= FALSE;
				END_FOR
				State:= IDLE;
				
			ERROR:
				Press.Cmd.Move:= FALSE;
				Press.Cmd.StartCycle:= FALSE;
				Press.Cmd.LoadProgram:= FALSE;
				RivetHead.Cmd.Rivet:= FALSE;
				RivetHead.Cmd.Purge:= FALSE;
				RivetHead.Cmd.VacuumOn:= FALSE;
				RivetHead.Stat.Running:= FALSE;
				RivetHead.Stat.Ready:= FALSE;
				TON_Singulator_Ext.IN:= FALSE;
				TON_Singulator_Timeout.IN:= FALSE;
				TON_Nest_Timeout.IN:= FALSE;
				TON_Singulator_To_Nest_Gap.IN:= FALSE;
				TON_Press_Move_Timeout.IN:= FALSE;
				FOR i:= 0 TO MAX_FEEDER DO
					Feeder[i].On:= FALSE;
				END_FOR
				//XXX - Other outputs to turn off?
				IF RivetHead.Cmd.ErrAck THEN
					RivetHead.Cmd.ErrAck:= FALSE;
					RivetHead.Stat.Error:= RIVETHEAD_OK;
					State:= IDLE;
				ELSIF NOT(AlarmMgr.ErrCritical OR AlarmMgr.ErrFatal) THEN
					State:= IDLE;
				END_IF		
				
		END_CASE
		
		
		//make sure if another error occurs elsewhere in the machine, that we are stuck in the error state here too
		IF (AlarmMgr.ErrCritical OR AlarmMgr.ErrFatal) THEN
			State:= ERROR;
		END_IF
		
		
		TON_Singulator_Ext();
		TON_Singulator_Timeout();
		TON_Nest_Timeout();
		TON_Singulator_To_Nest_Gap();
		TON_Hammer_Delay();
		TON_Press_Move_Timeout();
		CommDelay_TON();
		
		//IO Handling (now located in HMI ValveMotionScreen Action)
//		o_Rivet_Feeder_L_On_3505:= Feeder[0].On;
//		o_Rivet_Feeder_R_On_3506:= Feeder[1].On;
		
	END_IF
							
END_PROGRAM

					
							
							
							
PROGRAM _EXIT
		 
END_PROGRAM

