(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program:
 * File: SetupClampsAndValves.st
 * Author: zscott
 * Created: September 2, 2020
 ********************************************************************
 * Implementation of program
 ********************************************************************) 

 ACTION KissBlockEngage: 
 
	//Either prevent from engaging when not in kiss block box, or disengage if already engaged (from manual mode or somewhere else)
	IF EDGEPOS(NOT(inKissBox)) THEN
		Machine.Cmd.KissBlock.Disengage:= TRUE;
	END_IF;
	
	IF EDGEPOS(Machine.Cmd.KissBlock.Engage) THEN
		Machine.Cmd.KissBlock.Engage:= FALSE;
		IF inKissBox THEN
			Machine.Stat.KissBlock.Engaging:= TRUE;
			KissBlockTimer.IN:= FALSE;
		ELSE
			Machine.Stat.Error:= MACHINE_OUT_OF_KISS_BOX;
			State:= ERROR;
		END_IF;
	ELSIF EDGEPOS(Machine.Cmd.KissBlock.Disengage) THEN
		Machine.Cmd.KissBlock.Disengage:= FALSE;
		Machine.Stat.KissBlock.Disengaging:= TRUE;
	ELSIF (Machine.Cmd.KissBlock.LowerVert AND Machine.Stat.KissBlock.VertDown) THEN
		Machine.Cmd.KissBlock.Engage:= FALSE;
	ELSIF Machine.Stat.KissBlock.VertDown THEN
		Machine.Cmd.KissBlock.Disengage:= FALSE;
	END_IF
	
	IF Machine.Stat.KissBlock.Engaging THEN
		Machine.Cmd.KissBlock.LowerVert:= FALSE;
		Machine.Cmd.KissBlock.RaiseVert:= TRUE;
		IF Machine.Stat.KissBlock.VertUp THEN
			Machine.Cmd.KissBlock.RetractHor:= FALSE;
			Machine.Cmd.KissBlock.ExtendHor:= TRUE;
			IF Machine.Stat.KissBlock.HorExtended THEN
				Machine.Cmd.KissBlock.LowerVert:= TRUE;
				Machine.Cmd.KissBlock.RaiseVert:= FALSE;
				Machine.Stat.KissBlock.Engaging:= FALSE;
			END_IF
		END_IF
	ELSIF Machine.Stat.KissBlock.Disengaging THEN
		Machine.Cmd.KissBlock.LowerVert:= FALSE;
		IF Machine.Stat.KissBlock.VertUp THEN
			Machine.Cmd.KissBlock.RaiseVert:= TRUE;
		ELSE
			Machine.Cmd.KissBlock.RaiseVert:= FALSE;
		END_IF
		KissBlockTimer.PT:= T#100ms;
		KissBlockTimer.IN:= TRUE;
		IF KissBlockTimer.Q THEN
			Machine.Cmd.KissBlock.RetractHor:= TRUE;
			Machine.Cmd.KissBlock.ExtendHor:= FALSE;
			IF Machine.Stat.KissBlock.HorRetracted THEN
				Machine.Cmd.KissBlock.LowerVert:= TRUE;
				Machine.Cmd.KissBlock.RaiseVert:= FALSE;
				Machine.Stat.KissBlock.Disengaging:= FALSE;
				KissBlockTimer.IN:= FALSE;
			END_IF
		END_IF
	END_IF
	
	//Prevent horizontal valve from engaging when vertical is down
	IF Machine.Stat.KissBlock.VertDown THEN
	  	Machine.Cmd.KissBlock.ExtendHor:= FALSE;
	END_IF;

	//Prevent from engaging when outside of kiss block
	IF NOT(inKissBox) THEN
		Machine.Cmd.KissBlock.LowerVert:= TRUE;
		Machine.Cmd.KissBlock.RaiseVert:= FALSE;
		Machine.Cmd.KissBlock.Engage:= FALSE;
		Machine.Stat.KissBlock.Engaging:= FALSE;
	END_IF
	
	//Enaged condition
	IF ((i_Press_Support_Vert_Up_2031 AND o_Press_Support_Vert_Down_2508B) AND i_Press_Support_Horz_Ext_2029) THEN
		Machine.Stat.KissBlock.Engaged:= TRUE;
	ELSE
		Machine.Stat.KissBlock.Engaged:= FALSE;
	END_IF;
	
END_ACTION