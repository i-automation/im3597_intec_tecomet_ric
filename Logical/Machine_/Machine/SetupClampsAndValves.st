(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program:
 * File: SetupClampsAndValves.st
 * Author: zscott
 * Created: September 2, 2020
 ********************************************************************
 * Implementation of program
 ********************************************************************) 

 ACTION SetupClampsAndValves: 
 
	o_Anvil_Plate_Latch_L_Lch_2506A:= FALSE;
	o_Anvil_Plate_Latch_L_Unl_2506B:= TRUE;
	o_Anvil_Plate_Latch_R_Lch_2507A:= FALSE;
	o_Anvil_Plate_Latch_R_Unl_2507B:= TRUE;
	
	Feeder[0].On:= FALSE;
	Feeder[1].On:= FALSE;
	
	IF RivetHead.Stat.SingExtended[0] THEN
		RivetHead.Cmd.ExtendNest[0]:= FALSE;
		RivetHead.Cmd.RetractSing[0]:= TRUE;
		Machine.Stat.Error:= MACHINE_SETUP_LEFT_SING_EXT;
	END_IF;
	IF RivetHead.Stat.SingExtended[1] THEN
		RivetHead.Cmd.ExtendNest[1]:= FALSE;
		RivetHead.Cmd.RetractSing[1]:= TRUE;
		Machine.Stat.Error:= MACHINE_SETUP_RIGHT_SING_EXT;
	END_IF;
	
	o_Rivet_Vac_On_3507:= FALSE;
	
	IF (i_Anvil_Plt_Clp_L_Unlatched_2024 AND i_Anvil_Plt_Clp_R_Unatched_2026) THEN
		Machine.Stat.ClampsValvesSetup:= TRUE;
	ELSE
		Machine.Stat.ClampsValvesSetup:= FALSE;
	END_IF;
	
END_ACTION