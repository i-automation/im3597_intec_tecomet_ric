(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Program: mm
 * File: UpdateHomingParameters.st
 * Author: amusser
 * Created: February 20, 2012
 ********************************************************************
 * Implementation of program mm
 ********************************************************************) 

 ACTION UpdateHomingParameters: 
 
	Axis[AX_X].Par.Basic.Home.Mode:= mpAXIS_HOME_MODE_ABS_SWITCH;
	Axis[AX_X].Par.Basic.Home.HomingDirection:= mpAXIS_HOME_DIR_POSITIVE;
	Axis[AX_X].Par.Basic.Home.SwitchEdge	:= mpAXIS_HOME_DIR_NEGATIVE;
	Axis[AX_X].Par.Basic.Home.StartDirection:= mpAXIS_HOME_DIR_NEGATIVE;
	Axis[AX_X].Par.Basic.Home.ReferencePulse:= mpAXIS_HOME_OPTION_OFF;
	Axis[AX_X].Par.Basic.Home.Position:= SystemSettings.AxisConfig[AX_X].Move.HomeOffset;
	Axis[AX_X].Par.Basic.Home.Acceleration:= MAX(SystemSettings.AxisConfig[AX_X].Move.Accel, SystemSettings.AxisConfig[AX_X].Move.Decel);
	Axis[AX_X].Par.Basic.Home.StartVelocity:= SystemSettings.AxisConfig[AX_X].Move.JogSpeedHigh;
	Axis[AX_X].Par.Basic.Home.HomingVelocity:= Axis[AX_X].Par.Basic.Home.StartVelocity / 10;
	
	
	Axis[AX_Y].Par.Basic.Home.Mode:= mpAXIS_HOME_MODE_ABS_SWITCH;
	Axis[AX_Y].Par.Basic.Home.HomingDirection:= mpAXIS_HOME_DIR_POSITIVE;
	Axis[AX_Y].Par.Basic.Home.SwitchEdge	:= mpAXIS_HOME_DIR_NEGATIVE;
	Axis[AX_Y].Par.Basic.Home.StartDirection:= mpAXIS_HOME_DIR_NEGATIVE;
	Axis[AX_Y].Par.Basic.Home.ReferencePulse:= mpAXIS_HOME_OPTION_OFF;
	Axis[AX_Y].Par.Basic.Home.Position:= SystemSettings.AxisConfig[AX_Y].Move.HomeOffset;
	Axis[AX_Y].Par.Basic.Home.Acceleration:= MAX(SystemSettings.AxisConfig[AX_Y].Move.Accel, SystemSettings.AxisConfig[AX_Y].Move.Decel);
	Axis[AX_Y].Par.Basic.Home.StartVelocity:= SystemSettings.AxisConfig[AX_Y].Move.JogSpeedHigh;
	Axis[AX_Y].Par.Basic.Home.HomingVelocity:= Axis[AX_Y].Par.Basic.Home.StartVelocity / 10;
	
	
END_ACTION