﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.7.4.67 SP?>
<Program SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File>Machine.st</File>
    <File Private="true">Machine.var</File>
    <File>UpdateHomingParameters.st</File>
    <File>SetupClampsAndValves.st</File>
    <File>KissBlockEngage.st</File>
    <File Private="true">Machine.typ</File>
  </Files>
</Program>