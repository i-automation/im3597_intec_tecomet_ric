
PROGRAM _CYCLIC

	IF (IOForce.cmd.DisableAllForcing) THEN
		IOForce.cmd.DisableAllForcing:= FALSE;
		FOR i:= 0 TO (N-1) DO
			IOForce.cmd.Enable[i]:= FALSE;
		END_FOR
	END_IF
	
	IOForce.data.ForcingEnabled:= FALSE;
	
	// Force SMC Valve bits enable forcing based on lead variable
	FOR i := 1 TO 7 BY 1 DO
		IOForce.cmd.Enable[i]	:= IOForce.cmd.Enable[0];
		IOForce.cmd.Enable[i+8]	:= IOForce.cmd.Enable[8];
	END_FOR
	
	IOForce.cmd.Enable[17]		:= IOForce.cmd.Enable[16];
	
	FOR i:= 0 TO (N-1) DO
	
		CASE Channel[i].Step OF
			IO_NOT_FORCED:  // not forced;
				IF (IOForce.cmd.Enable[i]) THEN // IF enabled, first set the force value;
					Channel[i].FB.SetForceValue.value:= IOForce.cmd.ForceValue[i];
					Channel[i].FB.SetForceValue.enable:= TRUE;
					Channel[i].Step:= IO_WAIT_FORCE_VALUE;
				END_IF
			
			IO_WAIT_FORCE_VALUE: // wait for force value TO be set ;
				IF (Channel[i].FB.SetForceValue.status = 0) THEN
					Channel[i].FB.SetForceValue.enable:= FALSE;
					Channel[i].FB.EnableForcing.enable:= TRUE; // now enable forcing itself;
					Channel[i].Step:= IO_WAIT_FORCE_ENABLE;
				END_IF
			
			IO_WAIT_FORCE_ENABLE:   // wait for forcing TO be enabled;
				IF (Channel[i].FB.EnableForcing.status = 0) THEN
					Channel[i].FB.EnableForcing.enable:= FALSE;
					Channel[i].Step:= IO_FORCING_ENABLED;
				END_IF
			
			IO_FORCING_ENABLED:  // forcing is now enabled.;
				IF (NOT IOForce.cmd.Enable[i]) THEN
					Channel[i].FB.DisableForcing.enable:= TRUE;
					Channel[i].Step:= IO_DISABLE_FORCING;
				ELSIF (IOForce.cmd.ForceValue[i] <> Channel[i].FB.SetForceValue.value) THEN
					Channel[i].FB.SetForceValue.value:= IOForce.cmd.ForceValue[i];
					Channel[i].FB.SetForceValue.enable:= TRUE;
					Channel[i].Step:= IO_CHANGE_FORCE_VALUE;
				END_IF
			
			IO_CHANGE_FORCE_VALUE: // change force value;
				IF (Channel[i].FB.SetForceValue.status = 0) THEN
					Channel[i].FB.SetForceValue.enable:= FALSE;			
					Channel[i].Step:= IO_FORCING_ENABLED;
				END_IF
			
			IO_DISABLE_FORCING:
				IF (Channel[i].FB.DisableForcing.status = 0) THEN
					Channel[i].FB.DisableForcing.enable:= FALSE;
					Channel[i].Step:= IO_NOT_FORCED;
				END_IF

		END_CASE
	
		Channel[i].FB.EnableForcing();
		Channel[i].FB.DisableForcing();
		Channel[i].FB.SetForceValue();
		Channel[i].FB.DatapointStatus();
		
		IF Channel[i].FB.DatapointStatus.flags.2 THEN // Check if forcing is enabled
			IOForce.cmd.Enable[i]:= TRUE;
			IOForce.data.ForcingEnabled:= TRUE;
			CASE Channel[i].FB.DatapointStatus.datatype OF
				TYPE_SINT:
					tempsint ACCESS ADR(Channel[i].FB.DatapointStatus.forceValue);
					IOForce.data.PhysicalValue[i]:= tempsint;
					IOForce.cmd.ForceValue[i]:= tempsint;
				
				TYPE_INT:
					tempint ACCESS ADR(Channel[i].FB.DatapointStatus.forceValue);
					IOForce.data.PhysicalValue[i]:= tempint;
					IOForce.cmd.ForceValue[i]:= tempint;
				
				TYPE_DINT:
					tempdint ACCESS ADR(Channel[i].FB.DatapointStatus.forceValue);
					IOForce.data.PhysicalValue[i]:= tempdint;
					IOForce.cmd.ForceValue[i]:= tempdint;				
						
				ELSE
					IOForce.data.PhysicalValue[i]:= Channel[i].FB.DatapointStatus.forceValue;
					IOForce.cmd.ForceValue[i]:= Channel[i].FB.DatapointStatus.forceValue;		
				
			END_CASE
			
		ELSIF Channel[i].FB.DatapointStatus.flags.1 THEN // Check that the io point is valid
			IOForce.cmd.Enable[i]:= FALSE;
			CASE Channel[i].FB.DatapointStatus.datatype OF
				TYPE_SINT:
					tempsint ACCESS ADR(Channel[i].FB.DatapointStatus.value);
					IOForce.data.PhysicalValue[i]:= tempsint;
					IOForce.cmd.ForceValue[i]:= tempsint;
				
				TYPE_INT:
					tempint ACCESS ADR(Channel[i].FB.DatapointStatus.value);
					IOForce.data.PhysicalValue[i]:= tempint;
					IOForce.cmd.ForceValue[i]:= tempint;
				
				TYPE_DINT:
					tempdint ACCESS ADR(Channel[i].FB.DatapointStatus.value);
					IOForce.data.PhysicalValue[i]:= tempdint;
					IOForce.cmd.ForceValue[i]:= tempdint;
						
				ELSE
					IOForce.data.PhysicalValue[i]:= Channel[i].FB.DatapointStatus.value;
					IOForce.cmd.ForceValue[i]:= Channel[i].FB.DatapointStatus.value;		
				
			END_CASE
			
		ELSE
			IOForce.cmd.Enable[i]:= FALSE;
			CASE Channel[i].FB.DatapointStatus.datatype OF
				TYPE_SINT:
					tempsint ACCESS ADR(Channel[i].FB.DatapointStatus.defaultValue);
					IOForce.data.PhysicalValue[i]:= tempsint;
					IOForce.cmd.ForceValue[i]:= tempsint;
				
				TYPE_INT:
					tempint ACCESS ADR(Channel[i].FB.DatapointStatus.defaultValue);
					IOForce.data.PhysicalValue[i]:= tempint;
					IOForce.cmd.ForceValue[i]:= tempint;
				
				TYPE_DINT:
					tempdint ACCESS ADR(Channel[i].FB.DatapointStatus.defaultValue);
					IOForce.data.PhysicalValue[i]:= tempdint;
					IOForce.cmd.ForceValue[i]:= tempdint;
						
				ELSE
					IOForce.data.PhysicalValue[i]:= Channel[i].FB.DatapointStatus.defaultValue;
					IOForce.cmd.ForceValue[i]:= Channel[i].FB.DatapointStatus.defaultValue;		
				
			END_CASE			
		END_IF
	END_FOR
END_PROGRAM

PROGRAM _INIT;
	
	
	//IO_ModuleOK[0] = DI9371
	//IO_ModuleOK[1] = DO9322
	//IO_ModuleOK[2] = DS438A
	//IO_ModuleOK[3] = IF10D1-1
	//IO_ModuleOK[4] = CV_X300
	//IO_ModuleOK[5] = SR 750
	//IO_ModuleOK[6] = EX260
	//IO_ModuleOK[7] = IOL-104-002-Z046 #1
	//IO_ModuleOK[8] = IOL-104-002-Z046 #2
	//IO_ModuleOK[9] = IOL-801-102-Z037
	
	
	N:= SIZEOF(Channel) / SIZEOF(Channel[0]);
	
	IF ( ((SIZEOF(IOForce.cmd.Enable)/SIZEOF(IOForce.cmd.Enable[0])) <> N) OR
	     ((SIZEOF(IOForce.cmd.ForceValue)/SIZEOF(IOForce.cmd.ForceValue[0])) <> N) OR
	 	 ((SIZEOF(IOForce.data.PhysicalValue)/SIZEOF(IOForce.data.PhysicalValue[0])) <> N) ) THEN
		ERRxfatal(0,0,ADR('range of IOForce.cmd.Enable <> IOForce.cmd.ForceValue <> range of IOForce.data.PhysicalValue <> range of Channel'));
	END_IF
	
	// SMC Valve Manifold is separated into 4 USINTs each with 8 bits of data.  Therefore, the channels are not able to be broken out individually.
	// Output	8 Bit	%QB."EX260-SPL1".OUT1_I2200_S01Out
//	strcpy(ADR(Channel[0].Datapoint), ADR('%QB.IF3.ST1.OUT1_I2200_S01Out'));//SMC Valve Manifold Byte 1
//	strcpy(ADR(Channel[8].Datapoint), ADR('%QB.IF3.ST1.OUT2_I2200_S02Out'));//SMC Valve Manifold Byte 2
//	strcpy(ADR(Channel[16].Datapoint), ADR('%QB.IF3.ST1.OUT3_I2200_S03Out'));//SMC Valve Manifold Byte 3
//	strcpy(ADR(Channel[3].Datapoint), ADR('%QB.IF3.ST1.OUT4_I2200_S04Out'));//SMC Valve Manifold Byte 4
	
	
	// NOTE: YOU CAN EASILY GET THE CHANNEL NAMES BY MAPPING YOUR IO THEN OPENING THE IOMAP.IOM FILE AS TEXT!
	strcpy(ADR(Channel[0].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput01'));//o_PBLT_Start_3502
	strcpy(ADR(Channel[1].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput02'));//o_PBLT_Reset_3503
	strcpy(ADR(Channel[2].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput03'));//o_PBLT_PowerOn_3504
	strcpy(ADR(Channel[3].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput04'));//o_Rivet_Feeder_L_On_3505
	strcpy(ADR(Channel[4].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput05'));//o_Rivet_Feeder_R_On_3506
	strcpy(ADR(Channel[5].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput06'));//o_Rivet_Vac_On_3507
	strcpy(ADR(Channel[6].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput07'));//o_Rivet_Vac_Rel_3508
	strcpy(ADR(Channel[7].Datapoint), ADR('%QX.IF6.ST2.DigitalOutput08'));//o_Camera_Lights_3509
	
	strcpy(ADR(Channel[12].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput01'));//o_Press_Start_Cycle
	strcpy(ADR(Channel[13].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput02'));//o_Press_Manual_Mode
	strcpy(ADR(Channel[14].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput03'));//o_Press_Auto_Mode
	strcpy(ADR(Channel[15].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput04'));//o_Press_Home
	strcpy(ADR(Channel[16].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput05'));//o_Press_Err_Ack
	strcpy(ADR(Channel[17].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput06'));//o_Press_Program_Bit_0
	strcpy(ADR(Channel[18].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput07'));//o_Press_Program_Bit_1
	strcpy(ADR(Channel[19].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput08'));//o_Press_Program_Bit_2
	strcpy(ADR(Channel[20].Datapoint), ADR('%QX.IF6.ST4.DigitalOutput09'));//o_Press_Program_Bit_3
	

	FOR i:= 0 TO (N-1) DO
		Channel[i].FB.EnableForcing.pDatapoint:= ADR(Channel[i].Datapoint);
		Channel[i].FB.DisableForcing.pDatapoint:= ADR(Channel[i].Datapoint);
		Channel[i].FB.SetForceValue.pDatapoint:= ADR(Channel[i].Datapoint);
		Channel[i].FB.DatapointStatus.pDatapoint:= ADR(Channel[i].Datapoint);
		Channel[i].FB.DatapointStatus.enable:= TRUE;
	END_FOR


END_PROGRAM

