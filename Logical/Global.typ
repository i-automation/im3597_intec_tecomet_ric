(*
******************
MACHINE*)

TYPE
	Machine_typ : 	STRUCT 
		Cmd : Machine_Cmd_typ;
		Par : Machine_Par_typ;
		Stat : Machine_Stat_typ;
	END_STRUCT;
	Machine_Cmd_typ : 	STRUCT 
		Manual : ARRAY[0..MAX_AXIS_INDEX]OF Machine_Cmd_Manual_typ;
		Press : Machine_Cmd_Press_typ;
		Stop : BOOL;
		Park : BOOL;
		MoveToCamera : BOOL;
		MoveToPress : BOOL;
		Setup : BOOL;
		SetupClampsValves : BOOL;
		Start : BOOL;
		Pause : BOOL;
		Continue : BOOL;
		ResumeAfterFault : BOOL;
		SkipLastRivet : BOOL;
		KissBlock : Machine_Cmd_Kiss_typ;
		Purge : ARRAY[0..MAX_FEEDER]OF BOOL;
		ErrAck : BOOL;
	END_STRUCT;
	Machine_Cmd_Kiss_typ : 	STRUCT 
		Engage : BOOL;
		Disengage : BOOL;
		RaiseVert : BOOL;
		LowerVert : BOOL;
		ExtendHor : BOOL;
		RetractHor : BOOL;
	END_STRUCT;
	Machine_Stat_Kiss_typ : 	STRUCT 
		Engaged : BOOL;
		Engaging : BOOL;
		Disengaging : BOOL;
		VertUp : BOOL;
		VertDown : BOOL;
		HorExtended : BOOL;
		HorRetracted : BOOL;
	END_STRUCT;
	Machine_Cmd_Manual_typ : 	STRUCT 
		JogNegSlow : BOOL;
		JogPosSlow : BOOL;
		JogNegFast : BOOL;
		JogPosFast : BOOL;
		JogNegInc : BOOL;
		JogPosInc : BOOL;
		Home : BOOL;
		AutoTune : BOOL;
	END_STRUCT;
	Machine_Cmd_Press_typ : 	STRUCT 
		JogNegSlow : BOOL;
		JogPosSlow : BOOL;
		JogNegFast : BOOL;
		JogPosFast : BOOL;
		JogNegInc : BOOL;
		JogPosInc : BOOL;
		Home : BOOL; (*not used currently*)
		AutoTune : BOOL; (*not used currently*)
	END_STRUCT;
	Machine_Stat_typ : 	STRUCT 
		Mode : Machine_Modes_enm;
		Error : Machine_Err_enm;
		HomedToSwitch : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		AutoTuneStatus : UINT;
		Homed : BOOL;
		PlatePresent : BOOL;
		HeadStopperLocked : BOOL;
		ClampsValvesSetup : BOOL;
		RivetsReachable : BOOL;
		KissBlock : Machine_Stat_Kiss_typ;
		Purging : BOOL;
		Rivetting : BOOL;
		SettingUp : BOOL;
		PurgeComplete : ARRAY[0..MAX_FEEDER]OF BOOL;
		Paused : BOOL;
		CurrentPart : STRING[80];
	END_STRUCT;
	Machine_Modes_enm : 
		(
		MODE_INIT,
		MODE_OFF,
		MODE_MANUAL,
		MODE_AUTO,
		MODE_ERROR_STOP
		);
	Machine_Err_enm : 
		(
		MACHINE_OK,
		MACHINE_AXIS_ERROR,
		MACHINE_KISS_BOX_EXCEEDED,
		MACHINE_NEAR_NEST_COLLISION,
		MACHINE_NO_PLATE_PRESENT,
		MACHINE_STOPPER_NOT_LOCKED,
		MACHINE_RIVET_OUT_OF_BOUNDS,
		MACHINE_PRE_INSPECTION_FAILED,
		MACHINE_NEST_RETRACT_TIMEOUT,
		MACHINE_PRESS_NOT_READY,
		MACHINE_RIVET_NOT_IN_TRACK,
		MACHINE_PRESS_TOO_LOW,
		MACHINE_SETUP_PRESS_TIMEOUT,
		MACHINE_SETUP_LEFT_SING_EXT,
		MACHINE_SETUP_RIGHT_SING_EXT,
		MACHINE_NOT_READY_FOR_CONTINUE,
		MACHINE_OUT_OF_KISS_BOX,
		MACHINE_LOWER_X_LIMIT_EXCEEDED,
		MACHINE_UPPER_X_LIMIT_EXCEEDED,
		MACHINE_LOWER_Y_LIMIT_EXCEEDED,
		MACHINE_UPPER_Y_LIMIT_EXCEEDED,
		MACHINE_LEFT_LATCH_TIMEOUT,
		MACHINE_LEFT_UNLATCH_TIMEOUT,
		MACHINE_RIGHT_LATCH_TIMEOUT,
		MACHINE_RIGHT_UNLATCH_TIMEOUT,
		MACHINE_LEFT_SING_EXT_TIMEOUT,
		MACHINE_LEFT_SING_RET_TIMEOUT,
		MACHINE_RIGHT_SING_EXT_TIMEOUT,
		MACHINE_RIGHT_SING_RET_TIMEOUT,
		MACHINE_LEFT_NEST_EXT_TIMEOUT,
		MACHINE_LEFT_NEST_RET_TIMEOUT,
		MACHINE_RIGHT_NEST_EXT_TIMEOUT,
		MACHINE_RIGHT_NEST_RET_TIMEOUT,
		MACHINE_STOPPER_LATCH_TIMEOUT,
		MACHINE_STOPPER_UNLATCH_TIMEOUT,
		MACHINE_HORZ_SUPP_EXT_TIMEOUT,
		MACHINE_HORZ_SUPP_RET_TIMEOUT,
		MACHINE_VERT_SUPP_UP_TIMEOUT,
		MACHINE_VERT_SUPP_DOWN_TIMEOUT
		);
	Machine_Par_typ : 	STRUCT 
		Dummy : BOOL; (*XXX - Are Machine Pars needed?*)
	END_STRUCT;
END_TYPE

(*
******************
MACHINE SUBSYSTEMS*)

TYPE
	Barcode_typ : 	STRUCT 
		Cmd : Barcode_Cmd_typ;
		Data : STRING[80];
		Stat : Barcode_Stat_typ;
	END_STRUCT;
	Barcode_Cmd_typ : 	STRUCT 
		ErrAck : BOOL;
		Scan : BOOL;
	END_STRUCT;
	Barcode_Stat_typ : 	STRUCT 
		Error : Barcode_Err_enm;
		Data : BOOL;
		Valid : BOOL;
	END_STRUCT;
	Barcode_Err_enm : 
		(
		BARCODE_OK,
		BARCODE_READ_TIMEOUT,
		BARCODE_COMM_LOST,
		BARCODE_READ_FAILURE,
		BARCODE_GENERAL_FAILURE
		);
	AlarmMgr_typ : 	STRUCT 
		cmd : UINT;
		Init : BOOL;
		Banner : WSTRING[MAX_ALARM_LENGTH];
		ResetInProgress : BOOL;
		MessageState : UINT;
		PowerOnCycles : UDINT;
		OperatingHoursPP : UDINT;
		IOPowerLost : BOOL;
		BatteryStatusCPU : USINT;
		TemperatureCPU : UINT;
		TemperatureENV : UINT;
		EStopOK : BOOL;
		EStopOK1 : BOOL;
		SafetyInterlockOK : BOOL;
		ErrMMHandled : BOOL;
		ErrMessage : BOOL;
		ErrCritical : BOOL;
		ErrFatal : BOOL;
		RestartRequired : BOOL;
	END_STRUCT;
	ParseCtrl_typ : 	STRUCT 
		Cmd : ParseCtrl_Cmd_typ;
		Stat : ParseCtrl_Stat_typ;
		Par : ParseCtrl_Par_typ;
	END_STRUCT;
	ParseCtrl_Cmd_typ : 	STRUCT 
		ErrAck : BOOL;
		ParseFile : BOOL;
	END_STRUCT;
	ParseCtrl_Stat_typ : 	STRUCT 
		MissingRivetType : STRING[80];
		Error : ParseCtrl_Err_enm;
	END_STRUCT;
	ParseCtrl_Par_typ : 	STRUCT 
		PartName : STRING[80];
	END_STRUCT;
	ParseCtrl_Err_enm : 
		(
		PARSE_OK,
		PARSE_FILE_NOT_FOUND,
		PARSE_DIRECTORY_ERROR,
		PARSE_LOAD_ERROR,
		PARSE_READ_ERROR,
		PARSE_CLOSE_ERROR,
		PARSE_NO_RIVETS_FOUND,
		PARSE_RIVET_TYPE_NOT_IN_FEEDER,
		PARSE_CAMERA_PROG_NOT_FOUND,
		PARSE_CAMERA_PROG_OUT_OF_RANGE
		);
	Vision_typ : 	STRUCT 
		Cmd : Vision_Cmd_typ;
		Data : Vision_Data_typ;
		Stat : Vision_Stat_typ;
	END_STRUCT;
	Vision_Cmd_typ : 	STRUCT 
		ErrAck : BOOL;
		Scan : BOOL;
		ChangeProgram : BOOL;
		Lights : BOOL;
	END_STRUCT;
	Vision_Data_typ : 	STRUCT 
		Fail : BOOL;
		Pass : BOOL;
	END_STRUCT;
	Vision_Stat_typ : 	STRUCT 
		Error : Vision_Err_enm;
		Data : BOOL;
		Valid : BOOL;
		ProgramChanged : BOOL;
	END_STRUCT;
	Vision_Err_enm : 
		(
		VISION_OK,
		VISION_SET_TIMEOUT,
		VISION_PROG_NOT_EXIST,
		VISION_READ_TIMEOUT,
		VISION_COMM_LOST,
		VISION_READ_FAILURE,
		VISION_GENERAL_FAILURE
		);
	Feeder_typ : 	STRUCT 
		On : BOOL;
		RivetType : STRING[80];
	END_STRUCT;
	RivetHead_typ : 	STRUCT 
		Cmd : RivetHead_Cmd_typ;
		Par : RivetHead_Par_typ;
		Stat : RivetHead_Stat_typ;
	END_STRUCT;
	RivetHead_Cmd_typ : 	STRUCT 
		ErrAck : BOOL;
		Purge : BOOL;
		Stop : BOOL;
		Start : BOOL;
		Rivet : BOOL;
		ExtendSing : ARRAY[0..MAX_FEEDER]OF BOOL;
		RetractSing : ARRAY[0..MAX_FEEDER]OF BOOL;
		ExtendNest : ARRAY[0..MAX_FEEDER]OF BOOL;
		RetractNest : ARRAY[0..MAX_FEEDER]OF BOOL;
		VacuumOn : BOOL;
	END_STRUCT;
	RivetHead_Par_typ : 	STRUCT 
		Feeder : USINT;
	END_STRUCT;
	RivetHead_Stat_typ : 	STRUCT 
		Running : BOOL;
		Ready : BOOL;
		SingExtended : ARRAY[0..MAX_FEEDER]OF BOOL;
		SingRetracted : ARRAY[0..MAX_FEEDER]OF BOOL;
		NestExtended : ARRAY[0..MAX_FEEDER]OF BOOL;
		NestRetracted : ARRAY[0..MAX_FEEDER]OF BOOL;
		RivetSuctioned : BOOL;
		LastRivet : UINT; (*if errorred during auto, we store the current rivet where faulted here*)
		Error : RivetHead_Err_enm;
	END_STRUCT;
	RivetHead_Err_enm : 
		(
		RIVETHEAD_OK,
		RIVETHEAD_NOT_RUNNING,
		RIVETHEAD_PRESS_MOVE_FAILED,
		RIVETHEAD_SINGULATOR_TIMEOUT,
		RIVETHEAD_NEST_TIMEOUT,
		RIVETHEAD_LOST_RIVET_SUCTION,
		RIVETHEAD_PRESS_NOT_UP,
		RIVETHEAD_PRESS_MOVE_TIMEOUT
		);
END_TYPE

(*
******************
DATA*)

TYPE
	Recipe_typ : 	STRUCT  (*(Not Used - All part data comes from part files)*)
		Name : STRING[20];
	END_STRUCT;
	RivetList_typ : 	STRUCT 
		Unproven : BOOL;
		PartVersion : STRING[20];
		Valid : BOOL;
		CameraProg : DINT;
		NumRivets : UINT;
		Rivet : ARRAY[0..1023]OF RivetList_Rivet_typ;
	END_STRUCT;
	RivetList_Rivet_typ : 	STRUCT 
		X : REAL;
		Y : REAL;
		Type : STRING[20];
	END_STRUCT;
	SystemSettings_NetworkShare_typ : 	STRUCT 
		Domain : STRING[80];
		Server : STRING[80]; (*time in ms*)
		Name : STRING[80]; (*time in ms*)
		User : STRING[80]; (*time in ms*)
		Password : STRING[80]; (*time in ms*)
	END_STRUCT;
	SystemSettings_Rivet_typ : 	STRUCT 
		Name : STRING[80];
		PressProgram : USINT;
		SingulatorExtTime : UINT; (*time in ms*)
		SingulatorTimeoutTime : UINT; (*time in ms*)
		NestTimeoutTime : UINT; (*time in ms*)
		SingulatorToNestGapTime : UINT; (*time in ms*)
		HammerForce : REAL;
	END_STRUCT;
	SystemSettings_typ : 	STRUCT 
		RecipeNum : UINT;
		Display : SystemSettings_Display_typ;
		Rivet : ARRAY[0..9]OF SystemSettings_Rivet_typ;
		PurgePressProg : USINT;
		BarcodeTimeout : UINT; (*Time in ms*)
		HardwarePositions : SystemSettings_HwPositions_typ;
		LineVoltage : UINT;
		PhaseMonIgnore : BOOL;
		PressConfig : SystemSettings_PressConfig_typ;
		AxisConfig : ARRAY[0..MAX_AXIS_INDEX]OF iAAxis_Config_typ;
		MotorList : ARRAY[0..MAX_MOTORS_INDEX]OF STRING[10]; (*List of motors used--string length must not be changed, bc parameter table names cannot be longer than 10 characters. *)
		IO : ARRAY[0..199]OF SystemSettings_IO_typ;
		NetworkShare : SystemSettings_NetworkShare_typ;
		YAxisRestriction_NegLim : REAL;
		YAxisRestriction_PosLim : REAL;
	END_STRUCT;
	SystemSettings_PressConfig_typ : 	STRUCT 
		Disable : BOOL;
		JogSpeedHigh : REAL;
		JogSpeed : REAL;
		JogIncrement : REAL;
	END_STRUCT;
	SystemSettings_HwPositions_typ : 	STRUCT 
		BarcodeReader : Coordinate_typ;
		Camera : Coordinate_typ;
		Press : Coordinate_typ;
		Purge : ARRAY[0..MAX_FEEDER]OF Coordinate_typ;
	END_STRUCT;
	SystemSettings_Display_typ : 	STRUCT 
		Reserved : ARRAY[0..MAX_LOGIN_LVL]OF STRING[20];
		MaxRecipes : UINT;
		UnitType : UINT;
		Language : UINT;
		Brightness : USINT;
	END_STRUCT;
	SystemSettings_IO_typ : 	STRUCT 
		Type : USINT;
		Enabled : BOOL;
		ChannelMap : ARRAY[0..11]OF UINT;
		BaseType : USINT; (*0 = None, 1 = BB80, 2 = BM11, 3 = BM01, 4 = BM15, 5 = BM05*)
	END_STRUCT;
END_TYPE

(*
******************
MISC*)

TYPE
	DateTimeStr_typ : 	STRUCT 
		year : STRING[5];
		month : STRING[5];
		day : STRING[5];
		wday : STRING[5];
		hour : STRING[5];
		minute : STRING[5];
		second : STRING[5];
		millisec : STRING[5];
		microsec : STRING[5];
		full : STRING[40];
	END_STRUCT;
	Coordinate_typ : 	STRUCT 
		X : REAL;
		Y : REAL;
	END_STRUCT;
	DateTime_typ : 	STRUCT  (*XXX - Check where this is used, probably can be replaced with an existing type already defined in a library*)
		num : DTStructure;
		str : DateTimeStr_typ;
	END_STRUCT;
	SysDump_typ : 	STRUCT 
		cmd : BOOL;
		inProgress : BOOL;
		inError : BOOL;
		complete : BOOL;
	END_STRUCT;
	IOForce_typ : 	STRUCT 
		cmd : IOForce_cmd_typ;
		data : IOForce_data_typ;
	END_STRUCT;
	IOForce_cmd_typ : 	STRUCT 
		Enable : ARRAY[0..MAX_NUM_CHANNELS]OF BOOL;
		ForceValue : ARRAY[0..MAX_NUM_CHANNELS]OF DINT;
		DisableAllForcing : BOOL;
	END_STRUCT;
	IOForce_data_typ : 	STRUCT 
		PhysicalValue : ARRAY[0..MAX_NUM_CHANNELS]OF DINT;
		ForcingEnabled : BOOL;
	END_STRUCT;
	IO_typ : 	STRUCT 
		i : i_typ;
		o : o_typ;
		safety : safety_typ;
	END_STRUCT;
	i_typ : 	STRUCT 
		Main_Air_Dump_Fbk_1013 : BOOL;
		PB_Start_1408 : BOOL;
	END_STRUCT;
	o_typ : 	STRUCT 
	END_STRUCT;
	safety_typ : 	STRUCT 
	END_STRUCT;
END_TYPE
