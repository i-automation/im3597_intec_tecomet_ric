(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: alarmmgr
 * File: alarmmgr.st
 * Author: ?
 * Created: ?
 * Version: 1.00.0
 * Modified Date:8/2/2016
 * Modified BY:phouston
 ********************************************************************
 * Implementation of program alarmmgr

 * Description:

	This task handles setting and resetting alarms, and defines alarm
	level.

 * Options:

 * Version History:

 ********************************************************************)
PROGRAM _CYCLIC
	IF (DataMgrInit) THEN

		Reset:= (EDGEPOS(HMI.Common.ResetPB) OR EDGEPOS(i_PB_Reset_1412));
		HMI.Common.ResetPB:= FALSE;
		AlarmMgr.EStopOK:= i_EStop_OK;
		AlarmMgr.SafetyInterlockOK:= i_Safety_Interlock_OK;
		
		//XXX - Trigger automatic global error ack when estop is reset? (drive enbable errors will need resetting)
		
		
		CASE State OF
		
			IDLE:
				IF (Reset) THEN
					AlarmMgr.ResetInProgress:= TRUE;
					memset(ADR(AlarmMgr.Banner), 0, SIZEOF(AlarmMgr.Banner));
					//Reset Latched Faults
					//  - {Latched faults go here}
					ParseCtrl.Cmd.ErrAck:= TRUE;
					Barcode.Cmd.ErrAck:= TRUE;
					IF Press.Status.Error THEN
						TON_Reset.PT:= T#3s;
						TON_Press_Err_Reset_Move.IN:= FALSE;;
						State:= PRESS_RESET;
					ELSE
						TON_Reset.PT:= T#1s;
						State:= FAULT_RESET;
					END_IF
					IF NOT Press.Status.On THEN
						Press.Cmd.On:= FALSE; //so that On cmd is reset in order to be set again on init after errors cleared (mainly a problem when safety triggered)
					END_IF;
					IF NOT(Press.Status.CommOK) THEN
						Press.Cmd.ResetComm:= TRUE;
					END_IF;
					
					//Reset IO module faults
					FOR i:= 0 TO (SIZEOF(IO_ModuleOK)/SIZEOF(IO_ModuleOK[0])-1) DO
						IF ((IO_ModuleOK[i]) OR (NOT IO_ModuleRequired[i])) THEN
							AlarmBits.Hardware[i]:= FALSE;
						END_IF
					END_FOR
				END_IF
				
			PRESS_RESET:
				RivetHead.Cmd.ExtendNest[0]:= FALSE;
				RivetHead.Cmd.RetractNest[0]:= TRUE;
				RivetHead.Cmd.ExtendNest[1]:= FALSE;
				RivetHead.Cmd.RetractNest[1]:= TRUE;
				IF RivetHead.Stat.NestRetracted[0] AND RivetHead.Stat.NestRetracted[1] AND (Press.Data.Position > 40) THEN
					Press.Par.Mode:= PRESS_MODE_MANUAL;
					Press.Par.MoveType:= PRESS_MOVE_JOG;
					Press.Par.Velocity:= 5;
					Press.Cmd.JogNeg:= TRUE;
				END_IF
				
				TON_Press_Err_Reset_Move.IN:= TRUE;
				IF TON_Press_Err_Reset_Move.Q THEN
					TON_Press_Err_Reset_Move.IN:= FALSE;
					Press.Cmd.JogNeg:= FALSE;
					Press.Cmd.ErrAck:= TRUE;
					State:= FAULT_RESET;
				END_IF

			FAULT_RESET:
				TON_Reset.IN:= TRUE;
				Machine.Cmd.ErrAck:= TRUE;
				RivetHead.Cmd.ErrAck:= TRUE;
				Vision.Cmd.ErrAck:= TRUE;
				ResetDevLink:= TRUE;
				IF (TON_Reset.Q (*OR (Machine.Stat.Error = MACHINE_OK)*)) THEN
					TON_Reset.IN:= FALSE;
					Machine.Cmd.ErrAck:= FALSE;
					RivetHead.Cmd.ErrAck:= FALSE;
					Vision.Cmd.ErrAck:= FALSE;
					Press.Cmd.ErrAck:= FALSE;
					ResetDevLink:= FALSE;
					AlarmMgr.ResetInProgress:= FALSE;
					State:= IDLE;
				END_IF						
		END_CASE

		// IO module status
		IF (TON_IOInit.Q) THEN
			AllIOInit:= TRUE;  // flag for rest of program to know we have all IO modules 
			FOR i:= 0 TO ((SIZEOF(IO_ModuleOK)/SIZEOF(IO_ModuleOK[0])) - 1) DO
				IF ((NOT IO_ModuleOK[i]) AND (IO_ModuleRequired[i])) THEN
					AlarmBits.Hardware[i]:= TRUE;
					AllIOInit:= FALSE;
				END_IF
			END_FOR	
		END_IF
		
		//Emergency stop reset required
		AlarmBits.Application[1]:= NOT(AlarmMgr.EStopOK);
		
		//Safety interlock reset required
		AlarmBits.Application[2]:= NOT(AlarmMgr.SafetyInterlockOK);
		
		//Emergency stop pressed
		AlarmBits.Application[3]:= NOT(i_EStop_PB_OK[0]); // right farside
		AlarmBits.Application[4]:= NOT(i_EStop_PB_OK[1]); // left farside
		AlarmBits.Application[5]:= NOT(i_EStop_PB_OK[2]); // left nearside
		AlarmBits.Application[6]:= NOT(i_EStop_PB_OK[3]); // right nearside
		
		//Guard door unlocked
		AlarmBits.Application[7]:= NOT(i_Safety_Door_Locked[0]);  // right
		AlarmBits.Application[8]:= NOT(i_Safety_Door_Locked[1]);  // left farside
		AlarmBits.Application[9]:= NOT(i_Safety_Door_Locked[2]);  // left nearside
		AlarmBits.Application[10]:= NOT(i_Safety_Door_Locked[3]); // front left
		AlarmBits.Application[11]:= NOT(i_Safety_Door_Locked[4]); // front right
		AlarmBits.Application[12]:= NOT(i_Safety_Door_Locked[5]); // loading
		
		//Restart required
		AlarmBits.Application[15]:= AlarmMgr.RestartRequired;
		
		//Check if Machine is Homed
		AlarmBits.Application[20]:= NOT(Machine.Stat.Homed);
		
		AlarmBits.Application[21]:= IOForce.data.ForcingEnabled;
		
		//Call Action to Check Axis Parameter Faults
		AxisParameterCheck;
		
		// Technology Guarding Check
		AlarmBits.Application[22]:= NOT(GetGuardStatus.licenseOk);
		AlarmBits.Application[23]:= NOT(GetGuardStatus.licenseOk) AND (DongleInfo.serNo = 0);
		
		//Reset Dongle Info if license error occurs incase dongle was removed
		IF EDGENEG(GetGuardStatus.licenseOk) THEN
			memset(ADR(DongleInfo),0,SIZEOF(DongleInfo));
		END_IF
		
		//PLC Hardware Errors
		AlarmBits.Application[24]:= (AlarmMgr.BatteryStatusCPU <> 1); //1 is battery ok, 0 is battery voltage too low or battery missing	
		AlarmBits.Application[25]:= AlarmMgr.IOPowerLost;
		
		//Parsing/File Errors
		AlarmBits.Application[30]:= (ParseCtrl.Stat.Error = PARSE_CLOSE_ERROR);
		AlarmBits.Application[31]:= (ParseCtrl.Stat.Error = PARSE_FILE_NOT_FOUND);
		AlarmBits.Application[32]:= (ParseCtrl.Stat.Error = PARSE_LOAD_ERROR);
		AlarmBits.Application[33]:= (ParseCtrl.Stat.Error = PARSE_NO_RIVETS_FOUND);
		AlarmBits.Application[34]:= (ParseCtrl.Stat.Error = PARSE_READ_ERROR);
		AlarmBits.Application[35]:= (ParseCtrl.Stat.Error = PARSE_RIVET_TYPE_NOT_IN_FEEDER);
		AlarmBits.Application[36]:= (ParseCtrl.Stat.Error = PARSE_CAMERA_PROG_NOT_FOUND);
		AlarmBits.Application[37]:= (ParseCtrl.Stat.Error = PARSE_CAMERA_PROG_OUT_OF_RANGE);
			
		//Barcode Errors
		AlarmBits.Application[40]:= (Barcode.Stat.Error = BARCODE_COMM_LOST);
		AlarmBits.Application[41]:= (Barcode.Stat.Error = BARCODE_READ_TIMEOUT);
		AlarmBits.Application[42]:= (Barcode.Stat.Error = BARCODE_READ_FAILURE);
		AlarmBits.Application[43]:= (Barcode.Stat.Error = BARCODE_GENERAL_FAILURE);
		
		//Air Pressure
		AlarmBits.Application[50]:= NOT(i_Main_Air_Pressure_OK_2028);
		AlarmBits.Application[51]:= i_Main_Air_Dump_Fbk_1013;
		
		//Machine Sequencing Errors
		AlarmBits.Application[60]:= (Machine.Stat.Error = MACHINE_KISS_BOX_EXCEEDED);
		AlarmBits.Application[61]:= (Machine.Stat.Error = MACHINE_NEAR_NEST_COLLISION);
		AlarmBits.Application[62]:= (Machine.Stat.Error = MACHINE_NO_PLATE_PRESENT);
		AlarmBits.Application[63]:= (Machine.Stat.Error = MACHINE_STOPPER_NOT_LOCKED);
		AlarmBits.Application[64]:= (Machine.Stat.Error = MACHINE_RIVET_OUT_OF_BOUNDS);
		AlarmBits.Application[65]:= (Machine.Stat.Error = MACHINE_PRE_INSPECTION_FAILED);
		AlarmBits.Application[66]:= (RivetHead.Stat.Error = RIVETHEAD_SINGULATOR_TIMEOUT);
		AlarmBits.Application[67]:= (RivetHead.Stat.Error = RIVETHEAD_NEST_TIMEOUT);
		AlarmBits.Application[68]:= (RivetHead.Stat.Error = RIVETHEAD_LOST_RIVET_SUCTION);
		AlarmBits.Application[69]:= (RivetHead.Stat.Error = RIVETHEAD_PRESS_NOT_UP);
		AlarmBits.Application[70]:= (RivetHead.Stat.Error = RIVETHEAD_NOT_RUNNING);
		AlarmBits.Application[71]:= (Machine.Stat.Error = MACHINE_NEST_RETRACT_TIMEOUT);
		AlarmBits.Application[72]:= (Machine.Stat.Error = MACHINE_PRESS_NOT_READY);
		AlarmBits.Application[73]:= (Machine.Stat.Error = MACHINE_RIVET_NOT_IN_TRACK);
		AlarmBits.Application[74]:= (Machine.Stat.Error = MACHINE_PRESS_TOO_LOW);
		AlarmBits.Application[75]:= (Machine.Stat.Error = MACHINE_SETUP_PRESS_TIMEOUT);
		AlarmBits.Application[76]:= (Machine.Stat.Error = MACHINE_SETUP_LEFT_SING_EXT);
		AlarmBits.Application[77]:= (Machine.Stat.Error = MACHINE_SETUP_RIGHT_SING_EXT);
		AlarmBits.Application[78]:= (Machine.Stat.Error = MACHINE_NOT_READY_FOR_CONTINUE);
		AlarmBits.Application[79]:= (RivetHead.Stat.Error = RIVETHEAD_PRESS_MOVE_TIMEOUT);
		AlarmBits.Application[80]:= (Machine.Stat.Error = MACHINE_OUT_OF_KISS_BOX);
		AlarmBits.Application[81]:= (Machine.Stat.Error = MACHINE_LOWER_X_LIMIT_EXCEEDED);
		AlarmBits.Application[82]:= (Machine.Stat.Error = MACHINE_UPPER_X_LIMIT_EXCEEDED);
		AlarmBits.Application[83]:= (Machine.Stat.Error = MACHINE_LOWER_Y_LIMIT_EXCEEDED);
		AlarmBits.Application[84]:= (Machine.Stat.Error = MACHINE_UPPER_Y_LIMIT_EXCEEDED);
		
		//Other Axis Errors
		AlarmBits.Application[85]:= Press.Status.Error;
		AlarmBits.Application[86]:= (RivetHead.Stat.Error = RIVETHEAD_PRESS_MOVE_FAILED);
		AlarmBits.Application[87]:= NOT(Press.Status.CommOK);
		
		//Vision Errors
		AlarmBits.Application[90]:= (Vision.Stat.Error = VISION_COMM_LOST);
		AlarmBits.Application[91]:= (Vision.Stat.Error = VISION_SET_TIMEOUT);
		AlarmBits.Application[92]:= (Vision.Stat.Error = VISION_PROG_NOT_EXIST);
		AlarmBits.Application[93]:= (Vision.Stat.Error = VISION_READ_TIMEOUT);
		AlarmBits.Application[94]:= (Vision.Stat.Error = VISION_READ_FAILURE);
		AlarmBits.Application[95]:= (Vision.Stat.Error = VISION_GENERAL_FAILURE);
		
		//Timeouts
		AlarmBits.Application[100]:= (Machine.Stat.Error = MACHINE_LEFT_LATCH_TIMEOUT);
		AlarmBits.Application[101]:= (Machine.Stat.Error = MACHINE_LEFT_UNLATCH_TIMEOUT);
		AlarmBits.Application[102]:= (Machine.Stat.Error = MACHINE_RIGHT_LATCH_TIMEOUT);
		AlarmBits.Application[103]:= (Machine.Stat.Error = MACHINE_RIGHT_UNLATCH_TIMEOUT);
		AlarmBits.Application[104]:= (Machine.Stat.Error = MACHINE_LEFT_SING_EXT_TIMEOUT);
		AlarmBits.Application[105]:= (Machine.Stat.Error = MACHINE_LEFT_SING_RET_TIMEOUT);
		AlarmBits.Application[106]:= (Machine.Stat.Error = MACHINE_RIGHT_SING_EXT_TIMEOUT);
		AlarmBits.Application[107]:= (Machine.Stat.Error = MACHINE_RIGHT_SING_RET_TIMEOUT);
		AlarmBits.Application[108]:= (Machine.Stat.Error = MACHINE_LEFT_NEST_EXT_TIMEOUT);
		AlarmBits.Application[109]:= (Machine.Stat.Error = MACHINE_LEFT_NEST_RET_TIMEOUT);
		AlarmBits.Application[110]:= (Machine.Stat.Error = MACHINE_RIGHT_NEST_EXT_TIMEOUT);
		AlarmBits.Application[111]:= (Machine.Stat.Error = MACHINE_RIGHT_NEST_RET_TIMEOUT);
		AlarmBits.Application[112]:= (Machine.Stat.Error = MACHINE_STOPPER_LATCH_TIMEOUT);
		AlarmBits.Application[113]:= (Machine.Stat.Error = MACHINE_STOPPER_UNLATCH_TIMEOUT);
		AlarmBits.Application[114]:= (Machine.Stat.Error = MACHINE_HORZ_SUPP_EXT_TIMEOUT);
		AlarmBits.Application[115]:= (Machine.Stat.Error = MACHINE_HORZ_SUPP_RET_TIMEOUT);
		AlarmBits.Application[116]:= (Machine.Stat.Error = MACHINE_VERT_SUPP_UP_TIMEOUT);
		AlarmBits.Application[117]:= (Machine.Stat.Error = MACHINE_VERT_SUPP_DOWN_TIMEOUT);
		
		AlarmBits.Application[120]:= DevLinkFailed;
		AlarmBits.Application[121]:= DevLinking;
		AlarmBits.Application[122]:= DevLinkTimeout;
				
		
		//Set "alarm" to display in alarm banner - Purge mode, Progress in Auto mode, Time/date otherwise
		AlarmBitDateTimeRecipeNameUser[2]:= (Machine.Stat.Mode = MODE_AUTO) AND Machine.Stat.Purging;
		AlarmBitDateTimeRecipeNameUser[1]:= (Machine.Stat.Mode = MODE_AUTO) AND Machine.Stat.Rivetting;
		AlarmBitDateTimeRecipeNameUser[0]:= NOT(AlarmBitDateTimeRecipeNameUser[1]) AND NOT(AlarmBitDateTimeRecipeNameUser[2]);
				
		//Set the SDM application status		
		IF (AlarmMgr.ErrFatal) THEN
			SdmSetAppParam_0.appMode:= sdm_APPMODE_ERROR; 
		ELSIF (AlarmMgr.ErrCritical) THEN
			SdmSetAppParam_0.appMode:= sdm_APPMODE_WARNING; 
		ELSE
			SdmSetAppParam_0.appMode:= sdm_APPMODE_OK; 
		END_IF
				
		//Check Fault Level Of Active Alarms
		AlarmMgr.ErrMessage:= FALSE;
		AlarmMgr.ErrCritical:= FALSE;
		AlarmMgr.ErrFatal:= FALSE;
		FOR i:= 0 TO MAX_ALARM_APP_BIT DO //Application Alarms
			IF (AlarmBits.Application[i]) THEN
				CASE FaultLevel.Application[i] OF
					FAULTLEVEL_MESSAGE:
						AlarmMgr.ErrMessage := TRUE;
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical:= TRUE;
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal:= TRUE;
				END_CASE
			END_IF			
		END_FOR

		FOR i:= 0 TO MAX_ALARM_AXIS_BIT DO //Axis Alarms
			IF (AlarmBits.Axis[i]) THEN
				CASE FaultLevel.Axis[i] OF
					FAULTLEVEL_MESSAGE:
						AlarmMgr.ErrMessage := TRUE;
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical:= TRUE;
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal:= TRUE;
				END_CASE
			END_IF			
		END_FOR
		
		FOR i:= 0 TO MAX_ALARM_COMM_BIT DO //Communication Alarms
			IF (AlarmBits.Communication[i]) THEN
				CASE FaultLevel.Communication[i] OF
					FAULTLEVEL_MESSAGE:
						AlarmMgr.ErrMessage := TRUE;
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical:= TRUE;
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal:= TRUE;
				END_CASE
			END_IF			
		END_FOR
		
		FOR i:= 0 TO MAX_ALARM_HARDWARE_BIT DO //Hardware Alarms
			IF (AlarmBits.Hardware[i]) THEN
				CASE FaultLevel.Hardware[i] OF
					FAULTLEVEL_MESSAGE:
						AlarmMgr.ErrMessage := TRUE;
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical:= TRUE;
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal:= TRUE;
				END_CASE
			END_IF			
		END_FOR
				
		// Set Color of Banner Alarms		
		IF (NOT AlarmMgr.ErrMessage AND NOT AlarmMgr.ErrCritical AND NOT AlarmMgr.ErrFatal) THEN
			AlarmBanner_CDP:= 0 + 2*256;  //XXX - Use Constants!!
		ELSE
			AlarmBanner_CDP:= 0 + 51*256;  //XXX - Use Constants!!
		END_IF
		
		SdmSetAppParam_0();
		TON_IOInit();
		TON_Reset();
		GetGuardStatus();
		GetGuardDongles();
		TON_Press_Err_Reset_Move();
	END_IF
	
END_PROGRAM


PROGRAM _INIT

	TON_Reset.PT:= T#1s;

	TON_EStopDetectDelay.PT:= T#50ms;
	TON_Press_Err_Reset_Move.PT:= T#3s;
	TON_IOInit.PT:= T#10s;
	TON_IOInit.IN:= TRUE;

	//Used by HMI only, "use" vars in code so they get compiled correctly.
	AlarmMgr.PowerOnCycles:= AlarmMgr.PowerOnCycles;
	AlarmMgr.OperatingHoursPP:= AlarmMgr.OperatingHoursPP;
	AlarmMgr.TemperatureCPU:= AlarmMgr.TemperatureCPU;
	AlarmMgr.TemperatureENV:= AlarmMgr.TemperatureENV;

	AlarmGroupFilterMode:= 4; // This sets the alarm control filter mode to filter based on the group number
	AlarmPriorityFilter:= 2; // Filter Level for the Alarm Priority
	AlarmGroupFilter:= TRUE; // Group number for our alarms
	
	//Set the SDM Application status
	SdmSetAppParam_0.enable:= TRUE; 
	SdmSetAppParam_0.Option:= sdmOPTION_VOLATILE;
	//	SdmSetAppParam_0.pLink:= ;  -- Available for future custom HTML page if clicking on the application 'petal' in SDM
	
	//Set Up Tech Guard Check
	GetGuardStatus.enable:= TRUE;
	GetGuardDongles.enable:= TRUE;
	GetGuardDongles.dongleInfoSize:= SIZEOF(DongleInfo);
	GetGuardDongles.pDongleInfos:= ADR(DongleInfo);
	
	//Setup Text System Functions
	brsstrcpy(ADR(namespace),ADR('AxisConfig')); 
	
	FOR i:= 0 TO MAX_AXIS_INDEX DO
		ParTexts[i].GetBasicText.Namespace := ADR(namespace);
		ParTexts[i].GetLimitText.Namespace := ADR(namespace);
		ParTexts[i].GetTuningText.Namespace := ADR(namespace);
		ParTexts[i].GetBasicText.TextBufferSize:= 50;
		ParTexts[i].GetLimitText.TextBufferSize:= 50;
		ParTexts[i].GetTuningText.TextBufferSize:= 50;
		ParTexts[i].GetBasicText.TextBuffer := ADR(BasicText[i]);
		ParTexts[i].GetLimitText.TextBuffer := ADR(LimitText[i]);
		ParTexts[i].GetTuningText.TextBuffer:= ADR(TuningText[i]);
		ParTexts[i].GetBasicText.SearchMode:= arTEXTSYS_SEARCH_LANGUAGE_ONLY;
		ParTexts[i].GetLimitText.SearchMode:= arTEXTSYS_SEARCH_LANGUAGE_ONLY;
		ParTexts[i].GetTuningText.SearchMode:= arTEXTSYS_SEARCH_LANGUAGE_ONLY;
	END_FOR
	
	LanguageOld:= 10; //XXX - Use Constants!
	
(*
FaultLevel|   Class    | Description																								| Stop Behavior |
-----------------------------------------------------------------------------------------------------------------------------------------------------	
	0	  |	  Message  | These are not faults, but rather are messages.
	1     |  Critical  | These are the faults that should prevent the machine from running in auto but not prevent manual operation |	After Feed	|
    2     |   Fatal    | These are the faults that should stop any motion in progress and prevent the machine from even turning on  |	Immediate	|		
	
	NOTE 1: Define unused faults as fatal so if you add one and forget to classify it, it'll become immediately obvious
	NOTE 2: this list can be generated very rapdidly in Excel using the formula ="FaultLevel[" & ROW()-1 & "]:= FAULTLEVEL_FATAL;"
	*)
	
	FaultLevel.Hardware[1] := FAULTLEVEL_FATAL;		// IO fault DI9371
	FaultLevel.Hardware[2] := FAULTLEVEL_FATAL;		// IO fault DO9322
	FaultLevel.Hardware[3] := FAULTLEVEL_FATAL;		// IO fault DS438A
	FaultLevel.Hardware[4] := FAULTLEVEL_FATAL;		// IO fault IF10D1-1
	FaultLevel.Hardware[5] := FAULTLEVEL_FATAL;		// IO fault CV_X300
	FaultLevel.Hardware[6] := FAULTLEVEL_FATAL;		// IO fault SR 750
	FaultLevel.Hardware[7] := FAULTLEVEL_FATAL;		// IO fault EX260
	FaultLevel.Hardware[8] := FAULTLEVEL_FATAL;		// IO fault IOL-104-002-Z046 #1
	FaultLevel.Hardware[9] := FAULTLEVEL_FATAL;		// IO fault IOL-104-002-Z046 #2
	FaultLevel.Hardware[10]:= FAULTLEVEL_MESSAGE;	// IO fault IOL-801-102-Z037  (OK to run w/o stacklight, all others fatal)
	FaultLevel.Hardware[11] := FAULTLEVEL_FATAL;	// IO fault Festo Press

	FaultLevel.Application[0]:= FAULTLEVEL_MESSAGE;	// No Faults Message	
	FaultLevel.Application[1]:= FAULTLEVEL_FATAL;	// estop reset required
	FaultLevel.Application[2]:= FAULTLEVEL_FATAL;	// safety interlock reset required
	FaultLevel.Application[3]:= FAULTLEVEL_FATAL;	// estop 1 pressed - right farside
	FaultLevel.Application[4]:= FAULTLEVEL_FATAL;	// estop 2 pressed - left farside
	FaultLevel.Application[5]:= FAULTLEVEL_FATAL;	// estop 3 pressed - left nearside
	FaultLevel.Application[6]:= FAULTLEVEL_FATAL;	// estop 4 pressed - right nearside
	FaultLevel.Application[7]:= FAULTLEVEL_FATAL;	// door 1 unlocked - right
	FaultLevel.Application[8]:= FAULTLEVEL_FATAL;	// door 2 unlocked - left farside
	FaultLevel.Application[9]:= FAULTLEVEL_FATAL;	// door 3 unlocked - left nearside
	FaultLevel.Application[10]:= FAULTLEVEL_FATAL;	// door 4 unlocked - front left
	FaultLevel.Application[11]:= FAULTLEVEL_FATAL;	// door 5 unlocked - front right
	FaultLevel.Application[12]:= FAULTLEVEL_FATAL;	// door 6 unlocked - loading
	
	FaultLevel.Application[15]:= FAULTLEVEL_FATAL;	 // Restart required	
	FaultLevel.Application[20]:= FAULTLEVEL_CRITICAL;// Machine Not Homed
	FaultLevel.Application[21]:= FAULTLEVEL_CRITICAL;// IO forcing enabled
	FaultLevel.Application[22]:= FAULTLEVEL_CRITICAL;// Technology Guarding Error
	FaultLevel.Application[23]:= FAULTLEVEL_CRITICAL;// No Technology Guard Inserted
	FaultLevel.Application[24]:= FAULTLEVEL_MESSAGE; // CPU Battery
	FaultLevel.Application[25]:= FAULTLEVEL_CRITICAL;// CPU IO Status
		
	FaultLevel.Application[30]:= FAULTLEVEL_CRITICAL;// PARSE_CLOSE_ERROR
	FaultLevel.Application[31]:= FAULTLEVEL_CRITICAL;// PARSE_FILE_NOT_FOUND
	FaultLevel.Application[32]:= FAULTLEVEL_CRITICAL;// PARSE_LOAD_ERROR
	FaultLevel.Application[33]:= FAULTLEVEL_CRITICAL;// PARSE_NO_RIVETS_FOUND
	FaultLevel.Application[34]:= FAULTLEVEL_CRITICAL;// PARSE_READ_ERROR
	FaultLevel.Application[35]:= FAULTLEVEL_CRITICAL;// PARSE_RIVET_TYPE_NOT_IN_FEEDER
	FaultLevel.Application[36]:= FAULTLEVEL_CRITICAL;// PARSE_CAMERA_PROG_NOT_FOUND
	FaultLevel.Application[37]:= FAULTLEVEL_CRITICAL;// PARSE_CAMERA_PROG_OUT_OF_RANGE
	
	FaultLevel.Application[40]:= FAULTLEVEL_CRITICAL;// BARCODE_COMM_LOST
	FaultLevel.Application[41]:= FAULTLEVEL_CRITICAL;// BARCODE_READ_TIMEOUT
	FaultLevel.Application[42]:= FAULTLEVEL_CRITICAL;// BARCODE_READ_FAILURE
	FaultLevel.Application[43]:= FAULTLEVEL_CRITICAL;// BARCODE_GENERAL_FAILURE
		
	FaultLevel.Application[50]:= FAULTLEVEL_FATAL  ; //LOW AIR PRESSURE
	FaultLevel.Application[51]:= FAULTLEVEL_FATAL  ; //AIR DUMP OPEN
	
	FaultLevel.Application[60]:= FAULTLEVEL_FATAL   ; //KISS BLOCK BOX BREECHED
	FaultLevel.Application[61]:= FAULTLEVEL_FATAL   ; //NEST COLLISION ATTEMPTED
	FaultLevel.Application[62]:= FAULTLEVEL_FATAL   ; //PLATE NOT PRESENT WHEN AUTO MODE STARTED
	FaultLevel.Application[63]:= FAULTLEVEL_FATAL   ; //RIVET HEAD STOPPER NOT LATCHED IN PLACE
	FaultLevel.Application[64]:= FAULTLEVEL_FATAL   ; //RIVET OUT OF BOUNDS
	FaultLevel.Application[65]:= FAULTLEVEL_MESSAGE ; //PRE_INSPECTION_FAILED
	FaultLevel.Application[66]:= FAULTLEVEL_FATAL   ; //SINGULATOR EXTEND/RETRACT TIMED OUT
	FaultLevel.Application[67]:= FAULTLEVEL_FATAL   ; //NEST EXTEND/RETRACT TIMED OUT
	FaultLevel.Application[68]:= FAULTLEVEL_FATAL   ; //LOST RIVET SUCTION WHEN EXPECTING RIVET
	FaultLevel.Application[69]:= FAULTLEVEL_FATAL   ; //PRESS HAS NOT RETURNED UP BEFORE NEXT MOVE
	FaultLevel.Application[70]:= FAULTLEVEL_CRITICAL; //RIVET HEAD NOT RUNNING WHEN COMMANDED
	FaultLevel.Application[71]:= FAULTLEVEL_CRITICAL; //NEST RETRACT TIMEOUT DURING MASTER RESET
	FaultLevel.Application[72]:= FAULTLEVEL_CRITICAL; //PRESS NOT READY
	FaultLevel.Application[73]:= FAULTLEVEL_MESSAGE ; //RIVET NOT PRESENT IN TRACK
	FaultLevel.Application[74]:= FAULTLEVEL_FATAL   ; //ATTEMPTED MOVE WITH THE PRESS BELOW SAFE THRESHOLD
	FaultLevel.Application[75]:= FAULTLEVEL_CRITICAL; //PRESS/NEST SETUP TIMED OUT
	FaultLevel.Application[76]:= FAULTLEVEL_MESSAGE ; //LEFT SINGULATOR WAS RETRACTED DURING SETUP. CHECK FOR RIVET IN NEST
	FaultLevel.Application[77]:= FAULTLEVEL_MESSAGE ; //RIGHT SINGULATOR WAS RETRACTED DURING SETUP. CHECK FOR RIVET IN NEST
	FaultLevel.Application[78]:= FAULTLEVEL_CRITICAL ; //NOT READY FOR RESUME - RESET LIFT, NESTS, AND CLAMPS
	FaultLevel.Application[79]:= FAULTLEVEL_CRITICAL ; //PRESS MOVE TIMEOUT DURING RIVETING/PURGING
	FaultLevel.Application[80]:= FAULTLEVEL_CRITICAL ; //KISS BOX ENGAGED WHILE OUT OF Y LIMITS
	FaultLevel.Application[81]:= FAULTLEVEL_CRITICAL ; //LOWER X POSITION LIMIT EXCEEDED
	FaultLevel.Application[82]:= FAULTLEVEL_CRITICAL ; //UPPER X POSITION LIMIT EXCEEDED
	FaultLevel.Application[83]:= FAULTLEVEL_CRITICAL ; //LOWER Y POSITION LIMIT EXCEEDED
	FaultLevel.Application[84]:= FAULTLEVEL_CRITICAL ; //UPPER Y POSITION LIMIT EXCEEDED
	
	FaultLevel.Application[85]:= FAULTLEVEL_FATAL  ; //FESTO PRESS ERROR
	FaultLevel.Application[86]:= FAULTLEVEL_FATAL  ; //FESTO PRESS MOVE FAILED
	FaultLevel.Application[87]:= FAULTLEVEL_FATAL  ; //FESTO PRESS CYCLIC COMM NOT OK
	
	FaultLevel.Application[90]:= FAULTLEVEL_CRITICAL;// VISION_COMM_LOST
	FaultLevel.Application[91]:= FAULTLEVEL_CRITICAL;// VISION_SET_TIMEOUT
	FaultLevel.Application[92]:= FAULTLEVEL_CRITICAL;// VISION_PROG_NOT_EXIST
	FaultLevel.Application[93]:= FAULTLEVEL_CRITICAL;// VISION_READ_TIMEOUT
	FaultLevel.Application[94]:= FAULTLEVEL_CRITICAL;// VISION_READ_FAILURE
	FaultLevel.Application[95]:= FAULTLEVEL_CRITICAL;// VISION_GENERAL_FAILURE
	
	FaultLevel.Application[100]:= FAULTLEVEL_CRITICAL;// LEFT ANVIL PLATE LATCH TIMEOUT
	FaultLevel.Application[101]:= FAULTLEVEL_CRITICAL;// LEFT ANVIL PLATE UNLATCH TIMEOUT
	FaultLevel.Application[102]:= FAULTLEVEL_CRITICAL;// RIGHT ANVIL PLATE LATCH TIMEOUT
	FaultLevel.Application[103]:= FAULTLEVEL_CRITICAL;// RIGHT ANVIL PLATE UNLATCH TIMEOUT
	FaultLevel.Application[104]:= FAULTLEVEL_CRITICAL;// LEFT SINGULATOR EXTEND TIMEOUT
	FaultLevel.Application[105]:= FAULTLEVEL_CRITICAL;// LEFT SINGULATOR RETRACT TIMEOUT
	FaultLevel.Application[106]:= FAULTLEVEL_CRITICAL;// RIGHT SINGULATOR EXTEND TIMEOUT
	FaultLevel.Application[107]:= FAULTLEVEL_CRITICAL;// RIGHT SINGULATOR RETRACT TIMEOUT
	FaultLevel.Application[108]:= FAULTLEVEL_CRITICAL;// LEFT NEST EXTEND TIMEOUT
	FaultLevel.Application[109]:= FAULTLEVEL_CRITICAL;// LEFT NEST RETRACT TIMEOUT
	FaultLevel.Application[110]:= FAULTLEVEL_CRITICAL;// RIGHT NEST EXTEND TIMEOUT
	FaultLevel.Application[111]:= FAULTLEVEL_CRITICAL;// RIGHT NEST RETRACT TIMEOUT
	FaultLevel.Application[112]:= FAULTLEVEL_CRITICAL;// STOPPER LATCH TIMEOUT
	FaultLevel.Application[113]:= FAULTLEVEL_CRITICAL;// STOPPER UNLATCH TIMEOUT
	FaultLevel.Application[114]:= FAULTLEVEL_CRITICAL;// HORIZONTAL SUPPORT EXTEND TIMEOUT
	FaultLevel.Application[115]:= FAULTLEVEL_CRITICAL;// HORIZONTAL SUPPORT RETRACT TIMEOUT
	FaultLevel.Application[116]:= FAULTLEVEL_CRITICAL;// VERTICAL SUPPORT RAISE TIMEOUT
	FaultLevel.Application[117]:= FAULTLEVEL_CRITICAL;// VERTICAL SUPPORT LOWER TIMEOUT
	
	FaultLevel.Application[120]:= FAULTLEVEL_MESSAGE;// NOT CONNECTED TO WINDOWS SHARE
	FaultLevel.Application[121]:= FAULTLEVEL_MESSAGE;// CONNECTING TO WINDOWS SHARE...
	FaultLevel.Application[122]:= FAULTLEVEL_MESSAGE;// CONNECT TO WINDOWS SHARE TIMED OUT
	
	//Axis errors
	FOR i:= 0 TO MAX_AXIS_INDEX DO
		FaultLevel.Axis[i*6+1]:= FAULTLEVEL_FATAL;		// Axis not initialized
		FaultLevel.Axis[i*6+2]:= FAULTLEVEL_FATAL;		// Axis parameters invalid
		FaultLevel.Axis[i*6+3]:= FAULTLEVEL_FATAL; 		// Axis limits invalid
		FaultLevel.Axis[i*6+4]:= FAULTLEVEL_FATAL;		// Axis tuning invalid
		FaultLevel.Axis[i*6+5]:= FAULTLEVEL_CRITICAL; 	// Axis  faults codes (this is just to record the fault codes in the alarm history)
		FaultLevel.Axis[i*6+6]:= FAULTLEVEL_CRITICAL; 	// Axis in error (this is the real fault condition)
	END_FOR
END_PROGRAM
