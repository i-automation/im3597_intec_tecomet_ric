(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: alarmsmsg
 * File: alarmsmsg.st
 * Author: ?
 * Created: ?
 * Version: 1.00.0
 * Modified Date:
 * Modified BY:
 ********************************************************************
 * Implementation OF PROGRAM alarmsmsg

 * Description:

	This task displays alarms as a message in a banner, usually
	located in the footer. This task will show one alarm at a time,
	and is initially set to display for 1.75s .

 * Options:

 * Version History:

 ********************************************************************)
PROGRAM _CYCLIC


	IF (DataMgrInit) THEN
		
		IF (NOT AlarmMgr.ResetInProgress) THEN
			
			IF ( (TON_AlarmMessage.Q) AND (vcHandle > 0) ) THEN
				
				IF (VA_Saccess(1, vcHandle) = 0) THEN
					
					AlarmLength:= MAX_ALARM_LENGTH;
					//GetActAlarmListStatus:= VA_wcGetActAlarmList(1, vcHandle, ADR(AlarmTextRaw), ADR(AlarmLength), AlarmFunction, WSTRING_TO_UINT(seperatorchar), 6);  //  6 = no date/time
					GetActAlarmListStatus:= VA_wcGetActAlarmList(1, vcHandle, ADR(AlarmTextRaw), ADR(AlarmLength), AlarmFunction, 124, 6);  //  6 = no date/time
					
					CASE GetActAlarmListStatus OF
				
						ERR_OK:
							// success
							IF (AlarmFunction = 1) THEN
								AlarmFunction:= 2;
							
							END_IF
							endoflist:= FALSE;
							TON_AlarmMessage.IN:= FALSE;
								
				
						vaERR_EMPTY_ALARMLIST:
							// also success, but end OF list
							AlarmFunction:= 1;
							IF (endoflist) THEN
								memset(ADR(AlarmMgr.Banner), 0, SIZEOF(AlarmMgr.Banner));
							ELSE
								endoflist:= TRUE;
							END_IF
										 
						
					END_CASE
					
					// only parse alarm if it was read successfully.
					IF ( (AlarmLength > 0) AND ((GetActAlarmListStatus = 0) OR (GetActAlarmListStatus = 240)) ) THEN
						dashcount:= 0;
						startalarmgroup:= 0;
						startalarmnum:= 0;
						startalarmmsg:= 0;
						endalarmmsg:= 0;
								
						FOR i:= 0 TO DINT_TO_UINT((AlarmLength - 1)) DO
																
							IF (memcmp(ADR(AlarmTextRaw)+i*SIZEOFWCHAR, ADR(seperatorchar), SIZEOFWCHAR) = 0) THEN
								dashcount:= dashcount + 1;
								
								IF (dashcount = 1) THEN		
									startalarmgroup:= i+1;												
								ELSIF (dashcount = 2) THEN																									
									startalarmnum:= i+1;													
								ELSIF (dashcount = 3) THEN												
									startalarmmsg:= i+1;												
								ELSIF (dashcount = 4) THEN												
									endalarmmsg:= i;												
								END_IF											
							END_IF										
						END_FOR
						
						// only do memcpys if we found at least 4 seperater characters
						IF (dashcount >= 4) THEN
							//AlarmGroup = MID(AlarmTextRaw, startalarmnum - startalarmgroup - 2, startalarmgroup)
							memset(ADR(AlarmGroup), 0, SIZEOF(AlarmGroup));
							memcpy(ADR(AlarmGroup), ADR(AlarmTextRaw)+INT_TO_UDINT(startalarmgroup)*INT_TO_UDINT(SIZEOFWCHAR), INT_TO_UINT(startalarmnum - startalarmgroup - 1)*INT_TO_UINT(SIZEOFWCHAR));
										
							//AlarmNumber = MID(AlarmTextRaw, startalarmmsg - startalarmnum - 2, startalarmnum)
							memset(ADR(AlarmNumber), 0, SIZEOF(AlarmNumber));
							memcpy(ADR(AlarmNumber), ADR(AlarmTextRaw)+INT_TO_UDINT(startalarmnum)*INT_TO_UDINT(SIZEOFWCHAR), INT_TO_UINT(startalarmmsg - startalarmnum - 1)*INT_TO_UINT(SIZEOFWCHAR));
	
							//AlarmMgr.Banner = MID(AlarmTextRaw, endalarmmsg - startalarmmsg, startalarmmsg)
							memset(ADR(AlarmMgr.Banner), 0, SIZEOF(AlarmMgr.Banner));
							memcpy(ADR(AlarmMgr.Banner), ADR(AlarmTextRaw)+INT_TO_UDINT(startalarmmsg)*INT_TO_UDINT(SIZEOFWCHAR), INT_TO_UINT(endalarmmsg - startalarmmsg)*INT_TO_UINT(SIZEOFWCHAR));
						END_IF
						
					END_IF

					VA_Srelease(1, vcHandle);
				END_IF
			ELSE
				TON_AlarmMessage.IN:= TRUE;
			END_IF
														
		ELSE // alarmmgr is reset faults
								
			TON_AlarmMessage.IN:= FALSE;
		
		END_IF															

		TON_AlarmMessage();
															
	END_IF // end of datamgr init
	
END_PROGRAM


PROGRAM _INIT


AlarmFunction:= 1;  //initialize to read first alarm function
TON_AlarmMessage.PT:= T#1s750ms;

seperatorchar:= "|";

	
END_PROGRAM
