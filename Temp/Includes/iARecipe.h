/* Automation Studio generated header file */
/* Do not edit ! */
/* iARecipe 1.30.0 */

#ifndef _IARECIPE_
#define _IARECIPE_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _iARecipe_VERSION
#define _iARecipe_VERSION 1.30.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "FileIO.h"
		#include "DataObj.h"
		#include "AsMem.h"
		#include "standard.h"
		#include "CSV_File.h"
#endif
#ifdef _SG4
		#include "FileIO.h"
		#include "DataObj.h"
		#include "AsMem.h"
		#include "standard.h"
		#include "CSV_File.h"
#endif
#ifdef _SGC
		#include "FileIO.h"
		#include "DataObj.h"
		#include "AsMem.h"
		#include "standard.h"
		#include "CSV_File.h"
#endif


/* Constants */
#ifdef _REPLACE_CONST
 #define IARECIPE_STATUS_ERROR 65535U
 #define IARECIPE_STATUS_BUSY 65534U
 #define IARECIPE_STATUS_DONE 1U
 #define IARECIPE_STATUS_READY 0U
 #define IARECIPE_ERR_MALLOC 32776U
 #define IARECIPE_ERR_MCREATE 32775U
 #define IARECIPE_ERR_INVALID_CONFIG 32774U
 #define IARECIPE_ERR_INTERNAL 32773U
 #define IARECIPE_ERR_EXPORT_SYS 32772U
 #define IARECIPE_ERR_IMPORT_SYS 32771U
 #define IARECIPE_ERR_EXPORT 32770U
 #define IARECIPE_ERR_IMPORT 32769U
 #define IARECIPE_ERR_INVALID_PARAM 32768U
 #define IARECIPE_CMD_EXPORT_SYS 18U
 #define IARECIPE_CMD_IMPORT_SYS 17U
 #define IARECIPE_CMD_BCKGND_SAVE 13U
 #define IARECIPE_CMD_BCKGND_LOAD 12U
 #define IARECIPE_CMD_EXPORT_ALL 11U
 #define IARECIPE_CMD_EXPORT 10U
 #define IARECIPE_CMD_IMPORT_ALL 9U
 #define IARECIPE_CMD_IMPORT 8U
 #define IARECIPE_CMD_DELETE_ALL 7U
 #define IARECIPE_CMD_DELETE 6U
 #define IARECIPE_CMD_VIEW 5U
 #define IARECIPE_CMD_RENAME 4U
 #define IARECIPE_CMD_COPY 3U
 #define IARECIPE_CMD_SAVE 2U
 #define IARECIPE_CMD_LOAD 1U
 #define IARECIPE_CMD_NONE 0U
#else
 _GLOBAL_CONST unsigned short IARECIPE_STATUS_ERROR;
 _GLOBAL_CONST unsigned short IARECIPE_STATUS_BUSY;
 _GLOBAL_CONST unsigned short IARECIPE_STATUS_DONE;
 _GLOBAL_CONST unsigned short IARECIPE_STATUS_READY;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_MALLOC;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_MCREATE;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_INVALID_CONFIG;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_INTERNAL;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_EXPORT_SYS;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_IMPORT_SYS;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_EXPORT;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_IMPORT;
 _GLOBAL_CONST unsigned short IARECIPE_ERR_INVALID_PARAM;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_EXPORT_SYS;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_IMPORT_SYS;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_BCKGND_SAVE;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_BCKGND_LOAD;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_EXPORT_ALL;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_EXPORT;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_IMPORT_ALL;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_IMPORT;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_DELETE_ALL;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_DELETE;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_VIEW;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_RENAME;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_COPY;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_SAVE;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_LOAD;
 _GLOBAL_CONST unsigned short IARECIPE_CMD_NONE;
#endif




/* Datatypes and datatypes of function blocks */
typedef struct iARecipe_Config_typ
{	plcbit FileAutoSave;
	plctime SaveInterval;
	unsigned long pRemoteFileDevice;
	unsigned long pNameSys;
	unsigned long pNameFile;
	unsigned long pFile;
	unsigned long pFileName;
	unsigned long pFileView;
	unsigned long pFileBckGnd;
	unsigned long pFileTemp;
	unsigned long szFile;
	unsigned long szFileName;
	unsigned long pSys;
	unsigned long pSys_init;
	unsigned long szSys;
	plcstring SysObjName[9];
	plcstring FileObjBaseName[4];
} iARecipe_Config_typ;

typedef struct iARecipe_Internal_FB_typ
{	struct DatObjInfo SysInfo;
	struct DatObjCreate SysCreate;
	struct DatObjRead SysLoad;
	struct DatObjWrite SysSave;
	struct CSV_Init SysCSVInit;
	struct CSV_Search SysCSVSearch;
	struct CSV_WriteFile SysCSVWriteFile;
	struct CSV_ReadFile SysCSVReadFile;
	struct DatObjInfo FileInfo;
	struct DatObjCreate FileCreate;
	struct DatObjRead FileLoad;
	struct DatObjWrite FileSave;
	struct DatObjDelete FileDelete;
	struct CSV_Init FileCSVInit;
	struct CSV_Search FileCSVSearch;
	struct CSV_WriteFile FileCSVWriteFile;
	struct CSV_ReadFile FileCSVReadFile;
} iARecipe_Internal_FB_typ;

typedef struct iARecipe_Internal_typ
{	plcbit FileAutoSave;
	plctime SaveInterval;
	unsigned long pRemoteFileDevice;
	unsigned long pNameSys;
	unsigned long pNameFile;
	unsigned long pFile;
	unsigned long pFileName;
	unsigned long pFileView;
	unsigned long pFileBckGnd;
	unsigned long NameOffset;
	unsigned long szFile;
	unsigned long szFileName;
	unsigned long pSys;
	unsigned long pSys_init;
	unsigned long szSys;
	plcstring SysObjName[9];
	plcstring FileObjBaseName[4];
	plcstring FileObjName[9];
	unsigned long pSys_last;
	unsigned long pFile_last;
	unsigned long pFileTemp;
	unsigned short MaxFiles;
	unsigned short N;
	unsigned short NDst;
	unsigned long SysIdent;
	unsigned long FileIdents[1000];
	plcbit Init;
	struct iARecipe_Internal_FB_typ FB;
	unsigned char Separator;
	plcbit Flag;
} iARecipe_Internal_typ;

typedef struct iARecipeCyclic
{
	/* VAR_INPUT (analog) */
	unsigned short cmd;
	unsigned short N;
	unsigned short NDst;
	plcstring Name[31];
	struct iARecipe_Internal_typ* Internal;
	plcbit* ActiveFileMismatch;
	unsigned short* NActiveFile;
	unsigned short* MaxFiles;
	plcbit* NewSys;
	/* VAR_OUTPUT (analog) */
	unsigned short Status;
	unsigned short ErrorID;
	unsigned short ErrorID2;
	plcstring FileNames[1000][31];
	plcstring FileNames_vc[1000][36];
	/* VAR (analog) */
	unsigned short MAXFILES;
	unsigned short Step;
	unsigned short StepTrace[100];
	unsigned short i;
	plcstring numstring[6];
	unsigned short STATE_IDLE;
	unsigned short STATE_INIT_SYS_INFO;
	unsigned short STATE_INIT_SYS_LOAD;
	unsigned short STATE_INIT_SYS_CREATE;
	unsigned short STATE_INIT_REC_INFO;
	unsigned short STATE_INIT_REC_INFO2;
	unsigned short STATE_INIT_REC_READNAME;
	unsigned short STATE_INIT_REC_LOAD;
	unsigned short STATE_INIT_REC_LOAD2;
	unsigned short STATE_FILE_LOAD;
	unsigned short STATE_FILE_CREATE;
	unsigned short STATE_FILE_SAVE;
	unsigned short STATE_FILE_COPY_LOAD;
	unsigned short STATE_FILE_COPY_CREATE;
	unsigned short STATE_FILE_COPY_SAVE;
	unsigned short STATE_FILE_RENAME_LOAD;
	unsigned short STATE_FILE_RENAME_CREATE;
	unsigned short STATE_FILE_RENAME_SAVE;
	unsigned short STATE_FILE_VIEW;
	unsigned short STATE_FILE_DELETE;
	unsigned short STATE_FILE_DELETE_ALL;
	unsigned short STATE_FILE_DELETE_ALL_WAIT;
	unsigned short STATE_FILE_IMPORT;
	unsigned short STATE_FILE_IMPORT_CREATE;
	unsigned short STATE_FILE_IMPORT_SAVE;
	unsigned short STATE_FILE_IMPORT_ALL;
	unsigned short STATE_FILE_IMPORT_ALL_CREATE;
	unsigned short STATE_FILE_IMPORT_ALL_SAVE;
	unsigned short STATE_FILE_EXPORT_LOAD;
	unsigned short STATE_FILE_EXPORT_SEARCH;
	unsigned short STATE_FILE_EXPORT_WRITE;
	unsigned short STATE_FILE_EXPORT_ALL;
	unsigned short STATE_FILE_EXPORT_ALL_LOAD;
	unsigned short STATE_FILE_EXPORT_ALL_SEARCH;
	unsigned short STATE_FILE_EXPORT_ALL_WRITE;
	unsigned short STATE_FILE_BCKGND_LOAD;
	unsigned short STATE_FILE_BCKGND_CREATE;
	unsigned short STATE_FILE_BCKGND_SAVE;
	unsigned short STATE_SYS_SAVE;
	unsigned short STATE_SYS_IMPORT;
	unsigned short STATE_SYS_IMPORT_SAVE;
	unsigned short STATE_SYS_EXPORT;
	unsigned short STATE_SYS_EXPORT2;
	unsigned short STATE_DONE;
	unsigned short STATE_INTERNAL_ERROR;
	struct TON TON_AutoSaveInterval;
	/* VAR_OUTPUT (digital) */
	plcbit Init;
} iARecipeCyclic_typ;

typedef struct iARecipeInit
{
	/* VAR_INPUT (analog) */
	struct iARecipe_Config_typ* Config;
	struct iARecipe_Internal_typ* Internal;
	/* VAR_OUTPUT (analog) */
	unsigned short Status;
	/* VAR (analog) */
	struct AsMemPartCreate MemPartCreate;
	struct AsMemPartAlloc MemPartAlloc;
} iARecipeInit_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void iARecipeCyclic(struct iARecipeCyclic* inst);
_BUR_PUBLIC void iARecipeInit(struct iARecipeInit* inst);


#ifdef __cplusplus
};
#endif
#endif /* _IARECIPE_ */

