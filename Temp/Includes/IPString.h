/* Automation Studio generated header file */
/* Do not edit ! */
/* IPString 0.00.0 */

#ifndef _IPSTRING_
#define _IPSTRING_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _IPString_VERSION
#define _IPString_VERSION 0.00.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Datatypes and datatypes of function blocks */
typedef struct Str2IP
{
	/* VAR_INPUT (analog) */
	unsigned long IPAddress;
	/* VAR_OUTPUT (analog) */
	unsigned char Octet1;
	unsigned char Octet2;
	unsigned char Octet3;
	unsigned char Octet4;
} Str2IP_typ;

typedef struct IP2Str
{
	/* VAR_INPUT (analog) */
	unsigned long pIPString;
	unsigned char Octet1;
	unsigned char Octet2;
	unsigned char Octet3;
	unsigned char Octet4;
} IP2Str_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void Str2IP(struct Str2IP* inst);
_BUR_PUBLIC void IP2Str(struct IP2Str* inst);


#ifdef __cplusplus
};
#endif
#endif /* _IPSTRING_ */

