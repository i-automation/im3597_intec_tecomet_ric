/* Automation Studio generated header file */
/* Do not edit ! */
/* libACMot 0.00.0 */

#ifndef _LIBACMOT_
#define _LIBACMOT_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _libACMot_VERSION
#define _libACMot_VERSION 0.00.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG3
		#include "Runtime.h"
#endif
#ifdef _SG4
		#include "Runtime.h"
#endif
#ifdef _SGC
		#include "Runtime.h"
#endif


/* Datatypes and datatypes of function blocks */
typedef struct ACMotorParameterCalc
{
	/* VAR_INPUT (analog) */
	signed short RatedVoltage;
	signed short RatedFrequency;
	float RatedCurrent;
	signed short RatedSpeed;
	signed short RatedPower;
	float PowerFactor;
	unsigned char nParallel;
	/* VAR_OUTPUT (analog) */
	float Voltage;
	float Speed;
	float Rs;
	float Rr;
	float Lm;
	float Ls;
	float Lr;
	signed short zp;
	float I_stall;
	float I_rated;
	float I_mag;
	float I_max;
	float T_stall;
	float T_rated;
	float T_max;
	float Kt;
	float Acu;
	float Temp_max;
	/* VAR (analog) */
	float isqnp_;
	float Xs_;
	float isdnp_;
	float Xh_;
	float p_;
	float sigma_;
	float wrn_;
	float sigma;
	float pi;
	float p_temp;
	float stardeltacompensation;
	float np;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit isDelta;
} ACMotorParameterCalc_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void ACMotorParameterCalc(struct ACMotorParameterCalc* inst);


#ifdef __cplusplus
};
#endif
#endif /* _LIBACMOT_ */

