/* Automation Studio generated header file */
/* Do not edit ! */
/* SDPControl  */

#ifndef _SDPCONTROL_
#define _SDPCONTROL_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif



/* Prototyping of functions and function blocks */
_BUR_PUBLIC plcbit HIDE(unsigned long SDP);
_BUR_PUBLIC unsigned char SHOW(unsigned long SDP);
_BUR_PUBLIC unsigned char UNLOCK(unsigned long SDP);
_BUR_PUBLIC unsigned char LOCK(unsigned long SDP);


#ifdef __cplusplus
};
#endif
#endif /* _SDPCONTROL_ */

