/* Automation Studio generated header file */
/* Do not edit ! */
/* fputil 0.00.0 */

#ifndef _FPUTIL_
#define _FPUTIL_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _fputil_VERSION
#define _fputil_VERSION 0.00.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif



/* Prototyping of functions and function blocks */
_BUR_PUBLIC plcbit fpequal(float A, float B, signed long maxUlps);


#ifdef __cplusplus
};
#endif
#endif /* _FPUTIL_ */

