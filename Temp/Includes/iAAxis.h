/* Automation Studio generated header file */
/* Do not edit ! */
/* iAAxis  */

#ifndef _IAAXIS_
#define _IAAXIS_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Constants */
#ifdef _REPLACE_CONST
 #define MAX_MOTORS_INDEX 199U
 #define IAAXIS_ERR_NONE 4U
 #define IAAXIS_PI 3.14159f
 #define IAAXIS_MOTOR_UNDEFINED 0U
 #define IAAXIS_MOTOR_AC 1U
 #define IAAXIS_MOTOR_STEPPER 2U
 #define IAAXIS_MOTOR_NOTREQUIRED 65535U
#else
 _GLOBAL_CONST unsigned short MAX_MOTORS_INDEX;
 _GLOBAL_CONST unsigned char IAAXIS_ERR_NONE;
 _GLOBAL_CONST float IAAXIS_PI;
 _GLOBAL_CONST unsigned short IAAXIS_MOTOR_UNDEFINED;
 _GLOBAL_CONST unsigned short IAAXIS_MOTOR_AC;
 _GLOBAL_CONST unsigned short IAAXIS_MOTOR_STEPPER;
 _GLOBAL_CONST unsigned short IAAXIS_MOTOR_NOTREQUIRED;
#endif




/* Datatypes and datatypes of function blocks */
typedef enum AxisInitSteps_enum
{	SI_NOT_INIT = 0,
	SI_MOTOR1 = 10,
	SI_MOTOR2 = 11,
	SI_MOTOR_READ_MAX_TEMP = 20,
	SI_MOTOR_READ_RATED_CURR = 21,
	SI_MOTOR_READ_PEAK_CURR = 22,
	SI_MOTOR_COMMUT_OFFSET = 30,
	SI_MOTOR_TEMP_MODEL1 = 40,
	SI_MOTOR_TEMP_MODEL2 = 41,
	SI_MOTOR_LINE_CHK_IGNORE = 50,
	SI_UDC_NOMINAL = 60,
	SI_PHASE_MON = 70,
	SI_PAR_TABLE_DOWNLOAD = 80,
	SI_GLOBAL = 90,
	SI_GLOBAL_WAIT = 91,
	SI_SIMULATION_MODE = 100,
	SI_SIMULATION_MODE_WAIT = 101,
	SI_ERROR = 400,
	SI_DONE = 500
} AxisInitSteps_enum;

typedef enum iAAxis_MoveCalcXY_MoveType_enm
{	TRIANGULAR,
	TRAPEZOIDAL
} iAAxis_MoveCalcXY_MoveType_enm;

typedef struct iAAxis_Internal_FB_typ
{	struct MpAxisBasicConfig MpAxisBasicConfig;
	struct MC_BR_GetErrorText MC_BR_GetErrorText;
	struct MC_BR_InitParList MC_BR_InitParList;
	struct MC_BR_ReadParID MC_BR_ReadParID;
	struct MC_BR_WriteParID MC_BR_WriteParID;
	struct MC_BR_InitParTabObj MC_BR_InitParTabObj;
	struct ACMotorParameterCalc ACMotorParameterCalc;
} iAAxis_Internal_FB_typ;

typedef struct iAAxis_Internal_Constants_typ
{	unsigned short UI2_0;
	unsigned short UI2_1;
	unsigned short UI2_2;
	unsigned short UI2_514;
	unsigned char UI1_0;
	unsigned char UI1_1;
	unsigned char UI1_2;
	unsigned char UI1_3;
	float R4_0;
} iAAxis_Internal_Constants_typ;

typedef struct iAAxis_Config_Move_typ
{	float Accel;
	float Decel;
	float JogSpeedHigh;
	float JogSpeed;
	float JogIncrement;
	float HomeOffset;
} iAAxis_Config_Move_typ;

typedef struct iAAxis_Config_Motor_typ
{	unsigned short Type;
	plcstring ParObjName[13];
	unsigned short EncoderCheckBypass;
	unsigned short TemperatureModelDisable;
	unsigned short CommutationOverride;
	float CommutationOffset;
	unsigned char nParallel;
	plcbit isDelta;
	signed short Voltage;
	signed short Frequency;
	float Current;
	signed short Speed;
	float PowerFactor;
	signed short Power;
} iAAxis_Config_Motor_typ;

typedef struct iAAxis_Config_typ
{	plcbit Disabled;
	struct iAAxis_Config_Move_typ Move;
	struct iAAxis_Config_Motor_typ Motor;
	struct MpAxisBasicConfigType Mapp;
} iAAxis_Config_typ;

typedef struct iAAxis_Internal_typ
{	struct iAAxis_Internal_FB_typ FB;
	struct iAAxis_Internal_Constants_typ Constants;
	struct iAAxis_Config_typ AppliedAxisConfig;
} iAAxis_Internal_typ;

typedef struct iAAxis_ACMotor_Par_typ
{	float Voltage;
	float RatedSpeed;
	float MaxSpeed;
	float Rs;
	float Rr;
	float Lm;
	float Ls;
	float Lr;
	signed short zp;
	float I_stall;
	float I_rated;
	float I_mag;
	float I_max;
	float T_stall;
	float T_rated;
	float T_max;
	float Kt;
	float Temp_max;
	float Acu;
} iAAxis_ACMotor_Par_typ;

typedef struct iAAxis_ACMotor_typ
{	struct ACP10DATBL_typ ParSeq;
	struct ACP10PRADR_typ ParAdrRec[41];
	struct iAAxis_ACMotor_Par_typ Par;
} iAAxis_ACMotor_typ;

typedef struct iAAxis_MoveCalcXY_par_typ
{	float Accel;
	float MaxVelocity;
	double CurrentPos;
	double NewPos;
} iAAxis_MoveCalcXY_par_typ;

typedef struct iAAxisCyclic
{
	/* VAR_INPUT (analog) */
	unsigned long pMpLink;
	unsigned long pAxis;
	struct MpAxisBasicInfoType BasicInfo;
	struct iAAxis_Config_typ AxisConfig;
	plcstring MotorList[200][11];
	unsigned long pMotorRatedCurrent;
	unsigned long pMotorPeakCurrent;
	unsigned long pMaxWindingTemp;
	float UDCNominal;
	unsigned short ErrorNum;
	unsigned char ErrorNumType;
	unsigned long pErrTxt;
	struct iAAxis_Internal_typ* Internal;
	/* VAR_OUTPUT (analog) */
	unsigned short ErrorID;
	struct MpAxisInfoType MappInfo;
	/* VAR (analog) */
	unsigned short i;
	enum AxisInitSteps_enum InitErrorStep;
	enum AxisInitSteps_enum InitStep;
	enum AxisInitSteps_enum InitStepTrace[100];
	struct iAAxis_ACMotor_typ ACMotor;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	plcbit InitializeAxis;
	plcbit PhaseMonIgnore;
	plcbit EnableSWEndLimits;
	plcbit MotorSimulationActivated;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
	plcbit AxisInitialized;
	plcbit ActivateMotorSimulation;
	plcbit SWEndLimitsEnabled;
	/* VAR (digital) */
	plcbit IsPhysicalInverter;
} iAAxisCyclic_typ;

typedef struct MoveCalcXY
{
	/* VAR_INPUT (analog) */
	struct iAAxis_MoveCalcXY_par_typ Axis[2];
	/* VAR_OUTPUT (analog) */
	float MoveTime;
	float Accel[2];
	/* VAR (analog) */
	signed short i;
	float ConstVelTime_int[2];
	float ConstVelDistance_int[2];
	float AccelDistance_int[2];
	float AccelTime_int[2];
	float MoveDistance_int[2];
	enum iAAxis_MoveCalcXY_MoveType_enm MoveType_int[2];
	float MoveTime_int[2];
} MoveCalcXY_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void iAAxisCyclic(struct iAAxisCyclic* inst);
_BUR_PUBLIC void MoveCalcXY(struct MoveCalcXY* inst);


#ifdef __cplusplus
};
#endif
#endif /* _IAAXIS_ */

