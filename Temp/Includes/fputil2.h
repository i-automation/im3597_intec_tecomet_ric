/* Automation Studio generated header file */
/* Do not edit ! */
/* fputil2  */

#ifndef _FPUTIL2_
#define _FPUTIL2_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif



/* Prototyping of functions and function blocks */
_BUR_PUBLIC plcbit fpu2eq(double A, double B, double epsilon);
_BUR_PUBLIC plcbit fpu2lt(double A, double B, double epsilon);
_BUR_PUBLIC plcbit fpu2le(double A, double B, double epsilon);
_BUR_PUBLIC plcbit fpu2gt(double A, double B, double epsilon);
_BUR_PUBLIC plcbit fpu2ge(double A, double B, double epsilon);


#ifdef __cplusplus
};
#endif
#endif /* _FPUTIL2_ */

